<?php 

// DEFINE('STORAGE', 'Storage');
/* MENU */
DEFINE('MENU_ABOUT_US', 'About Us');
DEFINE('MENU_CONTACT', 'Contact');
DEFINE('MENU_LOGIN', 'Login / Register');
DEFINE('MENU_SEARCH', 'Search');
DEFINE('MENU_FORGOT_PASSWORD', 'Forgot Password');
DEFINE('MENU_RESEND_ACTIVATION_EMAIL', 'Resend email activation');

DEFINE('MENU_MY_ACCOUNT', 'My account');
DEFINE('MENU_MY_SETTING', 'Setting');
DEFINE('MENU_MY_PROFILE', 'My Profile');
DEFINE('MENU_MY_ARTICLE', 'My Article');
DEFINE('MENU_MY_FORUM', 'My Forum');
DEFINE('MENU_CHANGE_PASSWORD', 'Change Password');
DEFINE('MENU_LOGOUT', 'Logout');
DEFINE('MENU_LOGIN_REGISTER', 'Login / Register');
DEFINE('MENU_SUBSCRIBE', 'Subscribe');
//DEFINE('', '');

/* MESSAGE */
DEFINE('INFO_SAVE_SUCCESS', 'Save success.');
DEFINE('INFO_DATA_INVALID', 'Invalid data.');
DEFINE('INFO_ERROR_OCCURED', 'Sorry error occured.');
DEFINE('INFO_EMAIL_SUBSCRIBE_SUCCESS', 'Email success subscribe.');
DEFINE('INFO_EMAIL_SUBSCRIBE_EXIST', 'Email has been subscribed.');

DEFINE('INFO_I_AGREE_TERM_CONDITION', 'Saya setuju dengan syarat dan ketentuan');
DEFINE('INFO_REMEMBER_ME', 'Ingat saya');
/* END MESSAGE */

/* VOCAB */
DEFINE('ACCOUNT', 'Akun');
DEFINE('ARTICLE', 'Article');
DEFINE('ARTICLE_CATEGORY', 'Article category');
DEFINE('ABOUT_ME', 'About me');
DEFINE('CHANGE_PASSWORD', 'Change password');
DEFINE('DOB', 'Birthday');
DEFINE('EMAIL', 'Email');
DEFINE('GENDER', 'Gender');
DEFINE('INPUT', 'Input');
DEFINE('FROM', 'From');
DEFINE('FULL_NAME', 'Full Name');
DEFINE('FEMALE', 'Female');
DEFINE('MALE', 'Male');

DEFINE('LOGIN', 'Login');
DEFINE('PASSWORD', 'Password');
DEFINE('REGISTRATION', 'Registration');
DEFINE('REGISTER', 'Register');
DEFINE('SELECT_GENDER', 'Select gender');
DEFINE('SUBSCRIBE_NEWSLETTER', 'Subscribe newsletter');
DEFINE('STATISTIC', 'Statistic');

DEFINE('OLD_PASSWORD', 'Old password');
DEFINE('NEW_PASSWORD', 'New password');
DEFINE('CONFIRM_NEW_PASSWORD', 'Confirm new password');
/* */

/* */
DEFINE('ADD_NEW', 'Add new');
DEFINE('ADD', 'Add');
DEFINE('INSERT', 'Insert');
DEFINE('SAVE', 'Save');
DEFINE('UPDATE', 'Update');
DEFINE('DELETE', 'Delete');
//DEFINE('', '');

?>