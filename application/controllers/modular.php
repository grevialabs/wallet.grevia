<?php

class Modular extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('expense_model');
		$this->load->model('member_model');
		// $this->load->model('subscribe_model');
		// $this->load->model('company_model');
	}
	
	public function index()
	{
		// if (is_member()) redirect('expense/report');
		$data['SIDEBAR'] = $this->load->view('sidebar',NULL,TRUE);
		// $data['CONTENT'] = $this->load->view('modular/v_index',$data,TRUE);
		$data['CONTENT'] = $this->load->view('modular/v_index',$data,TRUE);
        $this->load->view('index_landing', $data);
	}
	
	public function admin()
	{
		redirect('admin/dashboard');
	}
	
	public function sitemap()
	{
		$this->load->view('modular/v_sitemap');
	}
	
	public function redir() 
	{
		if (isset($_GET['url'])) {
			$url = urldecode($_GET['url']);
			redirect($url);
		} else {
			redirect(base_url());
		}
	}
	
	public function logout()
	{
		session_start();
		session_destroy();
		unset($_SESSION['hash']);
		unset($_SESSION['hash_company']);
		setcookie('hash', '', time()-3600, '/');
		setcookie('hash_company', '', time()-3600, '/');
		unset($_COOKIE['hash']);
		unset($_COOKIE['hash_company']);
		redirect(base_url());
	}	
	
	public function error404()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Error 404');
		$data['PAGE_TITLE'] = 'Halaman tidak ditemukan.';
		$data['PAGE_HEADER'] = br(2).'<div class="talCnt">Error 404<br/>Halaman tidak ditemukan.</div>';
		$data['CONTENT'] = $this->load->view('modular/v_error404',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function about()
	{
        $data = NULL;
		$data['CONTENT'] = $this->load->view('modular/v_about',$data,TRUE);
        $this->load->view('index_landing', $data);
	}
	
	public function contact()
	{
        $data = NULL;
		$data['CONTENT'] = $this->load->view('modular/v_contact',$data,TRUE);
        $this->load->view('index_landing', $data);
	}
	
	public function showarticle()
	{
		$data['ArticleID'] = 0;
		if ($this->uri->segment(2) && is_numeric($this->uri->segment(2))) $data['ArticleID'] = $this->uri->segment(2);
		$data['Slug'] = $this->uri->segment(3);
		
		if (isset($data['ArticleID'])) 
		{ 
			$objarticle = $this->article_model->get(array('ArticleID' => $data['ArticleID']));
			if (isset($objarticle))
			{
				if ($data['ArticleID'] != $objarticle['ArticleID'] && $data['Slug'] != $objarticle['Slug'] ) 
				redirect(base_url().'article/'.$objarticle['ArticleID'].'/'.$objarticle['Slug']);
			}
			$data['data'] = $objarticle;
		}
		
		$data['SIDEBAR'] = $this->load->view('sidebar',NULL,TRUE);
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Article');
		//$data['PAGE_HEADER'] = 'Artikel';
        $data['CONTENT'] = $this->load->view('modular/v_showarticle',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function login()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Login / Register');
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_login',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function login_company()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Login Company');
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_login_company',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function login_expense()
	{
		$this->load->model('expense_model');
		
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Login Expense');
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_login_expense',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function forgotpassword()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, FORGOT_PASSWORD);
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_forgotpassword',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function register()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, REGISTER);
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_register',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function participant_test()
	{
		$this->load->model('participant_model');
		$this->load->model('test_model');
		$this->load->model('history_test_model');
		
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, PARTICIPANT_TEST);
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_participant_test',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function take_test()
	{
		$this->load->model('participant_model');
		$this->load->model('test_model');
		$this->load->model('history_test_model');
		$this->load->model('notification_model');
		
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, PARTICIPANT_TEST);
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_take_test',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function thankyou_test()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, PARTICIPANT_TEST);
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_thankyou_test',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function activation()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Aktivasi akun');
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_activation',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function landing1()
	{
		$data = NULL;
		$data['CONTENT'] = $this->load->view('modular/v_landing1',$data,TRUE);
		$this->load->view('index_landing',$data);
	}
	
	public function pricing()
	{
		$data = NULL;
		$data['CONTENT'] = $this->load->view('modular/v_pricing',$data,TRUE);
		$this->load->view('index_landing',$data);
	}
	
	public function why()
	{
		$data = NULL;
		$data['CONTENT'] = $this->load->view('modular/v_why',$data,TRUE);
		$this->load->view('index',$data);
	}
	
	public function search()
	{	
		$data['SIDEBAR_RIGHT'] = $this->load->view('sidebar',NULL,TRUE);
		$data['PAGE'] = 'Search';
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Article');
		$data['PAGE_HEADER'] = 'Search';
		$data['CONTENT'] = $this->load->view('modular/v_search',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function cron()
	{
		$sql = "SELECT * FROM job_test";
		$result = $this->db->row_arrays($sql);
		var_dump($result);
	}
	
	public function cron_email_reminder_expense()
	{
		// CRON EXPENSE DEADLINE MEMBER FOR EMAIL BLAST
		$this->expense_model->cron_email_reminder_expense();
	}
	
	public function cron_notif_reminder_expense()
	{
		// CRON EXPENSE DEADLINE MEMBER FOR EMAIL BLAST
		$this->expense_model->cron_notif_reminder_expense();
	}
	
	/* GENERAL AJAX */
	public function ajax()
	{
		$this->load->model('subscribe_model');
		$data = null;
		$data['CONTENT'] = $this->load->view('modular/v_ajax',$data,TRUE);
		//$this->load->view('index', $data);
	}

}