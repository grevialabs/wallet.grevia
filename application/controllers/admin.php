<?php

class Admin extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('member_model');
		$this->load->model('subscribe_model');
		$this->load->model('test_model');
		
		if (!is_member() || member_cookies('is_admin') != 1) redirect(base_url().'login');
	}
	
	private function _breadcrumb()
	{
		$MODULE = $this->uri->segment(2);
		$return = $this->common_model->breadcrumb(
			array(
			'admin' => 'admin', 
			'admin/'.$MODULE => $MODULE
			)
		);
		return $return;
	}
	
	public function index()
	{
		redirect(base_url().'admin/dashboard');
	}
	
	public function dashboard()
	{
		$data = NULL;
		
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_dashboard',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function articlecategory()
	{	
		$data = NULL;
		
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_articlecategory',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function article()
	{
		$data = NULL;
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_article',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function member()
	{
		$data = NULL;
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_member',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function test()
	{
		$data = NULL;
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_test',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function test_detail()
	{
		$data = NULL;
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		//$MODULE = $data['MODULE'] = str_replace('_',' ',$this->uri->segment(2));
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_test_detail',$data,TRUE);
        $this->load->view('index_admin', $data);
	}

	public function test_result()
	{
		$data = NULL;
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_test_result',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function subscribe()
	{
		$data = NULL;
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_subscribe',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function order()
	{
		$this->load->model('order_model');
		$this->load->model('quota_model');
		$this->load->model('company_model');
		
		$data = NULL;
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_order',$data,TRUE);
        $this->load->view('index_admin', $data);
	}

}