<?php 

class Member extends MY_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('member_model');
		$this->load->model('history_test_model');
		
		$this->load->helper('directory');
		
		$current_url = str_replace(base_url(),'',current_url());
		if (!member_cookies("member_id")) redirect(base_url().'login?url='.urlencode($current_url));
	}
	
	public function index()
	{
		$data = NULL;
		$PAGE_TITLE = HOME;
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		$data['PAGE_HEADER'] = NULL;
		$data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
        $data['CONTENT'] = $this->load->view('member/adm_member',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function profile()
	{
		$data = NULL;
		// $PAGE_TITLE = SETTING;
		// $data['PAGE_TITLE'] = $PAGE_TITLE;
		// $data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		// $data['PAGE_HEADER'] = NULL;
		// $data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['SETTINGMENUBAR'] = $this->load->view('member/mbr_settingmenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_profile',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function test()
	{
		$this->load->model('test_model');
		
		$data = NULL;
		// $PAGE_TITLE = TEST;
		// $data['PAGE_TITLE'] = $PAGE_TITLE;
		// $data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		// $data['PAGE_HEADER'] = NULL;
		// $data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
		// $data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_test',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function setting()
	{
		$data = NULL;
		$PAGE_TITLE = SETTING;
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		$data['PAGE_HEADER'] = NULL;
		$data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
		// $data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['SETTINGMENUBAR'] = $this->load->view('member/mbr_settingmenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_setting',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function take_test()
	{
		$this->load->model('test_model');
		
		$data = NULL;
		// $PAGE_TITLE = TEST;
		// $data['PAGE_TITLE'] = $PAGE_TITLE;
		// $data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		// $data['PAGE_HEADER'] = NULL;
		// $data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
		//$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_take_test',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function history_test()
	{
		$this->load->model('test_model');
		$this->load->model('history_test_model');
		
		$data = NULL;
		$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_history_test',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function task_old_nouse($uri = NULL)
	{
		$this->load->model('task_model');
			
		$data = NULL;
		$data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		
		if (in_array($uri,array('create','edit')))
			$data['CONTENT'] = $this->load->view('member/mbr_task_form',$data,TRUE);
		else if (in_array($uri,array('report')))
			$data['CONTENT'] = $this->load->view('member/mbr_task_report',$data,TRUE);
		else
			$data['CONTENT'] = $this->load->view('member/mbr_task',$data,TRUE);
		
        $this->load->view('index', $data);
	}
	
	public function task_project($uri = NULL)
	{
		$this->load->model('task_model');
		$this->load->model('task_project_model');

		$data = NULL;
		$data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		
		// default page
		$data['CONTENT'] = $this->load->view('member/mbr_task_project',$data,TRUE);
		
		if (in_array($uri,array('create','edit')))
			$data['CONTENT'] = $this->load->view('member/mbr_task_project_form',$data,TRUE);

		$this->load->view('index', $data);

	}

	public function task($uri = NULL)
	{
		$this->load->model('task_model');
		$this->load->model('task_project_model');
			
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		
		// default page
		$data['CONTENT'] = $this->load->view('member/mbr_task_project',$data,TRUE);
		
		if (in_array($uri,array('create','edit')))
			$data['CONTENT'] = $this->load->view('member/mbr_task_form',$data,TRUE);
		else if (in_array($uri,array('report')))
			$data['CONTENT'] = $this->load->view('member/mbr_task_report',$data,TRUE);			
		
        $this->load->view('index', $data);
	}

}