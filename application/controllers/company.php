<?php 

class Company extends MY_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('participant_model');
		$this->load->model('company_model');
		$this->load->model('history_test_model');
		//$this->load->model('invoice_model');
		$this->load->model('order_model');
		$this->load->model('product_model');
		$this->load->model('test_model');
		$this->load->model('quota_model');
		$this->load->model('notification_model');
		
		$this->load->helper('directory');
		$this->load->library('email');
		
		$current_url = str_replace(base_url(),'',current_url());
		if (!company_cookies("company_id")) redirect(base_url().'login_company?url='.urlencode($current_url));
	}
	
	public function index()
	{
		$data = NULL;
		$PAGE_TITLE = HOME;
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		$data['PAGE_HEADER'] = NULL;
		$data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
        $data['CONTENT'] = $this->load->view('company/adm_company',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function profile()
	{
		$data = NULL;
		$data['SETTINGMENUBAR'] = $this->load->view('company/com_settingmenubar',$data,TRUE);
		$data['SIDEMENUBAR'] = $this->load->view('company/com_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('company/com_profile',$data,TRUE);
        $this->load->view('index', $data);
	}
		
	public function changepass()
	{
		$data = NULL;
		$data['SETTINGMENUBAR'] = $this->load->view('company/com_settingmenubar',$data,TRUE);
		$data['SIDEMENUBAR'] = $this->load->view('company/com_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('company/com_changepass',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function billing()
	{
		$data = NULL;
		$data['SETTINGMENUBAR'] = $this->load->view('company/com_settingmenubar',$data,TRUE);
		$data['SIDEMENUBAR'] = $this->load->view('company/com_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('company/com_billing',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function billing_confirmation()
	{
		$data = NULL;
		$data['SETTINGMENUBAR'] = $this->load->view('company/com_settingmenubar',$data,TRUE);
		$data['SIDEMENUBAR'] = $this->load->view('company/com_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('company/com_billing_confirmation',$data,TRUE);
        $this->load->view('index', $data);
	}

	public function participant()
	{
		$this->load->model('test_model');
		$data = NULL;
		$data['SETTINGMENUBAR'] = $this->load->view('company/com_settingmenubar',$data,TRUE);
		$data['SIDEMENUBAR'] = $this->load->view('company/com_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('company/com_participant',$data,TRUE);
        $this->load->view('index', $data);
	}

	public function report()
	{
		if ($_POST) 
		{
			include_once('./asset/plugin/tcpdf/tcpdf.php');
		}
		$this->load->model('participant_model');
		$data = NULL;
		$data['SETTINGMENUBAR'] = $this->load->view('company/com_settingmenubar',$data,TRUE);
		$data['SIDEMENUBAR'] = $this->load->view('company/com_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('company/com_report',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function order()
	{
		$data = NULL;
		$data['SETTINGMENUBAR'] = $this->load->view('company/com_settingmenubar',$data,TRUE);
		$data['SIDEMENUBAR'] = $this->load->view('company/com_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('company/com_order',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function test_candidate()
	{
		$this->load->model('test_model');
		$this->load->model('participant_model');
		
		$data = NULL;
		$data['SIDEMENUBAR'] = $this->load->view('company/com_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('company/com_test_candidate',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function setting()
	{
		$data = NULL;
		$PAGE_TITLE = SETTING;
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		$data['PAGE_HEADER'] = NULL;
		$data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
		$data['SETTINGMENUBAR'] = $this->load->view('company/com_settingmenubar',$data,TRUE);
		$data['SIDEMENUBAR'] = $this->load->view('company/com_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('company/com_setting',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function take_test()
	{
		$this->load->model('test_model');
		
		$data = NULL;
		$data['CONTENT'] = $this->load->view('company/com_take_test',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function history_test()
	{
		$this->load->model('test_model');
		$this->load->model('history_test_model');
		
		$data = NULL;
		$data['SIDEMENUBAR'] = $this->load->view('company/com_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('company/com_history_test',$data,TRUE);
        $this->load->view('index', $data);
	}

	public function notification()
	{
		$data = NULL;
		$data['SIDEMENUBAR'] = $this->load->view('company/com_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('company/com_notification',$data,TRUE);
        $this->load->view('index', $data);
	}

}