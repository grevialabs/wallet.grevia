<?php

class Map extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		// $this->load->model('article_model');
		// $this->load->model('articlecategory_model');
		// $this->load->model('forumtopic_model');
		// $this->load->model('forumtopicdetail_model');
		// $this->load->model('member_model');
	}
	
	public function index()
	{
		$data['SIDEBAR'] = $this->load->view('sidebar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('map/v_index',$data,TRUE);
        $this->load->view('index_landing', $data);
	}
	
	public function admin()
	{
		redirect('admin/dashboard');
	}
	
	public function map()
	{
		$data['a'] = '';
		$data['CONTENT'] = $this->load->view('map/v_map',$data,TRUE);
		$this->load->view('index_landing',$data);
	}
	
	public function api()
	{
		$data['a'] = '';
		$data['CONTENT'] = $this->load->view('map/v_api',$data,TRUE);
		$this->load->view('index_landing',$data);
	}	
	
	public function radar()
	{
		$data['a'] = '';
		$data['CONTENT'] = $this->load->view('map/v_radar',$data,TRUE);
		$this->load->view('index_landing',$data);
	}
	
	public function json_openrice()
	{
		$data['CONTENT'] = $this->load->view('map/v_radar',$data,TRUE);
		$this->load->view('index_landing',$data);
	}

}