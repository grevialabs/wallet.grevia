<?php 

class Expense extends MY_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('member_model');
		$this->load->model('history_test_model');
		$this->load->model('expense_model');
		$this->load->model('category_model');
		$this->load->model('treasure_model');
		$this->load->model('location_model');
		$this->load->model('calendar_model');
		$this->load->model('fund_model');
		
		$this->load->helper('directory');
		
		$current_url = str_replace(base_url(),'',current_url());
		if (!member_cookies("member_id")) redirect(base_url().'login?url='.urlencode($current_url));
		
		$banned = member_cookies("member_id");
		if ( $banned == 13) { echo "get off bitch"; die;}
	}
	
	public function index()
	{
		redirect('expense/dashboard');
	}
	
	public function report()
	{
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_report',$data,TRUE);
        $this->load->view('index_v2', $data);
	}
	
	public function input()
	{
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_input',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function category()
	{
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_category',$data,TRUE);
        $this->load->view('index_v2', $data);
	}
	
	public function treasure()
	{
		$this->load->model('treasure_model');
		
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_treasure',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function search()
	{
		$this->load->model('treasure_model');
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_search',$data,TRUE);
        $this->load->view('index_v2', $data);
	}
	
	public function detail_report()
	{
		$this->load->model('treasure_model');
		
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_detail_report',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function absence()
	{
		$this->load->model('absence_model');
		
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_absence',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function journey()
	{
		$this->load->model('journey_model');
		
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_journey',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function dashboard()
	{
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_dashboard',$data,TRUE);
        $this->load->view('index_v2', $data);
	}
	
	public function dashboard_global()
	{
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_dashboard_global',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function dashboard_new()
	{
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_dashboard_new',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function calendar()
	{
		$this->load->model('absence_model');
		
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_calendar',$data,TRUE);
        $this->load->view('index_v2', $data);
	}
	
	public function fund()
	{
		// $this->load->model('fund_model');
		
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_fund',$data,TRUE);
        $this->load->view('index_v2', $data);
	}
	
	public function forecast()
	{
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_forecast',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function fund_mutation()
	{
		$this->load->model('fund_mutation_model');
		
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_fund_mutation',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function reminder()
	{
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_reminder',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function tab_detail_expense()
	{
		$this->load->view('expense/tab_detail_expense');
	}
	
	public function tab_fund_type()
	{
		$this->load->view('expense/tab_fund_type');
	}
	
	public function tab_chartbar()
	{
		$this->load->view('expense/tab_chartbar');
	}
	
	public function storage()
	{
		$this->load->model('storage_model');
		
		$data = NULL;
		// $data['SIDEMENUBAR'] = $this->load->view('expense/exp_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('expense/exp_storage',$data,TRUE);
        $this->load->view('index_v2', $data);
	}

}