<?php 

class Api extends MY_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('member_model');
		// $this->load->model('article_model');
		// $this->load->model('forumtopic_model');
		// $this->load->model('forumtopicdetail_model');
		header('Access-Control-Allow-Origin: *'); 
		header("Access-Control-Allow-Credentials: true");
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
		
	}

	public function index()
	{
		//echo $this->common_model->curl('api/forumtopic');		
		$json['status'] = '200';
		$json['message'] = 'Welcome to api';
		$json['description'] = 'no method selected';
		echo json_encode($json);
	}
	
	public function member($method = NULL)
	{
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
		header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
		header("Access-Control-Max-Age: 3600");
		header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

		$json = NULL;
		$json['code'] = 200;
		$json['action'] = 'nothing';
		$json['message'] = "berhasil";
		
		if ($method == "get_list") 
		{
			if ($this->input->get())
			{
				$params = $this->input->get();
			}
			$params['paging'] = TRUE;
			if (isset($_GET['page'])) $params['page'] = $_GET['page'];
				
			if ($this->input->get('member_id')) 
			{
				$params['member_id'] = $this->input->get('member_id');
				$json = ($this->member_model->get($params));
			}
			else 
			{
				$json = ($this->member_model->get_list($params));
			}
		}

		if (isset($_POST))
		{
			$post = file_get_contents('php://input');
			if (! empty($post)) $post = json_decode($post, true);

			$json['action'] = 'save';

			$param = NULL;
			if (isset($post['email']))
			{
				$param['email'] = $post['email'];
				$save = $this->member_model->save($param);

				if ($save) 
				{
					$json['code'] = 200;
					$json['action'] = 'save';
					$json['message'] = 'Data ' . $param['email'] .' has been saved'. $save['member_id'];
				} 
				else 
				{
					$json['code'] = 200;
					$json['action'] = 'error';
					$json['message'] = 'error occured';
				}
			}
		}

		echo json_encode($json,1);
		die;
	}

}