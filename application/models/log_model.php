<?php

Class Log_model extends MY_Model 
{
	public function __construct() {
        parent::__construct();
    }
	
	public function get($attr = NULL) 
	{
		$query = '
		SELECT *
		FROM wal_log WHERE 1';
		if (isset($attr['username'])) 
		{
			$query.= ' AND username = ' . replace_quote($attr['username']);
		}
		
		if (isset($attr['email'])) 
		{
			$query.= ' AND email = ' . replace_quote($attr['email']);
		}
		
		if (isset($attr['active_code'])) 
		{
			$query.= ' AND active_code = ' . replace_quote($attr['active_code']);
		}
		
		if (isset($attr['log_id'])) 
		{
			$query.= ' AND log_id = ' . $attr['log_id'];
		}
		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	public function get_list($attr = NULL) 
	{
		$query = '
		SELECT l.*, m.fullname
		FROM wal_log l
		LEFT JOIN co_member m ON m.member_id = l.creator_id
		WHERE 1';
		
		if (isset($attr['keyword'])) 
		{
			$query.= ' AND ( m.fullname LIKE "%' . $attr['keyword'] . '%" OR ';
			$query.= ' m.email LIKE "%' . $attr['keyword'] . '%" OR ';
			$query.= ' CONCAT(l.codename,":",l.codevalue) LIKE "%' . $attr['keyword'] . '%" OR ';
			$query.= ' l.codename LIKE "%' . $attr['keyword'] . '%" OR ';
			$query.= ' l.codevalue LIKE "%' . $attr['keyword'] . '%" OR ';
			$query.= ' l.content LIKE "%' . $attr['keyword'] . '%" OR ';
			$query.= ' l.notes LIKE "%' . $attr['keyword'] . '%") ';
		}
		
		if (isset($attr['log_id'])) 
		{
			$query.= ' AND log_id = ' . $attr['log_id'];
		}
		
		if (isset($attr['order'])) 
		{
			$query.= ' ORDER BY '.$attr['order'];
		} 
		else 
		{
			$query.= ' ORDER BY log_id DESC';
		}
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['offset']) || (isset($attr['paging']) && $attr['paging'] == TRUE))
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function get_list_report_commission($attr = NULL) 
	{
		$query = '
		SELECT m.fullname, SUM(od.qty * od.commission) as total_commission
		FROM wal_log m
		LEFT JOIN co_order o ON m.log_id = o.sales_id
		LEFT JOIN co_order_detail od USING(order_id)
		WHERE 1';
		
		if (isset($attr['keyword'])) 
		{
			$query.= ' AND fullname LIKE "%' . $attr['keyword'] . '%" OR ';
			$query.= ' username LIKE "%' . $attr['keyword'] . '%" OR ';
			$query.= ' email LIKE "%' . $attr['keyword'] . '%" ';
		}
		
		if (isset($attr['log_id'])) 
		{
			$query.= ' AND m.log_id = ' . $attr['log_id'];
		}
		
		if (isset($attr['order'])) 
		{
			$query.= ' ORDER BY '.$attr['order'];
		} 
		else 
		{
			$query.= ' ORDER BY m.log_id DESC';
		}
		
		// debug($query);
		// die;
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['offset']) || (isset($attr['paging']) && $attr['paging'] == TRUE))
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO wal_log ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$list_field.= ', creator_ip, creator_date';
		
		$list_value.= ','.replace_quote(getIP());
		$list_value.= ','.replace_quote(getDatetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	// public function update($id, $data)
	// {
		// $query = 'UPDATE wal_log SET';
		// $i = 1;
		// foreach($data as $key => $val)
		// {
			// $query.= ' '.$key .' = ' . replace_quote($val);
			// if ($i != count($data)) $query.= ' ,';
			// $i++;
		// }
		// $query.= ', editor_id = '.replace_quote($this->common_model->member_session('log_id'));
		// $query.= ', editor_ip = '.replace_quote(getIP());
		// $query.= ', editor_date = '.replace_quote(getDatetime());
		// $query.= ' WHERE log_id = '. replace_quote($id,'num');
		// $update = $this->db->query($query);
		// if ($update) return TRUE; else return FALSE;
	// }
	
	public function delete($id)
	{
		$query = 'DELETE FROM wal_log WHERE log_id = ' . $id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;	
	}
	
}