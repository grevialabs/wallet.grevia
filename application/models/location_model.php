<?php

Class Location_model extends MY_Model 
{
	public function __construct() {
        parent::__construct();
    }
	
	public function get($attr = NULL) 
	{
		$query = 'SELECT * FROM wal_location WHERE 1';
		
		if (isset($attr['creator_id'])) 
		{
			$query.= ' AND creator_id = ' . replace_quote($attr['creator_id']);
		}
		
		if (isset($attr['location_id'])) 
		{
			$query.= ' AND location_id = ' . $attr['location_id'];
		}
		
		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	public function get_list($attr = NULL) 
	{
		$query = "
		SELECT *
		FROM wal_location
		WHERE 1
		";
		
		if (isset($attr['creator_id']))
		{
			$query.= ' AND creator_id = ' .$attr['creator_id'];
		}
		
		if (isset($attr['order']))
		{
			$query .= ' ORDER BY '.$attr['order'];
		}
		else
		{
			$query .= ' ORDER BY location_id DESC';
		}
		
		//echo $query;die;
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO wal_location ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE wal_location SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDateTime());
		$query.= ' WHERE location_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM wal_location WHERE location_id = ' . $id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;	
	}
	
}