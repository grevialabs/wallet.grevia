<?php
class History_test_Model extends MY_Model
{
	public function get_history_test($attr = NULL)
	{
		$query = "
		SELECT ht.history_test_id, 
		t.title as title,
		c.name as company_name, 
		p.name as participant_name,
		ht.test_id,
		ht.creator_date as test_date,
		ht.start_time,
		ht.end_time
		FROM job_history_test ht
		LEFT JOIN job_history_test_detail htd USING(history_test_id)
		LEFT JOIN job_test t USING(test_id)
		LEFT JOIN job_participant p ON p.participant_id = ht.participant_id 
		LEFT JOIN job_company c USING(company_id)
		WHERE 1
		";
		
		if (isset($attr['participant_id']) && $attr['participant_id'] != NULL) 
		{
			$query.= " AND ht.participant_id = " . replace_quote($attr['participant_id']);
		}
		
		if (isset($attr['test_id']) && $attr['test_id'] != NULL) 
		{
			$query.= " AND ht.test_id = " . replace_quote($attr['test_id']);
		}
		
		if (isset($attr['history_test_id']) && $attr['history_test_id'] != NULL) 
		{
			$query.= " AND ht.history_test_id = " . replace_quote($attr['history_test_id']);
		}
		
		$query.= " GROUP BY ht.history_test_id";
		
		if (isset($attr['last'])) 
		{
			$query.= " ORDER BY ht.history_test_id DESC LIMIT 1";
		}

		$return = $this->db->query($query)->row_array();
		return $return;
	}
	
	// QUERY AMBIL DISC STRENGTH + WEAKNESS
	public function get_disc1_result($attr = NULL)
	{
		// CONTOH 
		$query = "
		SELECT tdo.point_count as the_order, COUNT(htd.history_test_detail_id) as show_count, ht.participant_id
		FROM job_history_test ht
		LEFT JOIN job_history_test_detail htd USING(history_test_id)
		LEFT JOIN job_test t USING(test_id)
		LEFT JOIN job_test_detail td USING(test_detail_id)
		LEFT JOIN job_test_detail_option tdo USING(test_detail_option_id)
		WHERE 1 ";
		
		if (isset($attr['history_test_id']) && isset($attr['participant_id']))
		{
			$query.= " AND ht.history_test_id = ".$attr['history_test_id']." AND ht.participant_id = ".$attr['participant_id'];
		}
		$query.= "
		GROUP BY tdo.point_count
		ORDER BY the_order
		";
		//echo $query;die;
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function get_list_history_test($attr = NULL)
	{
		// SELECT ht.*, t.test_code, t.title
		// FROM job_history_test ht
		// INNER JOIN job_test t USING(test_id)
		// WHERE 1
		$query = "
		SELECT ht.history_test_id, 
		t.title as title,
		c.name as company_name, 
		p.name as participant_name,
		ht.test_id,
		test_code
		FROM job_history_test ht
		LEFT JOIN job_history_test_detail htd USING(history_test_id)
		LEFT JOIN job_test t USING(test_id)
		LEFT JOIN job_participant p ON p.participant_id = ht.participant_id 
		LEFT JOIN job_company c USING(company_id)
		WHERE 1
		";
		
		if (isset($attr['participant_id']) && $attr['participant_id'] != NULL)
		{
			$query.= ' AND ht.participant_id = ' . $this->db->escape_like_str($attr['participant_id']);
		}
		
		if (isset($attr['test_id']) && $attr['test_id'] != NULL)
		{
			$query.= ' AND t.test_id = ' . $this->db->escape_like_str($attr['test_id']);
		}
		
		if (isset($attr['creator_id']) && $attr['creator_id'] != NULL)
		{
			$query.= ' AND ht.creator_id = "' . $this->db->escape_like_str($attr['creator_id']) . '"';
		}
		
		if (isset($attr['keyword']) && $attr['keyword'] != NULL)
		{
			// $query.= ' AND name LIKE "' . $this->db->escape_like_str($attr['keyword']) . '%"';
			// $query.= ' OR email LIKE "' . $this->db->escape_like_str($attr['keyword']) . '%"';
		}
		
		$query.= " GROUP BY ht.history_test_id";
		
		if(isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY ht.history_test_id DESC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset; 
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function get_list_history_test_participant($attr = NULL)
	{
		//*-----------------------------------------------------------
		$query = "
		SELECT DISTINCT(ht.history_test_id) history_test_id, 
		t.title as title,
		c.name as company_name, 
		p.name as participant_name,
		ht.test_id,
		htd.* 
		FROM job_history_test ht
		LEFT JOIN job_history_test_detail htd USING(history_test_id)
		LEFT JOIN job_test t USING(test_id)
		LEFT JOIN job_participant p ON p.participant_id = ht.participant_id 
		LEFT JOIN job_company c USING(company_id)
		WHERE 1";
		
		if (isset($attr['participant_id']) && $attr['participant_id'] != NULL)
		{
			$query.= ' AND ht.participant_id = "' . $this->db->escape_like_str($attr['participant_id']) . '"';
		}
		
		if (isset($attr['test_id']) && $attr['test_id'] != NULL) 
		{
			$query.= " AND ht.test_id = " . replace_quote($attr['test_id'],"num");
		}
		
		if (isset($attr['participant_id']) && $attr['participant_id'] != NULL) 
		{
			$query.= " AND ht.participant_id = " . replace_quote($attr['participant_id']);
		}
		
		if (isset($attr['history_test_id']) && $attr['history_test_id'] != NULL) 
		{
			$query.= " AND ht.history_test_id = " . replace_quote($attr['history_test_id']);
		}
		
		if (isset($attr['is_taken']) && $attr['is_taken'] != NULL) 
		{
			$query.= " AND start_time IS NOT NULL  ";
		}
		
		$query.= " GROUP BY ht.history_test_id";
		
		if(isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ht.' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY ht.history_test_id DESC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset; 
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function save_history_test($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO job_history_test ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$list_field.= ',creator_id, creator_ip, creator_date';
		
		$list_value.= ','.replace_quote(member_cookies('member_id'));
		$list_value.= ','.replace_quote(getIP());
		$list_value.= ','.replace_quote(getDatetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update_history_test($id, $data)
	{
		$query = 'UPDATE job_history_test SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		
		$query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDatetime());
		
		$query.= ' WHERE history_test_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete_history_test($id)
	{
		$query = 'DELETE FROM job_history_test WHERE history_test_id = ' . replace_quote($id,'num');
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;
	}
	/* 
	--------------------------------------------------------------------------------------
								HISTORY TEST DETAIL 
	*/
	public function get_history_test_detail($attr = NULL)
	{
		$query = "
		SELECT *
		FROM job_history_test_detail
		WHERE 1
		";
		if (isset($attr['history_test_detail_id']) && $attr['history_test_detail_id'] != NULL) 
		{
			$query.= " AND history_test_detail_id = " . replace_quote($attr['history_test_detail_id']);
		}
		
		if (isset($attr['email']) && $attr['email'] != NULL) 
		{
			$query.= " AND email = " . replace_quote($attr['email']);
		}
		
		if (isset($attr['last']) && $attr['last'] != NULL) 
		{
			$query.= " ORDER BY history_test_detail_id DESC LIMIT 1";
		}
		$return = $this->db->query($query)->row_array();
		return $return;
	}
	
	public function get_list_history_test_detail($attr = NULL)
	{
		$query = "
		SELECT *
		FROM job_history_test_detail 
		WHERE 1
		";
		if (isset($attr['test_detail_id']) && $attr['test_detail_id'] != NULL) 
		{
			$query.= " AND test_detail_id = " . $attr['test_detail_id'];
		}
		
		if (isset($attr['keyword']) && $attr['keyword'] != NULL)
		{
			$query.= ' AND name LIKE "' . $this->db->escape_like_str($attr['keyword']) . '%"';
			$query.= ' OR email LIKE "' . $this->db->escape_like_str($attr['keyword']) . '%"';
		}
		
		if(isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY history_test_detail_id DESC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset; 
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function save_history_test_detail($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO job_history_test_detail ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		
		$list_field.= ',creator_id, creator_ip, creator_date';
		
		$list_value.= ','.replace_quote(member_cookies('member_id'));
		$list_value.= ','.replace_quote(getIP());
		$list_value.= ','.replace_quote(getDatetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update_history_test_detail($id, $data)
	{
		$query = 'UPDATE job_history_test_detail SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		
		$query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDatetime());
		
		$query.= ' WHERE history_test_detail_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete_history_test_detail($id)
	{
		$query = 'DELETE FROM job_history_test_detail WHERE history_test_detail_id = ' . replace_quote($id,'num');
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;
	}
	/* HISTORY TEST DETAIL */
	
	//save_history_test_detail
	
	public function curl_get_list($attr = NULL) 
	{
		$return = $this->get_list($attr);
		$return = json_encode($return);
		return $return;
	}
	
}