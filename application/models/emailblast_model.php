<?php
class Emailblast_Model extends MY_Model
{
	public function get($attr = NULL)
	{
		$query = "
		SELECT 
		`emailblast_id`,
		`from_alias`,
		`subject`,
		`content`,
		`to_list`,
		`cc_list`,
		`bcc_list`,
		`is_html`,
		`is_sent`,
		`sent_count`,
		`remarks`,
		`log`,
		`creator_id`,
		`creator_date`,
		`editor_id`,
		`editor_date`
  		FROM wal_emailblast eb
		WHERE 1
		";
		if (isset($attr['emailblast_id']) && $attr['emailblast_id'] != NULL) 
		{
			$query.= " AND a.emailblast_id = " . $attr['emailblast_id'];
		}
		
		if (isset($attr['keyword']) && $attr['keyword'] != NULL)
		{
			$query.= ' AND content LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\' OR title LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\' OR short_description LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\'';
		}
		
		if (isset($attr['last']) && $attr['last'] != NULL) 
		{
			$query.= " ORDER BY emailblast_id DESC LIMIT 1";
		}
		$return = $this->db->query($query)->row_array();
		return $return;
	}
	
	public function get_list($attr = NULL)
	{
		$query = "
		SELECT 
		`emailblast_id`,
		`from_alias`,
		`subject`,
		`content`,
		`to_list`,
		`cc_list`,
		`bcc_list`,
		`is_html`,
		`is_sent`,
		`sent_count`,
		`remarks`,
		`log`,
		`creator_id`,
		`creator_date`,
		`editor_id`,
		`editor_date`
		FROM wal_emailblast eb 
		WHERE 1
		";
		if (isset($attr['creator_id']) && $attr['creator_id'] != NULL) 
		{
			$query.= " AND a.creator_id = " . replace_quote($attr['creator_id']);
		}
		
		if (isset($attr['title']) && $attr['title'] != NULL) 
		{
			$query.= " AND title = " . replace_quote($attr['title']);
		}
		
		if (isset($attr['keyword']) && $attr['keyword'] != NULL)
		{
			$query.= ' AND content LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\' OR title LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\' OR short_description LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\'';
		}
		
		if (isset($attr['is_publish'])) 
		{
			if ($attr['is_publish'] == 'all')
				$query.= '';
			else
				$query.= ' AND a.is_publish = ' . $attr['is_publish'];
		}
		else
		{
			$query.= ' AND a.is_publish = 1';
		}
		
		if(isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY emailblast_id DESC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset; 
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO wal_emailblast ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		
		// inactive due to manual parameter
		// $list_field.= ',creator_id, creator_ip, creator_date';
		
		// $list_value.= ','.replace_quote(member_cookies('member_id'));
		// $list_value.= ','.replace_quote(getIP());
		// $list_value.= ','.replace_quote(getDatetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE wal_emailblast SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDatetime());
		$query.= ' WHERE emailblast_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function update_view($id, $data)
	{
		$query = 'UPDATE wal_emailblast SET';
		$query.= ' view = '.replace_quote($data['view'],'num');
		$query.= ' WHERE emailblast_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM wal_emailblast WHERE emailblast_id = ' . replace_quote($id,'num');
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;
	}
	
	public function curl_get_list($attr = NULL) 
	{
		$return = $this->get_list($attr);
		$return = json_encode($return);
		return $return;
	}
	
}