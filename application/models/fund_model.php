<?php

Class Fund_model extends MY_Model 
{
	public function __construct() {
        parent::__construct();
    }
	
	public function get($attr = NULL) 
	{
		$query = 'SELECT * FROM wal_fund WHERE 1';
		if (isset($attr['email'])) 
		{
			$query.= ' AND email = ' . replace_quote($attr['email']);
		}
		if (isset($attr['fund_id']) && $attr['fund_id'] != '') 
		{
			$query.= ' AND fund_id = ' . $attr['fund_id'];
		}
		if (isset($attr['creator_id']) && $attr['creator_id'] != '') 
		{
			$query.= ' AND creator_id = ' . $attr['creator_id'];
		}
		if (isset($attr['input_date']) && $attr['input_date'] != '') 
		{
			$query.= ' AND input_date = ' . replace_quote($attr['input_date']);
		}
		
		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	// public function get_report($attr = NULL) 
	// {
		// $query = "
		// SELECT 
		// (
			// SELECT IFNULL(SUM(amount),0) as total
			// FROM wal_fund e
			// LEFT JOIN wal_category c USING(category_id)
			// WHERE 1 ";
			// if (isset($attr['start_date']) && isset($attr['end_date']))
			// {
				// $query .= " AND input_date BETWEEN '".date('Y-m-d',strtotime($attr['start_date']))."' AND '".date('Y-m-d',strtotime($attr['end_date']))."'";
			// }
			
			// if (isset($attr['month']) && isset($attr['year']))
			// {
				// $query.= " AND MONTH(input_date) = '".$attr['month']."' AND YEAR(input_date) = '".$attr['year']."'";
			// }
			
			// if (isset($attr['creator_id']) && isset($attr['creator_id']))
			// {
				// $query.= " AND e.creator_id = '".$attr['creator_id']."'";
			// }
			
			// $query .=" AND is_increment = 0
		// ) as total_expense
		// ,
		// (
			// SELECT IFNULL(SUM(amount),0) as total
			// FROM wal_fund e
			// LEFT JOIN wal_category c USING(category_id)
			// WHERE 1";
			
			// if (isset($attr['start_date']) && isset($attr['end_date']))
			// {
				// $query .= " AND input_date BETWEEN '".date('Y-m-d',strtotime($attr['start_date']))."' AND '".date('Y-m-d',strtotime($attr['end_date']))."'";
			// }
			
			// if (isset($attr['month']) && isset($attr['year']))
			// {
				// $query.= " AND MONTH(input_date) = '".$attr['month']."' AND YEAR(input_date) = '".$attr['year']."'";
			// }
			
			// if (isset($attr['creator_id']) && isset($attr['creator_id']))
			// {
				// $query.= " AND e.creator_id = '".$attr['creator_id']."'";
			// }
			// $query .= " AND is_increment = 1
			
			// ) - (
			// SELECT IFNULL(SUM(amount),0) as total
			// FROM wal_fund e
			// LEFT JOIN wal_category c USING(category_id)
			// WHERE 1";
			
			// if (isset($attr['start_date']) && isset($attr['end_date']))
			// {
				// $query .= " AND input_date BETWEEN '".date('Y-m-d',strtotime($attr['start_date']))."' AND '".date('Y-m-d',strtotime($attr['end_date']))."'";
			// }
			
			// if (isset($attr['month']) && isset($attr['year']))
			// {
				// $query.= " AND MONTH(input_date) = '".$attr['month']."' AND YEAR(input_date) = '".$attr['year']."'";
			// }
			
			// if (isset($attr['creator_id']) && isset($attr['creator_id']))
			// {
				// $query.= " AND e.creator_id = '".$attr['creator_id']."'";
			// }
			// $query.= " AND is_increment = 0
		// ) as total_revenue";

		// $result = $this->db->query($query)->row_array();
		// return $result;
	// }
	
	public function get_list($attr = NULL) 
	{
		$query = "
		SELECT f.*, IF(f.fund_type = 'cash', f.fund_type, CONCAT(f.name)) as fund_name
		FROM wal_fund f
		WHERE 1
		";
		
		// status
		if (isset($attr['is_delete']))
		{
			$query.= ' AND f.is_delete = ' . $attr['is_delete'] . '';
		}
		
		// include fund_type payment cash for global user
		if (isset($attr['creator_id']))
		{
			$query.= ' AND (f.creator_id IS NULL OR f.creator_id = ' . $attr['creator_id'] . ')';
		}
		
		if (isset($attr['start_date']) && isset($attr['end_date']))
		{
			$query.= ' AND input_date BETWEEN ' . replace_quote(date('Y-m-d',strtotime($attr['start_date']))) .' AND ' . replace_quote(date('Y-m-d',strtotime($attr['end_date'])));
		}
		
		if (isset($attr['order']) && isset($attr['order']))
		{
			$query.= ' ORDER BY '.$attr['order'];
		}
		else 
		{
			$query.= ' ORDER BY f.fund_id DESC';
		}
		
		// debug($query);
		// die;
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		
		// debug($query);
		// die;
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function get_list_by_fund_type($attr = NULL) 
	{
		$query = "
		SELECT f.*, IF(f.fund_type = 'cash', f.fund_type, CONCAT(f.name)) as fund_name, 
		(
			SELECT last_credit FROM wal_fund_mutation fm WHERE fund_type = 'bank account' ORDER BY fund_id DESC LIMIT 1
		) as last_credit
		FROM wal_fund f
		WHERE 1
		";
		
		// status
		if (isset($attr['is_delete']) && isset($attr['is_delete']))
		{
			$query.= ' AND f.is_delete = ' . $attr['is_delete'] . '';
		}
		
		// include fund_type payment cash for global user
		if (isset($attr['creator_id']) && isset($attr['creator_id']))
		{
			$query.= ' AND (f.fund_id = 1 OR f.creator_id = ' . $attr['creator_id'] . ')';
		}
		
		if (isset($attr['start_date']) && isset($attr['end_date']))
		{
			$query.= ' AND input_date BETWEEN ' . replace_quote(date('Y-m-d',strtotime($attr['start_date']))) .' AND ' . replace_quote(date('Y-m-d',strtotime($attr['end_date'])));
		}
		
		if (isset($attr['order']) && isset($attr['order']))
		{
			$query.= ' ORDER BY '.$attr['order'];
		}
		else 
		{
			$query.= ' ORDER BY f.fund_id DESC';
		}
		
		// debug($query);
		// die;
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		
		// debug($query);
		// die;
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO wal_fund ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		
		// $query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		// $query.= ', editor_ip = '.replace_quote(getIP());
		// $query.= ', editor_date = '.replace_quote(getDatetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE wal_fund SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDatetime());
		$query.= ' WHERE fund_id = '. replace_quote($id,'num');
		//debug($query);die;
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM wal_fund WHERE fund_id = ' . $id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;	
	}
	
}