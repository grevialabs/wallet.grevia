<?php

Class Storage_model extends MY_Model 
{
	public $table = 'wal_storage';
	public $pk = 'storage_id';
	
	public function __construct() {
        parent::__construct();
    }
	
	public function get($attr = NULL) 
	{
		$query = 'SELECT ls.name as locstorage_name, s.* ';
		$query.= ' FROM ' . $this->table . ' s';
		// $query.= ' AND = ' . replace_quote($attr['storage_id']);
		$query.= ' LEFT JOIN wal_locstorage ls ON ls.locstorage_id = s.locstorage_id';
		$query.= ' WHERE 1';
		$query.= ' AND s.status != -1';

		if (isset($attr['storage_id'])) 
		{
			$query.= ' AND s.storage_id = ' . replace_quote($attr['storage_id']);
		}
		
		if (isset($attr['creator_id'])) 
		{
			$query.= ' AND s.creator_id = ' . $attr['creator_id'];
		}
		// debug($query);
		// die;
		
		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	public function get_list($attr = NULL) 
	{
		$query = 'SELECT ls.name as locstorage_name, s.* FROM ' . $this->table .' s';
		
		$query.= ' LEFT JOIN wal_locstorage ls ON ls.locstorage_id = s.locstorage_id';
		$query.= ' WHERE 1';
		
		
		if (isset($attr['status'])) 
		{
			$query.= ' AND l.status = ' . $attr['status'];
		} 
		else 
		{
			$query.= ' AND l.status != -1' ;
		}
		
		if (isset($attr['creator_id'])) 
		{
			$query.= ' AND s.creator_id = ' . $attr['creator_id'];
		}
		
		if (isset($attr['keyword'])) 
		{
			$query.= ' AND s.name LIKE  "%' . $attr['keyword'] . '%"';
		}
		
		if (isset($attr['status'])) 
		{
			$query.= ' AND s.status = ' . $attr['status'];
		}
		else
		{
			$query.= ' AND s.status != -1';
		}
		
		if (isset($attr['order']))
		{
			$query .= ' ORDER BY '.$attr['order'];
		}
		else
		{
			$query .= ' ORDER BY ' . $this->pk . ' DESC';
		}
		// debug($query);die;
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			// if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			if (isset($attr['page']) && $attr['page'] > 0) $limit = ($attr['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO ' .$this->table . ' ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$list_field.= ', creator_ip, creator_date';
		
		$list_value.= ','.replace_quote(getIP());
		$list_value.= ','.replace_quote(getDatetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE ' .$this->table . ' SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDatetime());
		$query.= ' WHERE ' .$this->pk . ' = '. replace_quote($id,'num');
		
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM ' .$this->table . ' WHERE ' .$this->pk . ' = ' . $id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;	
	}
	
}