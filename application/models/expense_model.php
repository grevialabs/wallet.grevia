<?php

Class Expense_model extends MY_Model 
{
	public function __construct() {
        parent::__construct();
		
		$this->load->model('log_model');
		$this->load->model('emailblast_model');
    }
	
	public function get($attr = NULL) 
	{
		$query = '
		SELECT e.*, f.fund_type, IF(f.fund_type = "cash", f.fund_type, CONCAT(f.fund_type," ",f.name)) as fund_name
		FROM wal_expense e
		LEFT JOIN wal_fund f USING(fund_id)
		WHERE 1';
		if (isset($attr['email'])) 
		{
			$query.= ' AND e.email = ' . replace_quote($attr['email']);
		}
		if (isset($attr['expense_id'])) 
		{
			$query.= ' AND e.expense_id = ' . $attr['expense_id'];
		}
		if (isset($attr['creator_id'])) 
		{
			$query.= ' AND e.creator_id = ' . $attr['creator_id'];
		}
		// debug($query);
		// die;
		$result = $this->db->query($query)->row_array();
		return $result;
	}

	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO wal_expense ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE wal_expense SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDatetime());
		$query.= ' WHERE expense_id = '. replace_quote($id,'num');
		//debug($query);die;
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM wal_expense WHERE expense_id = ' . $id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;	
	}
	
	/*-------------------------------------*/
	public function get_member($attr = NULL) 
	{
		$query = 'SELECT * FROM wal_member WHERE 1';
		if (isset($attr['email']) && $attr['email'] != '') 
		{
			$query.= ' AND email = ' . replace_quote($attr['email']);
		}
		
		if (isset($attr['password']) && $attr['password'] != '') 
		{
			$query.= ' AND password = ' . replace_quote($attr['password']);
		}
		
		$query.= ';';
		
		$result = $this->db->query($query)->row_array();
		return $result;
	}

	// --------------------------------------
	// REPORT
	
	// Return
	public function get_average_report($attr = NULL) 
	{
		// Required:
		// @creator_id 
		// @is_increment
		
		// set default report for expense
		if (isset($attr['is_increment']) && in_array($attr['is_increment'],array('0','1')))
		{
			// $attr['is_increment'] 
		}
		else 
		{
			$attr['is_increment'] = 0;
		}
		
		$query = '
		SELECT SUM(totalamount) as total_amount, COUNT(monthdate) as total_month, (SUM(totalamount) / COUNT(monthdate)) as average_amount_monthly
		FROM 
		(
			SELECT MONTH(e.release_date) as monthdate, YEAR(e.release_date) as yeardate, SUM(e.amount) as totalamount
			FROM wal_expense e
			LEFT JOIN wal_category c ON c.category_id = e.category_id
			LEFT JOIN wal_fund f USING(fund_id)
			WHERE 1';
		
		if (isset($attr['creator_id']) && $attr['creator_id'] != '')
		{
			$query .= ' AND e.creator_id = '. $attr['creator_id'];
		}
		
		if ($attr['is_increment'] == 0) 
		{
			$query .= ' AND f.fund_type != "credit card"';
		}
		
		$query .= '	
			AND c.is_increment = '.$attr['is_increment'].' 
			AND YEAR(e.release_date) >= 1
			GROUP BY MONTH(e.release_date), YEAR(e.release_date)
			ORDER BY yeardate DESC, monthdate DESC
		) tmp ';
		// debug($query);
		// die;
		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	// SHOW INCOME AND OUTCOME PER MONTH
	public function get_report($attr = NULL) 
	{
		$query = "
		SELECT 
		(
			SELECT IFNULL(SUM(amount),0) as total
			FROM wal_expense e
			LEFT JOIN wal_category c USING(category_id)
			WHERE 1";
			if (isset($attr['start_date']) && isset($attr['end_date']))
			{
				$query.= " AND release_date BETWEEN '".date('Y-m-d',strtotime($attr['start_date']))."' AND '".date('Y-m-d',strtotime($attr['end_date']))."'";
			}
			
			if (isset($attr['month']) && isset($attr['year']))
			{
				$query.= " AND MONTH(release_date) = '".$attr['month']."' AND YEAR(release_date) = '".$attr['year']."'";
			}
			
			if (isset($attr['creator_id']) && isset($attr['creator_id']))
			{
				$query.= " AND e.creator_id = '".$attr['creator_id']."'";
			}
			
			$query.= " AND is_increment = 1 
		) as total_income
		,
		(
			SELECT IFNULL(SUM(amount),0) as total
			FROM wal_expense e
			LEFT JOIN wal_category c USING(category_id)
			LEFT JOIN wal_fund f USING(fund_id)
			WHERE 1 ";
			if (isset($attr['start_date']) && isset($attr['end_date']))
			{
				$query .= " AND release_date BETWEEN '".date('Y-m-d',strtotime($attr['start_date']))."' AND '".date('Y-m-d',strtotime($attr['end_date']))."'";
			}
			
			if (isset($attr['month']) && isset($attr['year']))
			{
				$query.= " AND MONTH(release_date) = '".$attr['month']."' AND YEAR(release_date) = '".$attr['year']."'";
			}
			
			if (isset($attr['creator_id']) && isset($attr['creator_id']))
			{
				$query.= " AND e.creator_id = '".$attr['creator_id']."'";
			}
			
			$query .=" AND is_increment = 0 AND is_paid = 1
		) as total_topup
		,
		(
			SELECT IFNULL(SUM(amount),0) as total
			FROM wal_expense e
			LEFT JOIN wal_category c USING(category_id)
			LEFT JOIN wal_fund f USING(fund_id)
			WHERE 1 ";
			if (isset($attr['start_date']) && isset($attr['end_date']))
			{
				$query .= " AND release_date BETWEEN '".date('Y-m-d',strtotime($attr['start_date']))."' AND '".date('Y-m-d',strtotime($attr['end_date']))."'";
			}
			
			if (isset($attr['month']) && isset($attr['year']))
			{
				$query.= " AND MONTH(release_date) = '".$attr['month']."' AND YEAR(release_date) = '".$attr['year']."'";
			}
			
			if (isset($attr['creator_id']) && isset($attr['creator_id']))
			{
				$query.= " AND e.creator_id = '".$attr['creator_id']."'";
			}
			
			$query .=" AND is_increment = -1 AND is_paid = 1 AND f.fund_type != 'credit card'
		) as total_expense";
		// ,
		// (
		// 	SELECT IFNULL(SUM(amount),0) as total
		// 	FROM wal_expense e
		// 	LEFT JOIN wal_category c USING(category_id)
		// 	WHERE 1";
			
		// 	if (isset($attr['start_date']) && isset($attr['end_date']))
		// 	{
		// 		$query .= " AND release_date BETWEEN '".date('Y-m-d',strtotime($attr['start_date']))."' AND '".date('Y-m-d',strtotime($attr['end_date']))."'";
		// 	}
			
		// 	if (isset($attr['month']) && isset($attr['year']))
		// 	{
		// 		$query.= " AND MONTH(release_date) = '".$attr['month']."' AND YEAR(release_date) = '".$attr['year']."'";
		// 	}
			
		// 	if (isset($attr['creator_id']) && isset($attr['creator_id']))
		// 	{
		// 		$query.= " AND e.creator_id = '".$attr['creator_id']."'";
		// 	}
		// 	$query .= " AND is_increment = 1
			
		// 	) - (
		// 	SELECT IFNULL(SUM(amount),0) as total
		// 	FROM wal_expense e
		// 	LEFT JOIN wal_category c USING(category_id)
		// 	LEFT JOIN wal_fund f USING(fund_id)
		// 	WHERE 1";
			
		// 	if (isset($attr['start_date']) && isset($attr['end_date']))
		// 	{
		// 		$query .= " AND release_date BETWEEN '".date('Y-m-d',strtotime($attr['start_date']))."' AND '".date('Y-m-d',strtotime($attr['end_date']))."'";
		// 	}
			
		// 	if (isset($attr['month']) && isset($attr['year']))
		// 	{
		// 		$query.= " AND MONTH(release_date) = '".$attr['month']."' AND YEAR(release_date) = '".$attr['year']."'";
		// 	}
			
		// 	if (isset($attr['creator_id']) && isset($attr['creator_id']))
		// 	{
		// 		$query.= " AND e.creator_id = '".$attr['creator_id']."'";
		// 	}
		// 	$query.= " AND is_increment = 0 AND is_paid = 1 AND f.fund_type != 'credit card'
		// ) as total_revenue";

		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	// SHOW INCOME / OUTCOME MONTHLY
	public function get_monthly_amount($attr = NULL) 
	{
		$query = "
		SELECT SUM(e.amount) as total_amount
		FROM wal_expense e
		LEFT JOIN wal_category c ON e.category_id = c.category_id
		WHERE 1 AND e.release_date IS NOT NULL";
			
		if (isset($attr['creator_id']) && isset($attr['creator_id']))
		{
			$query.= " AND e.creator_id = '".$attr['creator_id']."'";
		}
		
		if (isset($attr['is_increment']) && isset($attr['is_increment']))
		{
			$query.= " AND c.is_increment = ".$attr['is_increment']." ";
		}
		
		if (isset($attr['year']) && isset($attr['month']))
		{
			$query.= " AND YEAR(e.release_date) = ".$attr['year']." AND MONTH(e.release_date) = ".$attr['month']." ";
		}

		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	public function get_report_all_month($attr = NULL) 
	{
		$query = "
		SELECT CONCAT(YEAR(e.release_date),'-', MONTH(e.release_date))as tgl,e.release_date,
		(
			SELECT (
				SELECT IFNULL(SUM(amount),0) tto
				FROM wal_expense we
				LEFT JOIN wal_category wc USING(category_id)
				LEFT JOIN wal_fund wf USING(fund_id)
				WHERE 1 AND we.release_date BETWEEN date_add(e.release_date,interval -DAY(e.release_date)+1 DAY) AND LAST_DAY(e.release_date) AND wc.is_increment = 1 AND we.creator_id = c.creator_id
			)
		) income
		,
		(
			SELECT IFNULL(SUM(amount),0) tto
			FROM wal_expense we
			LEFT JOIN wal_category wc USING(category_id)
			LEFT JOIN wal_fund wf USING(fund_id)
			WHERE 1 AND we.release_date BETWEEN date_add(e.release_date,interval -DAY(e.release_date)+1 DAY) AND LAST_DAY(e.release_date) AND wc.is_increment = 0 
			AND wf.fund_type != 'credit card' 
			AND we.creator_id = c.creator_id
		) expense
		FROM wal_expense e
		LEFT JOIN wal_category c USING(category_id)
		WHERE 1";
		
		if (isset($attr['creator_id']) && $attr['creator_id'] != '')
		{
			$query .= " AND e.creator_id = ".$attr['creator_id'];
		}
		
		if (isset($attr['year']) && $attr['year'] != '')
		{
			$query .= " AND YEAR(e.release_date) = ".$attr['year'];
		}
		
		$query .= " 
		GROUP BY YEAR(e.release_date), MONTH(e.release_date)
		HAVING tgl != '0-0'";
		
		$query .= "
		ORDER BY e.release_date DESC";

		// debug($query);
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function get_list_report($attr = NULL) 
	{
		$query = "
		SELECT e.expense_id, c.category_id, c.monthly_budget_amount, l.location_id, l.location_name, e.title as title, c.name as category_name, IF(f.fund_type = 'cash', f.fund_type, CONCAT(f.fund_type,' ',f.name)) as fund_name, f.fund_type, release_date, reminder_date,reminder_minus_day,
		c.is_increment, IF(c.is_increment=1,'Income',IF(c.is_increment=-1,'Expense','Topup')),
		is_paid, is_get_notif, gmap_address,
		UNIX_TIMESTAMP(DATE_ADD(e.reminder_date, INTERVAL - e.reminder_minus_day DAY)) as email_blast_date,
		e.creator_date as expense_creator_date, e.notes as expense_notes,
		";

		// $query.= "(SELECT category_id FROM wal_category wc WHERE wc.category_id = c.parent_category_id ) as parent_category_id, ";
		// $query.= "(SELECT name FROM wal_category wa WHERE wa.category_id = c.parent_category_id ) as parent_category_name, ";
		
		$query .= " pc.name as parent_cat_name, pc.category_id as parent_cat_id, ";
		
		if (isset($attr['is_detail']) && $attr['is_detail'] == 1) 
			$query.= "(amount) as amount, (cashback) as cashback";
		else
			$query.= "SUM(amount) as amount, SUM(cashback) as cashback";
		
		
		$query.= " 
		FROM wal_category c
		LEFT JOIN wal_expense e ON c.category_id = e.category_id
		LEFT JOIN wal_location l ON e.location_id = l.location_id
		LEFT JOIN wal_fund f ON e.fund_id = f.fund_id
		LEFT JOIN wal_category pc ON c.parent_category_id = pc.category_id
		WHERE 1
		";
		
		if (isset($attr['keyword']) && $attr['keyword'] != '')
		{
			$query.= ' AND (title LIKE "%' .$attr['keyword'].'%"';
			$query.= ' OR location_name LIKE "%' .$attr['keyword'].'%"';
			$query.= ' OR c.name LIKE "%' .$attr['keyword'].'%"';
			$query.= ' OR e.notes LIKE "%' .$attr['keyword'].'%"';
			$query.= ' OR gmap_address LIKE "%' .$attr['keyword'].'%" )';
		}
		
		if (isset($attr['start_date']) && isset($attr['end_date']))
		{
			$query.= ' AND release_date BETWEEN ' . replace_quote(date('Y-m-d',strtotime($attr['start_date']))) .' AND ' . replace_quote(date('Y-m-d',strtotime($attr['end_date'])));
		}
		
		if (isset($attr['release_date']) && $attr['release_date'] != '')
		{
			$query.= ' AND e.release_date = ' .replace_quote(date('Y-m-d',strtotime($attr['release_date'])));
		}
		
		if (isset($attr['is_budget']))
		{
			$query.= ' AND e.is_budget = ' .$attr['is_budget'];
		}
		
		if (isset($attr['is_increment']) && in_array($attr['is_increment'],array(-1,0,1)))
		{
			$query.= ' AND c.is_increment = ' .$attr['is_increment'];
		}
		
		if (isset($attr['category_id']) && $attr['category_id'] != '')
		{
			$query.= ' AND (e.category_id = ' .$attr['category_id'];
			$query.= ' OR c.parent_category_id = ' .$attr['category_id'].')';
			
		}
		
		if (isset($attr['location_id']) && $attr['location_id'] != '')
		{
			$query.= ' AND e.location_id = ' .$attr['location_id'];
		}
		
		if (isset($attr['fund_id']) && $attr['fund_id'] != '')
		{
			$query.= ' AND e.fund_id = ' .$attr['fund_id'];
		}
		
		if (isset($attr['creator_id']) && $attr['creator_id'] != '')
		{
			$query.= ' AND e.creator_id = ' .$attr['creator_id'];
		}
		
		// GET INCOMING REMINDER
		// show future reminder only
		if (isset($attr['incoming_reminder']) && $attr['incoming_reminder'] != '')
		{
			$query.= ' AND is_get_notif = 1';
			$query.= ' AND reminder_date > now()';
		}
		
		// only show active month & future reminder
		if (isset($attr['active_month_reminder']) && $attr['active_month_reminder'] != '')
		{
			$query.= ' AND is_get_notif = 1';
			$query.= ' AND ( MONTH(now()) = MONTH(reminder_date)';
			$query.= ' AND YEAR(now()) = YEAR(reminder_date)';
			$query.= ' OR reminder_date > now())';
		}	
		
		if (isset($attr['group_by']) && (isset($attr['is_detail']) || $attr['is_detail'] == 1)) 
		{
			if ($attr['group_by'] == 'release_date')
			{
				$query .= ' GROUP BY release_date';
			}
			else if ($attr['group_by'] == 'category_name')
			{
				$query .= ' GROUP BY c.name';
			}
		}

		if (isset($attr['order_by'])) 
		{
			$query .= ' ORDER BY '.$attr['order_by'];
		}
		else 
		{
			$query .= ' ORDER BY e.release_date DESC';	
		}
		
		if (isset($attr['debug'])) {
			debug($query);
		}
		
		$result['total_rows'] = $this->db->query($query)->num_rows();

		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = PAGING_LIMIT;
			$offset = PAGING_OFFSET;
			
			$limit = 0;
			if (isset($attr['page']) && $attr['page'] > 1) {
				$limit = ($attr['page']-1) * $offset;
			}
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		// debug($query);
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function get_list($attr = NULL) 
	{
		$query = "
		SELECT e.expense_id, e.title, c.category_id, c.name as category_name, release_date, FORMAT(CONCAT(SUM(amount),''),3) as amount, amount as raw_amount
		FROM wal_expense e
		LEFT JOIN wal_category c USING(category_id)
		WHERE 1 
		";
		
		if (isset($attr['start_date']) && isset($attr['end_date']))
		{
			$query.= ' AND release_date BETWEEN ' . replace_quote(date('Y-m-d',strtotime($attr['start_date']))) .' AND ' . replace_quote(date('Y-m-d',strtotime($attr['end_date'])));
		}
		
		// FIND income
		if (isset($attr['is_increment'])) 
		{
			$query.= ' AND is_increment = '. $attr['is_increment'];
		}
		
		// FIND budget
		if (isset($attr['is_budget'])) 
		{
			$query.= ' AND e.is_budget = '. $attr['is_budget'];
		}
		
		// FIND routine expense
		if (isset($attr['is_routine'])) 
		{
			$query.= ' AND e.is_routine = '. $attr['is_routine'];
		}
		
		// FIND routine expense
		if (isset($attr['creator_id'])) 
		{
			$query.= ' AND e.creator_id = '. $attr['creator_id'];
		}
		
		$query .= ' GROUP BY e.expense_id';
		
		// error jadi comment dulu
		// $query .= ' GROUP BY e.expense_id DESC';
		
		// debug($query);die;
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function get_list_autocomplete($attr = NULL) 
	{
		$query = "
		SELECT title 
		FROM wal_expense e 
		WHERE 1
		";
		
		if (isset($attr['creator_id']) && $attr['creator_id'] != '') 
		{
			$query.= " AND e.creator_id = ". $attr['creator_id'];
		}
		
		if (isset($attr['title']) && $attr['title'] != '') 
		{
			$query.= " AND e.title LIKE '". $attr['title']."_%'";
		}
		
		$query.= " GROUP BY e.title";
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		// debug($query);
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	// public function get_list_report_by_category_name($attr = NULL) 
	// {
		// $query = "
		// SELECT c.name as category_name, FORMAT(CONCAT(SUM(amount),''),3) as expense
		// FROM wal_expense e
		// LEFT JOIN wal_category c USING(category_id)
		// WHERE 1
		// ";
		
		// if (isset($attr['start_date']) && isset($attr['end_date']))
		// {
			// $query.= ' AND release_date BETWEEN ' . replace_quote(date('Y-m-d',strtotime($attr['start_date']))) .' AND ' . replace_quote(date('Y-m-d',strtotime($attr['end_date'])));
		// }
		
		// if (isset($attr['is_increment']))
		// {
			// $query.= ' AND is_increment = ' .$attr['is_increment'];
		// }
		
		// $query .= ' GROUP BY c.name';
		// //echo $query;die;
		// $result['total_rows'] = $this->db->query($query)->num_rows();
		// if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		// {
			// $limit = 0;
			// $offset = OFFSET;
			
			// if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			// {
				// $offset = $_GET['per_page'];
			// }
			
			// if (isset($attr['limit'])) $limit = $attr['limit'];
			// if (isset($attr['offset'])) $offset = $attr['offset'];
			// if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			// $query.= ' LIMIT '.$limit.','.$offset;
		// }
		// $result['data'] = $this->db->query($query)->result_array();
		// return $result;
	// }
	
	// cron email reminder
	public function get_list_deadline_expense()
	{
		$query = "SELECT c.name, e.* 
		FROM wal_expense e
		LEFT JOIN wal_category c USING(category_id)
		WHERE 1 AND is_paid = 0
		AND (now() > reminder_date - INTERVAL 10 day)
		ORDER BY e.release_date DESC";
			
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;

	}
	
	// Return list 10 category which most frequent expense
	public function get_list_frequent_expense($attr)
	{
		$query = "
		SELECT DISTINCT(c.category_id), c.name";
		
		if (isset($attr['newest'])) {
			$query.= "";
		} else {
			$query.= ", COUNT(e.expense_id) as total_show";
		}
		
		$query .= "
		FROM wal_category c 
		LEFT JOIN wal_expense e ON e.category_id = c.category_id
		WHERE 1";
		
		// DEFAULT SHOULD BE NOT PARENT
		$query .= " AND is_parent = 0";

		if (isset($attr['creator_id']) && isset($attr['creator_id']))
		{
			$query .= " AND e.creator_id = ".$attr['creator_id'];
		}		
		
		
		if (isset($attr['order_by']) && isset($attr['order_by']))
		{
			$query .= " ORDER BY  ".$attr['order_by'];
		}
		else 
		{
			$query .= " GROUP BY e.category_id";
			$query .= " ORDER BY total_show DESC";
		}
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = 20;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		// debug($query);
		// die;
		$result['data'] = $this->db->query($query)->result_array();
		return $result;

	}
	
	// Return list 10 category which most newest expense
	public function get_list_newest_expense($attr)
	{
		$query = "
		SELECT DISTINCT(e.category_id), c.name, e.amount";
		
		$query .= "
		FROM wal_expense e 
		LEFT JOIN wal_category c ON e.category_id = c.category_id
		WHERE 1";

		// DEFAULT SHOULD BE NOT PARENT
		$query .= " AND is_parent = 0";
		
		if (isset($attr['is_increment']) && isset($attr['is_increment']))
		{
			$query .= " AND is_increment = ".$attr['is_increment'];
		}	
		
		if (isset($attr['creator_id']) && isset($attr['creator_id']))
		{
			$query .= " AND e.creator_id = ".$attr['creator_id'];
		}		
		
		if (isset($attr['order_by']) && isset($attr['order_by']))
		{
			$query .= " ORDER BY  ".$attr['order_by']." ";
		}
		
		// debug($query);
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = 20;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		// debug($query);
		// die;
		$result['data'] = $this->db->query($query)->result_array();
		return $result;

	}
	
	// whatsapp notif private to axis from 
	public function cron_notif_reminder_expense()
	{
		// https://api.callmebot.com/whatsapp.php?phone=6283891998825&text=This+is+a+test&apikey=5515218

		$query = "
		SELECT e.expense_id, CONCAT(m.first_name,' ',m.last_name) as fullname, m.email, m.phone, c.name, e.title , e.notes, e.amount, e.reminder_date, e.notes,
		UNIX_TIMESTAMP(DATE_ADD(e.reminder_date, INTERVAL - e.reminder_minus_day DAY)) as email_blast_date,
		e.reminder_minus_day, e.is_paid, e.release_date 
		FROM wal_expense e
		LEFT JOIN wal_category c USING(category_id)
		LEFT JOIN wal_member m ON e.creator_id = m.member_id
		WHERE is_get_notif = 1 
		HAVING unix_timestamp() >= email_blast_date AND unix_timestamp() <= email_blast_date+(60*15);
		";
		$result['data'] = $this->db->query($query)->result_array();
		// debug($query);
		// die;
		if (!empty($result['data'])) 
		{
			foreach ($result['data'] as $key => $rs) 
			{
				// $var = array();
				// $var['name'] = $rs['fullname'];
				// $var['expense_name'] = $rs['name'].' '.$rs['title'];
				// $var['amount'] = format_money($rs['amount']);
				// $var['is_paid'] = ($rs['is_paid'] == 1 ? '<b style="color:green;">Sudah lunas</b>': '<b style="color:red;">Belum bayar</b>');
				// $var['reminder_date'] = date('D d-m-Y H:i', $rs['email_blast_date']);
				// $var['notes'] = nl2br($rs['notes']);
				// $var['weblink'] = base_url()."expense/report?do=edit&expense_id=".$rs['expense_id'];
				// $content = $this->common_model->templating($template,$var);
				
				// https://stackoverflow.com/questions/22251400/php-replace-1st-occurrence-with-x-and-2nd-occurrence-of-string-with-y
				debug('jaln nih ');
				$smscontent = 'GREVIA reminder: Tagihan '.$rs['name'].' '.$rs['title'].' seharga '.format_money($rs['amount'],'').' jatuh tempo pada '.date('d-m-Y H:i', $rs['email_blast_date']);
				$smscontent = str_replace(' ','+',$smscontent);
				
				// replace first string with formula
				$target_phone = preg_replace('/08/', '628', $rs['phone'], 1);
				// START CLOCKWORK ---------------------------------------------------
				// // $target_phone = '+6283891998825';
				// // $smsapi = https://api.clockworksms.com/http/send.aspx?key=5910a0c4c7057508936230692a039d68481a3a43&to=+6283891998825&content=Hello+World+this+testing+clockworksms
				// $smsapi = 'https://api.clockworksms.com/http/send.aspx?key=5910a0c4c7057508936230692a039d68481a3a43&to='.$target_phone.'&content='.$smscontent;
				
				// here 
				
				// debug($smscontent);
				// debug("die berak 5");
				$uri = 'https://api.callmebot.com/whatsapp.php?phone='.$target_phone.'&text='.$smscontent.'&apikey=5515218';
		
				// debug($uri);
				
				$curl = curl_init();
				curl_setopt_array($curl, array(
					  CURLOPT_URL => $uri,
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "GET",
					  CURLOPT_SSL_VERIFYHOST => 0,
					  CURLOPT_SSL_VERIFYPEER => 0,
					  CURLOPT_HTTPHEADER => 0,
					  // CURLOPT_POSTFIELDS => json_encode($data),
					  // CURLOPT_HTTPHEADER => array(
						// "client-id: ".$client_id,
						// "client-secret: ".$client_secret,
						// "content-type: application/json",
					  // ),
				));

				$response['msg'] = curl_exec($curl);
				curl_close($curl);
				
				// $exec = $this->curl->simple_get($smsapi);
				debug($response);
			}
			
		}
		else 
		{
			$response['msg'] = 'No data';
			debug($response,1);
		}
		
	}
	
	public function cron_email_reminder_expense()
	{
		$this->load->model('emailblast_model');
		
		// GET ALL ACTIVE REMINDER, WITH DUEDATE HAS BEEN PASSED ALREADY -> SEND EMAIL BLAST
		// $query = "
		// SELECT e.expense_id, CONCAT(m.first_name,' ',m.last_name) as fullname, m.email, c.name, e.title , e.notes, e.amount, e.reminder_date,
		// DATE_ADD(e.reminder_date, INTERVAL - e.reminder_minus_day DAY) as email_blast_date,
		// e.reminder_minus_day, e.is_paid, e.release_date 
		// FROM wal_expense e
		// LEFT JOIN wal_category c USING(category_id)
		// LEFT JOIN wal_member m ON e.creator_id = m.member_id
		// WHERE is_get_notif = 1 AND now() >= DATE_ADD(e.reminder_date, INTERVAL - e.reminder_minus_day DAY);
		// ";
		$query = "
		SELECT e.expense_id, CONCAT(m.first_name,' ',m.last_name) as fullname, m.email, m.phone, c.name, e.title , e.notes, e.amount, e.reminder_date, e.notes,
		UNIX_TIMESTAMP(DATE_ADD(e.reminder_date, INTERVAL - e.reminder_minus_day DAY)) as email_blast_date,
		e.reminder_minus_day, e.is_paid, e.release_date 
		FROM wal_expense e
		LEFT JOIN wal_category c USING(category_id)
		LEFT JOIN wal_member m ON e.creator_id = m.member_id
		WHERE is_get_notif = 1 
		HAVING unix_timestamp() >= email_blast_date AND unix_timestamp() <= email_blast_date+(60*15);
		";
		$result['data'] = $this->db->query($query)->result_array();
		// debug($query);
		if (!empty($result['data'])) 
		{
			foreach ($result['data'] as $key => $rs) 
			{
				//debug($rs);
				//echo $rs['email_blast_date'].' == '.date('Y-m-d H:i:00').HR;
				// if ($rs['email_blast_date'] == date('Y-m-d H:i:00')) {
					// echo "berhasil";
				// };
				$email_string = '';
				
				// OLD TEMPLATE
				// $email_string = "
				// <html>
				// <head></head>
				// <body>
				// Dear ".$rs['fullname'].",".BR.BR."
				// Anda mendapat email reminder dari Apps <b>Expense Tracker Grevia</b> tentang tagihan anda <b>".$rs['name']." - ".$rs['title']."</b> sebesar <b>".format_money($rs['amount'])."</b> yang telah jatuh tempo pada ".date('d-m-Y H:i', $rs['email_blast_date']).BR."<b>Notes</b><br/>
				// <div style='padding:10px; border:1px dotted black'>".nl2br($rs['notes']).'</div>'.BR.BR."
				
				// Untuk mematikan notifikasi ini silakan <a href='".base_url()."expense/report?do=disable_notification&expense_id=".$rs['expense_id']."'>klik link berikut</a> atau ke <a href='".base_url()."expense/report?do=edit&expense_id=".$rs['expense_id']."'>klik link berikut</a> ini untuk mengedit data expense anda.".BR.BR."
				// <b>Tim Wallet Tracker Grevia.</b>".BR."
				// http://wallet.grevia.com
				// </body>
				// </html>
				// ";
				
				// --------------------------------------------------
				// NEW TEMPLATE
				$template = '
				
<html>
 <head>
	<title>Email reminder Grevia Wallet</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
</head>

<body>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="font-family: Arial, sans-serif;">
	<tbody>
		<tr>
			<td colspan="100%"><img height="90px" src="http://wallet.grevia.com/asset/images/logo_new.png?v=1" width="90px" /></td>
		</tr>
		<tr>
			<td colspan="100%">
			<br/>
			<p>Dear {name},</p>
			<p>Anda mendapat email reminder dari Apps Expense Tracker Grevia tentang tagihan:</p>
			</td>
		</tr>
		<tr>
			<td style="border-top:1px solid #000;border-left:1px solid #000;text-align:left;padding:10px;font-weight:bold">Tagihan</td>
			<td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;text-align:right;padding:10px;">{expense_name}</td>
		</tr>
		<tr>
			<td style="border-top:1px solid #000;border-left:1px solid #000;text-align:left;padding:10px;font-weight:bold">Nominal</td>
			<td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;text-align:right;padding:10px;">{amount}</td>
		</tr>
		<tr>
			<td style="border-top:1px solid #000;border-left:1px solid #000;text-align:left;padding:10px;font-weight:bold">Status</td>
			<td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;text-align:right;padding:10px;">{is_paid}</td>
		</tr>
		<tr>
			<td style="border-top:1px solid #000;border-left:1px solid #000;text-align:left;padding:10px;font-weight:bold">Jatuh tempo</td>
			<td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;text-align:right;padding:10px;">{reminder_date}</td>
		</tr>
		<tr>
			<td style="border-top:1px solid #000;border-left:1px solid #000;text-align:left;padding:10px;font-weight:bold;valign:top" valign="top">Notes</td>
			<td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;text-align:right;padding:10px;">{notes}</td>
		</tr>
		<tr>
			<td colspan="2" style="border:1px solid #000;padding:10px;"> </td>
		</tr>
		<tr>
			<td colspan="2"> 
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse;">
					<tbody>
						<tr>
							<td style="padding-top:0;padding-right:18px;padding-bottom:18px;padding-left:18px;" valign="top" align="center">
								<table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;border-radius:6px;background-color:#000000;">
									<tbody>
										<tr>
											<td align="center" valign="middle" style="font-family:Arial;font-size:16px;padding:10px;">
												<a rel="nofollow" title="Lihat detail" target="_blank" href="{weblink}" style="font-weight:bold;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;color:#FFFFFF;display:block;">Lihat detail</a>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>

				';
				
				// template 2
				$template = '
				<html>
				<head>Email reminder Grevia Wallet</head>
				<body>
					<br/>
					<p>Dear {name},</p>
					<p>Anda mendapat email reminder dari Apps Expense Tracker Grevia tentang tagihan:</p>
					<table>
					<tr>
						<td>Tagihan</td>
						<td>{expense_name}</td>
					</tr>
					<tr>
						<td>Nominal</td>
						<td>{amount}</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>{is_paid}</td>
					</tr>
					<tr>
						<td>Jatuh tempo</td>
						<td>{reminder_date}</td>
					</tr>
					<tr>
						<td>Notes</td>
						<td>{notes}</td>
					</tr>
					</table>
					<br/><br/>
					<a rel="nofollow" title="Lihat detail" target="_blank" href="{weblink}" style="font-weight:bold;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;color:#FFFFFF;display:block;">Lihat detail</a>
				</body>
				<footer></footer>
				</html>
				';
				
				// test replace " jadi ' on the fly
				// $template = str_replace('"',"'",$template);
				// $template = mysql_real_escape_string($template);
				
				if (isset($rs['phone'])) 
				{
					// https://stackoverflow.com/questions/22251400/php-replace-1st-occurrence-with-x-and-2nd-occurrence-of-string-with-y
					$smscontent = 'GREVIA reminder: tagihan '.$rs['name'].' '.$rs['title'].' seharga '.format_money($rs['amount'],'').'jatuh tempo pada '.date('d-m-Y H:i', $rs['email_blast_date']);
					// $smscontent = str_replace(' ','+',$smscontent);
					
					// replace first string with formula
					$target_phone = preg_replace('/08/', '628', $rs['phone'], 1);
					// START CLOCKWORK ---------------------------------------------------
					// // $target_phone = '+6283891998825';
					// // $smsapi = https://api.clockworksms.com/http/send.aspx?key=5910a0c4c7057508936230692a039d68481a3a43&to=+6283891998825&content=Hello+World+this+testing+clockworksms
					// $smsapi = 'https://api.clockworksms.com/http/send.aspx?key=5910a0c4c7057508936230692a039d68481a3a43&to='.$target_phone.'&content='.$smscontent;
					// $this->curl->simple_get($smsapi);
					// END CLOCKWORK ---------------------------------------------------
					
					// ---------------------------------- START MESABOT ---------------------------------------------------
					// DEPRECATED 30june 2018
					// project testing
					// $client_id = 'nJ7kTveg';
					// $client_secret = 'BPpsvDWG';
					
					// project wallet grevia
					// $client_id = 'uDJ5H7XH';
					// $client_secret = 'KeQVkmWn';

					// $data = array(
					  // "destination" => array($target_phone),
					  // "text" => $smscontent,
					// );

					// $curl = curl_init();

					// curl_setopt_array($curl, array(
					  // CURLOPT_URL => "https://mesabot.com/api/v2/send",
					  // CURLOPT_RETURNTRANSFER => true,
					  // CURLOPT_ENCODING => "",
					  // CURLOPT_MAXREDIRS => 10,
					  // CURLOPT_TIMEOUT => 30,
					  // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  // CURLOPT_CUSTOMREQUEST => "POST",
					  // CURLOPT_POSTFIELDS => json_encode($data),
					  // CURLOPT_HTTPHEADER => array(
						// "client-id: ".$client_id,
						// "client-secret: ".$client_secret,
						// "content-type: application/json",
					  // ),
					// ));

					// $response = curl_exec($curl);

					// curl_close ($curl);
					// DEPRECATED 30june 2018
					// ---------------------------------- END MESABOT ---------------------------------------------------
					
					// ---------------------------------- START CLOCKWORK ---------------------------------------------------
					
					// example
					//https://api.clockworksms.com/http/send.aspx?key=5910a0c4c7057508936230692a039d68481a3a43&to=6283891998825&content=Hello+World+from+grevia
					
					// DISABLE FOR TEMPORARY
					// $secretkey = '5910a0c4c7057508936230692a039d68481a3a43';
					// $smsapi = 'https://api.clockworksms.com/http/send.aspx?key='.$secretkey.'&to='.$target_phone.'&content='.$smscontent;
					// $response = $this->curl->simple_get($smsapi);
					
					// ---------------------------------- END CLOCKWORK ---------------------------------------------------
					
					// Save to log 			
					$param = NULL;
					// $param['codename'] = 'expense_id';
					// $param['codevalue'] = $rs['expense_id'];
					// $param['content'] = $response;
					// $param['notes'] = 'sms_reminder';
					// $this->log_model->save($param);
				}
				
				$var = array();
				$var['name'] = $rs['fullname'];
				$var['expense_name'] = $rs['name'].' '.$rs['title'];
				$var['amount'] = format_money($rs['amount']);
				$var['is_paid'] = ($rs['is_paid'] == 1 ? '<b style="color:green;">Sudah lunas</b>': '<b style="color:red;">Belum bayar</b>');
				$var['reminder_date'] = date('D d-m-Y H:i', $rs['email_blast_date']);
				$var['notes'] = nl2br($rs['notes']);
				$var['weblink'] = base_url()."expense/report?do=edit&expense_id=".$rs['expense_id'];
				$content = $this->common_model->templating($template,$var);
				// $curl_content = str_replace('"',"'",$content);
				// debug($email_string);
				// debug($rs);
				// debug($email_string);
				// die;
				
				// EMAIL HERE
				$subject = "Reminder tagihan Grevia Wallet ".$rs['name']." - ".$rs['title'];
				
				// --------------------------------------
				// Save to database
				
				$is_sent = 0;
			
				// Change accordingly admin
				$to = $rs['email'];
				$to_alias = $rs['name'];
				
				$from = 'wallet@grevia.com';
				$from_alias = 'Wallet Grevia';
				
				$paramcurl = NULL;
				$paramcurl['sender'] = array(
					'name' => $from_alias,
					'email' => $from
					);
				$paramcurl['to'][] = array(
					'name' => $to_alias,
					'email' => $to,
				);
				$paramcurl['subject'] = $subject;
				$paramcurl['htmlContent'] = $content;
				
				$curl = $this->curl_sendblue('post',$paramcurl);
				// callback in json messageId
				
				// Check callback exist or not
				
				if (isset($curl['response']['messageId'])) $is_sent = 1;
				$param = NULL;
				$param['from_alias'] = $from;
				$param['subject'] = $subject;
				$param['content'] = $content;
				$param['to_list'] = $to;
				// $param['cc_list'] = ;
				// $param['bcc_list'] = ;
				// $param['is_html'] = ;
				$param['is_sent'] = $is_sent;
				$param['sent_count'] = 1;
				$param['remarks'] = json_encode($paramcurl);
				if (!empty($curl)) $param['log'] = json_encode($curl);
				$param['creator_id'] = 999;
				$param['creator_date'] = getDatetime();
				$save = $this->emailblast_model->save($param);
				// --------------------------------------

				if ($is_sent) 
					$gInfo = "Thank you for your email, we will respond in 2 x 24 hour.";
				else
					$gInfo = "Sorry, error connection. Please try again.";
				
				echo $email_string.HR;
			}
		}
		else 
		{
			echo BR."No Data";
		}
		die;
	}
	
	public function curl_sendblue($method = 'post', $data)
	{
		$result = $smtp = NULL;
		
		$smtp['url'] = 'https://api.sendinblue.com/v3/smtp/email';
		// error n deprecate on 5 nov
		// $smtp['key'] = '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';
		
		// new key on 5nov 200
		$smtp['key'] = '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';
		
		// $smtp['from'] = 'grevianet@gmail.com';
		
		$ch = curl_init($smtp['url']);
		
		# Setup request to send json via POST.
		if (isset($method) && $method == "post")
		{
			
			$data_post = '';
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($data) );
		}
	
		# Return response instead of printing.
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		
		# bypass restriction ssl from localhost
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
		$headers = array(
			// 'Token: grevia',
			'accept: application/json',
			'api-key: ' . $smtp['key'],
			'content-type: application/json',
		);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);	

		# Send request.
		$result['headers'] = json_encode($headers);
		$result['response'] = curl_exec($ch);
		if (isset($result['response'])) $result['response'] = json_decode($result['response'],1);
		
		if (curl_errno($ch)) {
			$result['error_msg'] = curl_error($ch);
		}
		
		curl_close($ch);
		
		// Save to log
		$param_model = NULL;
		$param_model['codename'] = 'sendinblue';
		$param_model['codevalue'] = $result['response']['messageId'];
		$param_model['content'] = json_encode($data);
		$param_model['notes'] = json_encode($result);
		$this->log_model->save($param_model);
		// notes
		
		# Print response.
		return $result;
	}
	
	/*
	 | Return List report expense by loops of each year & month
	 | 
	 | @creator_id
	 | 
	*/
	public function get_report_all_month_new($attr = NULL) 
	{
		$query = "
		SELECT MONTH(e.release_date) as permonth, YEAR(e.release_date) as peryear,
		(
			SELECT SUM(amount) 
			FROM wal_expense we 
			LEFT JOIN wal_category wc ON we.category_id = wc.category_id
			WHERE wc.is_increment = 1 AND MONTH(we.release_date) = permonth AND YEAR(we.release_date) = peryear AND we.creator_id = e.creator_id
		) as total_income_month,
		(
			SELECT SUM(amount) 
			FROM wal_expense we 
			LEFT JOIN wal_category wc ON we.category_id = wc.category_id
			WHERE wc.is_increment = 0 AND MONTH(we.release_date) = permonth AND YEAR(we.release_date) = peryear AND we.creator_id = e.creator_id
		) as total_expense_month
		FROM wal_expense e 
		WHERE 1";
		
		if (isset($attr['creator_id']) && $attr['creator_id'] != '')
		{
			$query .= " AND e.creator_id = ".$attr['creator_id'];
		}
		
		if (isset($attr['year']) && $attr['year'] != '')
		{
			$query .= " AND YEAR(e.release_date) = ".$attr['year'];
		}
		
		$query .= " 
		GROUP BY permonth, peryear
		HAVING (permonth != 0 OR peryear != 0)";
		
		$query .= "
		ORDER BY peryear DESC,permonth DESC";

		// debug($query);
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	/*
	 | Return List report by fund type
	 | 
	 | @creator_id / @month @year
	 | 
	*/
	public function get_report_by_fund_type($attr = NULL) 
	{
		$query = "
		SELECT f.name as fund_name, f.fund_type, f.due_date, 
		(
			SELECT IFNULL(SUM(te.amount),0) as dd
			FROM wal_category tc 
			LEFT JOIN wal_expense te ON tc.category_id = te.category_id 
			WHERE 1 AND tc.is_increment = 1 
			AND te.creator_id = e.creator_id 
			AND MONTH(te.release_date) = MONTH(e.release_date) 
			AND YEAR(te.release_date) = YEAR(e.release_date)
			AND te.fund_id = f.fund_id
		) as total_income,
		(
			SELECT IFNULL(SUM(te.amount),0) as dd
			FROM wal_category tc 
			LEFT JOIN wal_expense te ON tc.category_id = te.category_id 
			WHERE 1 AND tc.is_increment = -1 
			AND te.creator_id = e.creator_id 
			AND MONTH(te.release_date) = MONTH(e.release_date) 
			AND YEAR(te.release_date) = YEAR(e.release_date)
			AND te.fund_id = f.fund_id
		) as total_expense
		FROM wal_expense e
		LEFT JOIN wal_fund f ON f.fund_id = e.fund_id
		WHERE 1
		";
		
		if (isset($attr['creator_id']) && $attr['creator_id'] != '')
		{
			$query .= " AND e.creator_id = ".$attr['creator_id'];
		}
		
		if (isset($attr['month']) && isset($attr['month']) && $attr['year'] != '' && $attr['year'] != '')
		{
			$query .= " AND YEAR(e.release_date) = ".$this->db->escape($attr['year'])." AND MONTH(e.release_date) = ".$this->db->escape($attr['month']);
		}
		else if (isset($attr['start_date']) && isset($attr['end_date']))
		{
			$query .= " AND e.release_date BETWEEN ".$this->db->escape(date('Y-m-d',strtotime($attr['start_date'])))." AND ".$this->db->escape(date('Y-m-d',strtotime($attr['end_date'])));
		}
		else 
		{
			$query .= " AND YEAR(e.release_date) = YEAR(now()) AND MONTH(e.release_date) = MONTH(now())";
		}
		
		$query .= " 
		GROUP BY e.fund_id";
		
		// $query .= "
		// ORDER BY total_amount DESC, release_date DESC";

		// debug($query);
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}

	// for chart report
	public function get_report_parent_expense_by_year($attr = NULL) 
	{
		/**
		 * SELECT c.name AS cat_name, 
		cp.name as parent_catname,
		SUM(e.amount) AS tot_amount,
		MONTH(e.release_date) as mndt,
		YEAR(e.release_date) AS yrdt
		FROM wal_expense e
		LEFT JOIN wal_category c ON e.category_id = c.category_id
		LEFT JOIN wal_category cp ON cp.category_id = c.parent_category_id
		WHERE 1 
		AND cp.is_parent = 1
		AND e.creator_id = 1 
		-- AND MONTH(e.release_date) = 2
		AND YEAR(e.release_date) = 2022
		GROUP BY parent_catname, mndt
		-- c.parent_category_id, mndt
		 */

		$query = "
		SELECT c.category_id,
		c.name AS catname, 
		cp.category_id as parent_catid,
		cp.name as parent_catname,
		SUM(e.amount) AS tot_amount,
		MONTH(e.release_date) as mndt,
		YEAR(e.release_date) AS yrdt
		FROM wal_expense e
		LEFT JOIN wal_category c ON e.category_id = c.category_id
		LEFT JOIN wal_category cp ON cp.category_id = c.parent_category_id
		WHERE 1 
		AND cp.is_parent = 1
		AND cp.is_show_report = 1
		";
		
		if (isset($attr['start_date']) && isset($attr['end_date']))
		{
			$query.= ' AND release_date BETWEEN ' . replace_quote(date('Y-m-d',strtotime($attr['start_date']))) .' AND ' . replace_quote(date('Y-m-d',strtotime($attr['end_date'])));
		}

		if (isset($attr['month']) && isset($attr['month']))
		{
			$query.= ' AND MONTH(release_date) = ' . $attr['month'];
		}

		if (isset($attr['year']) && isset($attr['year']))
		{
			$query.= ' AND YEAR(release_date) = ' . $attr['year'];
		}

		// FIND data by parent_cat
		if (isset($attr['is_parent'])) 
		{
			$query.= ' AND cp.is_parent = '. $attr['is_parent'];
		}
		
		
		// // FIND income
		// if (isset($attr['is_increment'])) 
		// {
		// 	$query.= ' AND is_increment = '. $attr['is_increment'];
		// }
		
		// // FIND budget
		// if (isset($attr['is_budget'])) 
		// {
		// 	$query.= ' AND e.is_budget = '. $attr['is_budget'];
		// }
		
		// FIND routine expense
		// if (isset($attr['is_routine'])) 
		// {
		// 	$query.= ' AND e.is_routine = '. $attr['is_routine'];
		// }
		
		// FIND routine expense
		if (isset($attr['creator_id'])) 
		{
			$query.= ' AND e.creator_id = '. $attr['creator_id'];
		}
		
		$query .= ' GROUP BY parent_catname, mndt';
		$query .= ' ORDER BY tot_amount DESC';
		
		// debug($query);die;
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	// for chart report
	public function get_report_parent_expense_global($attr = NULL) 
	{
		/**
		 * 
		
		 */

		$query = "
		SELECT c.category_id,
		c.name AS catname, 
		cp.category_id as parent_catid,
		cp.name as parent_catname,
		SUM(e.amount) AS tot_amount
		FROM wal_expense e
		LEFT JOIN wal_category c ON e.category_id = c.category_id
		LEFT JOIN wal_category cp ON cp.category_id = c.parent_category_id
		WHERE 1 
		AND cp.is_parent = 1
		AND cp.is_show_report = 1
		";
		
		if (isset($attr['start_date']) && isset($attr['end_date']))
		{
			$query.= ' AND release_date BETWEEN ' . replace_quote(date('Y-m-d',strtotime($attr['start_date']))) .' AND ' . replace_quote(date('Y-m-d',strtotime($attr['end_date'])));
		}

		if (isset($attr['month']) && isset($attr['month']))
		{
			$query.= ' AND MONTH(release_date) = ' . $attr['month'];
		}

		if (isset($attr['year']) && isset($attr['year']))
		{
			$query.= ' AND YEAR(release_date) = ' . $attr['year'];
		}

		// FIND data by parent_cat
		if (isset($attr['is_parent'])) 
		{
			$query.= ' AND cp.is_parent = '. $attr['is_parent'];
		}
		
		
		// // FIND income
		// if (isset($attr['is_increment'])) 
		// {
		// 	$query.= ' AND is_increment = '. $attr['is_increment'];
		// }
		
		// // FIND budget
		// if (isset($attr['is_budget'])) 
		// {
		// 	$query.= ' AND e.is_budget = '. $attr['is_budget'];
		// }
		
		// FIND routine expense
		// if (isset($attr['is_routine'])) 
		// {
		// 	$query.= ' AND e.is_routine = '. $attr['is_routine'];
		// }
		
		// FIND routine expense
		if (isset($attr['creator_id'])) 
		{
			$query.= ' AND e.creator_id = '. $attr['creator_id'];
		}
		
		$query .= ' GROUP BY parent_catname';
		$query .= ' ORDER BY tot_amount DESC';
		
		// debug($query);die;
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function get_list_expense_byday($attr = NULL) 
	{
		$query = '
		SELECT e.release_date, is_increment, SUM(IFNULL(e.amount,0)) AS total_amount
		FROM wal_expense e
		LEFT JOIN wal_category c ON e.category_id = c.category_id
		WHERE 1
		';
		
		if (isset($attr['fund_id'])) 
		{
			$query.= ' AND e.fund_id = ' . $attr['fund_id'];
		}
		
		if (isset($attr['is_increment'])) 
		{
			$query.= ' AND c.is_increment = ' . $attr['is_increment'];
		}
		
		if (isset($attr['release_date_start']) && isset($attr['release_date_end'])) 
		{
			$query.= ' AND e.release_date BETWEEN ' . replace_quote(date('Y-m-d',strtotime($attr['release_date_start']))) . ' AND ' . replace_quote(date('Y-m-d',strtotime($attr['release_date_end'])));
		}
		
		if (isset($attr['creator_id'])) 
		{
			$query.= ' AND e.creator_id = ' . $attr['creator_id'];
		}
		
		$query.= ' GROUP BY e.release_date';
		$query.= ' ORDER BY e.release_date ASC';
		// debug($query);
		// die;
		$result = $this->db->query($query)->result_array();
		return $result;
	}
	
	
	
	
	
	
}