<?php

Class Category_model extends MY_Model 
{
	public $table = 'wal_category';
	public $id = 'category_id';

	public function __construct() {
        parent::__construct();
    }
	
	public function get($attr = NULL) 
	{
		$query = "SELECT category_id, `name`, is_increment, is_budget, is_monthly_billing, is_parent, is_show_report, is_publish, parent_category_id, monthly_budget_amount, due_date, is_delete, notes, creator_id, creator_date, c.creator_date as category_creator_date, c.editor_date as category_editor_date ";
		$query.= ", IF(c.is_increment = 1,'income',IF(c.is_increment = 0,'topup','expense')) as category_type ";
		$query.= ", (SELECT category_id FROM wal_category wc WHERE wc.category_id = c.parent_category_id ) as parent_category_id ";
		$query.= ", (SELECT name FROM wal_category wa WHERE wa.category_id = c.parent_category_id ) as parent_category_name ";

		$query.= "FROM wal_category c ";
		$query.= "WHERE 1";
		
		if (isset($attr['keyword']) && $attr['keyword'] != '') 
		{
			$query.= ' AND c.name LIKE ' . replace_quote('%'.$attr['keyword'].'%');
		}
		
		if (isset($attr['creator_id'])) 
		{
			$query.= ' AND creator_id = ' . replace_quote($attr['creator_id']);
		}
		
		if (isset($attr['category_id'])) 
		{
			$query.= ' AND category_id = ' . $attr['category_id'];
		}
		
		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	public function get_list($attr = NULL) 
	{
		$query = "SELECT category_id, `name`, is_increment, is_budget, is_monthly_billing, is_parent, is_show_report, is_publish, parent_category_id, monthly_budget_amount, due_date, is_delete, notes, creator_id, creator_date, c.creator_date as category_creator_date, c.editor_date as category_editor_date ";
		$query.= ", IF(c.is_increment = 1,'income',IF(c.is_increment = 0,'topup','expense')) as category_type ";
		$query.= ", (SELECT category_id FROM wal_category wc WHERE wc.category_id = c.parent_category_id ) as parent_category_id ";
		$query.= ", (SELECT name FROM wal_category wa WHERE wa.category_id = c.parent_category_id ) as parent_category_name ";
		
		if (isset($attr['is_parent']) && $attr['is_parent']==1) {
			
			$query.= ", (SELECT COUNT(wca.category_id) FROM wal_category wca WHERE wca.is_parent = 0 AND wca.parent_category_id = c.category_id ) as total_child ";
		}
		
		if (isset($attr['total_trx_by_month_year']) && isset($attr['total_trx_by_month_year'])) 
		{
			$query .= "
			,(
				SELECT COUNT(e.expense_id) 
				FROM wal_expense e 
				WHERE e.creator_id = c.creator_id 
				AND e.category_id = c.category_id
				AND MONTH(e.release_date) = MONTH('".$attr['total_trx_by_month_year']."') 
				AND YEAR(e.release_date) = YEAR('".$attr['total_trx_by_month_year']."') 
			) as total_trx_this_month
			";
			// AND MONTH(e.release_date) = MONTH('".$attr['total_trx_by_month_year']."') 
				// AND YEAR(e.release_date) = YEAR('".$attr['total_trx_by_month_year']."') 
		}
		
		$query .= "
		FROM wal_category c
		WHERE 1
		";

		// if (isset($attr['keyword']) && $attr['keyword'] != '') 
		// {
		// 	$query.= ' AND c.name LIKE ' . replace_quote('%'.$attr['keyword'].'%');
		// }

		if (isset($attr['creator_id']))
		{
			$query.= ' AND creator_id = ' .$attr['creator_id'];
		}

		if (isset($attr['is_parent']))
		{
			$query.= ' AND is_parent = '. replace_quote($attr['is_parent']);
		}
		
		if (isset($attr['is_publish']))
		{
			$query.= ' AND is_publish = '. replace_quote($attr['is_publish']);
		}

		$query.= ' HAVING 1';

		if (isset($attr['no_parent']) && $attr['no_parent'] != '') 
		{
			$query.= ' AND parent_category_id IS NULL AND is_parent = 0';
		}
		
		if (isset($attr['keyword']) && $attr['keyword'] != '') 
		{
			$query.= ' AND ( parent_category_name LIKE ' . replace_quote('%'.$attr['keyword'].'%');
			$query.= ' OR c.name LIKE ' . replace_quote('%'.$attr['keyword'].'%');
			$query.= ')';
		}
		
		if (isset($attr['order']))
		{
			$query .= ' ORDER BY '.$attr['order'];
		}
		else
		{
			$query .= ' ORDER BY category_id DESC';
		}
		
		// debug($query,1);
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = PAGING_LIMIT;
			$offset = PAGING_OFFSET;
						
			$limit = 0;
			if (isset($attr['page']) && $attr['page'] > 1) {
				$limit = ($attr['page']-1) * $offset;
			}
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}

		if (isset($attr['debug'])) {
			debug($query);
		}


		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function get_list_parent($attr = NULL) 
	{
		$query = "
		SELECT c.name, 
		(
			SELECT IFNULL(SUM(te.amount),0)
			FROM wal_expense te
			LEFT JOIN wal_category tc USING(category_id)
			WHERE tc.parent_category_id = c.category_id
			";

		if (isset($attr['creator_id']))
		{
			$query.= ' AND tc.creator_id = ' .$attr['creator_id'];
		}
		
		
		if (isset($attr['start_date']) && isset($attr['end_date']))
		{
			$query.= ' AND te.release_date BETWEEN ' . replace_quote(date('Y-m-d',strtotime($attr['start_date']))) .' AND ' . replace_quote(date('Y-m-d',strtotime($attr['end_date'])));
		}

		// if (isset($attr['month']))
		// {
		// 	$query.= ' AND MONTH(te.release_date) = ' . replace_quote($attr['month']);
		// }

		// if (isset($attr['year']))
		// {
		// 	$query.= ' AND YEAR(te.release_date) = ' . replace_quote($attr['year']);
		// }

		$query.= "
		) AS total_expense
		FROM wal_category c 
		WHERE 1 
		AND c.is_parent = 1 
		AND c.is_delete != -1
		";		

		if (isset($attr['keyword']) && $attr['keyword'] != '') 
		{
			$query.= ' AND c.name LIKE ' . replace_quote('%'.$attr['keyword'].'%');
		}

		if (isset($attr['creator_id']))
		{
			$query.= ' AND c.creator_id = ' .$attr['creator_id'];
		}

		// if (isset($attr['is_parent']))
		// {
		// 	$query.= ' AND is_parent = '. replace_quote($attr['is_parent']);
		// }

		$query.= ' HAVING 1';
		
		if (isset($attr['order']))
		{
			$query .= ' ORDER BY '.$attr['order'];
		}
		else
		{
			$query .= ' ORDER BY total_expense DESC';
		}
		// debug($query);
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = PAGING_LIMIT;
			$offset = PAGING_OFFSET;
			
			// if (isset($attr['per_page']) && ($attr['per_page']==10 || $attr['per_page']==30 || $attr['per_page']==50))
			// {
			// 	$offset = $attr['per_page'];
			// }
			
			$limit = 0;
			// $offset = 0;
			// if (isset($attr['limit'])) $limit = $attr['limit'];
			// if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($attr['page']) && $attr['page'] > 1) {
				// $limit = $offset;
				// $offset = 99;
				$limit = ($attr['page']-1) * $offset;
			}
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}

		if (isset($attr['debug'])) {
			debug($query);
			// echo $query;die;
		}

		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}

	public function get_list_report_by_parent($attr = NULL) 
	{
		$query = "
		SELECT c.name, 
		(
			SELECT IFNULL(SUM(te.amount),0)
			FROM wal_expense te
			LEFT JOIN wal_category tc USING(category_id)
			WHERE tc.parent_category_id = c.category_id
			";

		if (isset($attr['creator_id']))
		{
			$query.= ' AND tc.creator_id = ' .$attr['creator_id'];
		}
		
		
		if (isset($attr['start_date']) && isset($attr['end_date']))
		{
			$query.= ' AND te.release_date BETWEEN ' . replace_quote(date('Y-m-d',strtotime($attr['start_date']))) .' AND ' . replace_quote(date('Y-m-d',strtotime($attr['end_date'])));
		}

		// if (isset($attr['month']))
		// {
		// 	$query.= ' AND MONTH(te.release_date) = ' . replace_quote($attr['month']);
		// }

		// if (isset($attr['year']))
		// {
		// 	$query.= ' AND YEAR(te.release_date) = ' . replace_quote($attr['year']);
		// }

		$query.= "
		) AS total_expense
		FROM wal_category c 
		WHERE 1 
		AND c.is_parent = 1 
		AND c.is_delete != -1
		AND c.is_show_report = 1
		";		

		if (isset($attr['keyword']) && $attr['keyword'] != '') 
		{
			$query.= ' AND c.name LIKE ' . replace_quote('%'.$attr['keyword'].'%');
		}

		if (isset($attr['creator_id']))
		{
			$query.= ' AND c.creator_id = ' .$attr['creator_id'];
		}

		// if (isset($attr['is_parent']))
		// {
		// 	$query.= ' AND is_parent = '. replace_quote($attr['is_parent']);
		// }

		$query.= ' HAVING 1';
		
		if (isset($attr['order']))
		{
			$query .= ' ORDER BY '.$attr['order'];
		}
		else
		{
			$query .= ' ORDER BY total_expense DESC';
		}
		// debug($query);
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = PAGING_LIMIT;
			$offset = PAGING_OFFSET;
			
			// if (isset($attr['per_page']) && ($attr['per_page']==10 || $attr['per_page']==30 || $attr['per_page']==50))
			// {
			// 	$offset = $attr['per_page'];
			// }
			
			$limit = 0;
			// $offset = 0;
			// if (isset($attr['limit'])) $limit = $attr['limit'];
			// if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($attr['page']) && $attr['page'] > 1) {
				// $limit = $offset;
				// $offset = 99;
				$limit = ($attr['page']-1) * $offset;
			}
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}

		if (isset($attr['debug'])) {
			debug($query);
			// echo $query;die;
		}

		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	
	public function get_list_monthly_billing_category($attr = NULL) 
	{
		$query = "
		SELECT category_id, name as category_name, c.monthly_budget_amount ,
		(
			SELECT COUNT(e.expense_id) as a 
			FROM wal_expense e
			WHERE e.creator_id = c.creator_id 
			AND e.category_id = c.category_id";
			
		if (isset($attr['start_date']) && isset($attr['end_date']))
		{
			$query.= ' AND e.release_date BETWEEN ' . replace_quote(date('Y-m-d',strtotime($attr['start_date']))) .' AND ' . replace_quote(date('Y-m-d',strtotime($attr['end_date'])));
		}
			
		$query.="
		) as total_transaction_count,
		(
			SELECT SUM(e.amount) as a 
			FROM wal_expense e
			WHERE e.creator_id = c.creator_id 
			AND e.category_id = c.category_id 
		";
		
		if (isset($attr['start_date']) && isset($attr['end_date']))
		{
			$query.= ' AND e.release_date BETWEEN ' . replace_quote(date('Y-m-d',strtotime($attr['start_date']))) .' AND ' . replace_quote(date('Y-m-d',strtotime($attr['end_date'])));
		}
		
		$query.= 
		"
		) as total_transaction_amount,
		c.due_date, c.notes
		FROM wal_category c
		WHERE c.is_monthly_billing = 1
		";

		if (isset($attr['creator_id']))
		{
			$query.= ' AND c.creator_id = ' .$attr['creator_id'];
		}
		
		if (isset($attr['is_publish']))
		{
			$query .= ' AND c.is_publish = '.$attr['order'];
		}
		else
		{
			$query .= ' AND c.is_publish = 1';
		}
		
		if (isset($attr['order']))
		{
			$query .= ' ORDER BY '.$attr['order'];
		}
		else
		{
			$query .= ' ORDER BY category_id DESC';
		}
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	/* Return all category with total amount / income per user */
	public function get_list_category_with_total_amount_global_date($attr = NULL) 
	{
		/*
		-> untuk pake selisih tanggal release date awal - akhir: SELECT DATEDIFF(MAX(twe.release_date),MIN(twe.release_date)) as total_days_count
		*/ 
		$query = "
		SELECT c.category_id, if(c.is_increment = 1,'income','expense') as category_type, c.name ,
		(
			SELECT SUM(we.amount) 
			FROM wal_expense we
			WHERE we.creator_id = e.creator_id AND we.category_id = c.category_id
		) as total_amount,
		(
			
			SELECT COUNT(twe.release_date) as total_days_count
			FROM wal_expense twe
			WHERE twe.creator_id = e.creator_id AND e.category_id = twe.category_id
		) as total_days_count
		FROM wal_category c
		LEFT JOIN wal_expense e ON e.category_id = c.category_id
		WHERE 1
		";
		
		if (isset($attr['creator_id']) && $attr['creator_id'] != '')
		{
			$query.= ' AND e.creator_id = ' .$attr['creator_id'];
		}
		
		if (isset($attr['is_show_report']) && $attr['is_show_report'] != '')
		{
			$query.= ' AND c.is_show_report = ' .$attr['is_show_report'];
		}
		else 
		{
			$query.= ' AND c.is_show_report = 1';
		}
		
		$query .= ' GROUP BY c.category_id';
		
		if (isset($attr['order']))
		{
			$query .= ' ORDER BY '.$attr['order'];
		}
		else
		{
			$query .= ' ORDER BY category_id DESC';
		}
		
		//echo $query;die;
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO ' . $this->table . ' ';
		$i = 1;
		
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$list_field.= ', creator_ip, creator_date';
		$list_value.= ', '.replace_quote(getIP()) . ',' . replace_quote(getDateTime());
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;     
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE ' . $this->table . ' SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDateTime());
		$query.= ' WHERE category_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}

	public function update_bulk($list_id, $data)
	{
		$arr_id = NULL;
		if (is_array($list_id)) {
			$arr_id = implode(',', $list_id);
		}

		$query = 'UPDATE ' . $this->table . ' SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDateTime());
		$query.= ' WHERE category_id IN ( '. replace_quote($arr_id,'num') . ')';
		// debug($query,1);
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM ' . $this->table . ' WHERE ' . $this->id . ' = ' . $id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;	
	}
	
}