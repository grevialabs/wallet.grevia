<?php

Class Task_project_model extends MY_Model 
{
	public function __construct() {
        parent::__construct();
    }
	
	public function get($attr = NULL) 
	{
		$query = '
		SELECT t.*
		FROM wal_task_project t
		WHERE 1';
		if (isset($attr['email'])) 
		{
			$query.= ' AND email = ' . replace_quote($attr['email']);
		}
		
		if (isset($attr['active_code'])) 
		{
			$query.= ' AND active_code = ' . replace_quote($attr['active_code']);
		}
		
		if (isset($attr['task_project_id'])) 
		{
			$query.= ' AND task_project_id = ' . $attr['task_project_id'];
		}
		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	public function get_list($attr = NULL) 
	{
		$query = '
		SELECT t.*	
		FROM wal_task_project t
		WHERE 1';
		
		if (isset($attr['is_done']) && in_array($attr['is_done'],array(0,1))) {
			$query.= ' AND is_done = ' . $attr['is_done'];
		}
		
		if (isset($attr['task_project_id'])) {
			$query.= ' AND task_project_id = ' . $attr['task_project_id'];
		}
		
		if (isset($attr['creator_id'])) {
			$query.= ' AND creator_id = ' . $attr['creator_id'];
		}
		
		if (isset($attr['today'])) {
			$query.= ' AND creator_date BETWEEN "'.date('Y-m-d').' 00:00:00" AND "'.date('Y-m-d').' 23:59:59"';
		}
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO wal_task_project ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$list_field.= ', creator_ip, creator_date';
		
		$list_value.= ','.replace_quote(getIP());
		$list_value.= ','.replace_quote(getDatetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE wal_task_project SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . $this->db->escape($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDatetime());
		$query.= ' WHERE task_project_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM wal_task_project WHERE task_project_id = ' . $id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;	
	}
	
}