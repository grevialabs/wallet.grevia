<?php

Class Order_model extends MY_Model 
{
	public function __construct() {
        parent::__construct();
    }
	
	public function generate_payment_id($str,$prefix = "INV")
	{
		$date_month = date("Ymd", time() );
		$newLen = $str + 1;
		$val = "";
		if (!isset($prefix)) $prefix = "INV";
		$len = strlen($newLen);
		if($len == 1){$val = $prefix.$date_month."0000".$newLen; 	}
		if($len == 2){$val = $prefix.$date_month."000".$newLen;	}
		if($len == 3){$val = $prefix.$date_month."00".$newLen;		}
		if($len == 4){$val = $prefix.$date_month."0".$newLen;		}
		if($len == 5){$val = $prefix.$date_month."".$newLen;		}
		return $val;
	}
	
	public function generate_random()
	{
		$random = rand(1000000,9999999);
		return $random;
	}
	
	public function get_new_order_code()
	{
		// GENERAETE NEW ORDER CODE NOT EXIST IN DATABASE
		$try = false;
		do
		{
			$random = $this->generate_random();
			$query = "SELECT * FROM job_order WHERE order_id = ".$random;
			$result = $this->db->query($query)->row_array();

			// IF RECORD NOT EXIST
			if (empty($result))
			{
				$try = TRUE;
				break;
			}
		} while($try == false);
		
		if ($try) return $random;
	}
	
	public function get_new_id($prefix = "INV")
	{
		$date_month = date("Ymd", time() );
		$sql = "
		SELECT invoice_code 
		FROM job_order 
		WHERE invoice_code LIKE '%".$prefix.$date_month."_%' 
		ORDER BY invoice_code DESC 
		LIMIT 1;";
		$query = $this->db->query($sql)->row_array();
		if (!empty($query))
		{
			$lastPaymentID = $query['invoice_code'];
			$newPaymentID = $this->common_model->get_right($lastPaymentID,5);
			$val = $this->generate_payment_id($newPaymentID,$prefix);
		}
		else 
		{
			$val = $prefix."".$date_month."00001";
		}
		return $val;
	}
	
	public function generate_key()
	{
		$query = "SELECT * FROM job_order WHERE 1 ORDER BY id DESC LIMIT 1";
		//$
	}
	
	public function get($attr = NULL) 
	{
		$query = 'SELECT job_order.* FROM job_order WHERE 1';
		if (isset($attr['order_id'])) 
		{
			$query.= ' AND order_id = ' . replace_quote($attr['order_id']);
		}
		
		// INVOICE FOR COMPANY
		if (isset($attr['company_id'])) 
		{
			$query.= ' AND company_id = ' . $attr['company_id'];
		}
		
		// INVOICE FOR INDIVIDUAL
		if (isset($attr['member_id'])) 
		{
			$query.= ' AND member_id = ' . $attr['member_id'];
		}
		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	public function get_list($attr = NULL) 
	{
		$query = '
		SELECT o.*, c.name as company_name 
		FROM job_order o
		LEFT JOIN job_company c ON o.company_id = c.company_id
		WHERE 1
		
		';
		if (isset($attr['member_id'])) 
		{
			$query.= ' AND member_id = ' . $attr['member_id'];
		}
		
		if (isset($attr['company_id'])) 
		{
			$query.= ' AND o.company_id = ' . $attr['company_id'];
		}
		
		if (isset($attr['payment_status'])) 
		{
			$query.= ' AND payment_status = '. $attr['payment_status'];
		}
		
		if (isset($attr['order']) && isset($attr['orderby']))
		{
			$query.= ' ORDER BY '.$attr['order'].' '.$attr['orderby'];
		}
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			$offset = 1;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function get_list_detail($attr = NULL) 
	{
		$query = '
		SELECT o.*, od.*, p.name as product_name, p.description, p.participant_credit
		FROM job_order o
		INNER JOIN job_order_detail od USING(order_id)
		INNER JOIN job_product p USING(product_id)
		WHERE 1';
		if (isset($attr['order_id'])) 
		{
			$query.= ' AND o.order_id = ' . $attr['order_id'];
		}
		
		// INVOICE FOR COMPANY
		if (isset($attr['company_id'])) 
		{
			$query.= ' AND od.company_id = ' . $attr['company_id'];
		}
		
		// INVOICE FOR INDIVIDUAL USER
		if (isset($attr['creator_id'])) 
		{
			$query.= ' AND o.creator_id = ' . $attr['creator_id'];
		}
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO job_order ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$list_field.= ', creator_id, creator_ip, creator_date';
		
		$list_value.= ','.replace_quote(company_cookies('company_id'));
		$list_value.= ','.replace_quote(getIP());
		$list_value.= ','.replace_quote(getDatetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE job_order SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ', editor_id = '.replace_quote(company_cookies('company_id'));
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDatetime());
		$query.= ' WHERE order_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM job_order WHERE order_id = ' . $id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;	
	}
	
	/*-----------------------------------------------------------------------------------*/
	
	public function save_detail($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO job_order_detail ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		// $list_field.= 'creator_id, creator_ip, creator_date';
		
		// $list_value.= ','.replace_quote(company_cookies('company_id'));
		// $list_value.= ','.replace_quote(getIP());
		// $list_value.= ','.replace_quote(getDatetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update_detail($id, $data)
	{
		$query = 'UPDATE job_order_detail SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ', editor_id = '.replace_quote(company_cookies('company_id'));
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDatetime());
		$query.= ' WHERE order_detail_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete_detail($id)
	{
		$query = 'DELETE FROM job_order_detail WHERE order_detail_id = ' . $id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;	
	}
	
}