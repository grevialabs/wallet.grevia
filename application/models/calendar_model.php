<?php

Class Calendar_model extends MY_Model 
{
	public function __construct() {
        parent::__construct();
    }
	
	public function get($attr = NULL) 
	{
		$query = 'SELECT * FROM wal_calendar WHERE 1';
		
		if (isset($attr['is_global_calendar']) && in_array($attr['is_global_calendar'], array('0','1')))
		{
			$query.= ' AND is_global_calendar = ' . $attr['is_global_calendar'];
		}
		
		if (isset($attr['email'])) 
		{
			$query.= ' AND email = ' . replace_quote($attr['email']);
		}
		if (isset($attr['calendar_id'])) 
		{
			$query.= ' AND calendar_id = ' . $attr['calendar_id'];
		}
		if (isset($attr['is_finish'])) 
		{
			$query.= ' AND is_finish = ' . $attr['is_finish'];
		}
		if (isset($attr['creator_id'])) 
		{
			$query.= ' AND creator_id = ' . $attr['creator_id'];
		}
		if (isset($attr['creator_date'])) 
		{
			$start = date('Y-m-d', strtotime($attr['creator_date'])).' 00:00:00';;
			$end = date('Y-m-d', strtotime($attr['creator_date'])).' 23:59:59';
			$query.= ' AND creator_date BETWEEN ' . replace_quote($start) .' AND '. replace_quote($end);
		}
		if (isset($attr['last'])) 
		{
			$query.= ' ORDER BY calendar_id DESC LIMIT 1';
		}
		// debug($query);
		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	// public function get_report($attr = NULL) 
	// {
		// $query = "
		// SELECT 
		// (
			// SELECT IFNULL(SUM(amount),0) as total
			// FROM wal_calendar e
			// LEFT JOIN wal_category c USING(category_id)
			// WHERE 1 ";
			// if (isset($attr['start_date']) && isset($attr['end_date']))
			// {
				// $query .= " AND input_date BETWEEN '".date('Y-m-d',strtotime($attr['start_date']))."' AND '".date('Y-m-d',strtotime($attr['end_date']))."'";
			// }
			
			// if (isset($attr['month']) && isset($attr['year']))
			// {
				// $query.= " AND MONTH(input_date) = '".$attr['month']."' AND YEAR(input_date) = '".$attr['year']."'";
			// }
			
			// if (isset($attr['creator_id']) && isset($attr['creator_id']))
			// {
				// $query.= " AND e.creator_id = '".$attr['creator_id']."'";
			// }
			
			// $query .=" AND is_increment = 0
		// ) as total_expense
		// ,
		// (
			// SELECT IFNULL(SUM(amount),0) as total
			// FROM wal_calendar e
			// LEFT JOIN wal_category c USING(category_id)
			// WHERE 1";
			
			// if (isset($attr['start_date']) && isset($attr['end_date']))
			// {
				// $query .= " AND input_date BETWEEN '".date('Y-m-d',strtotime($attr['start_date']))."' AND '".date('Y-m-d',strtotime($attr['end_date']))."'";
			// }
			
			// if (isset($attr['month']) && isset($attr['year']))
			// {
				// $query.= " AND MONTH(input_date) = '".$attr['month']."' AND YEAR(input_date) = '".$attr['year']."'";
			// }
			
			// if (isset($attr['creator_id']) && isset($attr['creator_id']))
			// {
				// $query.= " AND e.creator_id = '".$attr['creator_id']."'";
			// }
			// $query .= " AND is_increment = 1
			
			// ) - (
			// SELECT IFNULL(SUM(amount),0) as total
			// FROM wal_calendar e
			// LEFT JOIN wal_category c USING(category_id)
			// WHERE 1";
			
			// if (isset($attr['start_date']) && isset($attr['end_date']))
			// {
				// $query .= " AND input_date BETWEEN '".date('Y-m-d',strtotime($attr['start_date']))."' AND '".date('Y-m-d',strtotime($attr['end_date']))."'";
			// }
			
			// if (isset($attr['month']) && isset($attr['year']))
			// {
				// $query.= " AND MONTH(input_date) = '".$attr['month']."' AND YEAR(input_date) = '".$attr['year']."'";
			// }
			
			// if (isset($attr['creator_id']) && isset($attr['creator_id']))
			// {
				// $query.= " AND e.creator_id = '".$attr['creator_id']."'";
			// }
			// $query.= " AND is_increment = 0
		// ) as total_revenue";

		// $result = $this->db->query($query)->row_array();
		// return $result;
	// }
	
	public function get_list($attr = NULL) 
	{
		$query = "
		SELECT *
		FROM wal_calendar e
		WHERE 1
		";
		
		if (isset($attr['is_global_calendar']) && in_array($attr['is_global_calendar'], array('0','1')))
		{
			$query.= ' AND is_global_calendar = ' . $attr['is_global_calendar'];
		}
		
		if (isset($attr['creator_id']) && isset($attr['creator_id']))
		{
			$query.= ' AND creator_id = ' . $attr['creator_id'];
		}
		
		if (isset($attr['start_date']) && isset($attr['end_date']))
		{
			$query.= ' AND input_date BETWEEN ' . replace_quote(date('Y-m-d',strtotime($attr['start_date']))) .' AND ' . replace_quote(date('Y-m-d',strtotime($attr['end_date'])));
		}
		
		if (isset($attr['order']) && isset($attr['order']))
		{
			$query.= ' ORDER BY '.$attr['order'];
		}

		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
		
	// public function cron_email_reminder_expense()
	// {
		// // GET ALL ACTIVE REMINDER, WITH DUEDATE HAS BEEN PASSED ALREADY -> SEND EMAIL BLAST
		// // $query = "
		// // SELECT e.calendar_id, CONCAT(m.first_name,' ',m.last_name) as fullname, m.email, c.name, e.title , e.notes, e.amount, e.reminder_date,
		// // DATE_ADD(e.reminder_date, INTERVAL - e.reminder_minus_day DAY) as email_blast_date,
		// // e.reminder_minus_day, e.is_paid, e.release_date 
		// // FROM wal_calendar e
		// // LEFT JOIN wal_category c USING(category_id)
		// // LEFT JOIN wal_member m ON e.creator_id = m.member_id
		// // WHERE is_get_notif = 1 AND now() >= DATE_ADD(e.reminder_date, INTERVAL - e.reminder_minus_day DAY);
		// // ";
		// $query = "
		// SELECT e.calendar_id, CONCAT(m.first_name,' ',m.last_name) as fullname, m.email, c.name, e.title , e.notes, e.amount, e.reminder_date, e.notes,
		// UNIX_TIMESTAMP(DATE_ADD(e.reminder_date, INTERVAL - e.reminder_minus_day DAY)) as email_blast_date,
		// e.reminder_minus_day, e.is_paid, e.release_date 
		// FROM wal_calendar e
		// LEFT JOIN wal_category c USING(category_id)
		// LEFT JOIN wal_member m ON e.creator_id = m.member_id
		// WHERE is_get_notif = 1 
		// HAVING unix_timestamp() >= email_blast_date AND unix_timestamp() <= email_blast_date+(60*15);
		// ";
		// $result['data'] = $this->db->query($query)->result_array();
		// //debug($query);
		// if (!empty($result['data'])) 
		// {
			// foreach ($result['data'] as $key => $rs) 
			// {
				// //debug($rs);
				// //echo $rs['email_blast_date'].' == '.date('Y-m-d H:i:00').HR;
				// // if ($rs['email_blast_date'] == date('Y-m-d H:i:00')) {
					// // echo "berhasil";
				// // };
				// $email_string = '';
				// $email_string = "
				// <html>
				// <head></head>
				// <body>
				// Dear ".$rs['fullname'].",".BR.BR."
				// Anda mendapat email reminder dari Apps <b>Expense Tracker Grevia</b> tentang tagihan anda <b>".$rs['name']." - ".$rs['title']."</b> sebesar <b>".format_money($rs['amount'])."</b> yang telah jatuh tempo pada ".date('d-m-Y H:i', $rs['email_blast_date']).BR."<b>Notes</b><br/>
				// <div style='padding:10px; border:1px dotted black'>".nl2br($rs['notes']).'</div>'.BR.BR."
				
				// Untuk mematikan notifikasi ini silakan <a href='".base_url()."expense/report?do=disable_notification&calendar_id=".$rs['calendar_id']."'>klik link berikut</a> atau ke <a href='".base_url()."expense/report?do=edit&calendar_id=".$rs['calendar_id']."'>klik link berikut</a> ini untuk mengedit data expense anda.".BR.BR."
				// <b>Tim Wallet Tracker Grevia.</b>".BR."
				// http://wallet.grevia.com
				// </body>
				// </html>
				// ";
				// // EMAIL HERE
				// $subject = "Reminder tagihan Grevia Wallet ".$rs['name']." - ".$rs['title'];
				// $headers  = 'MIME-Version: 1.0' . "\r\n";
				// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				// if (!is_internal()) 
				// {
					// mail($rs['email'] ,$subject , $email_string, $headers);
				// }
				// echo $email_string.HR;
			// }
		// }
		
	// }
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO wal_calendar ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		
		// $query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		// $query.= ', editor_ip = '.replace_quote(getIP());
		// $query.= ', editor_date = '.replace_quote(getDatetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE wal_calendar SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		$query.= ', editor_ip = '.replace_quote(getIP());
		$query.= ', editor_date = '.replace_quote(getDatetime());
		$query.= ' WHERE calendar_id = '. replace_quote($id,'num');
		//debug($query);die;
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM wal_calendar WHERE calendar_id = ' . $id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;	
	}
	
}