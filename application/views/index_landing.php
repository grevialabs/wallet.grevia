<?php 
global $PAGE, $PAGE_TITLE, $BREADCRUMB , $PAGE_HEADER;

if (!isset($PAGE)) $PAGE = DEFAULT_PAGE_TITLE . ' - '.DEFAULT_PAGE_DESCRIPTION;
if (!isset($PAGE_TITLE)) $PAGE_TITLE = $PAGE;
$vrs = '201706221';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="google-site-verification" content="NM1j6BG3n_-7PcXeKwQnS6R-JABiEe2PL7Lm7M_Tbjo" />
	<meta charset="utf-8">
	<title><?php echo $PAGE_TITLE ?></title>
	<link rel="shortcut icon" href="<?php echo base_url()?>favicon.ico?v=1" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url()?>favicon.ico?v=1" type="image/x-icon">
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!-- Latest compiled and minified CSS 
		<link rel="stylesheet" href="http://localhost/ci/assets/css/bootstrap.min.css">
		-->

	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.css"/>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/font-awesome-animation.min.css?v=20160708"/>
	
	<!-- Include required JS files -->
	<link rel='stylesheet' href='<?php echo base_url(); ?>asset/css/ui-lightness/jquery-ui-1.10.4.css' />

	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css"/>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css.map"/>
	<!--<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css"/>-->
	
    <link href="<?php echo base_url()?>asset/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-social.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/style.css?v=<?php echo $vrs; ?>"/>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/animate.css"/>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery-ui-1.10.4.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery.validate.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/ui/jquery.ui.core.js'></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/scrollReveal.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.backstretch.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/lazyload/jquery.lazyload.js?v=1.9.1"></script>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/colorbox.css" />
	<script src="<?php echo base_url()?>asset/js/jquery.colorbox.js"></script>
	<script src="<?php echo base_url()?>asset/js/jquery.spoiler.min.js"></script>
	<script src="<?php echo base_url()?>asset/js/wow.min.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){	
		$(function() {	
			$("img.lazy").lazyload({ effect: "fadeIn" });
			
			$('[data-toggle="popover"]').popover();
			// Enabling some options
			$(".spoiler").spoiler({
				triggerEvents: true,
				paddingValue: 20
			});
		});
		
		 
	});
	new WOW().init();
	</script>
</head>
<?php 
global $fetch_class, $fetch_method;
$fetch_class = $this->router->fetch_class();
$fetch_method = $this->router->fetch_method();
function show_active_menu($menu_title) 
{
	global $fetch_class, $fetch_method;
	$str = '';
	if ($fetch_class == 'modular') 
	{
		if ($fetch_method == $menu_title) 
		{
			$str = 'btn btn-info';
			$str = '<span class="sr-only">(current)</span>';
		}
	}
	return $str;
}

?>
<body>
<?php include_once 'navheader.php';?>
<?php echo $CONTENT?>
<?php include_once 'navfooter.php';?>

</body>
</html>