<!--
<div class="container" style="bottom:0">
	<div class="col-md-12" style="bottom:0">
	<ul class="list-inline topList">
		<li style="border-left:1px solid #000">Test</li>
		<li style="border-left:1px solid #000">AA</li>
		<li style="border-left:1px solid #000">AABB</li>
	</ul>
	</div>
</div>
-->
<!-- Static navbar 549431 -->
    <nav class="navbar navbar-default" style="padding-bottom:10px; margin-bottom:0px; border-bottom:1px solid #DBDBEA; background: #FFFFFF">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            
            <span class="fa fa-bars" style="max-height:10px;padding:none"></span>
          </button>
          <a class="navbar-brand no-u img-responsive" href="<?php echo base_url()?>"><img src="<?php echo base_url().'asset/images/logo_new.png?v=1'?>" height="40px" /></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" style="margin-top:5px">
		  <ul class="nav navbar-nav lnkMenu">
			<li><a href="<?php echo base_url()?>"><i class="fa fa-home" aria-hidden="true"></i> Home <?php echo show_active_menu('index')?></a></li>
			<!--<li><a href="<?php echo base_url()?>forum"><i class="fa fa-comments"></i> Forum</a></li>-->
            <li><a href="<?php echo base_url()?>contact?subject=<?php echo urlencode('Kritik & saran'); ?>">Kontak / Kritik & Saran</a></li>
			<li><a href="<?php echo base_url()?>about">Tentang kami</a></li>
			<!--
			<li>
				
				<form class="navbar-form navbar-left" role="search" action="<?php echo base_url()?>" method="get">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Search" name="q" value="<?php if (is_filled(get('q'))) echo get('q')?>">
					<button type="submit" class="btn btn-default"> <span class="glyphicon glyphicon-search"></span></button>
				</div>
				</form>
			</li>
			-->
			
          </ul>
		  <ul class="nav navbar-nav navbar-right clrWht lnkMenu">
			<?php if (is_member()) { ?>
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i> <?php echo MENU_MY_ACCOUNT?> <span class="caret"></span></a>
			  <ul class="dropdown-menu " role="menu">
                <!--<li><a href="<?php echo base_url()?>member/setting"><i class="fa fa-cog"></i> <?php echo MENU_MY_SETTING?></a></li>-->
                <li><a href="<?php echo base_url()?>expense/report"><i class="fa fa-cog"></i> Report</a></li>
                <li><a href="<?php echo base_url()?>expense/absence"><i class="fa fa-user"></i> Absence</a></li>
                <li><a href="<?php echo base_url()?>logout"><i class="fa fa-sign-out"></i> <?php echo MENU_LOGOUT?></a></li>
              </ul>
			</li>
            <?php } elseif(is_company()) { ?>
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i> <?php echo MENU_MY_ACCOUNT?> <span class="caret"></span></a>
			  <ul class="dropdown-menu " role="menu">
                <li><a href="<?php echo base_url()?>company/billing"><i class="fa fa-money"></i> <?php echo MENU_COMPANY_BILLING?></a></li>
                <li><a href="<?php echo base_url()?>company/setting"><i class="fa fa-cog"></i> <?php echo MENU_MY_SETTING?></a></li>
                <li><a href="<?php echo base_url()?>logout"><i class="fa fa-sign-out"></i> <?php echo MENU_LOGOUT?></a></li>
              </ul>
			</li>
            <?php } else { ?>
			<li><a href="<?php echo base_url().'login'; ?>"><i class="fa fa-user"></i> <?php echo MENU_LOGIN_REGISTER?></a></li>
			<?php } ?>
			
		  </ul>
          <!--
		  <ul class="nav navbar-nav navbar-right">
            <li><a href="../navbar/">Default</a></li>
            <li class="active"><a href="./">Static top <span class="sr-only">(current)</span></a></li>
            <li><a href="../navbar-fixed-top/">Fixed top</a></li>
          </ul>
		  -->
        </div><!--/.nav-collapse -->
      </div>
    </nav>

	<!-- new menu start here -->
	
	<!-- new start here -->

	<!--
	<nav class="navbar navbar-light navbar-inverse bg-light">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">WebSiteName</a>
			</div>
			<ul class="nav navbar-nav">
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Expense
				<span class="caret"></span></a>
				<ul class="dropdown-menu">
				<li class="<?php echo member_active('expense/search').member_active('search')?>"><a class="" href="<?php echo base_url().'expense/search'?>"><i class="fa fa-search fa-fw"></i>&nbsp; Search Expense</a></li>
				<li class="<?php echo member_active('expense/dashboard').member_active('dashboard')?>"><a href="<?php echo base_url().'expense/dashboard'?>"><i class="fa fa-desktop fa-fw"></i>&nbsp; Dashboard report</a></li>
				<li class="<?php echo member_active('expense/reminder').member_active('reminder')?>"><a href="<?php echo base_url().'expense/reminder'?>"><i class="fa fa-money fa-fw"></i>&nbsp; Reminder</a></li>
				<li class="<?php echo member_active('expense/report').member_active('report')?>"><a class="" href="<?php echo base_url().'expense/report'?>"><i class="fa fa-cog fa-fw"></i>&nbsp; Report</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Masterdata
				<span class="caret"></span></a>
				<ul class="dropdown-menu">
				<li class="<?php echo member_active('member/task').member_active('task')?>"><a  href="<?php echo base_url().'member/task'?>"><i class="fa fa-truck fa-fw"></i>&nbsp; Task<sup class="clrRed b">*new</sup></a></li>
				<li class="<?php echo member_active('expense/treasure').member_active('treasure')?>"><a  href="<?php echo base_url().'expense/treasure'?>"><i class="fa fa-dollar fa-fw"></i>&nbsp; Treasure</a></li>
				<li class="<?php echo member_active('expense/category').member_active('category');?>"><a href="<?php echo base_url().'expense/category'?>"><i class="fa fa-folder fa-fw"></i>&nbsp; Category Expense</a></li>
				<li class="<?php echo member_active('expense/fund').member_active('fund')?>"><a href="<?php echo base_url().'expense/fund'?>"><i class="fa fa-money fa-fw"></i>&nbsp; Fund<sup class="clrRed b">*new</sup></a></li>
				<li class="<?php echo member_active('expense/fund_mutation').member_active('fund_mutation')?>"><a  href="<?php echo base_url().'expense/fund_mutation'?>"><i class="fa fa-calculator fa-fw"></i>&nbsp; Fund Mutation<sup class="clrRed b">*new</sup></a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Others
				<span class="caret"></span></a>
				<ul class="dropdown-menu">
				<li class=" <?php echo member_active('expense/calendar').member_active('calendar')?>"><a  href="<?php echo base_url().'expense/calendar'?>"><i class="fa fa-calendar fa-fw"></i>&nbsp; Calendar<sup class="clrRed b">*new</sup></a></li>
				<li class=" <?php echo member_active('expense/absence').member_active('absence')?>"><a href="<?php echo base_url().'expense/absence'?>"><i class="fa fa-user fa-fw"></i>&nbsp; Absence<sup class="clrRed b">*new</sup></a></li>
				<li class="<?php echo member_active('expense/journey').member_active('journey')?>"><a class="<?php echo member_active('expense/journey').member_active('journey')?>" href="<?php echo base_url().'expense/journey'?>"><i class="fa fa-motorcycle fa-fw"></i>&nbsp; Journey<sup class="clrRed b">*new</sup></a></li>
				<li class=""<?php echo member_active('member/profile').member_active('profile')?>"><a class="list-group-item"  href="<?php echo base_url().'member/profile'?>"><i class="fa fa-user fa-fw"></i>&nbsp; Profile</a></li>
				</ul>
			</li>
			</ul>
		</div>
	</nav>
	-->
	<!-- new end here -->

	<!-- new menu end here -->



<script>
$(document).ready(function(){
	$(".form_submit").submit(function(e){
		// call blocker from footer 
		modal_loading_block();
	});
});
</script>