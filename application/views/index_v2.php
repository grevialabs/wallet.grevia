<?php
global $PAGE, $PAGE_TITLE, $BREADCRUMB , $PAGE_HEADER;
 
if (!isset($PAGE)) $PAGE = DEFAULT_PAGE_TITLE . ' - '.DEFAULT_PAGE_DESCRIPTION;
if (!isset($PAGE_TITLE)) $PAGE_TITLE = $PAGE;

$base_url = base_url();
$vrs = '202207171';


global $current_segment;
if ($this->uri->segment(2)) $current_segment = $this->uri->segment(2);


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="google-site-verification" content="NM1j6BG3n_-7PcXeKwQnS6R-JABiEe2PL7Lm7M_Tbjo" />
	<meta charset="utf-8">
	<title><?php echo $PAGE_TITLE ?></title>
	<link rel="shortcut icon" href="<?php echo $base_url?>favicon.ico?v=<?php echo $vrs; ?>" type="image/x-icon">
	<link rel="icon" href="<?php echo $base_url?>favicon.ico?v=201706221" type="image/x-icon">
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!-- Latest compiled and minified CSS 
		<link rel="stylesheet" href="http://localhost/ci/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/bootstrap.css?v=201706221"/>
		-->

	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/bootstrap.3.3.5.min.css?v=20230131"/>
	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/font-awesome-animation.min.css?v=20160708"/>
		

	
	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/floating-left.css?v=20230128"/>
	
	<link rel='stylesheet' href='<?php echo $base_url; ?>asset/css/ui-lightness/jquery-ui-1.10.4.css?v=201706221' />
	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/bootstrap-theme.css?v=201706221"/>
	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/bootstrap-theme.css.map?v=201706221"/>
	
	
	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/bootstrap-social.css?v=201706221"/>
    <link rel="stylesheet" href="<?php echo $base_url?>asset/css/animate.css?v=201706221"/>

    <link rel="stylesheet" href="<?php echo $base_url?>asset/css/style.css?v=<?php echo $vrs; ?>"/>
    <link href="<?php echo $base_url?>asset/css/font-awesome.css?v=201706221" rel="stylesheet">
	
    <link rel="stylesheet" href="<?php echo $base_url?>asset/css/floating-left.css?v=20230128"/>

	<script type="text/javascript" src='<?php echo $base_url; ?>asset/js/jquery.1.11.2.min.js?v=201706221'></script>
	<script type="text/javascript" src='<?php echo $base_url; ?>asset/js/jquery-ui-1.10.4.js?v=201706221'></script>
	<script type="text/javascript" src='<?php echo $base_url; ?>asset/js/jquery.validate.js?v=201706221'></script>
	<script type="text/javascript" src='<?php echo $base_url; ?>asset/js/ui/jquery.ui.core.js?v=201706221'></script>
	<!--
	<script type="text/javascript" src="<?php echo $base_url; ?>asset/js/bootstrap.js?v=201706221"></script>
	-->
	<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'></script>
	<script type="text/javascript" src="<?php echo $base_url; ?>asset/js/scrollReveal.js?v=201706221"></script>
	<script type="text/javascript" src="<?php echo $base_url; ?>asset/js/jquery.backstretch.min.js?v=201706221"></script>
	<!-- <script type="text/javascript" src="<?php echo $base_url; ?>asset/js/lazyload/jquery.lazyload.js?v=1.9.1"></script> -->
	
	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/colorbox.css?v=201706221" />
	<script src="<?php echo $base_url?>asset/js/jquery.colorbox.js?v=201706221"></script>
	<script src="<?php echo $base_url?>asset/js/wow.min.js?v=201706221"></script>
	<script src="<?php echo $base_url?>asset/js/moment.min.js?v=201706221"></script>
	<script src="<?php echo $base_url?>asset/js/jquery.maskMoney.min.js?v=20190214" type="text/javascript"></script>
	
	<style>
.nav-pills > li > a {
   border-radius: 0;
}
#wrapper {
   padding-left: 0;
   -webkit-transition: all 0.3s ease;
   -moz-transition: all 0.3s ease;
   -o-transition: all 0.3s ease;
   transition: all 0.3s ease;
   overflow: hidden;
}
#wrapper.toggled {
   padding-left: 200px;
   overflow: hidden;
}
#sidebar-wrapper {
   z-index: 1000;
   position: fixed;
   left: 250px;
   width: 0;
   height: 100%;
   margin-left: -250px;
   overflow-y: auto;
   background: #000;
   -webkit-transition: all 0.3s ease;
   -moz-transition: all 0.3s ease;
   -o-transition: all 0.3s ease;
   transition: all 0.3s ease;
}
#wrapper.toggled #sidebar-wrapper {
   width: 200px;
}
#page-content-wrapper {
   position: absolute;
   padding: 15px;
   width: 100%;
   overflow-x: hidden;
}
.xyz {
   min-width: 360px;
}
#wrapper.toggled #page-content-wrapper {
   position: relative;
   margin-right: 0px;
}
.fixed-brand {
   width: auto;
}
/* Sidebar Styles */
.sidebar-nav {
   position: absolute;
   top: 0;
   width: 200px;
   margin: 0;
   padding: 0;
   list-style: none;
   margin-top: 2px;
}
.sidebar-nav li {
   text-indent: 15px;
   line-height: 40px;
}
.sidebar-nav li a {
   display: block;
   text-decoration: none;
   color: #999999;
}
.sidebar-nav li a:hover {
   text-decoration: none;
   color: #fff;
   background: rgba(255, 255, 255, 0.2);
   border-left: red 2px solid;
}
.sidebar-nav li a:active,
.sidebar-nav li a:focus {
   text-decoration: none;
}
.sidebar-nav > .sidebar-brand {
   height: 65px;
   font-size: 18px;
   line-height: 60px;
}
.sidebar-nav > .sidebar-brand a {
   color: #999999;
}
.sidebar-nav > .sidebar-brand a:hover {
   color: #fff;
   background: none;
}
.no-margin {
   margin: 0;
}

@media (min-width: 768px) {
   #wrapper {
      padding-left: 550px;
   }
   .fixed-brand {
      width: 250px;
   }
   #wrapper.toggled {
      padding-left: 0;
   }
   #sidebar-wrapper {
      width: 200px;
   }
   #wrapper.toggled #sidebar-wrapper {
      width: 200px;
   }
   #wrapper.toggled #sidebar-wrapper:hover {
      width: 200px;
   }
   #wrapper.toggled-2 #sidebar-wrapper {
      width: 50px;
   }
   #wrapper.toggled-2 #sidebar-wrapper:hover {
      width: 200px;
   }
   #page-content-wrapper {
      padding: 20px;
      position: relative;
      -webkit-transition: all 0.9s ease;
    -moz-transition: all 0.9s ease;
    -o-transition: all 0.9s ease;
    transition: all 0.9s ease;
   }
   #wrapper.toggled #page-content-wrapper {
      position: relative;
      margin-right: 0;
      padding-left: 200px;
   }
   #wrapper.toggled-2 #page-content-wrapper {
      position: relative;
      margin-right: 0;
      margin-left: -250px;
      -webkit-transition: all 0.9s ease;
    -moz-transition: all 0.9s ease;
    -o-transition: all 0.9s ease;
    transition: all 0.9s ease;
      width: auto;
   }
}
@media (min-width: 368px) {
#wrapper {
    padding-left: 200px;
}
.fixed-brand {
    width: 50px;
}
#wrapper.toggled {
    padding-left: 0;
}
#sidebar-wrapper {
    width: 200px;
}
#wrapper.toggled #sidebar-wrapper {
    width: 50px;
}
#wrapper.toggled-2 #sidebar-wrapper {
    width: 50px;
}
#wrapper.toggled-2 #sidebar-wrapper:hover {
    width: 50px;
}
#page-content-wrapper {
    padding: 20px;
    position: relative;
    -webkit-transition: all 0.9s ease;
    -moz-transition: all 0.9s ease;
    -o-transition: all 0.9s ease;
    transition: all 0.9s ease;
}
#wrapper.toggled #page-content-wrapper {
    position: relative;
    margin-right: 0;
    padding-left: 50px;
}
#wrapper.toggled-2 #page-content-wrapper {
    position: relative;
    margin-right: 0;
    margin-left: -200px;
    -webkit-transition: all 0.9s ease;
    -moz-transition: all 0.9s ease;
    -o-transition: all 0.9s ease;
    transition: all 0.9s ease;
    width: auto;
}
}
</style>
	<script type="text/javascript">
	$(document).ready(function(){	
		$(function() {	
			//$.backstretch("http://dl.dropbox.com/u/515046/www/garfield-interior.jpg");
			// $("img.lazy").lazyload({ effect: "fadeIn" });
			
			$('[data-toggle="popover"]').popover();
			// $('.popover').popover(options);
			$( ".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true,
				showAnim: "slideDown",
				yearRange: '1950:+10' 
			});
			
			$(".numeric").keypress(function(e){
				if (e.which != 8 && e.which != 13 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					return false;
				}
			});
			
			
		});
		
	});
	
	window.sr = new scrollReveal();
	new WOW().init();
	
	</script>
	
	<link rel="stylesheet" href="<?php echo base_url().'asset/css/chosen/'?>chosen.min.css?v=201706221">
	<style type="text/css" media="all">
	/* fix rtl for demo */
	.chosen-rtl .chosen-drop { left: -9000px; }
	</style>
	
	
<!-- css start-->
<style>


</style>
<!-- css end -->
</head>
<?php 
global $fetch_class, $fetch_method;
$fetch_class = $this->router->fetch_class();
$fetch_method = $this->router->fetch_method();
function show_active_menu($menu_title) 
{
	global $fetch_class, $fetch_method;
	$str = '';
	if ($fetch_class == 'modular') 
	{
		if ($fetch_method == $menu_title) 
		{
			$str = 'btn btn-info';
			$str = '<span class="sr-only">(current)</span>';
		}
	}
	return $str;
}

?>
<body>
<?php include_once 'navheader.php';?>

<!-- v2 navbar top start -->
<nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
	<div class="">
        
    </div>
    <!-- navbar-header-->
    <!--
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li class="active">
                <button class="navbar-toggle collapse in" data-toggle="collapse" id="menu-toggle-2">
                    <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
					
                </button>
            </li>
        </ul>
    </div>
	-->
    <!-- bs-example-navbar-collapse-1 -->
</nav>
<!-- v2 navbar top end -->

<!--
<div class="container">	
	<div class="row">
		<div class="col-md-12" style="min-height:500px"><br/>
			<?php //if (isset($BREADCRUMB)) echo $BREADCRUMB.'' ?>
			<?php //if (isset($PAGE_HEADER)) echo '<h2>'.$PAGE_HEADER.'</h2>' ?>

			<?php //echo $CONTENT; ?>
		</div>
	</div>
</div>
-->

<div id="wrapper">
	
	<?php include_once 'sidebar_v2.php';?>
	
    <!-- Page Content start -->
    <div id="page-content-wrapper">
        <div class="container-fluid xyz">
            <div class="row" style="min-height:700px">
				<?php echo $CONTENT; ?>
            </div>
			
			<?php include_once 'navfooter.php';?>
        </div>
    </div>
    <!-- Page content end -->
</div>

<?php include_once 'navfooter.php';?>

<script src="<?php echo base_url().'asset/js/'?>chosen.jquery.min.js?v=201706221" type="text/javascript"></script>
<script type="text/javascript">

<!-- float menu start -->
	$("#menu-toggle").click(function(e) {
	   e.preventDefault();
	   $("#wrapper").toggleClass("toggled");
	});
	$("#menu-toggle-2").click(function(e) {
	   e.preventDefault();
	   $("#wrapper").toggleClass("toggled-2");
	   $('#menu ul').hide();
	});
	function initMenu() {
	   $('#menu ul').hide();
	   $('#menu ul').children('.current').parent().show();
	   $('#menu ul:first').show();
	   $('#menu li a').click(
		  function() {
			 var checkElement = $(this).next();
			 if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
				return false;
			 }
			 if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
				$('#menu ul:visible').slideUp('normal');
				checkElement.slideDown('normal');
				return false;
			 }
		  }
	   );
	}
	$(document).ready(function() {
	   initMenu();
	   $("#wrapper").toggleClass("toggled");
	});
<!-- float menu end -->

var config = {
  '.chosen-select'           : {allow_single_deselect:true 
								
								},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}

$('.money').maskMoney({precision:0, thousand:',',allowNegative: false});

function togglebox(){
	if ($('.togglebox').prop('checked')) {
		$('.chkbox').prop('checked',true);
	}
	if (!$('.togglebox').prop('checked')) {
		$('.chkbox').prop('checked',false);
	}
}

function doConfirm(str = 'delete') {
	if (str == 'delete') {
		str = 'Yakin menghapus data ini ?'
	} else 
		str = 'Yakin melakukan aksi ini ?'
	return confirm(str)
}

function resubmit(target, obj){
	var url = obj.value;
	window.location = target + url;
}
</script>
</body>
</html>