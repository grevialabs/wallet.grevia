<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = ABSENCE;
$bread['member'] = ABSENCE;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
$do = $absence_id = NULL;

if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('absence_id')) $absence_id = $this->input->get('absence_id');

function get_hour_minute($duration) {
    $hours = floor($duration / 3600);
    $minutes = floor(($duration / 60) % 60);
    $seconds = $duration % 60;
    if ($hours != 0)
        return "$hours hour $minutes min";
    else
        return "$minutes min $seconds sec";
}

$arr_absent_status = array(
	'0' => 'Masuk normal',
	'-1' => 'Tidak Masuk',
	'1' => 'Izin',
	'2' => 'Cuti sehari',
	'3' => 'Cuti setengah hari',
);

// SAVE HERE
if ($_POST && (isset($_POST['in_date']) || isset($_POST['out_date'])))
{
	$post = array();
	$post = $_POST;
	unset($post['btn_insert']);
	
	$post['creator_id'] = member_cookies('member_id');
		$post['input_date'] = date('Y-m-d');
	if (isset($post['in_date'])) 
	{
		$post['in_date'] = getDatetime();
		$post['creator_date'] = getDatetime();
	}
	if (isset($post['absent_status'])) $post['absent_status'] = $post['absent_status'];
	if (isset($post['notes'])) $post['notes'] = $post['notes'];
	if (isset($post['out_date'])) $post['out_date'] = date('Y-m-d H:i:s');

	$insert = $this->absence_model->save($post);
	
	redirect(base_url().'expense/absence');
	
	($insert)?$message['message'] = MESSAGE::SAVE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
}

// SAVE HERE
if ($_POST && isset($_POST['btn_insert']))
{
	$post = array();
	$post = $_POST;
	unset($post['btn_insert']);
	
	if (isset($post['release_date']) && $post['release_date']!='') $post['release_date'] = date('Y-m-d',strtotime($post['release_date']));
	if (isset($post['reminder_date']) && $post['reminder_date']!='') $post['reminder_date'] = date('Y-m-d',strtotime($post['reminder_date']));
	//debug($post);die;
	$insert = $this->absence_model->save($post);
	
	if ($insert) {
		$this->session->set_flashdata('message', getMessage(MESSAGE::SAVE));
	} else {
		$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
	}
	redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
}

// UPDATE
if ($_POST && isset($_POST['btn_update']))
{
	$post = array();
	$post = $_POST;
	unset($post['btn_update']);
	
	if (isset($post['release_date']) && $post['release_date']!='') $post['release_date'] = date('Y-m-d',strtotime($post['release_date']));
	if (isset($post['reminder_date']) && $post['reminder_date']!='') $post['reminder_date'] = date('Y-m-d',strtotime($post['reminder_date']));
	
	$get['member_id'] = member_cookies('member_id');
	$get['absence_id'] = $absence_id;
	$obj_absence = $this->absence_model->get($get);
	
	if (!empty($obj_absence)) 
	{
		// debug($post);die;
		$update = $this->absence_model->update($absence_id, $post);
		
		if ($update) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	}
	else
	{
		// DATA NOT FOUND
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	}
	$obj_absence = NULL;
	
}

// DELETE ABSENCE
if ($do == "delete" && is_numeric($absence_id)) 
{
	$absence = $this->absence_model->get(array('absence_id' => $absence_id, 'creator_id' => member_cookies('member_id')));
	if (!empty($treasure)) 
	{
		$delete = $this->absence_model->delete($absence_id);
		if ($delete) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::DELETE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	} 
	else 
	{
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	}
}

$start_date = $end_date = NULL;
$param = array();

if ($this->input->get('start_date') && $this->input->get('start_date')) 
{
	$start_date = date('d-m-Y',strtotime($this->input->get('start_date')));
	$end_date = date('d-m-Y',strtotime($this->input->get('end_date')));
}
else
{
	$param['start_date'] = $start_date = date("01-m-Y");
	$param['end_date'] = $end_date = date("t-m-Y");
}
	
$param['creator_id'] = member_cookies('member_id');
if (isset($start_date) && isset($end_date)) 
{
	$param['start_date'] = $start_date;
	$param['end_date'] = $end_date;
}

// Get data member 
$obj_member = $this->member_model->get(array(
	'creator_id' => member_cookies('member_id')
));
// debug($obj_member);
// die;
?>

<div class="col-md-10">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"> </i> DESKRIPSI</b><br/>
		Fitur ini berguna untuk mencatat absen masuk dan absen keluar kantor anda.<br/>
		<b>Note:</b> Kamu hanya bisa melakukan tap IN dan tap OUT <b>satu kali</b> setiap harinya.
	</div><hr/>
	
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>
	<?php if ($do != "insert") { ?>
	<!--
	<a class="btn btn-success btn-sm" href="?do=insert">+ NEW ABSENCE</a><br/><br/>
	-->
	<?php } ?>
		
	<?php 
	// CHECK TODAY EXIST OR NOT
	$obj_absence_today = $this->absence_model->get(array(
		'input_date' => date('Y-m-d'),
		'creator_id' => member_cookies('member_id')
	));
	
	
	if (!empty($obj_absence_today)) 
	{
		$work_duration_estimate = $progress_bar = $normal_workhour = $normal_workhour_timestamp = NULL;
		$progress_bar = $est_work_hour = $est_work_minute = 0;
		$normal_workhour = $obj_member['set_working_minute'] / 60;
		$normal_workhour_timestamp = $normal_workhour*3600; 
		$normal_workhour_in_minute = $normal_workhour_timestamp/60; 
		
		if (!isset($obj_absence_today['out_date'])) 
		{
			$work_duration_estimate = time() - strtotime($obj_absence_today['in_date']);
		}
		else 
		{
			$work_duration_estimate = strtotime($obj_absence_today['out_date']) - strtotime($obj_absence_today['in_date']);
		}
		
		$est_work_in_minute = 60*9;
		
		// IF WORKTIME MORE THAN HOURS
		if ($work_duration_estimate > 3600) 
		{
			$est_work_hour = floor($work_duration_estimate / 3600);
			$est_work_minute = floor(($work_duration_estimate - ($est_work_hour*3600)) /60);
			$est_work_in_minute = $work_duration_estimate/60;
			
			$work_hour_left = $work_minute_left = 0;
			// IF WORKTIME LESS THAN NORMAL WORKHOUR(jam) ; ELSE nol;
			if ($work_duration_estimate <= $normal_workhour_timestamp)
			{
				// MINUS 1 HOUR FOR THE MINUTES
				$work_hour_left = ($normal_workhour - $est_work_hour) - 1;
				$work_minute_left = 60 - $est_work_minute;
			}
		}
		else 
		{
			$est_work_minute = floor(($work_duration_estimate) /60);
			
			$work_hour_left = $normal_workhour - 1;
			$work_minute_left = 60 - $est_work_minute;
		}
		$progress_bar = floor(($work_duration_estimate / $normal_workhour_timestamp) *100);
		
		// debug($progress_bar);
		?>
		<!-- SHOW ABSENCE OUT BUTTON -->
		<div class="">
			<div class="bdrBlk bgSftGry col-sm-12">
				<h2 class="b f24 text-uppercase">TODAY ABSENCE</h2>
				<br/>
				
				<div class="row">
					<div class="col-sm-4 br-md">
						<div class="btn btn-info wdtFul">You already Tap in Today
						<br/><br/><br/>
						<?php echo date('H:i:s',strtotime($obj_absence_today['in_date']))?>
						</div>
					</div>
					<div class="col-sm-4 br-md">
						<?php 
						// NOT YET TAPPED OUT
						if (!isset($obj_absence_today['out_date'])) 
						{
							?>
						<form method="post" class="form_submit">
							<select class="input br wdtFul" name="f_absent_status">
								<?php foreach ($arr_absent_status as $k => $v) { ?>
								<option <?php if ($k < 0) echo "class='clrRed b'";?> value="<?php echo $k ?>"> <?php echo $v; ?></option>
								<?php } ?>
							</select>
							<textarea class="input br wdtFul" name="f_notes" rows="4" placeholder="notes"></textarea><br/>
							<button class="btn btn-success wdtFul" onclick="return confirm('Yakin ingin tap out absen hari ini ?')">
							<input type="hidden" name="f_out_date" value="1" />
							<i class="fa fa-power-off fa-3x" aria-hidden="true"></i><br/><br/> TAP OUT HERE</button>
						</form>
							<?php 
						}
						else 
						{
							?>
							<div class="btn btn-info wdtFul br-md">You already Tap Out Today
								<br/><br/><br/>
								<?php echo date('H:i:s',strtotime($obj_absence_today['out_date']))?>
							</div>
							<?php
						}
						
						$progress_class = 'progress-bar-striped active';
						if (isset($obj_absence_today['in_date']) && isset($obj_absence_today['out_date']))
						{	
							if ($est_work_in_minute >= $normal_workhour_in_minute) 
							{
								$progress_class = 'progress-bar-success';
							}
							else 
							{
								$progress_class = 'progress-bar-danger';
							}
						}
						?>
					</div>
					
					<div class="col-sm-4 br-md">
						<div class="btn btn-default br wdtFul">
							Leaving in<br/>
							<span class="text-success b f30"><?php echo date('H:i',(strtotime($obj_absence_today['in_date']) + $normal_workhour_timestamp)); ?></span>
							<?php if (!isset($obj_absence_today['out_date'])) { ?>
							<?php // echo BR.'<b>'.$work_hour_left .' hour '.$work_minute_left.' minutes</b>.'?>
							<br/>
							<span id="span_worktime" class="b"></span>
							<?php } ?>
						</div>
					</div>
				
					<div class="col-sm-12 br-md">
						<br/>
						<div class="btn btn-default wdtFul br-md">WORK DURATION
							<div class="progress">
							  <div class="progress-bar <?php echo $progress_class?>" role="progressbar" aria-valuenow="<?php echo $progress_bar?>"
							  aria-valuemin="0" aria-valuemax="100" style="">
								<?php echo $progress_bar?>%
							  </div>
							</div>
						</div>
						<div class="b f18 text-uppercase">Total <span class="text-primary"><?php echo get_hour_minute($normal_workhour*3600); ?> waktu kerja standard</span>
						</div>
					</div>
				
				<?php 
				// debug($obj_absence_today);
				// SUMMARY IF TAP IN AND TAP OUT DATA EXIST
				if (isset($obj_absence_today['in_date']) && isset($obj_absence_today['out_date']))
				{
					$summary_absence = $color_absence = $icon_absence = NULL;

					
					// if ($est_work_hour >= $normal_workhour) {
					if ($est_work_in_minute >= $normal_workhour_in_minute) {
						$summary_absence = 'Anda pegawai sejati disiplin mantab. :D';
						$color_absence = 'alert-success';
						$icon_absence = '<i class="fa fa-thumbs-up fa-3x bgGrn" aria-hidden="true"></i>';
					} else {
						$summary_absence = 'Whoops, anda telat hari ini.';
						$color_absence = 'alert-danger';
						$icon_absence = '<i class="fa fa-thumbs-down fa-3x bgGrn" aria-hidden="true"></i>';
					}
					?>
					<div class="col-sm-12">
						<?php if (isset($obj_absence_today['notes'])) { ?>
						<div class="alert alert-info"><b>NOTES</b><br/><?php echo nl2br($obj_absence_today['notes']); ?></div>
						<?php } ?>
						<?php if (isset($obj_absence_today['absent_status'])) { ?>
						<div>Status Absen hari ini: <?php echo absent_status($obj_absence_today['absent_status']); ?></div>
						<?php } ?>
						<br/>
						<div class="talCnt alert <?php echo $color_absence?> wdtFul"><h2 class="b f24">SUMMARY</h2>
							<?php echo $icon_absence.BR.BR?>
							<?php echo $summary_absence ?>
						</div>
						
					</div>
					<?php 
				}
				?>

					<div class="col-sm-12">
						<br/>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<br/>
			</div>
		</div>
		
		<?php
	}
	else 
	{
		?>
		<!-- SHOW INPUT BUTTON -->
		<div class="bdrGry bgSftGry col-sm-12">
			<h2 class="b f24 text-uppercase">TODAY ABSENCE</h2>
			<br/>
			<form method="post" class="form_submit">
				<button class="btn btn-success" onclick="return confirm('Yakin ingin tap in absen hari ini ?')">
				<i class="fa fa-power-off fa-3x" aria-hidden="true"></i><br/>
				<input type="hidden" name="f_in_date" value="1"/>
				TAP IN ABSENCE TODAY</button>
			</form>
			<br/>
		</div>
		<div class="col-sm-12">
			<br/>
		</div>

		<?php
	}
	?>
	<?php 
	$do = $test_id = NULL;
	if ($this->input->get('do')) $do = $this->input->get('do');
	if ($this->input->get('test_id') && is_numeric($this->input->get('test_id'))) $test_id = $this->input->get('test_id');
	

	if ($do == 'insert' || $do == "edit")
	{
		if (isset($absence_id)) $obj = $this->absence_model->get(array(
			'absence_id' => $absence_id,
			'creator_id' => member_cookies('member_id')
		));
		// debug($obj);
		?>
		<!--
		<form method="post">
		<table class='table hover table-bordered'>
			<tr>
				<td colspan="2" class="bg-danger talCnt">New </td>
			</tr>
			<tr>
				<td>Type</td>
				<td>
				<input type="radio" value="1" id="radio_category1" name="f_treasure_type" <?php if (isset($obj['treasure_type']) && $obj['treasure_type'] == 1) echo "checked" ?> /> <label for="radio_category1">Rekening</label> &nbsp;
				<input type="radio" value="2" id="radio_category2" name="f_treasure_type" <?php if (isset($obj['treasure_type']) && $obj['treasure_type'] == 2) echo "checked" ?> /> <label for="radio_category2">Cash</label>
				</td>
			</tr>
			<tr>
				<td width="180px">Name</td>
				<td><input type="text" class="input wdtFul" name="f_name" placeholder="ex: Rekening BCA / Tagihan Internet" value="<?php if (isset($obj['name'])) echo $obj['name']?>" required /></td>
			</tr>
			<tr>
				<td>Amount</td>
				<td><input type="text" class="input numeric" name="f_amount" placeholder="10000" value="<?php if (isset($obj['amount'])) echo $obj['amount']?>" required /></td>
			</tr>
			<tr>
				<td>Notes</td>
				<td><textarea name="f_notes" class="wdtFul" placeholder="catatan" rows="2"><?php if (isset($obj['notes'])) echo $obj['notes']?></textarea></td>
			</tr>
			<tr>
				<td>Reminder Date *opsional</td>
				<td><input type="text" class="input datepicker" placeholder="Tgl pengingat" name="f_reminder_date" value="<?php if (isset($obj['reminder_date'])) echo date('d-m-Y',strtotime($obj['reminder_date']))?>"/> Email reminder will send each day, for every expense not paid</td>
			</tr>
			<tr>
				<td colspan="2" class="talCnt">
				<?php if ($do == "edit") { ?> 
				<button type="submit" name="btn_update" value="1" class="btn btn-success btn-sm">UPDATE</button></td>
				<?php } else { ?> 
				<button type="submit" name="btn_insert" value="1" class="btn btn-success btn-sm">SUBMIT</button></td>
				<?php }?>
			</tr>
		</table>
		</form>
		-->
		

	<?php 
	}
	else 
	{
	?>
		<?php
		$param2 = array();
		$param2['creator_id'] = member_cookies('member_id');
		$param2['order'] = 'input_date DESC';
		// $param2['paging'] = TRUE;
		$param2['start_date'] = date('Y-m-01');
		$param2['end_date'] = date('Y-m-31');
		$list_absence = $this->absence_model->get_list($param2);
		$list_absence = $list_absence['data'];
		
		?>
		
		Total telat bulanan: <span class="b" id="spn_total_telat_bulanan"></span> hari dari total input absen <b><?php echo count($list_absence)?> hari</b>
		<table class="table table-bordered">
			<tr class="bg-success">
				<td>#</td>
				<td>Tgl input</td>
				<td>TapIn</td>
				<td>TapOut</td>
				<td>Status</td>
				<!--
				<td>Lama kerja</td>
				<td>Option</td>
				-->
			</tr>
		<?php 
		$total_late_day = 0;
		if (!empty($list_absence))
		{
			
			$grandtotal = 0;
			foreach ($list_absence as $key => $rs)
			{
				$late_bg = $late_info = $work_duration = NULL;
				if (isset($rs['out_date'])) 
				{
					$work_duration = strtotime($rs['out_date']) - strtotime($rs['in_date']);
					if ($work_duration > 0)
					{
						// check if less than default normal_workhour (default 9 hour) hour then late
						if ( ($work_duration / 3600) < $normal_workhour )
						{
							$late_bg = 'bg-danger';
							$late_info = '<div class="">Nakal ya kurang '.$normal_workhour.' jam nih</div>';
							$total_late_day++;
						}
						$late_info.= '<div class="">You work for <b>'.get_hour_minute($work_duration).'</b></div>';
					}
				}
				?>
			<tr class="<?php echo $late_bg ?>">
				<td><?php echo $key+1?></td>
				<td>
				<?php 
				echo date('D d-m-Y ',strtotime($rs['input_date']));
				echo BR;
				echo $late_info;
				if (isset($rs['creator_date'])) echo "<div style=float:left class='b clrBlu'>Created ".BR.date('D d-m-Y H:i', strtotime($rs['creator_date']))."</div>";
				if (isset($rs['editor_date'])) echo "<div style=float:right class='b clrGrn'>Update ".BR.date('D d-m-Y H:i', strtotime($rs['editor_date']))."</div>";
				?>
				</td>
				<td><?php echo date('H:i', strtotime($rs['in_date'])); ?></td>
				<td><?php if (isset($rs['out_date'])) echo date('H:i', strtotime($rs['out_date'])); ?></td>
				<td><?php echo absent_status($rs['absent_status']); if (isset($rs['notes'])) echo "<hr/><span class='i'>Notes</span><br/>".$rs['notes']; ?></td>
				<!--
				<td><?php // echo $work_duration; ?></td>
				<td class="talCnt">
				<a class="btn btn-success btn-xs br" href="<?php echo current_url().'?do=edit&absence_id='.$rs['absence_id']; ?>"><?php echo UPDATE?></a> 
				<a class="btn btn-danger btn-xs br" href="<?php echo current_url().'?do=delete&absence_id='.$rs['absence_id']?>">Delete</a>
				</td>
				-->
			</tr>
				<?php
			}
			?>
			<tr class="b">
				<td colspan="2">Info</td>
				<td colspan="4"><?php echo "Total telat <b>".$total_late_day."</b> hari";?></td>
			</tr>
			<?php
		}
		else 
		{
			?>
			<tr>
				<td colspan="6">No Data</td>
			</tr>
			<?php 
		}
		?>
		</table>
		<?php
	}
	?>
</div>
<script>
$(document).ready(function(){
	$('#spn_total_telat_bulanan').html('<?php echo $total_late_day?>');
	<?php if (isset($progress_bar) && $progress_bar > 0) { ?>
	$('div[role="progressbar"]').css('width','<?php echo $progress_bar?>%');
	<?php } ?>
	
	<?php if ( ! empty($obj_absence_today)) { ?>
	function countdown() {
		
		var now = new Date();
		var leave_date = new Date('<?php echo date('M d, Y H:i:s',(strtotime($obj_absence_today['in_date']) + $normal_workhour_timestamp + 60)); ?>').getTime();
		
		var distance = leave_date - now;
		var countup = now - leave_date;
		
		// var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		
		// hours = now.getHours();
		// if (hours < 10) hours = '0' + hours;
		
		// minutes = now.getMinutes();
		if (minutes < 10) minutes = '0' + minutes;
		
		// seconds = now.getSeconds();
		if (seconds < 10) seconds = '0' + seconds;

		document.getElementById('span_worktime').innerHTML = hours + 'h ' + minutes + 'm ' + seconds + 's ';
		
		// Pulang naik jadi timer count up ya
		if (distance < 0) {
			// clearInterval(timer);
			// document.getElementById('span_worktime').innerHTML = 'Waktunya pulang';
			// $('#span_worktime').addClass('text-danger text-uppercase');
			
			var countup = now - leave_date;
		
			// var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor((countup % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((countup % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((countup % (1000 * 60)) / 1000);
			
			
			// hours = now.getHours();
			// if (hours < 10) hours = '0' + hours;
			
			// minutes = now.getMinutes();
			if (minutes < 10) minutes = '0' + minutes;
			
			// seconds = now.getSeconds();
			if (seconds < 10) seconds = '0' + seconds;

			$('#span_worktime').addClass('text-danger text-uppercase');
			document.getElementById('span_worktime').innerHTML = 'overtime ' + hours + 'h ' + minutes + 'm ' + seconds + 's ';
			// document.getElementById('span_worktime').innerHTML = countup;
		}
	}
	
	countdown();
	timer = setInterval(countdown, 1000);
	<?php } ?>
	
});

<?php if ( ! empty($obj_absence_today)) { ?>


<?php } ?>
</script>