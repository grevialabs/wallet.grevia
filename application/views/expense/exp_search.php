<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = SEARCH;
$bread['member'] = SEARCH;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;

$start_date = $end_date = $is_get_notif = $get_location_id = $get_category_id = $get_fund_id = NULL;
$param = array();

$do = $expense_id = $q = NULL;
if ($this->input->get('q')) $q = $this->input->get('q');
if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('expense_id') && is_numeric($this->input->get('expense_id'))) $expense_id = $this->input->get('expense_id');
if ($this->input->get('location_id')) $get_location_id = $this->input->get('location_id');
if ($this->input->get('category_id')) $get_category_id = $this->input->get('category_id');
if ($this->input->get('fund_id')) $get_fund_id = $this->input->get('fund_id');

// PAGING
$get = $getkeyword = $getorder_allowed_list = $getorderby_allowed_list = $getorder_list = $getorder = $getorderby = $offset = $page = $perpage = $total_rows = NULL;

$perpage = 20;
$page = 1;
if (isset($_GET)) $get = $_GET;
if (isset($get['page']) && $get['page'] > 1) $page = $get['page'];
if (isset($get['perpage']) && in_array($get['perpage'],$perpage_allowed)) $perpage = $get['perpage'];
	
if ($_GET)
{
	// DELETE EXPENSE
	if ($do == "delete" & is_numeric($expense_id)) 
	{
		$temp['expense_id'] = $expense_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj_delete = $this->expense_model->get($temp);
		
		if (!empty($obj_delete))
		{
			$obj_delete = $this->expense_model->delete($expense_id);
			if ($obj_delete)
			{
				redirect(current_url().'?delete_success=1');
			} else {
				redirect(current_url().'?delete_success=0');
			}
		}
	}
	
	// DISABLE NOTIFICATION
	if ($do == "disable_notification" & is_numeric($expense_id)) 
	{
		$temp['expense_id'] = $expense_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj_disable = $this->expense_model->get($temp);
		
		if (!empty($obj_disable))
		{
			$param_disabled = NULL;
			$param_disabled['is_get_notif'] = 0;
			$obj_disable = $this->expense_model->update($expense_id,$param_disabled);
			if ($obj_disable)
			{
				redirect(current_url().'?disable_success=1');
			} else {
				redirect(current_url().'?disable_success=0');
			}
		}
	}
}

if ($_POST)
{
	$post = $param_insert = $param_update = array();
	$post = $_POST;
	
}

// REPORT 
if ($this->input->get('start_date')) {
	$start_date = date('d-m-Y',strtotime($this->input->get('start_date')));
}

if ($this->input->get('end_date')) {
	$end_date = date('d-m-Y',strtotime($this->input->get('end_date')));
}
else
{
	// $curMnth = date('m')-12;
	$curMnth = 1;
	$curYear = date('Y');
	
	if ($curMnth <= 0) $curMnth+=12;
	if ($curMnth < 10) $curMnth = '0' . $curMnth;
	$param['start_date'] = $start_date = date("01-").$curMnth.'-'.$curYear;
	// get end date
	$param['end_date'] = $end_date = date("t-m").'-'.$curYear;
	
	// // get current date
	// $param['end_date'] = $end_date = date("d-m-Y", time());
}
	
$param['creator_id'] = member_cookies('member_id');
if (isset($start_date)) 
{
	$param['start_date'] = $start_date;
	if (! isset($end_date)) {
		$param['end_date'] = $start_date;
		$end_date = $start_date;
	}
}

if (isset($end_date)) 
{
	$param['end_date'] = $end_date;
	if (! isset($start_date)) {
		$param['start_date'] = $end_date;
		$start_date = $end_date;
	}
}
if (isset($q)) $param['keyword'] = $q;
if (isset($get_category_id)) $param['category_id'] = $get_category_id;
if (isset($get_location_id)) $param['location_id'] = $get_location_id;
if (isset($get_fund_id)) $param['fund_id'] = $get_fund_id;

$param['is_detail'] = 0;
// $param['is_increment'] = 0;
$param['report'] = 'expense';
$param['group_by'] = 'release_date';

// disable hardcode
// $param['order_by'] = 'release_date DESC';
$param['creator_id'] = member_cookies('member_id');

$listdata = $data = NULL;

// order by 
$get_orderkey = $get_orderby = NULL;

if (isset($q) || isset($get_category_id) || isset($get_location_id) || isset($get_fund_id)) 
{
	$get_orderkey = 'release_date';
	$get_orderby = 'desc';
	
	if (isset($_GET['order']) && in_array($_GET['order'],array('release_date','amount')) === TRUE) {
		$get_orderkey = strtolower($_GET['order']);
	}
	
	if (isset($_GET['orderby']) && in_array($_GET['orderby'],array('asc','desc')) === TRUE) {
		$get_orderby = strtolower($_GET['orderby']);
	}
	$param['order_by'] = $get_orderkey.' '.$get_orderby;

	// paging	
	$param['paging'] = TRUE;
	$param['page'] = $page;
	$param['limit'] = $perpage;
	
	$data = $this->expense_model->get_list_report($param);
	
	if (isset($data['data'])) $listdata = $data['data'];
	if (isset($data['total_rows'])) $total_rows = $data['total_rows'];
	
}

function get_url_order($param = NULL) {
	// AUTO REMOVE PARAMETER "PAGE"
	$currentUrlParameter = current_url();
	$i = 1;
	if (!empty($param))
	{
		$_GET['order'] = $param['order'];
		$_GET['orderby'] = $param['orderby'];
	}
	
	if (!empty($_GET))
	{
		// this is making bug zzzz
		// if (isset($_GET['page'])) {
			// unset($_GET['page']);
		// }
		

		foreach($_GET as $key => $val)
		{
			if(is_array($val) == false){
				if ($key == 0 && strpos($currentUrlParameter,'?') === FALSE) $currentUrlParameter.= '?';
				$currentUrlParameter.= $key.'='.$val;
				if (count($_GET) != $i) $currentUrlParameter.= '&';
			}
			$i++;
		}
		
	}
	return $currentUrlParameter;
}
?>

<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>

<!-- input reset input -->
<script src="<?php echo base_url()?>asset/js/resetinput.js?v=170812"></script>
<link href="<?php echo base_url()?>asset/css/resetinput.css?v=170812" rel="stylesheet">

<!-- nanti dulu deh highlighter
<script src="https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.11.1/mark.min.js"></script>

<style>
mark{
    background: orange;
    color: black;
}
-->
</style>

<div class="col-md-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"></i> DESKRIPSI</b><br/>Fitur untuk mencari histori data income / expense berdasarkan nama kategori, nama expense & deskripsi expense
	</div><hr/>

	<?php 
	// if (is_filled($message['message']))echo message($message['message']).BR;
	if (isset($message['message']))echo message($message['message']).BR;
	
	if (isset($_GET['delete_success']))
	{
		$str = NULL;
		if ($_GET['delete_success'] == 1) 
		{
			$str = 'Delete success';
		}
		else if ($_GET['delete_success'] == 0) 
		{
			$str = 'Delete failed';
		}
		echo message($str).BR;
	}
	
	?>
	<!-- table responsive end -->
	<div class="table-responsive">
	<?php 
	
	if ($do == 'showresult' && is_numeric($expense_id)) 
	{
		
	}
	else 
	{
		$list_location = $this->location_model->get_list(array(
			'order' => 'location_name ASC'
		));
		$list_category_expense = $this->category_model->get_list(array(
			'order' => 'name ASC',
			'creator_id' => member_cookies('member_id'),
			// 'paging' => false
		));
		// debug($list_category_expense);
		
		$list_fund = $this->fund_model->get_list(array(
			'order' => 'fund_type ASC, name ASC',
			'creator_id' => member_cookies('member_id'),
		));
		
		// debug($list_fund);
		// die;
		?>
		
		<form method="get">
			
		<table class="table table-bordered table-stripe bg-info">
			<tr>
				<td>Search </td>
				<td><input type="text" class="input br wdtFul" placeholder="<?php echo ENTER_KEYWORD?>" name="q" value="<?php if (isset($_GET['q'])) echo $_GET['q']?>"/></td>
			</tr>
			<tr>
				<td>From</td>		
				<td>
					<input type="text" class="input datepicker br w300" placeholder="start date" name="start_date" id="start_date" value="<?php if (isset($start_date)) echo $start_date; ?>" /> 
					<br/>
					<?php for ($y=date('Y')-2;$y<=date('Y');$y++) { ?>
					<button type="button" onclick="$('#start_date').val('<?php echo '01-01-'.$y?>');" class="btn btn-sm btn-primary"><?php echo $y?></button>
					<?php } ?> <br/>
					
					to <br/>
					<input type="text" class="input datepicker br w300" placeholder="end date" name="end_date" id="end_date" value="<?php if (isset($end_date)) echo $end_date ?>"/> <br/>
					<?php for ($y=date('Y')-2;$y<=date('Y');$y++) { ?>
					<button type="button" onclick="$('#end_date').val('<?php echo '31-12-'.$y?>');" class="btn btn-sm btn-primary"><?php echo $y?></button>
					<?php } ?><br/>
				</td>
			</tr>
			<tr>
				<td>Location</td>
				<td>
				<select name="location_id" id="location_id" class="input br chosen-select w300">
					<option value="">--Select location *Optional</option>
					<?php 
					if (!empty($list_location['data']))
					{
						foreach ($list_location['data'] as $key => $location) 
						{
						?>
						<option value="<?php echo $location['location_id']?>" <?php if (isset($get_location_id) && $get_location_id == $location['location_id']) echo "selected" ?>><?php echo $location['location_name']?></option>
						<?php 
						}
					}
					?>
				</select>
				<button type="button" class="btn btn-default btn-sm" onclick="$('#location_id').val('');$('#location_id').trigger('chosen:updated')">Reset</button>

				</td>
			</tr>
			<tr>
				<td>by Category</td>
				<td>
					<select name="category_id" id="category_id" class="input br chosen-select">
						<option value="">--Select category *Optional</option>
						<optgroup label="Expense">
						<?php 
						if (!empty($list_category_expense['data']))
						{
							foreach ($list_category_expense['data'] as $key => $category) 
							{
								if ($category['is_increment'] == -1)
								{
							?>
							<option value="<?php echo $category['category_id']?>" <?php if (isset($get_category_id) && $get_category_id == $category['category_id']) echo "selected" ?>><?php echo $category['name']?></option>
							<?php
								}
							}
						}
						?>
						</optgroup>
						
						<optgroup label="Income">
							<?php 
							if (!empty($list_category_expense['data']))
							{
								foreach ($list_category_expense['data'] as $key => $category) 
								{
									if ($category['is_increment'] == 1)
									{
								?>
								<option value="<?php echo $category['category_id']?>" <?php if (isset($get_category_id) && $get_category_id == $category['category_id']) echo "selected" ?>><?php echo $category['name']?></option>
								<?php 
									}
								}
							}
							?>
						</optgroup>
					</select>
					<button type="button" class="btn btn-default btn-sm" onclick="$('#category_id').val('');$('#category_id').trigger('chosen:updated')">Reset</button>
				</td>
			</tr>
			<tr>
				<td>by Fund</td>
				<td>
					<select name="fund_id" id="fund_id" class="input br chosen-select">
						<option value="">--Select fund *Optional</option>
						<?php foreach ($list_fund['data'] as $key => $fund) { ?>
						<option value="<?php echo $fund['fund_id']?>" <?php if (isset($get_fund_id) && $get_fund_id == $fund['fund_id']) echo "selected" ?> ><?php echo $fund['fund_name'] ?></option>
						<?php } ?>
					</select>
					<button type="button" class="btn btn-default btn-sm" onclick="$('#fund_id').val('');$('#fund_id').trigger('chosen:updated')">Reset</button>
				</td>
			</tr>
			<tr>
				<td colspan="3">
				<button type="submit" name="" value="" class="btn btn-info btn-block btn-sm w-100" style="">SUBMIT</button>
				</td>
			</tr>
		</table>
		</form><br/>
		<?php 
		$param['is_detail'] = 1;
		$param['is_increment'] = 1;
		
		// START SEARCH QUERY KEYWORD
		if ($_GET) 
		{ 
			if (isset($total_rows))
			{
				?>
			<div class="alert alert-warning text-success"><i class="fa fa-info"></i> Total <b><?php echo $total_rows ?></b> data found.</div>
			<div class="alert alert-info b">Total amount <span id="total_amount"></span></div>
				<?php
			}
			else 
			{
				?><div class="alert alert-warning text-success"><i class="fa fa-info"></i> Data not found</div><?php
			}
		}
		// END QUERY SEARCH KEYWORD
		
		if (!empty($listdata)) 
		{
			
			
			$_GET['order'] = $get_orderkey;
			$_GET['orderby'] = $get_orderby;
			
			
		?>
		<div class="talRgt">
			Sort by 
			<!--
			<select name="">
				<option value="release_date">ReleaseDate</option>
				<option value="amount">Amount</option>
			</select>
			
			<select>
				<option value="asc">ASC</option>
				<option value="desc">DESC</option>
			</select>
			-->ReleaseDate
			<a href="<?php echo get_url_order(array('order' => 'release_date', 'orderby' => 'asc')); ?>" class="btn btn-xs btn-<?php echo ($get_orderkey == 'release_date' && $get_orderby == 'asc') ? 'primary disabled' : 'default'; ?>"> Asc</a> &nbsp;
			<a href="<?php echo get_url_order(array('order' => 'release_date', 'orderby' => 'desc')); ?>" class="btn btn-xs btn-<?php echo ($get_orderkey == 'release_date' && $get_orderby == 'desc') ? 'primary disabled' : 'default'; ?>"> Desc</a>
			or 
			Amount
			<a href="<?php echo get_url_order(array('order' => 'amount', 'orderby' => 'asc')); ?>" class="btn btn-xs btn-<?php echo ($get_orderkey == 'amount' && $get_orderby == 'asc') ? 'primary disabled' : 'default'; ?>"> Asc</a> &nbsp;
			<a href="<?php echo get_url_order(array('order' => 'amount', 'orderby' => 'desc')); ?>" class="btn btn-xs btn-<?php echo ($get_orderkey == 'amount' && $get_orderby == 'desc') ? 'primary disabled' : 'default'; ?>"> Desc</a>
		</div><br/>
		<table class='table hover table-bordered table-content'>
			<tr class="table-info">
				<th colspan="4" class="talCnt">DETAIL EXPENSE REPORT</th>
			</tr>
			<tr class="table-info">
				<th>#</th>
				<th>Day</th>
				<th>Date</th>
				<th>Expense</th>
			</tr>
			<?php 
			$i = 1;
			if ($page > 1) $i = $perpage*($page-1) + $i;
			$total = 0;
			$total_expense_day = array();
			foreach ($listdata as $key => $rs) 
			{ 
			//$id = $rs['expense_id'];
			?>
			<tr>
			<td><?php echo $i?></td>
			<td><?php echo date('d',strtotime($rs['release_date']))?></td>
			<td><?php echo date('D d-m-Y', strtotime($rs['release_date']))?><br/>
			<?php 
			$list_expense_day = array();
			if (isset($q)) $tmp1['keyword'] = $q;
			if (isset($get_category_id)) $tmp1['category_id'] = $get_category_id;
			if (isset($get_location_id)) $tmp1['location_id'] = $get_location_id;
			$tmp1['release_date'] = $rs['release_date'];
			// $tmp1['is_increment'] = 0;
			$tmp1['is_detail'] = 1;			
			
			$tmp1['creator_id'] = member_cookies('member_id');
			$list_expense_day = $this->expense_model->get_list_report($tmp1);
			$list_expense_day = $list_expense_day['data'];
			// debug($list_expense_day);
			if (!empty($list_expense_day)) 
			{
				?>
				<table class="table table-bordered table-hover wdtFul" align="right">
				<?php
				foreach($list_expense_day as $day) 
				{
					$daytitle = $daycat_name = NULL;
					$daytitle = str_replace($q,'<span class="b bgRed clrWht">&nbsp;'.$q.' </span>',$day['title']);
					$daycat_name = str_replace($q,'<span class="b bgRed clrWht">&nbsp;'.$q.' </span>',$day['category_name']);
				?>
				<tr class="bg-warning">
				<td class="">
				<?php 
				$expense_type = ' <span class="text-danger b">Expense</span>';
				if ($day['is_increment'] == 1) $expense_type = ' <span class="text-primary b">Income</span>';
				?>
				<a href="<?php echo base_url().'expense/report?do=edit&expense_id='.$day['expense_id'];?>" title="Edit data"><i class="fa fa-edit fa-lg"></i></a> 
				<?php echo $expense_type.' '.get_fund_type($day['fund_type']).' '.$day['fund_name']; ?>
				<br/>
				<?php echo "<span class='b text-uppercase'>".$daycat_name."</span> - ".$daytitle?>
				<?php 
				// PRINT STATUS AND DATE REMINDER HERE
				if (isset($day['is_get_notif']) && $day['is_get_notif'] == 1) { 
					//echo " jalan";die;
					
					// 15 minutes for cron
					$disable_reminder = NULL;
					if (time() >= strtotime($day['reminder_date']) && time() < (strtotime($day['reminder_date']) + 15*60)) 
					{
						$disable_reminder = " <a href='?do=disable_notification&expense_id=".$day['expense_id']."' class='btn btn-danger btn-xs' title='Nonaktifkan reminder ini'> Disable notif</a>";
						echo BR.BR."<i class='clrGrn b'>Reminder active</i>";
						if (isset($day['reminder_date'])) echo " on ".date('D d-m-Y H:i',$day['email_blast_date']);
					}
					elseif (time() >= strtotime($day['reminder_date'])) 
					{
						$disable_reminder = "";
						echo BR.BR."<i class='clrRed b'>Reminder expired</i>";
						if (isset($day['reminder_date'])) echo " on ".date('D d-m-Y H:i',$day['email_blast_date']);
					}
					// elseif ()
					// {
						// echo BR.BR."<i class='clrRed b'>Reminder expired</i>";
					// }
					
					//if (isset($day['reminder_date'])) echo " on ".date('D d-m-Y H:i',strtotime($day['reminder_date']));
					
					echo $disable_reminder;
				}
				// if ($day['is_paid'] == 1) echo " <i class='fa fa-money clrGrn' title='Tagihan sudah dibayar'></i>";
				// else echo " <i class='fa fa-money clrRed' title='Tagihan belum dibayar'></i>";
				//debug($day);//die;
				if (isset($day['location_name'])) echo BR." <b class='clrRed b'><i class='fa fa-car'></i> ".$day['location_name']."</b>";
				if (isset($day['expense_notes'])) echo BR." <i class='fa fa-book'></i> <b class='clrRed b'> ".$day['expense_notes']."</b>".BR;
				
				//if (isset()) 
					//debug($rs);die;
				
				
				?>
				</td>
				<td width="50px" class="talRgt"><?php echo format_money($day['amount'],'')?></td>
				<!--
				<td width="20px">
				<a href="<?php echo current_url().'?do=delete&expense_id='.$day['expense_id'];?>" title="Delete data"><i class="fa fa-times-circle fa-lg clrRed"></i></a>
				</td>
				-->
				</tr>
				<?php 
				}
				?>
				</table>
			<?php 
			}
			?>
			</td>
			<td><?php echo format_money(str_replace('.','',$rs['amount']))?></td>
			</tr>
			<?php 
				// FILL KEY ARRAY WITH DATE FOR REPORT USE
				if (left(date('d',strtotime($rs['release_date'])),1) == 0) {
					// STRIP 0 FROM 01 to 09
					$total_expense_day[right(date('d',strtotime($rs['release_date'])),1)] = str_replace('.','',$rs['amount']);
				} else {
					// NORMAL DATE > 10 like 10,11,20
					$total_expense_day[date('d',strtotime($rs['release_date']))] = str_replace('.','',$rs['amount']);
				}
				
				$i++;
				$total+= str_replace('.','',$rs['amount']);
			}

			?>
			<tr>
			<td colspan="3">TOTAL</td>
			<td><?php echo format_money($total) ?></td>
			</tr>
		</table>
		
		<?php if ( ! empty($listdata)) echo $this->common_model->common_paging($total_rows, $perpage); ?>

		<br/>

		<?php 
		}
	}
	?>
	</div>
	<!-- table responsive end -->
</div>
<script>
$(document).ready(function(){
	// run reset button plugin
	resetinput();
	
	<?php if (!isset($is_get_notif) || $is_get_notif != 1) { ?>
	$("#table_input tr.reminder").hide();
	$("#table_input tr.reminder :input").attr('disabled','disabled');
	<?php } ?>
	
	<?php 
	if (isset($total)) {
	?>
	$('#total_amount').text('<?php echo format_money($total) ?>');
	<?php	
	}
	?>
	
	$("#is_reminder1").click(function(){
		if ($(this).is(":checked")) { 
			$("#table_input tr.reminder").show();
			$("#table_input tr.reminder :input").removeAttr('disabled');
			$('input[name=f_is_get_notif]').prop('checked',true);
		}
	});
	$("#is_reminder0").click(function(){
		if ($(this).is(":checked")) { 
			$("#table_input tr.reminder").hide();
			$("#table_input tr.reminder :input").attr('disabled','disabled');
			$('input[name=f_is_get_notif]').prop('checked',false);
		}
	});
	
	// nanti dulu deh
	// $(".table-content").mark("checkout");
})
</script>