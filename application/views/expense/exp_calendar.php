<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = CALENDAR;
$bread['member'] = CALENDAR;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;

$start_date = $end_date = $is_get_notif = $get_location_id = $get_category_id = NULL;
$asset_url = base_url().'asset/';
$base_url = base_url();

$rpl = 'https://';
$rplp = 'https://';
$rplw = '//';

// $rpl = 'http://localhost/';
// $rplp = 'http://localhost/';
// $rplw = '//';
// $rplp = '/http:\/\/localhost\/i';
// if (strpos($asset_url,$rpl) !== FALSE ) $asset_url = preg_replace($rpl,'//',$asset_url);
if (strpos($asset_url,$rpl) !== FALSE ) $asset_url = str_replace($rplp,$rplw,$asset_url);

$param = array();

$do = $expense_id = $q = NULL;
if ($this->input->get('q')) $q = $this->input->get('q');
if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('expense_id') && is_numeric($this->input->get('expense_id'))) $expense_id = $this->input->get('expense_id');
if ($this->input->get('calendar_id')) $get_calendar_id = $this->input->get('calendar_id');
if ($this->input->get('category_id')) $get_category_id = $this->input->get('category_id');
	
if ($_GET)
{
	// DELETE EXPENSE
	if ($do == "delete" & is_numeric($expense_id)) 
	{
		$temp['expense_id'] = $expense_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj_delete = $this->expense_model->get($temp);
		
		if (!empty($obj_delete))
		{
			$obj_delete = $this->expense_model->delete($expense_id);
			if ($obj_delete)
			{
				redirect(current_url().'?delete_success=1');
			} else {
				redirect(current_url().'?delete_success=0');
			}
		}
	}
}

// $_POST = array(
			// 'do' => 'save',
			// 'input_date' => date('Y-m-d'),
			// 'notes' => 'tes pertama',
			// 'creator_id' => '1',
			// 'creator_date' => date('Y-m-d H:i:s'),
		// );
// SAVE VIA AJAX
if ($_POST)
{
	$post = $_POST;
	if (isset($post['do']) && $post['do'] == "save")
	{
		$save = $param_save = NULL;
		$param_save = array(
			'input_date' => date('Y-m-d',strtotime($post['input_date'])),
			'start_time' => $post['start_time'],
			'end_time' => $post['end_time'],
			'bg_color' => $post['bg_color'],
			'notes' => $post['notes'],
			'is_global_calendar' => 0,
			'creator_id' => member_cookies('member_id'),
			'creator_ip' => getIP(),
			'creator_date' => getDateTime()
		);
		$save = $this->calendar_model->save($param_save);
		
		if ($save) 
			echo "success";
		else
			echo "failed";
		die;
	}
}

// for database calendar indonesia
$list_calendar_global = NULL;
$list_calendar_global = $this->calendar_model->get_list(array(
	'is_global_calendar' => 1
));
$list_calendar_global = $list_calendar_global['data'];
// debug($list_calendar_global);

// get list data
$list_calendar = NULL;
$list_calendar = $this->calendar_model->get_list(array(
	'creator_id' => member_cookies('member_id')
));
$list_calendar = $list_calendar['data'];

$list_absence = NULL;
$list_absence = $this->absence_model->get_list(array(
	'creator_id' => member_cookies('member_id')
));
$list_absence = $list_absence['data'];
// debug($list_absence);
// die;

// get list reminder
$param_reminder['group_by'] = NULL;
$param_reminder['is_budget'] = 0;
$param_reminder['is_detail'] = 1;
$param_reminder['creator_id'] = member_cookies('member_id');
$param_reminder['active_month_reminder'] = 1;
$list_incoming_reminder = $this->expense_model->get_list_report($param_reminder);
$list_incoming_reminder = $list_incoming_reminder['data'];
// debug($list_incoming_reminder);
// die;
?>
<style>
.fc {
	direction: ltr;
	text-align: left;
	}
	
.fc table {
	border-collapse: collapse;
	border-spacing: 0;
	}
	
html .fc,
.fc table {
	font-size: 1em;
	}
	
.fc td,
.fc th {
	padding: 0;
	vertical-align: top;
	}



/* Header
------------------------------------------------------------------------*/

.fc-header td {
	white-space: nowrap;
	}

.fc-header-left {
	width: 25%;
	text-align: left;
	}
	
.fc-header-center {
	text-align: center;
	}
	
.fc-header-right {
	width: 25%;
	text-align: right;
	}
	
.fc-header-title {
	display: inline-block;
	vertical-align: top;
	}
	
.fc-header-title h2 {
	margin-top: 0;
	white-space: nowrap;
	}
	
.fc .fc-header-space {
	padding-left: 10px;
	}
	
.fc-header .fc-button {
	margin-bottom: 1em;
	vertical-align: top;
	}
	
/* buttons edges butting together */

.fc-header .fc-button {
	margin-right: -1px;
	}
	
.fc-header .fc-corner-right,  /* non-theme */
.fc-header .ui-corner-right { /* theme */
	margin-right: 0; /* back to normal */
	}
	
/* button layering (for border precedence) */
	
.fc-header .fc-state-hover,
.fc-header .ui-state-hover {
	z-index: 2;
	}
	
.fc-header .fc-state-down {
	z-index: 3;
	}

.fc-header .fc-state-active,
.fc-header .ui-state-active {
	z-index: 4;
	}
	
	
	
/* Content
------------------------------------------------------------------------*/
	
.fc-content {
	clear: both;
	zoom: 1; /* for IE7, gives accurate coordinates for [un]freezeContentHeight */
	}
	
.fc-view {
	width: 100%;
	overflow: hidden;
	}
	
	

/* Cell Styles
------------------------------------------------------------------------*/

.fc-widget-header,    /* <th>, usually */
.fc-widget-content {  /* <td>, usually */
	border: 1px solid #ddd;
	}
	
.fc-state-highlight { /* <td> today cell */ /* TODO: add .fc-today to <th> */
	background: #fcfcfc;
	}
	
.fc-cell-overlay { /* semi-transparent rectangle while dragging */
	background: #bcccbc;
	opacity: .3;
	filter: alpha(opacity=30); /* for IE */
	}
	


/* Buttons
------------------------------------------------------------------------*/

.fc-button {
	position: relative;
	display: inline-block;
	padding: 0 .6em;
	overflow: hidden;
	height: 1.9em;
	line-height: 1.9em;
	white-space: nowrap;
	cursor: pointer;
}

.fc-event-inner {
	cursor: pointer;
}


.fc-text-arrow {
	margin: 0 .1em;
	font-size: 2em;
	font-family: "Courier New", Courier, monospace;
	vertical-align: baseline; /* for IE7 */
	}


	
/*
  button states
  borrowed from twitter bootstrap (http://twitter.github.com/bootstrap/)
*/

.fc-state-default {
	background-color: #f5f5f5;
	}

.fc-state-hover,
.fc-state-down,
.fc-state-active,
.fc-state-disabled {
	color: #333333;
	background-color: #e6e6e6;
	}

.fc-state-hover {
	color: #333333;
	text-decoration: none;
	background-position: 0 -15px;
	-webkit-transition: background-position 0.1s linear;
	   -moz-transition: background-position 0.1s linear;
	     -o-transition: background-position 0.1s linear;
	        transition: background-position 0.1s linear;
	}

.fc-state-down,
.fc-state-active {
	background-color: #cccccc;
	background-image: none;
	outline: 0;
	box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
	}

.fc-state-disabled {
	cursor: default;
	background-image: none;
	opacity: 0.55;
	filter: alpha(opacity=65);
	box-shadow: none;
	}

	

/* Global Event Styles
------------------------------------------------------------------------*/

.fc-event-container > * {
	z-index: 8;
	}

.fc-event-container > .ui-draggable-dragging,
.fc-event-container > .ui-resizable-resizing {
	z-index: 9;
	}
	 
.fc-event {
	border: 1px solid #3a87ad; /* default BORDER color */
	background-color: #3a87ad; /* default BACKGROUND color */
	color: #fff;               /* default TEXT color */
	font-size: .85em;
	cursor: default;
	}

a.fc-event {
	text-decoration: none;
	}
	
a.fc-event,
.fc-event-draggable {
	cursor: pointer;
	}
	
.fc-rtl .fc-event {
	text-align: right;
	}

.fc-event-inner {
	width: 100%;
	height: 100%;
	overflow: hidden;
	}
	
.fc-event-time,
.fc-event-title {
	padding: 0 1px;
	}
	
.fc .ui-resizable-handle {
	display: block;
	position: absolute;
	z-index: 99999;
	overflow: hidden; /* hacky spaces (IE6/7) */
	font-size: 300%;  /* */
	line-height: 50%; /* */
	}
	
	
	
/* Horizontal Events
------------------------------------------------------------------------*/

.fc-event-hori {
	border-width: 1px 0;
	margin-bottom: 1px;
	}

.fc-ltr .fc-event-hori.fc-event-start,
.fc-rtl .fc-event-hori.fc-event-end {
	border-left-width: 1px;
	}

.fc-ltr .fc-event-hori.fc-event-end,
.fc-rtl .fc-event-hori.fc-event-start {
	border-right-width: 1px;
	}
	
/* resizable */
	
.fc-event-hori .ui-resizable-e {
	top: 0           !important; /* importants override pre jquery ui 1.7 styles */
	right: -3px      !important;
	width: 7px       !important;
	height: 100%     !important;
	cursor: e-resize;
	}
	
.fc-event-hori .ui-resizable-w {
	top: 0           !important;
	left: -3px       !important;
	width: 7px       !important;
	height: 100%     !important;
	cursor: w-resize;
	}
	
.fc-event-hori .ui-resizable-handle {
	_padding-bottom: 14px; /* IE6 had 0 height */
	}
	

table.fc-border-separate {
	border-collapse: separate;
	}
	
.fc-border-separate th,
.fc-border-separate td {
	border-width: 1px 0 0 1px;
	}
	
.fc-border-separate th.fc-last,
.fc-border-separate td.fc-last {
	border-right-width: 1px;
	}
	
.fc-border-separate tr.fc-last th,
.fc-border-separate tr.fc-last td {
	border-bottom-width: 1px;
	}
	
.fc-border-separate tbody tr.fc-first td,
.fc-border-separate tbody tr.fc-first th {
	border-top-width: 0;
	}
	
	

/* Month View, Basic Week View, Basic Day View
------------------------------------------------------------------------*/

.fc-grid th {
	text-align: center;
	}

.fc .fc-week-number {
	width: 22px;
	text-align: center;
	}

.fc .fc-week-number div {
	padding: 0 2px;
	}
	
.fc-grid .fc-day-number {
	float: right;
	padding: 0 2px;
	}
	
.fc-grid .fc-other-month .fc-day-number {
	opacity: 0.3;
	filter: alpha(opacity=30); /* for IE */
	/* opacity with small font can sometimes look too faded
	   might want to set the 'color' property instead
	   making day-numbers bold also fixes the problem */
	}
	
.fc-grid .fc-day-content {
	clear: both;
	padding: 2px 2px 1px; /* distance between events and day edges */
	}
	
/* event styles */
	
.fc-grid .fc-event-time {
	font-weight: bold;
	}
	
/* right-to-left */
	
.fc-rtl .fc-grid .fc-day-number {
	float: left;
	}
	
.fc-rtl .fc-grid .fc-event-time {
	float: right;
	}
	
	

/* Agenda Week View, Agenda Day View
------------------------------------------------------------------------*/

.fc-agenda table {
	border-collapse: separate;
	}
	
.fc-agenda-days th {
	text-align: center;
	}
	
.fc-agenda .fc-agenda-axis {
	width: 50px;
	padding: 0 4px;
	vertical-align: middle;
	text-align: right;
	white-space: nowrap;
	font-weight: normal;
	}

.fc-agenda .fc-week-number {
	font-weight: bold;
	}
	
.fc-agenda .fc-day-content {
	padding: 2px 2px 1px;
	}
	
/* make axis border take precedence */
	
.fc-agenda-days .fc-agenda-axis {
	border-right-width: 1px;
	}
	
.fc-agenda-days .fc-col0 {
	border-left-width: 0;
	}
	
/* all-day area */
	
.fc-agenda-allday th {
	border-width: 0 1px;
	}
	
.fc-agenda-allday .fc-day-content {
	min-height: 34px; /* TODO: doesnt work well in quirksmode */
	_height: 34px;
	}
.fc-day {cursor: pointer}
	
/* divider (between all-day and slots) */
	
.fc-agenda-divider-inner {
	height: 2px;
	overflow: hidden;
	}
	
.fc-widget-header .fc-agenda-divider-inner {
	background: #eee;
	}
	
/* slot rows */
	
.fc-agenda-slots th {
	border-width: 1px 1px 0;
	}
	
.fc-agenda-slots td {
	border-width: 1px 0 0;
	background: none;
	}
	
.fc-agenda-slots td div {
	height: 20px;
	}
	
.fc-agenda-slots tr.fc-slot0 th,
.fc-agenda-slots tr.fc-slot0 td {
	border-top-width: 0;
	}

.fc-agenda-slots tr.fc-minor th,
.fc-agenda-slots tr.fc-minor td {
	border-top-style: dotted;
	}
	
.fc-agenda-slots tr.fc-minor th.ui-widget-header {
	*border-top-style: solid; /* doesn't work with background in IE6/7 */
	}
	


/* Vertical Events
------------------------------------------------------------------------*/

.fc-event-vert {
	border-width: 0 1px;
	}

.fc-event-vert.fc-event-start {
	border-top-width: 1px;
	}

.fc-event-vert.fc-event-end {
	border-bottom-width: 1px;
	}
	
.fc-event-vert .fc-event-time {
	white-space: nowrap;
	font-size: 10px;
	}

.fc-event-vert .fc-event-inner {
	position: relative;
	z-index: 2;
	}
	
.fc-event-vert .fc-event-bg { /* makes the event lighter w/ a semi-transparent overlay  */
	position: absolute;
	z-index: 1;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #fff;
	opacity: .25;
	filter: alpha(opacity=25);
	}
	
.fc .ui-draggable-dragging .fc-event-bg, /* TODO: something nicer like .fc-opacity */
.fc-select-helper .fc-event-bg {
	display: none\9; /* for IE6/7/8. nested opacity filters while dragging don't work */
	}
	
/* resizable */
	
.fc-event-vert .ui-resizable-s {
	bottom: 0        !important; /* importants override pre jquery ui 1.7 styles */
	width: 100%      !important;
	height: 8px      !important;
	overflow: hidden !important;
	line-height: 8px !important;
	font-size: 11px  !important;
	font-family: monospace;
	text-align: center;
	cursor: s-resize;
	}
	
</style>

<script>
$.getScript('<?php echo $asset_url?>js/fullcalendar.1.6.4.min.js',function(){
  
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();
  
  $('#calendar').fullCalendar({
	header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
	firstDay: 1,
	weekNumbers: true,
    editable: false,
    events: [
	  /*
	  {
        title: 'All Day Event',
        start: new Date(y, m, 1)
      },
	  {
        title: 'Berak',
        start: new Date(2016, 7, 28, 13, 20),
        end: new Date(2016, 7, 28, 18, 50),
		allDay: false
      },
      {
        title: 'Long Event',
        start: new Date(y, m, d-5),
        end: new Date(y, m, d-2)
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: new Date(y, m, d-3, 16, 0),
        allDay: false
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: new Date(y, m, d+4, 16, 0),
        allDay: false
      },
      {
        title: 'Meeting',
        start: new Date(y, m, d, 10, 30),
        allDay: false
      },
	  {
        title: 'Meeting',
        start: new Date(y, m, d, 10, 30),
        allDay: false
      },
	  {
        title: 'Meeting',
        start: new Date(y, m, d, 10, 30),
        allDay: false
      },
	  {
        title: 'Meeting',
        start: new Date(y, m, d, 10, 30),
        allDay: false
      },
	  {
        title: 'Meeting',
        start: new Date(y, m, d, 10, 30),
        allDay: false
      },
	  {
        title: 'Meeting',
        start: new Date(y, m, d, 10, 30),
        allDay: false
      },
	  {
        title: 'Meeting',
        start: new Date(y, m, d, 10, 30),
        allDay: false
      },
      {
        title: 'Lunch',
        start: new Date(y, m, d, 12, 0),
        end: new Date(y, m, d, 14, 0),
        allDay: false
      },
	  
      {
        title: 'Birthday Party',
        start: new Date(y, m, d+1, 19, 0),
        end: new Date(y, m, d+1, 22, 30),
        allDay: false
      },
      {
        title: 'Click for Google',
        start: new Date(y, m, 28),
        end: new Date(y, m, 29),
        url: 'http://google.com/',
		backgroundColor:'red',
      },
	  */
	  
	  
	  <?php 
	  $str = NULL;
	  
	  // CALENDAR GLOBAL (NOT USED BECAUSE DATA PROCESSED BY JQUERY BELOW
	  // if (!empty($list_calendar_global))
	  // {
		// foreach ($list_calendar_global as $key => $rs)
		// {
		  // $y = $m = $d = $start_time = $end_time = NULL;
		  // $y = date('Y',strtotime($rs['input_date']));
		  // $m = date('m',strtotime($rs['input_date']))-1;
		  // $d = date('d',strtotime($rs['input_date']));
		  
		  // if (isset($rs['start_time'])) $start_time .= ', '.date('H',strtotime($rs['start_time'])).', 00';
		  // if (isset($rs['end_time'])) $end_time = ', '.date('H',strtotime($rs['end_time'])).', 00';
		  
		  // if (($key) != 0) $str.= "\t \t";
		  // if (($key) != 0) $str.= "\r\n";
		  // $str.= "{\r\n";
		  // $str.= "\t\t title: '".$rs['notes']."',\r\n";
		  // $str.= "\t\t start: new Date(".$y.", ".$m.", ".$d.$start_time."),\r\n";
		  
		  // if (isset($rs['end_time'])) 
			// $str.= "\t\t end: new Date(".$y.", ".$m.", ".$d.$end_time."),\r\n";
		  
		  // // $str.= "\t\t url: '".$rs['notes']."', \r\n";
		  
		  // if (isset($rs['bg_color'])) 
			// $str.= "\t\t backgroundColor:'".$rs['bg_color']."', \r\n";
		  
		  // $str.= "\t\t allDay: false, \r\n";
		  // // $str.= "\t\t timeFormat: 'H(:mm)', \r\n";
		  // $str.= "\t \t }";
		  // if (($key + 1) != count($list_calendar)) $str.= ',';
		// }
		
		// // echo $str;
	  // }
	  
	  // CALENDAR LIST INCOMING REMINDER
	  if (!empty($list_incoming_reminder))
	  {
		foreach ($list_incoming_reminder as $key => $rs)
		{
		  $y = $m = $d = $start_time = $end_time = NULL;
		  $y = date('Y',strtotime($rs['reminder_date']));
		  $m = date('m',strtotime($rs['reminder_date']))-1;
		  $d = date('d',strtotime($rs['reminder_date']));
		  
		  // if (isset($rs['start_time'])) 
		  $start_time .= ', '.date('H',strtotime($rs['reminder_date'])).', 00';
		  
		  // if (isset($rs['end_time'])) $end_time = ', '.date('H',strtotime($rs['end_time'])).', 00';
		  
		  if (($key) != 0) $str.= "\t \t";
		  if (($key) != 0) $str.= "\r\n";
		  $str.= "{\r\n";
		  $str.= "\t\t title: 'REMINDER EXPENSE - ".$rs['category_name']." - ".$rs['title']."',\r\n";
		  $str.= "\t\t start: new Date(".$y.", ".$m.", ".$d.$start_time."),\r\n";
		  
		  if (isset($rs['end_time'])) 
			$str.= "\t\t end: new Date(".$y.", ".$m.", ".$d.$end_time."),\r\n";
		  
		  // $str.= "\t\t url: '".$rs['notes']."', \r\n";
		  
		  $str.= "\t\t backgroundColor:'#00b217', \r\n";
		  
		  $str.= "\t\t allDay: false, \r\n";
		  // $str.= "\t\t timeFormat: 'H(:mm)', \r\n";
		  $str.= "\t \t }";
		  if (($key + 1) != count($list_incoming_reminder)) $str.= ',';
		}
		$str.= ",";
		// echo $str;
	  }
	  
	  // show list calendar user
	  if (!empty($list_calendar))
	  {
		foreach ($list_calendar as $key => $rs)
		{
		  $y = $m = $d = $start_time = $end_time = NULL;
		  $y = date('Y',strtotime($rs['input_date']));
		  $m = date('m',strtotime($rs['input_date']))-1;
		  $d = date('d',strtotime($rs['input_date']));
		  
		  if (isset($rs['start_time'])) $start_time .= ', '.date('H',strtotime($rs['start_time'])).', 00';
		  if (isset($rs['end_time'])) $end_time = ', '.date('H',strtotime($rs['end_time'])).', 00';
		  
		  if (($key) != 0) $str.= "\t \t";
		  if (($key) != 0) $str.= "\r\n";
		  $str.= "{\r\n";
		  $str.= "\t\t title: '".preg_replace('/\s\s+/', ' ',$rs['notes'])."',\r\n";
		  $str.= "\t\t start: new Date(".$y.", ".$m.", ".$d.$start_time."),\r\n";
		  
		  if (isset($rs['end_time'])) $str.= "\t\t end: new Date(".$y.", ".$m.", ".$d.$end_time."),\r\n";
		  
		  // $str.= "\t\t url: '".$rs['notes']."', \r\n";
		  
		  if (isset($rs['bg_color'])) 
			$str.= "\t\t backgroundColor:'".$rs['bg_color']."', \r\n";
		  
		  $str.= "\t\t allDay: false, \r\n";
		  // $str.= "\t\t timeFormat: 'H(:mm)', \r\n";
		  $str.= "\t \t }";
		  if (($key + 1) != count($list_calendar)) $str.= ',';
		}
		
	  }
	  
	  // show list absence calendar user
	  if (!empty($list_absence))
	  {
		$str.= ",";
		foreach ($list_absence as $key => $rs)
		{
		  $y = $m = $d = $start_time = $end_time = NULL;
		  $y = date('Y',strtotime($rs['input_date']));
		  $m = date('m',strtotime($rs['input_date']))-1;
		  $d = date('d',strtotime($rs['input_date']));
		  
		  if (isset($rs['start_time'])) $start_time .= ', '.date('H',strtotime($rs['start_time'])).', 00';
		  if (isset($rs['end_time'])) $end_time = ', '.date('H',strtotime($rs['end_time'])).', 00';
		  
		  if (($key) != 0) $str.= "\t \t";
		  if (($key) != 0) $str.= "\r\n";
		  $str.= "{\r\n";
		  
		  $title = null;
		  $title.= "\t\t title: 'Absen kantor ".preg_replace('/\s\s+/', ' ',str_replace('<br/>','\r\n',$rs['notes']))." \\n";
		  
		  if (isset($rs['in_date'])) $title.= "Masuk: ".date('H:i',strtotime($rs['in_date']))." \\n ";
		  if (isset($rs['out_date'])) $title.= "Keluar: ".date('H:i',strtotime($rs['out_date']))." \\n";
		  
		  if (isset($rs['absent_status'])) $title.= "Absen: ".absent_status($rs['absent_status'],FALSE);
		  
		  $title.= "',";
		  
		  // $str.= preg_replace('/\s\s+/', ' ',$title);
		  // $str.= preg_replace('/\s\s+/', ' ',$title);
		  $str.= preg_replace('/\s{2,}+/', ' ',$title);
		  // $str.= $title;
		  
		  $str.= "\t\t start: new Date(".$y.", ".$m.", ".$d.$start_time."),\r\n";
		  
		  // if (isset($rs['end_time'])) $str.= "\t\t end: new Date(".$y.", ".$m.", ".$d.$end_time."),\r\n";  
		  
		  if (isset($rs['bg_color'])) 
			$str.= "\t\t backgroundColor:'".$rs['bg_color']."', \r\n";
		  
		  $str.= "\t\t allDay: false, \r\n";
		  // $str.= "\t\t timeFormat: 'H(:mm)', \r\n";
		  $str.= "\t \t }";
		  if (($key + 1) != count($list_absence)) $str.= ',';
		}
		
	  }
	  echo $str;
	  ?>
    ],
	timeFormat: 'H:mm',
	// dayClick: function(date, jsEvent, view) {

        // // alert('Clicked on: ' + date.format());

        // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

        // alert('Current view: ' + view.name);

        // // change the day's background color just for fun
        // $(this).css('background-color', 'red');

    // },
  });
  
})
</script>
<style>
.fc-past {background-color:#ededed }
.fc-today {background-color:#96e3ff !important}
.fc-sat {background-color:#ffeded;}
.fc-sun {background-color:#f7cdcd}
.bgRed {background-color:red}
</style>
<script src="<?php echo $asset_url?>js/jquery.simplecolorpicker.js"></script>

<link rel="stylesheet" href="<?php echo $asset_url?>css/jquery.simplecolorpicker.css">
<link rel="stylesheet" href="<?php echo $asset_url?>css/jquery.simplecolorpicker-regularfont.css">
<link rel="stylesheet" href="<?php echo $asset_url?>css/jquery.simplecolorpicker-glyphicons.css">
<link rel="stylesheet" href="<?php echo $asset_url?>css/jquery.simplecolorpicker-fontawesome.css">

<div class="col-md-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"></i> DESKRIPSI</b><br/>Fitur untuk mengatur jadwal kalendar sehari hari.
	</div><hr/>

	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	
	if (isset($_GET['delete_success']))
	{
		$str = NULL;
		if ($_GET['delete_success'] == 1) 
		{
			$str = 'Delete success';
		}
		else if ($_GET['delete_success'] == 0) 
		{
			$str = 'Delete failed';
		}
		echo message($str).BR;
	}
	
	?>
	<div id="loading-response"></div>
	<div id="calendar"></div>
	<!--
	<br/>
	<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_calendar">Open Modal</button>
	-->
</div>

<!-- Trigger the modal with a button -->
<style>
.wdtHlf{width:49.5%}
</style>

<!-- Start Modal calendar -->
<div id="modal_calendar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Calendar</h4>
      </div>
      <div class="modal-body" id="loading_popup">
        <p>Input data calendar.</p><br/>
		<input type="text" placeholder="input_date" class="input br wdtFul datepicker" name="input_date" id="f_input_date" />
		
		<select placeholder="start time" class="input br datepicker wdtHlf" name="start_time">
		<?php 
		for ($i=0;$i<=24;$i++)
		{
		?>
			<option value="<?php echo $i ?>:00:00"><?php echo $i; ?>.00 WIB</option>
		<?php 
		}
		?>
		</select>
		
		<select placeholder="end time" class="input br datepicker wdtHlf" name="end_time">
		<?php 
		for ($i=0;$i<=24;$i++)
		{
		?>
			<option value="<?php echo $i ?>:00:00"><?php echo $i; ?>.00 WIB</option>
		<?php 
		}
		?>
		</select>
		
		Pilih background color<br/>
		<select id="colorpicker-longlist" name="bg_color">
			<option value="#000">#000</option>
			<option value="#ac725e">#ac725e</option>
			<option value="#d06b64">#d06b64</option>
			<option value="#f83a22">#f83a22</option>
			<option value="#fa573c">#fa573c</option>
			<option value="#ff7537">#ff7537</option>
			<option value="#ffad46">#ffad46</option>
			<option value="#42d692">#42d692</option>
			<option value="#16a765">#16a765</option>
			<option value="#7bd148">#7bd148</option>
			<option value="#b3dc6c">#b3dc6c</option>
			<option value="#fbe983">#fbe983</option>
			<option value="#fad165">#fad165</option>
			<option value="#92e1c0">#92e1c0</option>
			<option value="#9fe1e7">#9fe1e7</option>
			<option value="#9fc6e7">#9fc6e7</option>
			<option value="#4986e7">#4986e7</option>
			<option value="#9a9cff">#9a9cff</option>
			<option value="#b99aff">#b99aff</option>
			<option value="#c2c2c2">#c2c2c2</option>
			<option value="#cabdbf">#cabdbf</option>
			<option value="#cca6ac">#cca6ac</option>
			<option value="#f691b2">#f691b2</option>
			<option value="#cd74e6">#cd74e6</option>
			<option value="#a47ae2">#a47ae2</option>
		</select>
		
		<textarea placeholder="Notes" class="input br wdtFul" rows="4" name="notes" ></textarea><br/>
		<div class="talCnt">
			<button type="button" class="btn btn-primary " id="btn_submit_calendar">Simpan data</button>
		</div>
      </div>
    </div>

  </div>
</div>
<!-- End Modal calendar -->

<!-- Start Edit Modal calendar -->
<div id="modal_edit_calendar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Calendar</h4>
      </div>
      <div class="modal-body" id="loading_popup">
        <p>Edit data calendar.</p><br/>
		<input type="text" placeholder="input_date" class="input br wdtFul" name="edit_input_date" id="f_edit_input_date" />
		
		<select placeholder="start time" class="input br datepicker wdtHlf" name="edit_start_time">
		<?php 
		for ($i=0;$i<=24;$i++)
		{
		?>
			<option value="<?php echo $i ?>:00:00"><?php echo $i; ?>.00 WIB</option>
		<?php 
		}
		?>
		</select>
		
		<select placeholder="end time" class="input br datepicker wdtHlf" name="edit_end_time">
		<?php 
		for ($i=0;$i<=24;$i++)
		{
		?>
			<option value="<?php echo $i ?>:00:00"><?php echo $i; ?>.00 WIB</option>
		<?php 
		}
		?>
		</select>
		
		Pilih background color<br/>
		<select id="colorpicker-longlist" name="edit_bg_color">
			<option value="#000">#000</option>
			<option value="#ac725e">#ac725e</option>
			<option value="#d06b64">#d06b64</option>
			<option value="#f83a22">#f83a22</option>
			<option value="#fa573c">#fa573c</option>
			<option value="#ff7537">#ff7537</option>
			<option value="#ffad46">#ffad46</option>
			<option value="#42d692">#42d692</option>
			<option value="#16a765">#16a765</option>
			<option value="#7bd148">#7bd148</option>
			<option value="#b3dc6c">#b3dc6c</option>
			<option value="#fbe983">#fbe983</option>
			<option value="#fad165">#fad165</option>
			<option value="#92e1c0">#92e1c0</option>
			<option value="#9fe1e7">#9fe1e7</option>
			<option value="#9fc6e7">#9fc6e7</option>
			<option value="#4986e7">#4986e7</option>
			<option value="#9a9cff">#9a9cff</option>
			<option value="#b99aff">#b99aff</option>
			<option value="#c2c2c2">#c2c2c2</option>
			<option value="#cabdbf">#cabdbf</option>
			<option value="#cca6ac">#cca6ac</option>
			<option value="#f691b2">#f691b2</option>
			<option value="#cd74e6">#cd74e6</option>
			<option value="#a47ae2">#a47ae2</option>
		</select>
		
		<textarea placeholder="Notes" class="input br wdtFul" rows="4" name="edit_notes" ></textarea><br/>
		<input type="text" name="hdn_calendar_id" placeholder="" />
		<div class="row">
			<div class="col-sm-6">
				<button type="button" class="btn btn-danger " id="btn_delete_calendar">Hapus data</button>
			</div>
			<div class="col-sm-6">
				<div class="talRgt"><button type="button" class="btn btn-primary " id="btn_update_calendar">Update data</button></div>
			</div>
		</div>
      </div>
    </div>

  </div>
</div>
<!-- End Edit Modal calendar -->

<script>

// $(document).ready(function(){
$(window).on("load", function() {
	$('select[name="bg_color"]').simplecolorpicker();
	
	$('select[name="bg_color"]').change(function(){
		$('textarea[name="notes"]').css('background', $('select[name="bg_color"]').val());
	});
	
	$('select[name="start_time"]').change(function(){
		starttimeindex = $('select[name="start_time"]')[0].selectedIndex;
		$('select[name="end_time"] option').eq(starttimeindex).prop('selected',true);
		// $('select[name="end_time"]').val("'"+starttimeindex+"'");
		// alert();
		// $('select[name="start_time"]')[0].selectedIndex
	})
	
	

	function load_event()
	{
		setTimeout(function(){
			// add title when hover
			$('.fc-day').each(function(){
				$(this).attr('title','click for add data calendar here');
			});
			
			// show title on data exist when hover
			$('.fc-event-inner').each(function(){
				$(this).attr('title','Click here for show detail data');
			});
			
			// show popup
			$('.fc-day').click(function(){
				
				var input_date, notes, content, start_time, end_time, is_global, bg_color;
					
				input_date = $(this).attr('data-date');
				input_date = input_date.split('-');
				input_date = input_date[2]+"-"+input_date[1]+"-"+input_date[0];
				$('#f_input_date').val(input_date);
				$('#modal_calendar').modal('show');
				
				$('#btn_submit_calendar').click(function(){
					
					notes = $('textarea[name="notes"]').val();
					bg_color = $('select[name="bg_color"]').val();
					start_time = $('select[name="start_time"]').val();
					end_time = $('select[name="end_time"]').val();
					
					is_valid = 0;
					// alert(notes);
					if (notes.length <= 0 || notes == 'undefined')
					{
						alert('notes harus diisi');
						$('textarea[name="notes"]').focus();
					}
					else 
					{
						is_valid = 1; 
						// alert(notes);
						// alert(bg_color);
						// alert(start_time + ' '+ end_time);
					}
					
					if (is_valid)
					{
						$('#btn_submit_calendar').attr('disabled',true);
						$('#loading_popup').html('<div class="talCnt"><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i></div>');
						$.ajax({
							type: "POST",
							url: "<?php echo $base_url?>expense/calendar",
							data: "apitoken=<?php echo rand(100000000,9999999999);?>&do=save&input_date="+ input_date +"&bg_color="+bg_color+"&start_time="+start_time+"&end_time="+end_time+"&notes=" + notes +"&creator_id=<?php echo member_cookies('member_id')?>",
							cache: false,
							success: function(resp) {

								$('#loading-response').html('<div class="alert alert-info talCnt"><i class="fa fa-cog fa-spin fa-lg fa-fw"></i></div>');
								// $('#merchant_inquiry_form').modal('show');
								setTimeout(function(){
									if (resp == "success") {
										$('#loading_popup').html('<div class="alert alert-info talCnt">Data telah tersimpan</div>');
										$('#loading-response').html('<div class="alert alert-info talCnt">Data telah tersimpan</div>');
										
										// var redir = "<?php echo $base_url.'thankyou_register?email='?>" + encodeURIComponent(the_email);
										// content = $("#calendar").load( "<?php echo base_url()?>belajar/calendar #calendar" );
										// $('#loading-response').html(content);
										setTimeout(function(){
											// window.location.replace('<?php echo current_url()?>');
											location.reload();
										}, 2000)
										
									} else if (resp == "failed") {
										// alert('respon gagal');
										$('#loading-response').html('<div class="alert alert-info talCnt">Mohon maaf, data gagal disimpan.</div>');
										$('#btn_submit_calendar').attr('disabled',false);
									} else {
										$('#loading-response').html('<div class="alert alert-info talCnt">Mohon maaf, terjadi error</div>');
										$('#btn_submit_calendar').attr('disabled',false);
									}
									
								}, 2000);
							}
						});
					}
				})
			});
			
			// show when data exist and clicked
			$('.fc-event-inner').click(function(){
				// alert('jalan');
				time = $(this).find('.fc-event-time').html();
				title = $(this).find('.fc-event-title').html();
				prompt('ni ',time + ' - '+title);
				// $('#modal_edit_calendar').modal('show');
				
			});
			
			// append today date with notes
			$('.fc-today').find('.fc-day-number').append(' - <b>TODAY</b> <i class="fa fa-calendar"></i><br/>');
			
			// hari raya global
			<?php 
			// hari raya global
			if (!empty($list_calendar_global)) 
			{
				foreach ($list_calendar_global as $cg)
				{
					?>
			// $('td[data-date="2016-09-12"]').addClass('bgRed clrWht');
			var globalday = null;
			globalday = $('td[data-date="<?php echo date('Y-m-d',strtotime($cg['input_date']))?>"]');
			globalday.addClass('bgRed clrWht');
			// append date with notes
			globalday.find('.fc-day-number').append(' - HOLIDAY <i class="fa fa-calendar"></i><br/><?php echo $cg['notes']?><br/>');
				<?php 
				}
			}
			?>
			
			
		}, 500);
	};
	
	load_event();
	
	setTimeout(function(){
		$('.fc-text-arrow, .fc-state-default').click(function(){
			load_event();
		})
		// $('.fc-text-arrow').each(function(index){
			// $(this).on("click", function(){
				// alert('jalan');
				// // load_event();
			// });
		// });
	},500);
})
</script>