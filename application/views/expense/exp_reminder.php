<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = REMINDER;
$bread['member'] = REMINDER;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;

$param = array();

$do = $category_id = NULL;
if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('category_id') && is_numeric($this->input->get('category_id'))) $category_id = $this->input->get('category_id');

// POST HERE
if ($_POST)
{
	$post = $param_insert = $param_update = array();
	$post = $_POST;
	
	// SAVE
	if (isset($_POST['hdn_insert']))
	{
		$param_insert['is_increment'] = 1;
		// if (!isset($post['is_increment']) || $post['is_increment'] == 0) $param_insert['is_increment'] = 0;
		
		if (isset($post['is_increment']) && in_array($post['is_increment'],array(-1,0,1))) 
			$param_insert['is_increment'] = $post['is_increment'];
		
		$param_insert['is_monthly_billing'] = 1;
		if (!isset($post['is_monthly_billing']) || $post['is_monthly_billing'] == 0) $param_insert['is_monthly_billing'] = 0;
		
		$param_insert['creator_id'] = member_cookies('member_id');	
		$param_insert['name'] = $post['name'];
		$param_insert['due_date'] = $post['due_date'];
		$param_insert['monthly_budget_amount'] = $post['monthly_budget_amount'];
		$param_insert['notes'] = $post['notes'];
		
		if (isset($post['name']) && $post['name'] != '')
		{
			$insert = $this->category_model->save($param_insert);
			
			if ($insert) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::SAVE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		else 
		{
			$message['message'] = 'Nama harus diisi';
		}
	}
	
	// UPDATE
	if (isset($_POST['hdn_update']) && is_numeric($category_id))
	{
		$temp = NULL;
		$temp['category_id'] = $category_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj = $this->category_model->get($temp);
		
		if ($obj)
		{
			$param_update['is_increment'] = 1;
			
			if (isset($post['is_increment']) && in_array($post['is_increment'],array(-1,0,1))) 
			$param_update['is_increment'] = $post['is_increment'];
			
			$param_update['is_monthly_billing'] = 1;
			if (!isset($post['is_monthly_billing']) || $post['is_monthly_billing'] == 0) $param_update['is_monthly_billing'] = 0;
			
			$param_update['name'] = $post['name'];
			$param_update['due_date'] = $post['due_date'];
			$param_update['monthly_budget_amount'] = $post['monthly_budget_amount'];
			$param_update['notes'] = $post['notes'];
			
			$update = $this->category_model->update($category_id, $param_update);
			
			if ($update) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		else 
		{
			// NOT FOUND
			$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
	}
}

// DELETE
if ($_GET)
{
	if ($do == "delete" && is_numeric($category_id))
	{
		$temp = $param = NULL;
		$temp['category_id'] = $category_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj = $this->category_model->get($temp);
		
		if (!empty($obj))
		{
			$param['is_delete'] = -1;
			$delete = $this->category_model->update($category_id,$param);
			if ($delete) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::DELETE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		else
		{
			// NOT FOUND
			$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
	}
}


?>

<div class="col-md-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"></i> DESKRIPSI</b><br/>Data Kategori pengeluaran yang akan muncul saat mengisi <i>expense tracker</i>
	</div><hr/>
	
	<?php 
	if (isset($message['message']))echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>
		
	<?php 
	$do = $test_id = NULL;
	if ($this->input->get('do')) $do = $this->input->get('do');
	if ($this->input->get('test_id') && is_numeric($this->input->get('test_id'))) $test_id = $this->input->get('test_id');

	?>
	
	<?php 
	if ($do == "insert" || $do == "edit")
	{
		$obj = NULL;
		if ($do == "edit" && is_numeric($category_id))
		{
			$temp['category_id'] = $category_id;
			$temp['creator_id'] = member_cookies('member_id');
			$obj = $this->category_model->get($temp);
		}
		
		if ($do == "insert" || ($do == "edit" && !empty($obj)))
		{
	?>
	<form method="post">
	<table class='table hover table-bordered'>
		<tr>
			<td colspan="2" class="bg-success talCnt">CATEGORY</td>
		</tr>
		<tr>
			<td width="180px">Nama Kategori Pengeluaran</td>
			<td><input type="text" class="input wdtFul" name="f_name" placeholder="Nama kategori pengeluaran" value="<?php if (isset($obj['name'])) echo $obj['name']?>" required /></td>
		</tr>
		<tr>
			<td><label for="f_is_increment">Tipe</label></td>
			<td>
			<input type="radio" name="f_is_increment" value="1" id="tambah" <?php if (isset($obj['is_increment']) && $obj['is_increment']==1) echo 'checked="true"'; ?> required /> <label for="tambah" class="clrGrn b">Income / Pemasukan</label><br/>
			<input type="radio" name="f_is_increment" value="0" id="topup" <?php if (isset($obj['is_increment']) && $obj['is_increment']==0) echo 'checked="true"'; ?> required /> <label for="topup" class="clrBlk b">Topup Saldo</label><br/>
			<input type="radio" name="f_is_increment" value="-1" id="kurang" <?php if (isset($obj['is_increment']) && $obj['is_increment']==-1) echo 'checked="true"'; ?> required /> <label for="kurang" class="clrRed b">Expense / Pengeluaran</label>
			
			<!--
			<input type="checkbox" name="f_is_increment" id="f_is_increment" value="1" <?php if (isset($obj['is_increment']) && $obj['is_increment']==1) echo 'checked="true"'; ?>/><br/>
			
			*centang jika category ini berupa PEMASUKAN(penambahan), dan abaikan jika berupa EXPENSE(pengurangan) 
			-->
			</td>
		</tr>
		</tr>
			<td>
				<label for="f_is_monthly_billing" style="cursor:pointer">Is monthly billing</label>
			</td>
			<td >
			<input type="checkbox" name="f_is_monthly_billing" id="f_is_monthly_billing" value="1" <?php if (isset($obj['is_monthly_billing']) && $obj['is_monthly_billing']==1) echo 'checked="true"'; ?>/><br/>(jika dicentang, maka kategori ini akan muncul menjadi reminder di halaman dashboard expense)
			</td>
		</tr>
		<tr>
			<td>
				Tgl jatuh tempo *<b>optional</b>
			</td>
			<td>
				<input class="input wdtFul numeric" maxlength="2" name="f_due_date" placeholder="Tgl Jatuh tempo tiap bulan" value="<?php if (isset($obj['due_date'])) echo $obj['due_date']?>" />
			</td>
		</tr>
		<tr>
			<td>
				Budget bulanan *<b>optional</b>
			</td>
			<td>
				<input class="input wdtFul numeric" maxlength="8" name="f_monthly_budget_amount" placeholder="Budget bulanan" value="<?php if (isset($obj['monthly_budget_amount'])) echo $obj['monthly_budget_amount']?>" />
			</td>
		</tr>
		<tr>
			<td>
				Notes *<b>optional</b>
			</td>
			<td>
				<textarea class="input wdtFul" name="f_notes" placeholder="Notes"><?php if (isset($obj['notes'])) echo $obj['notes']?></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="talCnt">
			<?php if ($do == "insert") { ?>
			<input type="hidden" name="hdn_insert" value="1" />
			<button class="btn btn-success btn-sm">SAVE</button>
			<?php } else { ?>
			<input type="hidden" name="hdn_update" value="1" />
			<button class="btn btn-success btn-sm">UPDATE</button>
			<?php } ?>
			
			</td>
		</tr>
	</table>
	</form>
	<br/>
	<?php 
		}
		elseif($do == "edit" && empty($obj))
		{
			echo message(getMessage(MESSAGE::NOT_FOUND));
		}
	}
	?>
	
	<?php
	$list_monthly_billing_category = $param_monthly = array();
	$param_monthly['is_delete'] = 0;
	
	// pass month and year
	$param_monthly['month'] = 0;
	$param_monthly['year'] = 0;
	
	$param_monthly['creator_id'] = member_cookies('member_id');
	$param_monthly['order'] = "total_transaction_count DESC, category_name ASC";
	$list_monthly_billing_category = $this->category_model->get_list_monthly_billing_category($param_monthly);
	$list_monthly_billing_category = $list_monthly_billing_category['data'];
	if ( ! empty($list_monthly_billing_category))
	{
	?>

	<a href=""></a>
	<table class="table table-bordered">
		<tr>
			<td colspan="5" class="bg-primary talCnt">List Active Monthly Billing<br/>menampilkan seluruh category expense bulanan yang aktif</td>
		</tr>
		<tr class="bg-success">
			<td width="1">#</td>
			<td>Nama kategori</td>
			<td>Tgl Due Date</td>
			<td>Total trx<br/>bulan ini</td>
			<td>Payment</td>
		</tr>
		<?php
			// debug($list_monthly_billing_category);
			foreach($list_monthly_billing_category as $key => $rsb)
			{
				$key++;

				$status_paid = $trcolor = NULL;
				if (isset($rsb['due_date']) && $rsb['due_date'] > 0)
				{
					//if (date('d') >= $rsb['due_date'] || $rsb['total_transaction_count'] >= 1) {
					if ($rsb['total_transaction_count'] >= 1) {
						$status_paid = '<i class="fa fa-check text-success"></i>';
					} else if (date('d') >= $rsb['due_date']) {
						// $trcolor = 'bgRed clrWht lnkWht';
						$trcolor = 'bg-danger';
						$status_paid = '<i class="fa fa-remove text-danger"></i>';
						$status_paid.= ' <span class="clrRed">Passed '. ceil(date('d') - $rsb['due_date']).' day</span>';
					} else {
						$trcolor = 'bg-default';
						$status_paid = '<i class="fa fa-remove text-primary"></i>';
						$status_paid.= ' <span class="text-warning">will expired in -'. ceil($rsb['due_date'] - date('d')).' days</span>';
					}
				}
		?>
		<tr class="<?php echo $trcolor; ?>">
			<td><?php echo $key; ?></td>
			<td><?php echo $rsb['category_name']?><?php if (isset($rsb['notes'])) echo BR.'<div class="text-info b">Notes:'.BR.nl2br($rsb['notes'].'</div>'); ?></td>
			<td><?php if (isset($rsb['due_date'])) echo $rsb['due_date']; else echo '-'; ?></td>
			<td><?php echo $rsb['total_transaction_count']?></td>
			<td><?php echo $status_paid.BR.format_money($rsb['total_transaction_amount']); ?></td>
		</tr>
		<?php
			}
		?>
	</table>
	<?php
	}
	?>
	
</div>