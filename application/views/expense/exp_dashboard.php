<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = DASHBOARD;
$bread['member'] = DASHBOARD;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
$do = $journey_id = $gYear = NULL;

if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('journey_id')) $journey_id = $this->input->get('journey_id');
if ($this->input->get('year')) $gYear = $this->input->get('year');

if (! isset($gYear)) $gYear = date('Y');

$list_report = NULL;

$select_report = NULL;
if (isset($_GET['select_report'])) $select_report = $_GET['select_report'];
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script>
  $( function() {
    $( "#tabs" ).tabs({
      beforeLoad: function( event, ui ) {
        ui.jqXHR.fail(function() {
          ui.panel.html(
            "Couldn't load this tab. We'll try to fix this as soon as possible. " +
            "If this wouldn't be a demo." );
        });
      }
    });
  } );
  </script>

<div class="col-md-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"> </i> DESKRIPSI</b><br/>
		Fitur ini mencatat hasil rekapan setiap bulan dari expense kamu ya.
	</div><hr/>
	
	<!--
	<div id="tabs">
	  <ul>
	  	<li class="pointer"><a href="#tabs-0">Choose report</a></li>
		<li><a href="tab-fund-type">Fund Type</a></li>
		<li><a href="tab-chartbar">Year chart</a></li>
		
	  </ul>

	  <div id="tabs-0">
			Choose your menu
	  </div>

	</div>
	<br/>
	-->

	<?php 
	?>
	<form method="get">
		Pilih Tahun: <select name="year" class="input" onchange="this.form.submit()">
			<?php 
			$selYear = NULL;
			for($i=2010;$i<=date('Y');$i++) { 
				$selYear[] = $i;
			}
			$selYear = array_reverse($selYear);
			
			foreach ($selYear as $yr) {
			?>
			<option value="<?php echo $yr; ?>" <?php if ($gYear == $yr) echo "selected"; ?>><?php echo $yr?></option>
			<?php } ?>
		</select>

		<?php 
		$list_report = array(
		'chartbar',
		'income_expense',
		// 'parent_category',
		'budget'
		);
		?>
		Report
		<select name="select_report" id="income_expense" class="input" onchange="this.form.submit()">
			<option value="">-- Choose Yearly Report --</option>
			<?php foreach ($list_report as $k => $dt) { ?>
			<option value="<?php echo $dt?>">by <?php echo str_replace('_',' ',$dt)?></option>
			<?php } ?>
		</select>
		<input type="submit" value="submit" class="btn btn-info" />
	</form>
	<br/>
	
	<?php 
	if (isset($message['message']) && $message['message'] != '') echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;

	if (! isset($select_report)) {

	} 
	else 
	{

		switch ($select_report)
		{
			case 'income_expense':
	
	?>
	<form method="post">
		
		<table class="table table-bordered">
			<tr class="bg-success">
				<td>Year</td>
				<td>Month</td>
				<td>Income</td>
				<td>Expense</td>
				<td>Current</td>

			</tr>
		<?php 
		$list_report = $this->expense_model->get_report_all_month(array(
			'creator_id' => member_cookies('member_id'),
			'year' => $gYear
		));
		$list_report = $list_report['data'];
		if (!empty($list_report))
		{
			$subtotal_income = $subtotal_expense = $subtotal_current = 0;
			$grandtotal_income = $grandtotal_expense = $grandtotal_current = 0;
			foreach ($list_report as $key => $rs)
			{
				$is_change_year = FALSE;
				
				$start_date = $end_date = $url = NULL;
				
				$subtotal_income += $rs['income'];
				$subtotal_expense += $rs['expense'];
				
				$icon_status = "<i class='fa fa-plus-square clrGrn'></i>";
				if ($rs['income'] < $rs['expense']) $icon_status = "<i class='fa fa-minus-square clrRed'></i>";
				if ($rs['income'] == $rs['expense']) $icon_status = "<i class='fa fa-money'></i>";

				$prev_release_date = NULL;
				if (isset($list_report[$key + 1]['release_date'])) $prev_release_date = $list_report[$key + 1]['release_date'];
				
				// SHOW REKAP DATA PER YEAR IF END OF MONTH
				if ($key >= 0 && $key <= count($list_report) && date('Y',strtotime($rs['release_date'])) != date('Y',strtotime($prev_release_date))) {
					$is_change_year = TRUE;
				}
				
				?>
			<tr>
				<td><?php if (isset($rs['release_date'])) echo date('Y', strtotime($rs['release_date'])); ?></td>
				<td><?php if (isset($rs['release_date'])) echo date('m', strtotime($rs['release_date'])); ?></td>
				<td><?php if (isset($rs['income'])) echo format_money($rs['income'])?></td>
				<td><?php if (isset($rs['expense'])) echo format_money($rs['expense'])?></td>
				<td>
				<?php echo format_money($rs['income'] - $rs['expense']).' '.$icon_status;?>
				<?php 
				$start_date = date("01-m-Y", strtotime($rs['release_date']));
				$end_date = date("t-m-Y", strtotime($rs['release_date']));
				$url = base_url()."expense/report?start_date=".$start_date."&end_date=".$end_date;
				?>
				<a class="btn btn-xs btn-info" alt="detail monthly report" title="detail monthly report" href="<?php echo $url?>" target="_blank"><i class="fa fa-line-chart"></i> detail</a>
				</td>
			</tr>
				<?php
				// SHOW REKAP PER YEAR
				if ($is_change_year)
				{
					$icon_status = "<i class='fa fa-plus-square clrGrn'></i>";
					if ($subtotal_income < $subtotal_expense) $icon_status = "<i class='fa fa-minus-square clrRed'></i>";
					if ($subtotal_income == $subtotal_expense) $icon_status = "<i class='fa fa-money'></i>";
					?>
			<tr class="alert-warning">
				<td colspan="2">Subtotal in <?php if (isset($rs['release_date'])) echo date('Y', strtotime($rs['release_date'])); ?></td>
				<td><?php echo format_money($subtotal_income) ?></td>
				<td><?php echo format_money($subtotal_expense) ?></td>
				<td><?php echo format_money($subtotal_income - $subtotal_expense).' '.$icon_status;?></td>
			</tr>	
					<?php
					$is_change_year = FALSE;
					$grandtotal_income += $subtotal_income;
					$grandtotal_expense += $subtotal_expense;
					
					$subtotal_income = $subtotal_expense = 0;
				}			
			}
			$subtotal_current = $subtotal_income - $subtotal_expense;
			$grandtotal_current = $grandtotal_income - $grandtotal_expense;
			
			$grandtotal_icon_status = "<i class='fa fa-plus-square clrGrn'></i>";
			$grandtotal_status = "<span class='b clrGrn'>Congratulation, you have a positive cashflow for the year.</span>";
			if ($grandtotal_income < $grandtotal_expense) {
				$grandtotal_icon_status = "<i class='fa fa-minus-square clrRed'></i>";
				$grandtotal_status = "<span class='b clrRed'>Ooops, you have a negative cashflow for the year.</span>";
			}
			if ($grandtotal_income == $grandtotal_expense) { 
				$grandtotal_icon_status = "<i class='fa fa-money'></i>";
				$grandtotal_status = "<span class='b clrBlk'>Well, you have a neutral cashflow for the year.</span>";
			}
			?>
			<tr class="b">
				<td colspan="2">Grandtotal All year</td>
				<td><?php echo format_money($grandtotal_income)?></td>
				<td><?php echo format_money($grandtotal_expense)?></td>
				<td><?php echo format_money($grandtotal_current). ' '.$grandtotal_icon_status?></td>
			</tr>
			<tr class="b">
				<td colspan="2">SUMMARY</td>
				<td colspan="3"><?php echo $grandtotal_status?></td>
			</tr>
			<?php
		}
		else 
		{
			?>
			<tr>
				<td colspan="6">No Data</td>
			</tr>
			<?php 
		}
		?>
		</table>

		<?php 
		break;
		?>

		<?php
		case 'parent_category':
			?>
			Still under development
			<!-- 
			<table class="table table-striped table-hover">
			<thead>
				<tr>
					<td>tester</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>123</td>
				</tr>
			</tbody>
			</table>
			-->
			<?php
			break;

		case 'chartbar':
		// -------------------------------	
		// START PARENT CATEGORY
		$listdata = $listdataglobal = NULL;
		$getyear = date('Y');
		if (isset($_GET['year'])) $getyear = $_GET['year'];

		$attr = NULL;
		// $attr['month'] = 11;
		$attr['is_parent'] = 1;
		$attr['creator_id'] = member_cookies('member_id');
		$attr['year'] = $getyear;
		
		$listdataglobal = $this->expense_model->get_report_parent_expense_global($attr);
		// debug($listdataglobal);
		
		$listdata = $this->expense_model->get_report_parent_expense_by_year($attr);
		$listdata = $listdata['data'];

		$tampungs = $listCategoryId = NULL;
		if (!empty($listdata)) 
		{
			foreach ($listdata as $k => $v) 
			{
				$listCategoryId[$v['parent_catname']] = $v['parent_catid'];

				// loop here to check months amount exist or not, set default 0
				for ($i=1; $i<=12;$i++) 
				{
					if (isset($tampungs[$v['parent_catname']][$i])) {

						if ($tampungs[$v['parent_catname']][$i] == $v['mndt'] AND ! isset($tampungs[$v['parent_catname']]['tot_amount'])) {
							$tampungs[$v['parent_catname']][$i] = 0;
						}
					} else {
						$tampungs[$v['parent_catname']][$i] = 0;
					}
				}

				// if (! isset($v['tot_amount'])) $v['tot_amount'] = 0; 
				$tampungs[$v['parent_catname']][$v['mndt']] = $v['tot_amount'];

			}

			$tempe = $tempe_total = $xCat = NULL;
			foreach ($tampungs as $k => $v) {
				// debug($v);
				$tempe_total[$k] = array_sum($v);
				$tempe[$k] = implode(',',$v);
				$xCat .= "'" . $k . "',"; 
			}
		}
		// debug($tempe);

		$total_peryear = $avg_peryear = 0;
		
		$most_expensive = $most_cheapest = $tmpexp = NULL;
		if (!empty($tempe)) 
		{
			foreach ($tempe as $key => $rs) 
			{
				$tmpexp['name'] = $key;
				$tmpexp['amount'] = $tempe_total[$key];
				if (! isset($most_expensive['amount']) || $tempe_total[$key] > $most_expensive['amount']) {
					$most_expensive = $tmpexp;
				}
				if (! isset($most_cheapest['amount']) || $tempe_total[$key] < $most_cheapest['amount']) {
					$most_cheapest = $tmpexp;
				}
				
				$total_peryear += $tempe_total[$key];
			}
			$avg_peryear = $total_peryear / count($tempe);
		
		}
		// $tempe_total[$k] += $v;
		// debug($tempe);
		// debug($tempe_total);
		// debug($tampungs);
		$xMonths = array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec');
		$xMonths = sprintf("'%s'", implode("','", $xMonths ) );

			?>
			<h2 class="talCnt">Bar chart</h2>
			<div id="chartbar" class="col-sm-12 talCnt" style="height:800px">
			
			</div>
			
			<div class="col-sm-12 talCnt">
				Filter menu:
				<button class="btn btn-default btn-xs" id="toggleChartFilter">Toggle Chart Filter</button><br/><br/>
			</div>
			<div class="col-sm-3 talCnt">
				<div class="alert bg-primary">Total expense in <?php echo $getyear; ?><br/><br/><i class="fa fa-archive fa-3x"></i> <br/> <b class="h3"><?php echo format_money($total_peryear)?></b></div>
			</div>
			<div class="col-sm-3 talCnt">
				<div class="alert alert-warning">Average Expense per month<br/><br/><i class="fa fa-bar-chart fa-3x"></i> <br/><b class="h3"><?php echo format_money($avg_peryear)?></b></div>
			</div>
			<div class="col-sm-3 talCnt">
				<div class="alert alert-danger">Most Expensive Category<br/><i class="fa fa-level-up fa-3x"></i> <br/><b class=""><?php echo $most_expensive['name'];?></b><br/> <b class="h3"><?php echo format_money($most_expensive['amount'])?></b></div>
			</div>
			<div class="col-sm-3 talCnt">
				<div class="alert alert-success">Most Cheapest Category<br/><i class="fa fa-level-down fa-3x"></i> <br/><b class=""><?php echo $most_cheapest['name'];?></b><br/> <b class="h3"><?php echo format_money($most_cheapest['amount'])?></b></div>
			</div>
			<div class="col-sm-12 talCnt">
			<?php 
			$objlist = $listdataglobal['data'];
			if (! empty($objlist)) 
			{
				$rsssubtotal = $rsptotal = 0;
				
				// save all total for percentage
				foreach ($objlist as $key => $rs) 
				{
					$rsubtotal = $rs['tot_amount']; 
					$rsssubtotal += $rsubtotal; 
				}
				
			?>
				<table class="table table-bordered bg-info table-striped table-hover pointer">
				<thead class="bg-primary">
					<tr>
						<td width="20px" class="talCnt">#</td>
						<td width="450px">Name</td>
						<td>Amount</td>
						<td width="50px">Percentage</td>
					</tr>
				</thead>
				<tbody>
				<?php 
				$xx = 0;
				foreach ($objlist as $key => $rs) 
				{
					$xx++;
					$rsubtotal = $rs['tot_amount']; 
					
					$rsp = ($rsubtotal/$rsssubtotal)*100;
					$rsp = sprintf('%0.2f', round($rsp, 2));;
					$rsptotal += $rsp;
				?>
					<tr>
						<td><?php echo $xx; ?></td>
						<td class="talLft"><a class="clrGrn" href="search?q=&start_date=01-01-<?php echo $getyear?>&end_date=31-12-<?php echo $getyear?>&category_id=<?php echo $rs['parent_catid']?>" target="_blank" title="Click to see detail report"><?php echo $rs['parent_catname']; ?></a></td>
						<td class="talRgt"><?php echo format_money($rsubtotal) ?> <span class="f12 b">(avg: <?php echo format_money($rsubtotal/date('m'),''); ?>)</span></td>
						<td class="talRgt"><?php echo ($rsp) ?> %</td>
					</tr>
				<?php 
				}
				?>
				<tr>
					<td colspan="2" class="talLft">Grandtotal</td>
					<td class="talRgt b"><?php echo format_money($rsssubtotal)?></td>
					<td class="talRgt b"><?php echo ($rsptotal)?> %</td>
				</tr>
				</tbody>
				</table>
				<?php 
			}
			else 
			{
			?>
			No data
			<?php 
			}
			?>
			</div>
			

			<script>
			$(function () {
				// Bar chart
				// var listCategories = <?php echo json_encode($listCategoryId,1);	?>;
				var listCategories = <?php echo json_encode($listCategoryId,1);	?>;
				
				// for shortcut nav links on click
				var months = {
					 'Jan':'01', 'Feb':'02', 'Mar':'03', 'Apr':'04', 'May':'05', 'Jun':'06', 'Jul':'07', 'Aug':'08', 'Sep':'09', 'Oct':'10', 'Nov':'11', 'Dec':'12'
				};
				const thechart = Highcharts.chart('chartbar', {
					chart: {
						type: 'column'
					},
					title: {
						text: 'Monthly report bar <?php echo $getyear?>' 
					},
					subtitle: {
						align: 'center',
						text: 'Source: <a href="#linkhere" target="_blank">XXX</a>'
					},
					xAxis: {
						categories: [<?php echo $xMonths ?>],
						crosshair: true,
						labels: {
						formatter: function () {
							return '<a href="<?php echo base_url().'expense/search?year=' . $getyear . '&month='?>' + months[this.value] + '">' + this.value + ' </a>'
						},
						useHTML: true
					}
					},
					yAxis: {
						title: {
							useHTML: true,
							text: 'Expense spending by monthly <?php echo $getyear; ?>'
						},
						crosshair: true
					},
					credits: {
						enabled: false
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.series.name +'</b><br/>'+
								this.x +': '+ this.y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						}
					},
					plotOptions: {
						column: {
							pointPadding: 0.1,
							borderWidth: 0
						},
						series: {
							cursor:'pointer',
							point:{
								events:{
									click:function() {
										var themonth = this.x + 1;
										var stdate = '01-' + themonth + '-<?php echo $getyear; ?>';
										// var endate = '31-' + this.x + '-<?php echo $getyear; ?>';
										
										// var lastDayEndDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
										var lastDayEndDate = new Date(<?php echo $getyear; ?>, themonth, 0);
										var endate = lastDayEndDate + '-' + this.x + '-<?php echo $getyear; ?>';
										endate = formatDate(lastDayEndDate);
										
										// split due to series name already concat with format money total so need to split
										var seriesKey = this.series.name;
										seriesKey = seriesKey.split(" - ");
										seriesKey = seriesKey[0];
										
										var href = '<?php echo base_url(); ?>expense/search?start_date=' + stdate + '&end_date=' + endate + '&category_id=' + listCategories[seriesKey];
										
										// console.log(endate);
										window.open(href);
									}
								}
							}
						}
					},
					series: [
						<?php 
							// $total_peryear = 0;
							foreach ($tempe as $key => $rs) 
							{
								// $total_peryear += $tempe_total[$key];
							// add 0 due to start chart needs 0
							?>
						{
							name: '<?php echo $key.' - '.format_money($tempe_total[$key],''); ?>',
							pointWidth: 10,
							data: [<?php echo $rs; ?>]
						},	
							<?php 
							}
							?>
					],
				});
				
				let fshow = false;
				document.getElementById('toggleChartFilter').addEventListener('click', function() {
				  fshow ^= true; //negation state
				  thechart.series.forEach(s => {
					if (fshow) {
					  s.hide();
					} else {
					  s.show();
					}
				  });
				});
				
			});
				</script>
			<?php
			break;
		// END PARENT CATEGORY	
		// -------------------------------	

		case 'budget':
			?>
			<!-- Budget start -->
			Ini report budget bro
			<!-- Budget End -->
			<?php
			break;
			
		
		default:
			echo "mantap";
			break;
		}	
	}
	?>
	</form>
</div>
<script>
$(function(){
	var selectbar = $('#income_expense').val();
	if (selectbar === '') {
		$('select[name=select_report]').val('chartbar').change();
	}
	// $('select[name=select_report]').trigger('change');
	// $('#income_expense').change('');
});

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [day, month, year].join('-');
}
<?php 
if (isset($select_report)) {
	?>
	$('#income_expense').val('<?php echo $select_report?>').prop('checked',true);
	<?php
}
?>
</script>

<?php 

// QUERY GET DATA PARENT
/** 
 * 
 SELECT c.name AS cat_name, 
 cp.name as parent_catname,
 SUM(e.amount) AS tot_amount,
 MONTH(e.release_date) as mndt,
 YEAR(e.release_date) AS yrdt
 FROM wal_expense e
 LEFT JOIN wal_category c ON e.category_id = c.category_id
 LEFT JOIN wal_category cp ON cp.category_id = c.parent_category_id
 WHERE 1 
 AND cp.is_parent = 1
 AND e.creator_id = 1 
 -- AND MONTH(e.release_date) = 2
 AND YEAR(e.release_date) = 2022
 GROUP BY parent_catname, mndt
 -- c.parent_category_id, mndt
 */
 
?>