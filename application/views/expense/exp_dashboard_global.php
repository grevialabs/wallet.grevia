<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = DASHBOARD;
$bread['member'] = DASHBOARD;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
$do = $journey_id = NULL;

if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('journey_id')) $journey_id = $this->input->get('journey_id');

$list_report = $this->category_model->get_list_category_with_total_amount_global_date(array(
	'order' => 'total_amount DESC',
	'creator_id' => member_cookies('member_id')
));
?>

<div class="col-md-2">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-10">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"> </i> DESKRIPSI</b><br/>
		Fitur ini mencatat hasil rekapan setiap bulan dari expense kamu.
	</div><hr/>
	
	<?php 
	if (is_filled($message['message']))echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>
	<form method="post">
		
		<table class="table table-bordered table-striped">
			<tr class="bg-success">
				<td class="talCnt">ID</td>
				<td>Name expense</td>
				<td class="talRgt">Total Amount</td>
				<td class="talRgt">Total request</td>
				<td class="talRgt">Average expense per request</td>
			</tr>
		<?php 
		$list_report = $list_report['data'];
		if (!empty($list_report))
		{
			// debug($list_report);
			
			$subtotal_income = $subtotal_expense = $subtotal_current = 0;
			$grandtotal_income = $grandtotal_expense = $grandtotal_current = 0;
			foreach ($list_report as $key => $rs)
			{
				// $is_change_year = FALSE;
				
				// $start_date = $end_date = $url = NULL;
				
				// $subtotal_income += $rs['income'];
				// $subtotal_expense += $rs['expense'];
				
				// $icon_status = "<i class='fa fa-plus-square clrGrn'></i>";
				// if ($rs['income'] < $rs['expense']) $icon_status = "<i class='fa fa-minus-square clrRed'></i>";
				// if ($rs['income'] == $rs['expense']) $icon_status = "<i class='fa fa-money'></i>";

				// $prev_release_date = NULL;
				// if (isset($list_report[$key + 1]['release_date'])) $prev_release_date = $list_report[$key + 1]['release_date'];
				
				// SHOW REKAP DATA PER YEAR IF END OF MONTH
				// if ($key > 0 && $key <= count($list_report) && date('Y',strtotime($rs['release_date'])) != date('Y',strtotime($prev_release_date))) {
					// $is_change_year = TRUE;
				// }
				$average_expense_per_day = 0;
				if ($rs['total_days_count'] > 0) $average_expense_per_day = format_money($rs['total_amount'] / $rs['total_days_count']);
				
				
				if ($rs['category_type'] == 'expense') 
					$rs['category_type'] = '<div class="clrRed">'.$rs['category_type'].'</div>';
				else 
					$rs['category_type'] = '<div class="clrGrn">'.$rs['category_type'].'</div>';
				?>
			<tr>
				<td class="talCnt"><?php echo $rs['category_id']; ?></td>
				<td><?php echo $rs['category_type'].' '.$rs['name']; ?></td>
				<td class="talRgt"><?php if (isset($rs['total_amount'])) echo format_money($rs['total_amount'])?></td>
				<td class="talRgt"><?php echo $rs['total_days_count']; ?></td>
				<td class="talRgt"><?php echo $average_expense_per_day.' per request'; ?></td>
			</tr>
				<?php	
			}
			
			// $subtotal_current = $subtotal_income - $subtotal_expense;
			// $grandtotal_current = $grandtotal_income - $grandtotal_expense;
			
			// $grandtotal_icon_status = "<i class='fa fa-plus-square clrGrn'></i>";
			// $grandtotal_status = "<span class='b clrGrn'>Congratulation, you have a positive cashflow for the year.</span>";
			// if ($grandtotal_income < $grandtotal_expense) {
				// $grandtotal_icon_status = "<i class='fa fa-minus-square clrRed'></i>";
				// $grandtotal_status = "<span class='b clrRed'>Ooops, you have a negative cashflow for the year.</span>";
			// }
			// if ($grandtotal_income == $grandtotal_expense) { 
				// $grandtotal_icon_status = "<i class='fa fa-money'></i>";
				// $grandtotal_status = "<span class='b clrBlk'>Well, you have a neutral cashflow for the year.</span>";
			// }
			?>
			<tr class="b">
				<td colspan="2">Grandtotal All year</td>
				<td><?php //echo format_money($grandtotal_income)?></td>
				<td><?php //echo format_money($grandtotal_income)?></td>
				<td><?php //echo format_money($grandtotal_income)?></td>
			</tr>
			<tr class="b">
				<td colspan="2">SUMMARY</td>
				<td colspan="3"><?php //echo $grandtotal_status?></td>
			</tr>
			<?php
		}
		else 
		{
			?>
			<tr>
				<td colspan="6">No Data</td>
			</tr>
			<?php 
		}
		?>
		</table>
	</form>
</div>