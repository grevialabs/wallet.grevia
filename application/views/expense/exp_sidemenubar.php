<style>
.list-group-header {
position: relative;
display: block;
padding: 10px 15px;
margin-bottom: -1px;
background-color: #e1e2e4;
border: 1px solid #ddd;
font-weight: bolder;
}
</style>
	<?php
	$base_url = base_url();
	?>
	
	<div class="row">
		<div class="col-md-12">
			<div class="pull-left">
				Hola, <?php echo member_cookies('fullname')?><br/><br/>
				<form method="get" action="search">
					<input type="text" class="input br w300" placeholder="Cari di Expense...<?php // echo ENTER_KEYWORD?>" name="q" value="<?php if (isset($_GET['q'])) echo $_GET['q']?>"/>
					<button name="" value="" class="btn btn-info btn-sm br">SUBMIT</button>
				</form>
			</div>
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="list-group f12 bdrNon">
			<!--
			<a class="list-group-item <?php echo member_active('expense/test')?>" href="<?php echo $base_url.'member/test'?>"><i class="fa fa-file-text fa-fw"></i>&nbsp; <?php echo MENU_TEST?></a>
			<a class="list-group-item <?php echo member_active('history_test')?>" href="<?php echo $base_url.'expense/history_test'?>"><i class="fa fa-th fa-fw"></i>&nbsp; <?php echo MENU_HISTORY_TEST?></a>
			-->
			<div class="list-group-header">Expense</div>
			<a class="list-group-item <?php echo member_active('expense/search').member_active('search')?>" href="<?php echo $base_url.'expense/search'?>"><i class="fa fa-search fa-fw"></i>&nbsp; Search Expense</a>
			<a class="list-group-item <?php echo member_active('expense/dashboard').member_active('dashboard')?>" href="<?php echo $base_url.'expense/dashboard'?>"><i class="fa fa-desktop fa-fw"></i>&nbsp; Dashboard report</a>
			<a class="list-group-item <?php echo member_active('expense/reminder').member_active('reminder')?>" href="<?php echo $base_url.'expense/reminder'?>"><i class="fa fa-money fa-fw"></i>&nbsp; Reminder</a>
			<a class="list-group-item <?php echo member_active('expense/report').member_active('report')?>" href="<?php echo $base_url.'expense/report'?>"><i class="fa fa-cog fa-fw"></i>&nbsp; Report aa</a>
	
			<!--
			<a class="list-group-item <?php echo member_active('expense/forecast').member_active('forecast')?>" href="<?php echo $base_url.'expense/forecast'?>"><i class="fa fa-line-chart fa-fw"></i>&nbsp; Forecast</a>
			<a class="list-group-item <?php echo member_active('expense/dashboard_global').member_active('dashboard_global')?>" href="<?php echo $base_url.'expense/dashboard_global'?>"><i class="fa fa-building fa-fw"></i>&nbsp; Dashboard All<sup class="clrRed b">*beta</sup></a>
			-->
			
			
			<!--
			<a class="list-group-item <?php echo member_active('expense/detail_report').member_active('detail_report')?>" href="<?php echo $base_url.'expense/detail_report'?>"><i class="fa fa-building fa-fw"></i>&nbsp; Detail report</a>
			-->
			
			<!-- MASTER DATA START -->
			<div class="list-group-header">Masterdata</div>
			<a class="list-group-item <?php echo member_active('member/task').member_active('task')?>" href="<?php echo $base_url.'member/task'?>"><i class="fa fa-truck fa-fw"></i>&nbsp; Task<sup class="clrRed b">*new</sup></a>
			<a class="list-group-item <?php echo member_active('expense/treasure').member_active('treasure')?>" href="<?php echo $base_url.'expense/treasure'?>"><i class="fa fa-dollar fa-fw"></i>&nbsp; Treasure</a>
			<a class="list-group-item <?php echo member_active('expense/category').member_active('category');?>" href="<?php echo $base_url.'expense/category'?>"><i class="fa fa-folder fa-fw"></i>&nbsp; Category Expense</a>
			<a class="list-group-item <?php echo member_active('expense/fund').member_active('fund')?>" href="<?php echo $base_url.'expense/fund'?>"><i class="fa fa-money fa-fw"></i>&nbsp; Fund<sup class="clrRed b">*new</sup></a>
			<a class="list-group-item <?php echo member_active('expense/fund_mutation').member_active('fund_mutation')?>" href="<?php echo $base_url.'expense/fund_mutation'?>"><i class="fa fa-calculator fa-fw"></i>&nbsp; Fund Mutation<sup class="clrRed b">*new</sup></a>
			<!-- MASTER DATA END -->
			
			<div class="list-group-header">Others</div>
			<a class="list-group-item <?php echo member_active('expense/calendar').member_active('calendar')?>" href="<?php echo $base_url.'expense/calendar'?>"><i class="fa fa-calendar fa-fw"></i>&nbsp; Calendar<sup class="clrRed b">*new</sup></a>
			<a class="list-group-item <?php echo member_active('expense/absence').member_active('absence')?>" href="<?php echo $base_url.'expense/absence'?>"><i class="fa fa-user fa-fw"></i>&nbsp; Absence<sup class="clrRed b">*new</sup></a>
			<a class="list-group-item <?php echo member_active('expense/journey').member_active('journey')?>" href="<?php echo $base_url.'expense/journey'?>"><i class="fa fa-motorcycle fa-fw"></i>&nbsp; Journey<sup class="clrRed b">*new</sup></a>
			<a class="list-group-item <?php echo member_active('member/profile').member_active('profile')?>" href="<?php echo $base_url.'member/profile'?>"><i class="fa fa-user fa-fw"></i>&nbsp; Profile</a>
		</div>
	</div>
	<?php 
	// style="background:#000"
	?>
	<div class="panel panel-primary">
		<div class="padLrg bdrGry clrWht bgBlu" > 
			<div class="talCnt">
				<span style="font-size:20px"><?php echo date('D')?></span><br/>
				<span style="font-size:50px"><?php echo date('d')?></span><br/>
				<span style="font-size:20px"><?php echo date('M')?></span><br/>
				<span style="font-size:20px" id="span_date" ></span>
			</div>
		</div>
	</div>
	<script>
	// var end = new Date('02/19/2012 10:1 AM');
	// var end = new Date(new Date().setFullYear(new Date().getFullYear() + 1));

	function timetick() {
		var now = new Date();
		
		hours = now.getHours();
		if (hours < 10) hours = '0' + hours;
		
		minutes = now.getMinutes();
		if (minutes < 10) minutes = '0' + minutes;
		
		seconds = now.getSeconds();
		if (seconds < 10) seconds = '0' + seconds;

		document.getElementById('span_date').innerHTML = hours + ':' + minutes + ':' + seconds;
	}
	timetick();
	timer = setInterval(timetick, 1000);
	</script>