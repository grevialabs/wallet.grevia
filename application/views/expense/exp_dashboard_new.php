<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = DASHBOARD;
$bread['member'] = DASHBOARD;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
$do = $journey_id = NULL;

if ($this->input->get('do')) $do = $this->input->get('do');
// if ($this->input->get('journey_id')) $journey_id = $this->input->get('journey_id');

$list_report = $this->expense_model->get_report_all_month_new(array(
	'creator_id' => member_cookies('member_id')
));
?>

<div class="col-md-2">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-10">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"> </i> DESKRIPSI</b><br/>
		Fitur ini mencatat hasil rekapan setiap bulan dari expense kamu.
	</div><hr/>
	
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>
	<form method="post">
		
		<table class="table table-bordered">
			<tr class="bg-success">
				<td>Year</td>
				<td>Month</td>
				<td>Income</td>
				<td>Expense</td>
				<td>Current</td>

			</tr>
		<?php 
		$list_report = $list_report['data'];
		if (!empty($list_report))
		{
			$subtotal_income = $subtotal_expense = $subtotal_current = 0;
			$grandtotal_income = $grandtotal_expense = $grandtotal_current = 0;
			foreach ($list_report as $key => $rs)
			{
				$is_change_year = FALSE;
				
				$start_date = $end_date = $url = NULL;
				
				// set here
				// $rs['total_income_month'] = $rs['income'];
				// $rs['total_expense_month'] = $rs['expense'];
				$rs['income'] = $rs['total_income_month'];
				$rs['expense'] = $rs['total_expense_month'];
				
				$subtotal_income += $rs['income'];
				$subtotal_expense += $rs['expense'];
				
				$icon_status = "<i class='fa fa-plus-square clrGrn'></i>";
				if ($rs['income'] < $rs['expense']) $icon_status = "<i class='fa fa-minus-square clrRed'></i>";
				if ($rs['income'] == $rs['expense']) $icon_status = "<i class='fa fa-money'></i>";

				$prev_release_date = NULL;
				if (isset($list_report[$key + 1]['release_date'])) $prev_release_date = $list_report[$key + 1]['release_date'];
				
				// SHOW REKAP DATA PER YEAR IF END OF MONTH
				if ($key > 0 && $key <= count($list_report) && date('Y',strtotime($rs['release_date'])) != date('Y',strtotime($prev_release_date))) {
					$is_change_year = TRUE;
				}
				
				?>
			<tr>
				<td><?php if (isset($rs['release_date'])) echo date('Y', strtotime($rs['release_date'])); ?></td>
				<td><?php if (isset($rs['release_date'])) echo date('m', strtotime($rs['release_date'])); ?></td>
				<td><?php if (isset($rs['income'])) echo format_money($rs['income'])?></td>
				<td><?php if (isset($rs['expense'])) echo format_money($rs['expense'])?></td>
				<td>
				<?php echo format_money($rs['income'] - $rs['expense']).' '.$icon_status;?>
				<?php 
				$start_date = date("01-m-Y", strtotime($rs['release_date']));
				$end_date = date("t-m-Y", strtotime($rs['release_date']));
				$url = base_url()."expense/report?start_date=".$start_date."&end_date=".$end_date;
				?>
				<a class="btn btn-xs btn-info" alt="detail monthly report" title="detail monthly report" href="<?php echo $url?>" target="_blank"><i class="fa fa-line-chart"></i> detail</a>
				</td>
			</tr>
				<?php
				// SHOW REKAP PER YEAR
				if ($is_change_year)
				{
					$icon_status = "<i class='fa fa-plus-square clrGrn'></i>";
					if ($subtotal_income < $subtotal_expense) $icon_status = "<i class='fa fa-minus-square clrRed'></i>";
					if ($subtotal_income == $subtotal_expense) $icon_status = "<i class='fa fa-money'></i>";
					?>
			<tr class="alert-warning">
				<td colspan="2">Subtotal in <?php if (isset($rs['release_date'])) echo date('Y', strtotime($rs['release_date'])); ?></td>
				<td><?php echo format_money($subtotal_income) ?></td>
				<td><?php echo format_money($subtotal_expense) ?></td>
				<td><?php echo format_money($subtotal_income - $subtotal_expense).' '.$icon_status;?></td>
			</tr>	
					<?php
					$is_change_year = FALSE;
					$grandtotal_income += $subtotal_income;
					$grandtotal_expense += $subtotal_expense;
					
					$subtotal_income = $subtotal_expense = 0;
				}			
			}
			$subtotal_current = $subtotal_income - $subtotal_expense;
			$grandtotal_current = $grandtotal_income - $grandtotal_expense;
			
			$grandtotal_icon_status = "<i class='fa fa-plus-square clrGrn'></i>";
			$grandtotal_status = "<span class='b clrGrn'>Congratulation, you have a positive cashflow for the year.</span>";
			if ($grandtotal_income < $grandtotal_expense) {
				$grandtotal_icon_status = "<i class='fa fa-minus-square clrRed'></i>";
				$grandtotal_status = "<span class='b clrRed'>Ooops, you have a negative cashflow for the year.</span>";
			}
			if ($grandtotal_income == $grandtotal_expense) { 
				$grandtotal_icon_status = "<i class='fa fa-money'></i>";
				$grandtotal_status = "<span class='b clrBlk'>Well, you have a neutral cashflow for the year.</span>";
			}
			?>
			<tr class="b">
				<td colspan="2">Grandtotal All year</td>
				<td><?php echo format_money($grandtotal_income)?></td>
				<td><?php echo format_money($grandtotal_expense)?></td>
				<td><?php echo format_money($grandtotal_current). ' '.$grandtotal_icon_status?></td>
			</tr>
			<tr class="b">
				<td colspan="2">SUMMARY</td>
				<td colspan="3"><?php echo $grandtotal_status?></td>
			</tr>
			<?php
		}
		else 
		{
			?>
			<tr>
				<td colspan="6">No Data</td>
			</tr>
			<?php 
		}
		?>
		</table>
	</form>
</div>