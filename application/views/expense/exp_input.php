<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = REPORT;
$bread['member'] = REPORT;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
// POST HERE
if ($_POST)
{
	$var = array();
	$var = $_POST;
	
	if (!isset($var['is_paid'])) $var['is_paid'] = 0;
	if (isset($var['release_date']) && $var['release_date']!='') $var['release_date'] = date('Y-m-d',strtotime($var['release_date']));
	if (isset($var['reminder_date']) && $var['reminder_date']!='') $var['reminder_date'] = date('Y-m-d',strtotime($var['reminder_date']));
	$var['creator_id'] = member_cookies('member_id');	
	$insert = $this->expense_model->save($var);
	
	($insert)?$message['message'] = MESSAGE::SAVE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
}

$start_date = $end_date = NULL;
$param = array();

// if ($this->input->get('start_date') && $this->input->get('start_date')) 
// {
	// $start_date = date('d-m-Y',strtotime($this->input->get('start_date')));
	// $end_date = date('d-m-Y',strtotime($this->input->get('end_date')));
// }
// else
// {
	// $param['start_date'] = $start_date = date("01-m-Y");
	// $param['end_date'] = $end_date = date("t-m-Y");
// }
	
// $param['creator_id'] = member_cookies('member_id');
// if (isset($start_date) && isset($end_date)) 
// {
	// $param['start_date'] = $start_date;
	// $param['end_date'] = $end_date;
// }

?>

<div class="col-md-2">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-10">
	<h1 class=""><?php echo $MODULE?></h1><br/>
	<?php if(is_filled($message['message']))echo message($message['message']).BR?>
	
	<a href="?do=insertbudget" class="btn btn-success">NEW EXPENSE</a><br/><br/>
	
	<?php 
	$do = $test_id = NULL;
	if ($this->input->get('do')) $do = $this->input->get('do');
	if ($this->input->get('test_id') && is_numeric($this->input->get('test_id'))) $test_id = $this->input->get('test_id');
	
	if ($do == 'showresult' && is_numeric($test_id)) 
	{
		
	}
	else 
	{
	?>
	
	INPUT BUDGET
	<hr>
	<?php
	$param['is_detail'] = 0;
	$param['order'] = ' is_increment ASC, name ASC';

	$list_category = $this->category_model->get_list($param);
	$list_category = $list_category['data'];
	?>
	<br/>
	<br/>
	<form method="post">
	<table class='table hover table-bordered'>
		<tr>
			<td colspan="2" class="bg-danger talCnt">DETAIL EXPENSE REPORT BY CATEGORY</td>
		</tr>
		<tr>
			<td>Category</td>
			<td><?php 
			$i = 1;
			$total = $subtotal_expense =  0;
			foreach ($list_category as $key => $rs) { 				
				if ($key > 0 && $rs['is_increment'] != $list_category[$key-1]['is_increment']) echo "<hr>";
			?>
				<label for="f_category_id[<?php echo $rs['category_id']?>]" style="font-weight:normal;height:15px"><div style="width:140px; float:left"><input type="radio" id="f_category_id[<?php echo $rs['category_id']?>]" name="f_category_id" value="<?php echo $rs['category_id']?>"> <?php echo $rs['name']?></div></label>
			<?php 
	
			} 
			?>
			</td>
		</tr>
		<tr>
			<td width="180px">Expense Name</td>
			<td><input type="text" class="input wdtFul" name="f_title" value="<?php if (isset($obj['name'])) echo $obj['name']?>"/></td>
		</tr>
		<tr>
			<td>Amount</td>
			<td><input type="text" class="input" name="f_amount" value="<?php if (isset($obj['amount'])) echo $obj['amount']?>"/> .000</td>
		</tr>
		<tr>
			<td>Notes</td>
			<td><textarea name="f_notes" class="wdtFul" rows="5"><?php if (isset($obj['notes'])) echo $obj['notes']?></textarea></td>
		</tr>
		<tr>
			<td>Release Date</td>
			<td><input type="text" class="input datepicker" name="f_release_date" value="<?php if (isset($obj['release_date'])) echo $obj['release_date'];else echo date('d-m-Y')?>"/></td>
		</tr>
		<tr>
			<td><label for="f_is_paid">IsPaid</label></td>
			<td><input type="checkbox" name="f_is_paid" id="f_is_paid" value="1" <?php if (isset($obj['is_paid']) && $obj['is_paid']==1) echo 'checked="true"'; ?>" checked="true"/></td>
		</tr>
		<tr>
			<td>Reminder Date</td>
			<td><input type="text" class="input datepicker" name="f_reminder_date" value="<?php if (isset($obj['reminder_date'])) echo $obj['reminder_date']?>"/> Email reminder will send each day, for every expense not paid</td>
		</tr>
		<!--
		<tr>
			<td>Type</td>
			<td><input type="radio" name="f_is_increment" value="0" id="f_is_increment0"/><label for="f_is_increment0"><i class="fa fa-minus fa-4x"></i></label> <input type="radio" name="f_is_increment" id="f_is_increment1" value="1"/> <label for="f_is_increment1"><i class="fa fa-plus fa-4x"></i></label> </td>
		</tr>
		-->
		<tr>
			<td colspan="2" class="talCnt"><button class="btn btn-success">SUBMIT</button></td>
		</tr>
	</table>
	</form>
	

	<?php 
	}
	?>
</div>