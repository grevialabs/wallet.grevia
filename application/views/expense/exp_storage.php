<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = STORAGE;
$bread['member'] = STORAGE;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;

$param = array();

$do = $storage_id = $getq = $getexp = NULL;
if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('q')) $getq = $this->input->get('q');
if ($this->input->get('exp')) $getexp = $this->input->get('exp');
if ($this->input->get('storage_id') && is_numeric($this->input->get('storage_id'))) $storage_id = $this->input->get('storage_id');

// PAGING
$get = $getkeyword = $getorder_allowed_list = $getorderby_allowed_list = $getorder_list = $getorder = $getorderby = $offset = $page = $perpage = $totalrows = NULL;

$perpage = 20;
$page = 1;
if (isset($_GET)) $get = $_GET;
if (isset($get['page']) && $get['page'] > 1) $page = $get['page'];
if (isset($get['perpage']) && in_array($get['perpage'],$perpage_allowed)) $perpage = $get['perpage'];


// show today or yesterday or xxx days ago
function get_day_name($timestamp) 
{

    $stime = strtotime($timestamp);
	$date = date('d/m/Y', $stime);

    if($date == date('d/m/Y')) {
      $date = '<b class="clrGrn">Today</b>';
    } 
    else if($date == date('d/m/Y',time() - (86400))) {
      $date = '<b class="clrBlu">Yesterday</b>';
    } else {
		$date = (time() - $stime) / 86400;
		
		$strdate = '';
		if ($date > 0.5) $date = ceil($date); else $date = floor($date);
		
		$cssclr = 'clrRed';
		if ($date <= 3) { 
			$cssclr = 'clrGrn';
		}
			
		$strdate .= '<div class="'.$cssclr .' b i">';
		$strdate .= 'H+'.$date . ' ago';
		$strdate .= ' <small class="btn btn-danger btn-xs">expired</small>';
		$strdate .= '</div>';
	
		$date = $strdate;
		
	}
    return $date;
}

// POST HERE
if ($_POST)
{
	$post = $param_insert = $param_update = array();
	$post = $_POST;
	
	// BULK
	if (isset($_POST['hdn_bulk_action']))
	{
		$post = $_POST;
		$list_id = $post['chkbox'];
		$param_update = NULL;
		$update_bulk = $this->storage_model->update_bulk($list_id,$param_update);
		if ($update_bulk) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		// debug($post,1);
	}	

	// SAVE
	if (isset($_POST['hdn_insert']))
	{
		
		$param_insert['name'] = $post['name'];
		$param_insert['creator_id'] = member_cookies('member_id');	
		if (isset($post['param_insert'])) $param_insert['storage_id'] = $post['storage_id'];
		$param_insert['shortdesc'] = $post['shortdesc'];
		
		if (isset($post['input_date'])){
			$param_insert['input_date'] = date("Y-m-d h:i:s", strtotime($post['input_date']));
		}
		// $param_insert['creator_ip'] = getIP();
		// $param_insert['creator_date'] = getDateTime();
		
		if (isset($post['name']) && $post['name'] != '')
		{
			// debug($param_insert);
			// die;
			$insert = $this->storage_model->save($param_insert);
			
			if ($insert) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::SAVE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		else 
		{
			$message['message'] = 'Nama harus diisi';
		}
	}
	
	// UPDATE
	if (isset($_POST['hdn_update']) && is_numeric($storage_id))
	{
		$temp = NULL;
		$temp['storage_id'] = $storage_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj = $this->storage_model->get($temp);
		
		if ($obj)
		{
			
			// $param_update['is_publish'] = 0;
			// if (isset($post['is_publish']) && in_array($post['is_publish'],array(1))) 
				// $param_update['is_publish'] = 1;
			
			// if (isset($post['parent_category_id'])) $param_update['parent_category_id'] = $post['parent_category_id'];
			
			$param_update['name'] = $post['name'];
<<<<<<< Updated upstream
			
			if (isset($post['input_date'])){
				$param_update['input_date'] = date("Y-m-d h:i:s", strtotime($post['input_date']));
			}
			
			// $param_update['monthly_budget_amount'] = $post['monthly_budget_amount'];
=======
			$param_update['input_date'] = $post['input_date'];
>>>>>>> Stashed changes
			$param_update['shortdesc'] = $post['shortdesc'];
			// debug($post,1);
			// debug($param_update,1);
			
			$update = $this->storage_model->update($storage_id, $param_update);
			
			if ($update) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		else 
		{
			// NOT FOUND
			$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
	}
}

// DELETE
if ($_GET)
{
	if ($do == "delete" && is_numeric($storage_id))
	{
		$temp = $param = NULL;
		$temp['storage_id'] = $storage_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj = $this->storage_model->get($temp);
		
		if (!empty($obj))
		{
			$param['status'] = -1;
			$delete = $this->storage_model->update($storage_id,$param);
			if ($delete) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::DELETE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		else
		{
			// NOT FOUND
			$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
	}
}

// GET LIST 
$data = $listdata = $list_storage = $param = $paramp = NULL;
$param['creator_id'] = member_cookies('member_id');
$param['paging'] = TRUE;
$page = 1;
$param['page'] = $page;
$param['limit'] = $perpage;

// $param['debug'] = TRUE;

if (isset($getq)) $param['keyword'] = $getq;

$param['order'] = ' input_date DESC';
if (isset($getexp)) {
	if ($getexp == 'soon') $param['order'] = 'input_date DESC';
	if ($getexp == 'last') $param['order'] = 'input_date ASC';
}

$data = $this->storage_model->get_list($param);
// debug($data);
if (isset($data['data'])) $list_storage = $listdata = $data['data'];
if (isset($data['total_rows'])) $total_rows = $data['total_rows'];


?>
<!-- help: http://www.bootstraptoggle.com/ -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css?v=170812" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js?v=170812"></script>


<div class="col-lg-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"></i> DESKRIPSI</b><br/><?php echo $PAGE_TITLE;?>
	</div><hr/>
	
	<?php 
	// if (is_filled($message['message']))echo message($message['message']).BR;
	if (isset($message['message']))echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>
	
	<?php if ($do != "insert") { ?>
	<a href="?do=insert" class="btn btn-success btn-sm">+ New <?php echo $MODULE?></a><br/><br/>
	<?php } ?>
	
	<?php 
	$do = $test_id = NULL;
	if ($this->input->get('do')) $do = $this->input->get('do');
	if ($this->input->get('test_id') && is_numeric($this->input->get('test_id'))) $test_id = $this->input->get('test_id');

	?>
	<form>
		Search <input type="text" name="q" value="<?php echo $getq; ?>" placeholder="Search keyword..." class="input input-sm" /> <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-magnifier"></i>Search</button><br/><br/>
	</form>
	<?php 
	if (isset($getq) && isset($list_storage)) echo "<div class='alert alert-info'>" . count($list_storage) . " Data found for <b>" . $getq . "</b></div>";
	?>
	<?php 
	if ($do == "insert" || $do == "edit")
	{
		$obj = NULL;
		if ($do == "edit" && is_numeric($storage_id))
		{
			$temp['storage_id'] = $storage_id;
			$temp['creator_id'] = member_cookies('member_id');
			$obj = $this->storage_model->get($temp);
		}
		
		if ($do == "insert" || ($do == "edit" && !empty($obj)))
		{
	?>
	<form method="post">
	<table class='table hover table-bordered table-striped'>
		<tr>
			<td colspan="2" class="bg-success talCnt"><?php echo $MODULE?></td>
		</tr>
		<tr>
			<td width="200px">Lokasi</td>
			<td>
			<?php if (isset($obj['locstorage_name'])) echo $obj['locstorage_name']?>
			KULKAS(Sementara ini hardcode dulu)
			<input type="hidden" value="1" name="f_locstorage_id" />
			</td>
		</tr>
		<tr>
			<td width="200px">Nama Item</td>
			<td><input type="text" class="input wdtFul" name="f_name" placeholder="Nama kategori pengeluaran" value="<?php if (isset($obj['name'])) echo $obj['name']?>" required /></td>
		</tr>
		<tr>
			<td>
				Tanggal input
			</td>
			<td>
				<input class="input wdtFul numeric datepicker" maxlength="2" name="f_input_date" placeholder="Tgl Jatuh tempo tiap bulan" value="<?php if (isset($obj['input_date'])) echo date('d-m-Y', strtotime($obj['input_date'])); else echo date('d-m-Y');?>" />
			</td>
		</tr>
		
		<tr>
			<td>
				ShortDescription
			</td>
			<td>
				<textarea class="input wdtFul" name="f_shortdesc" placeholder="Notes"><?php if (isset($obj['shortdesc'])) echo $obj['shortdesc']?></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="talCnt">
			<?php if ($do == "insert") { ?>
			<input type="hidden" name="hdn_insert" value="1" />
			<button class="btn btn-success btn-sm">SAVE</button>
			<?php } else { ?>
			<input type="hidden" name="hdn_update" value="1" />
			<button class="btn btn-success btn-sm">UPDATE</button>
			<?php } ?>
			
			</td>
		</tr>
	</table>
	</form>
	<br/>
	<?php 
		}
		elseif($do == "edit" && empty($obj))
		{
			echo message(getMessage(MESSAGE::NOT_FOUND));
		}
	}
	
	?>

	<!--
	Filter data by 
<<<<<<< Updated upstream
	<a href="<?php echo base_url().'expense/storage'; ?>" class="btn btn-xs <?php echo (! isset($get['exp']) ) ? "btn-success" : "btn-default"; ?>">All data</a> &nbsp;
	<a href="?exp=soon" class="btn btn-xs <?php echo (isset($get['exp']) && $get['exp'] == 'soon') ? "btn-success" : "btn-default"; ?>">Expired soon</a> &nbsp;
	<a href="?exp=last" class="btn btn-xs <?php echo (isset($get['exp']) && $get['exp'] == 'last') ? "btn-success" : "btn-default"; ?>">Expired last</a> &nbsp;<br/><br/>
=======
	<a href="<?php echo base_url().'expense/category'; ?>" class="btn btn-xs <?php echo (! isset($get['parent']) && ! isset($get['no_parent'])) ? "btn-success" : "btn-default"; ?>">All data</a> &nbsp;
	<a href="?parent=1" class="btn btn-xs <?php echo (isset($get['parent'])) ? "btn-success" : "btn-default"; ?>">Parent only</a> &nbsp;
	<a href="?no_parent=1" class="btn btn-xs <?php echo (isset($get['no_parent'])) ? "btn-success" : "btn-default"; ?>">Child only</a><br/><br/>
	-->
	Filter data by 
	<a href="<?php echo base_url().'expense/category'; ?>" class="btn btn-xs <?php echo (! isset($get['parent']) && ! isset($get['no_parent'])) ? "btn-success" : "btn-default"; ?>">All data</a> &nbsp;
	<a href="?soonexpd=1" class="btn btn-xs <?php echo (isset($get['parent'])) ? "btn-success" : "btn-default"; ?>">Soon Expired</a> &nbsp;
	<br/><br/>
>>>>>>> Stashed changes

	<form method="post">
	<table class="table hover table-bordered table-hover table-striped">
	<thead>
	<tr class="alert bg-warning b talCnt">
		<td class="" width="1"><input type="checkbox" class="chkbox togglebox pointer" onclick="togglebox()" /></td>
		<td width="1">#</td>
		<td>Name</td>
		<td width="120px">Option</td>
	</tr>
	</thead>
	<tbody>
	<?php 
	if (!empty($list_storage))
	{
		$i = 0;
		if (is_numeric($page) && $page > 0) {
			$i = ($page - 1) * $perpage ;
		}

		foreach($list_storage as $key => $rs)
		{
			$i++;
			
			$id = $parent_category = $alert_delete = NULL;
			
			$id = $rs['storage_id'];

			$delete_unique = $rs['name'] . ' - ID ' . $id;
			
			$is_publish = '<span class="clrRed b"><i class="fa fa-ban"></i> Unpublish</span>';
			if ($rs['status'] == 1) $is_publish = '<span class="clrGrn b"> <i class="fa fa-check"></i> Published</span>';
		?>
	<tr>
		<td class="parentcheckbox"><input type="checkbox" name="chkbox[]" id="chkbox[]" class="chkbox pointer" value="<?php echo $id?>"/></td>
		<td class="talCnt"><?php echo $i; ?></td>
		<td>
		<div>
			<div class="">
			<?php echo $rs['name']?>&nbsp;
			| &nbsp;<b><?php echo $rs['locstorage_name']?></b>
			
			| &nbsp; <a href="<?php echo current_url().'?do=edit&storage_id='.$rs['storage_id']?>"><i class="fa fa-edit fa-lg clrBlu"></i></a> &nbsp;
			</div>
			
		</div>
		
		<?php echo get_day_name($rs['input_date'])?>
		
		<?php if (isset($rs['shortdesc'])) echo BR.'<div class="text-info b">Shortdesc:'.BR.nl2br($rs['shortdesc'].'</div>'); ?>
		</td>

		<td class="talCnt">
			<a href="<?php echo current_url().'?do=delete&storage_id='.$rs['storage_id']?>" onclick="return confirm('Yakin ingin menghapus data <?php echo $delete_unique; ?> ?')"><i class="fa fa-times-circle fa-lg clrRed"></i></a>
		</td>
	</tr>
		<?php 
		}
		?>
	<tr>
		<td colspan="100%">
			<div id="group_action" class="btnedit">With checked do 
			<!--
			<select class="input" name="lst_group_action">
				<option class="" value="1">Active</option>
				<option class="" value="0">Inactive</option>
				<option class="" value="-1">Delete</option>
			</select>
			-->
			<select name="f_parent_category_id" class="input ">
				<option value="0">-- Select action --    </option>
				<?php 
				if (! empty($list_storage))
				{
					?>
					<option value="">Delete bulk</option>
					<?php
				}
			?>
			</select>
			<input type="hidden" name="hdn_bulk_action" value="1">
			<button class="btn btn-default btn-sm" name="btn_group_action" value="1">Action</button></div>
		</td>
	</tr>	
		<?php
	}
	else 
	{
		?>
		<tr>
			<td colspan="100%">No data</td>
		</tr>
		<?php 
	}
	?>
	</tbody>
	</table>
	</form>
	<?php if ( ! empty($listdata)) echo $this->common_model->common_paging($total_rows, $perpage); ?>
</div>

<script>
$(document).ready(function() {	
	<!-- Basic function -->

	$('#group_action').hide();
	$('.chkbox').click(function(){
		var count = $("[type='checkbox']:checked").length;
		if (count >= 1) {
			$('#group_action').show();
		} else if(count <= 0){
			$('#group_action').hide();
		}
	});

	
})
</script>