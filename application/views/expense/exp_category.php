<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = CATEGORY;
$bread['member'] = CATEGORY;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;

$param = array();

$do = $category_id = $getq = NULL;
if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('q')) $getq = $this->input->get('q');
if ($this->input->get('category_id') && is_numeric($this->input->get('category_id'))) $category_id = $this->input->get('category_id');

// PAGING
$get = $getkeyword = $getorder_allowed_list = $getorderby_allowed_list = $getorder_list = $getorder = $getorderby = $offset = $page = $perpage = $totalrows = NULL;

$perpage = 20;
$page = 1;
if (isset($_GET)) $get = $_GET;
if (isset($get['page']) && $get['page'] > 1) $page = $get['page'];
if (isset($get['perpage']) && in_array($get['perpage'],$perpage_allowed)) $perpage = $get['perpage'];

// POST HERE
if ($_POST)
{
	$post = $param_insert = $param_update = array();
	$post = $_POST;
	
	// BULK
	if (isset($_POST['hdn_bulk_action']))
	{
		$post = $_POST;
		$list_id = $post['chkbox'];
		$param_update = NULL;
		$param_update['parent_category_id'] = $post['parent_category_id'];
		$update_bulk = $this->category_model->update_bulk($list_id,$param_update);
		if ($update_bulk) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		// debug($post,1);
	}	

	// SAVE
	if (isset($_POST['hdn_insert']))
	{
		$param_insert['is_increment'] = 1;
		// if (!isset($post['is_increment']) || $post['is_increment'] == 0) $param_insert['is_increment'] = 0;
		
		if (isset($post['is_increment']) && in_array($post['is_increment'],array(-1,0,1))) 
			$param_insert['is_increment'] = $post['is_increment'];
		
		if (isset($post['is_parent']) && in_array($post['is_parent'],array(-1,0,1))) 
			$param_insert['is_parent'] = $post['is_parent'];
		
		if (isset($post['is_show_report']) && in_array($post['is_show_report'],array(-1,0,1))) 
			$param_insert['is_show_report'] = $post['is_show_report'];
		
		$param_insert['is_publish'] = 0;
		if (isset($post['is_publish']) && in_array($post['is_publish'],array(1))) 
			$param_insert['is_publish'] = 1;
		
		$param_insert['is_monthly_billing'] = 1;
		if (!isset($post['is_monthly_billing']) || $post['is_monthly_billing'] == 0) 
			$param_insert['is_monthly_billing'] = 0;
		
		// $param_insert['is_publish'] = 1;	
		$param_insert['creator_id'] = member_cookies('member_id');	
		$param_insert['name'] = $post['name'];
		if (isset($post['param_insert'])) $param_insert['parent_category_id'] = $post['parent_category_id'];
		$param_insert['due_date'] = $post['due_date'];
		$param_insert['monthly_budget_amount'] = $post['monthly_budget_amount'];
		$param_insert['notes'] = $post['notes'];
		// $param_insert['creator_ip'] = getIP();
		// $param_insert['creator_date'] = getDateTime();
		
		if (isset($post['name']) && $post['name'] != '')
		{
			$insert = $this->category_model->save($param_insert);
			
			if ($insert) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::SAVE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		else 
		{
			$message['message'] = 'Nama harus diisi';
		}
	}
	
	// UPDATE
	if (isset($_POST['hdn_update']) && is_numeric($category_id))
	{
		$temp = NULL;
		$temp['category_id'] = $category_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj = $this->category_model->get($temp);
		
		if ($obj)
		{
			$param_update['is_increment'] = 1;
			
			if (isset($post['is_increment']) && in_array($post['is_increment'],array(-1,0,1))) 
			$param_update['is_increment'] = $post['is_increment'];
			
			if (isset($post['is_parent']) && in_array($post['is_parent'],array(-1,0,1))) 
			$param_update['is_parent'] = $post['is_parent'];
		
			if (isset($post['is_show_report']) && in_array($post['is_show_report'],array(-1,0,1))) 
			$param_update['is_show_report'] = $post['is_show_report'];
			
			$param_update['is_monthly_billing'] = 1;
			if (!isset($post['is_monthly_billing']) || $post['is_monthly_billing'] == 0) 
				$param_update['is_monthly_billing'] = 0;
			
			$param_update['is_publish'] = 0;
			if (isset($post['is_publish']) && in_array($post['is_publish'],array(1))) 
				$param_update['is_publish'] = 1;
			
			if (isset($post['parent_category_id'])) $param_update['parent_category_id'] = $post['parent_category_id'];
			$param_update['name'] = $post['name'];
			$param_update['due_date'] = $post['due_date'];
			$param_update['monthly_budget_amount'] = $post['monthly_budget_amount'];
			$param_update['notes'] = $post['notes'];
			// debug($post,1);
			// debug($param_update,1);
			
			$update = $this->category_model->update($category_id, $param_update);
			
			if ($update) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		else 
		{
			// NOT FOUND
			$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
	}
}

// DELETE
if ($_GET)
{
	if ($do == "delete" && is_numeric($category_id))
	{
		$temp = $param = NULL;
		$temp['category_id'] = $category_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj = $this->category_model->get($temp);
		
		if (!empty($obj))
		{
			$param['is_delete'] = -1;
			$delete = $this->category_model->update($category_id,$param);
			if ($delete) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::DELETE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		else
		{
			// NOT FOUND
			$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
			redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
	}
}

// GET LIST CATE
$data = $listdata = $list_category = $list_parent_category = $param = $paramp = NULL;
$param['creator_id'] = member_cookies('member_id');
$param['is_detail'] = 0;
$param['paging'] = TRUE;
$param['page'] = $page;
$param['limit'] = $perpage;

// $param['debug'] = TRUE;

if (isset($get['no_parent'])) $param['no_parent'] = 1;
if (isset($get['parent'])) $param['is_parent'] = 1;
if (isset($getq)) $param['keyword'] = $getq;
$param['order'] = ' category_creator_date DESC, is_increment ASC, name ASC';

$data = $this->category_model->get_list($param);
if (isset($data['data'])) $list_category = $listdata = $data['data'];
if (isset($data['total_rows'])) $total_rows = $data['total_rows'];

// GET PARENT FOR SELECT OPTION
$paramp['is_parent'] = TRUE;
$paramp['order'] = 'name DESC';
$paramp['creator_id'] = member_cookies('member_id');
$list_parent_category = $this->category_model->get_list($paramp);
$list_parent_category = $list_parent_category['data'];


?>
<!-- help: http://www.bootstraptoggle.com/ -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css?v=170812" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js?v=170812"></script>


<div class="col-lg-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"></i> DESKRIPSI</b><br/>Data Kategori pengeluaran yang akan muncul saat mengisi <i>expense tracker</i>
	</div><hr/>
	
	<?php 
	// if (is_filled($message['message']))echo message($message['message']).BR;
	if (isset($message['message']))echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>
	
	<?php if ($do != "insert") { ?>
	<a href="?do=insert" class="btn btn-success btn-sm">+ NEW CATEGORY EXPENSE</a><br/><br/>
	<?php } ?>
	
	<?php 
	$do = $test_id = NULL;
	if ($this->input->get('do')) $do = $this->input->get('do');
	if ($this->input->get('test_id') && is_numeric($this->input->get('test_id'))) $test_id = $this->input->get('test_id');

	?>
	<form>
		Search Category <input type="text" name="q" value="<?php echo $getq; ?>" placeholder="Search keyword..." class="input" /> <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-magnifier"></i>Search</button><br/><br/>
	</form>
	<?php 
	if ($do == "insert" || $do == "edit")
	{
		$obj = NULL;
		if ($do == "edit" && is_numeric($category_id))
		{
			$temp['category_id'] = $category_id;
			$temp['creator_id'] = member_cookies('member_id');
			$obj = $this->category_model->get($temp);
		}
		
		if ($do == "insert" || ($do == "edit" && !empty($obj)))
		{
	?>
	<form method="post">
	<table class='table hover table-bordered'>
		<tr>
			<td colspan="2" class="bg-success talCnt">CATEGORY</td>
		</tr>
		<tr>
			<td width="200px">Nama Kategori Pengeluaran</td>
			<td><input type="text" class="input wdtFul" name="f_name" placeholder="Nama kategori pengeluaran" value="<?php if (isset($obj['name'])) echo $obj['name']?>" required /></td>
		</tr>
		<tr>
			<td><label for="f_is_increment">Is Parent Category</label></td>
			<td>
				<input class="pointer" type="checkbox" name="f_is_parent" id="f_is_parent" value="1" <?php if (isset($obj['is_parent']) && $obj['is_parent']==1) echo 'checked="true"'; ?> data-size="small" data-toggle="toggle" data-on="ya" data-off="tidak" data-onstyle="primary" />
			</td>
		</tr>
		<tr>
			<td><label for="f_is_increment">Show on Report<br/><span class="lblHlpRed">*will show on report page when active</span></label></td>
			<td>
				<input class="pointer" type="checkbox" name="f_is_show_report" id="f_is_parent" value="1" <?php if (isset($obj['is_show_report']) && $obj['is_show_report']==1) echo 'checked="true"'; ?> data-size="small" data-toggle="toggle" data-on="ya" data-off="tidak" data-onstyle="primary" />
				
			</td>
		</tr>
		<tr>
			<td><label for="f_is_publish">Active<br/><span class="lblHlpRed">*will show in expense when active</span></label></td>
			<td>
				<input class="pointer" type="checkbox" name="f_is_publish" id="f_is_publish" value="1" <?php if ((isset($obj['is_publish']) && $obj['is_publish']==1) || $do == 'insert') echo 'checked="true"'; ?> data-size="small" data-toggle="toggle" data-on="ya" data-off="tidak" data-onstyle="primary" />
			</td>
		</tr>
		<tr>
			<td><label for="f_parent_category_id">Parent ID</label></td>
			<td>
			<div>
				<div class="pull-left">
					<input class="pointer" type="checkbox" id="chk_parent_category_id" data-size="small" data-toggle="toggle" data-on="ya" data-off="tidak" data-onstyle="primary"  />
				</div>

				<div class="pull-right" style="width:70%">
					<select id="f_parent_category_id" name="f_parent_category_id" class="input wdtFul">
						<option value="0">No parent. </option>
						<?php 
						if (! empty($list_parent_category)) { 
							foreach($list_parent_category as $k => $rs) {
								// validate cannot same parent cat_id
								if($rs['category_id'] == $category_id) continue;
							?>
							<option value="<?php echo $rs['category_id']; ?>"  <?php if (isset($obj['parent_category_id']) && $rs['category_id'] == $obj['parent_category_id']) echo "selected='selected' " ?>><?php echo $rs['name']. ' - ' . $rs['category_type']; ?></option>
							<?php 
							}
						}  else {
							?>
							<option value="">No data. You have to register minimum 1 category as a parent.</option>
							<?php
						}
					?>
					</select>
				</div>
			</td>
		</tr>
		<tr>
			<td><label for="f_is_increment">Tipe</label></td>
			<td>
			<input type="radio" class="pointer" name="f_is_increment" value="1" id="tambah" <?php if (isset($obj['is_increment']) && $obj['is_increment']==1) echo 'checked="true"'; ?> required /> <label for="tambah" class="clrGrn b pointer">Income / Pemasukan</label><br/>
			<input type="radio" class="pointer" name="f_is_increment" value="0" id="topup" <?php if (isset($obj['is_increment']) && $obj['is_increment']==0) echo 'checked="true"'; ?> required /> <label for="topup" class="clrBlk b pointer">Topup Saldo</label><br/>
			<input type="radio" class="pointer" name="f_is_increment" value="-1" id="kurang" <?php if (isset($obj['is_increment']) && $obj['is_increment']==-1) echo 'checked="true"'; ?> required /> <label for="kurang" class="clrRed b pointer">Expense / Pengeluaran</label>
			
			<!--
			<input type="checkbox" name="f_is_increment" id="f_is_increment" value="1" <?php if (isset($obj['is_increment']) && $obj['is_increment']==1) echo 'checked="true"'; ?>/><br/>
			
			*centang jika category ini berupa PEMASUKAN(penambahan), dan abaikan jika berupa EXPENSE(pengurangan) 
			-->
			</td>
		</tr>
		</tr>
			<td>
				<label for="f_is_monthly_billing" style="cursor:pointer">Is monthly billing</label>
			</td>
			<td >
			<input class="pointer" type="checkbox" name="f_is_monthly_billing" id="chk_is_monthly_billing" value="1" <?php if (isset($obj['is_monthly_billing']) && $obj['is_monthly_billing']==1) echo 'checked="true"'; ?> data-size="mini" data-toggle="toggle" data-on="ya" data-off="tidak" data-onstyle="primary" />
			<br/>(jika ya,  kategori akan muncul menjadi reminder di halaman dashboard expense)
			</td>
		</tr>
		<tr>
			<td>
				Tgl jatuh tempo *<b>optional</b>
			</td>
			<td>
				<input class="input wdtFul numeric" maxlength="2" name="f_due_date" placeholder="Tgl Jatuh tempo tiap bulan" value="<?php if (isset($obj['due_date'])) echo $obj['due_date']?>" />
			</td>
		</tr>
		<tr>
			<td>
				Budget bulanan *<b>optional</b>
			</td>
			<td>
				<input class="input wdtFul numeric" maxlength="8" name="f_monthly_budget_amount" placeholder="Budget bulanan" value="<?php if (isset($obj['monthly_budget_amount'])) echo $obj['monthly_budget_amount']?>" />
			</td>
		</tr>
		<tr>
			<td>
				Notes *<b>optional</b>
			</td>
			<td>
				<textarea class="input wdtFul" name="f_notes" placeholder="Notes"><?php if (isset($obj['notes'])) echo $obj['notes']?></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="talCnt">
			<?php if ($do == "insert") { ?>
			<input type="hidden" name="hdn_insert" value="1" />
			<button class="btn btn-success btn-sm">SAVE</button>
			<?php } else { ?>
			<input type="hidden" name="hdn_update" value="1" />
			<button class="btn btn-success btn-sm">UPDATE</button>
			<?php } ?>
			
			</td>
		</tr>
	</table>
	</form>
	<br/>
	<?php 
		}
		elseif($do == "edit" && empty($obj))
		{
			echo message(getMessage(MESSAGE::NOT_FOUND));
		}
	}
	
	?>

	Filter data by 
	<a href="<?php echo base_url().'expense/category'; ?>" class="btn btn-xs <?php echo (! isset($get['parent']) && ! isset($get['no_parent'])) ? "btn-success" : "btn-default"; ?>">All data</a> &nbsp;
	<a href="?parent=1" class="btn btn-xs <?php echo (isset($get['parent'])) ? "btn-success" : "btn-default"; ?>">Parent only</a> &nbsp;
	<a href="?no_parent=1" class="btn btn-xs <?php echo (isset($get['no_parent'])) ? "btn-success" : "btn-default"; ?>">Child only</a><br/><br/>

	<form method="post">
	<table class="table hover table-bordered">
	<tr class="alert bg-warning b talCnt">
		<td class="" width="1"><input type="checkbox" class="chkbox togglebox pointer" onclick="togglebox()" /></td>
		<td width="1">#</td>
		<td>Name</td>
		<td width="240px">Details</td>
		<td width="120px">Option</td>
	</tr>
	<?php 
	if (!empty($list_category))
	{
		$i = 0;
		if (is_numeric($page) && $page > 0) {
			$i = ($page - 1) * $perpage ;
		}

		foreach($list_category as $key => $rs)
		{
			$i++;
			
			$id = $parent_category = $parent_category_name = $cat_type = $str_ismonthly = $alert_delete = NULL;
			
			$id = $rs['category_id'];
			$parent_category = '<a class="btn btn-xs btn-warning">child</a>';
			if ($rs['is_parent'] == 1) $parent_category = '<a class="btn btn-xs btn-primary">Parent</a>';
			if (isset($rs['parent_category_name'])) $parent_category_name = $rs['parent_category_name'];

			$cat_type = "<b class='clrRed'>Expense</b>";
			if ($rs['is_increment'] == 1) {
				$cat_type = "<b class='clrGrn'>Income</b>"; 
			} else if ($rs['is_increment'] == 0) { 
				$cat_type = "<b class='clrBlk'>Topup</b>"; 
			}

			if (isset($rs['is_monthly_billing']) && $rs['is_monthly_billing'] == 1) {
				$str_ismonthly = "<br/><span class='btn btn-xs btn-default'>monthly expense</span>";
			}

			$str_monthly_budget = NULL;
			if ($rs['monthly_budget_amount'] > 0) {
				$str_monthly_budget = 'Budget: '.format_money($rs['monthly_budget_amount']);
			} 

			$delete_unique = $rs['name'] . ' - ID ' . $id;
			
			$is_publish = '<span class="clrRed b"><i class="fa fa-ban"></i> Unpublish</span>';
			if ($rs['is_publish'] == 1) $is_publish = '<span class="clrGrn b"> <i class="fa fa-check"></i> Published</span>';
		?>
	<tr>
		<td class="parentcheckbox"><input type="checkbox" name="chkbox[]" id="chkbox[]" class="chkbox pointer" value="<?php echo $id?>"/></td>
		<td class="talCnt"><?php echo $i; ?></td>
		<td>
		<div>
			<div class="pull-left"><?php echo $rs['name']?> &nbsp;</div>
			<div class="pull-right">Tipe: <?php echo $cat_type?> <br/>
			<span><?php echo $is_publish ?></span>
			</div>
		</div>
		
		<a href="<?php echo current_url().'?do=edit&category_id='.$rs['category_id']?>"><i class="fa fa-edit fa-lg clrBlu"></i></a> &nbsp;
		<br/>On <?php echo convert_date('d M Y',$rs['creator_date'])?>
		<br/><?php if (isset($rs['notes'])) echo BR.'<div class="text-info b">Notes:'.BR.nl2br($rs['notes'].'</div>'); ?></td>
		<td class="talLft">
			<?php if (isset($parent_category_name)) { ?>Parent <b><?php echo $parent_category_name; ?></b><br/> <?php } ?>
			<?php echo $parent_category; ?><br/>
			<?php if (isset($rs['due_date'])) { ?>Due Date every <b><?php echo $rs['due_date']?></b> <br/> <?php } ?>
			<?php if (isset($rs['total_child'])) { ?>Child: <b><?php echo $rs['total_child']?></b> <br/> <?php } ?>
			<?php echo $str_monthly_budget; ?>
			<?php echo $str_ismonthly; ?>
		</td>
		<td class="talCnt">
			<a href="<?php echo current_url().'?do=delete&category_id='.$rs['category_id']?>" onclick="return confirm('Yakin ingin menghapus data <?php echo $delete_unique; ?> ?')"><i class="fa fa-times-circle fa-lg clrRed"></i></a>
		</td>
	</tr>
		<?php 
		}
		?>
	<tr>
		<td colspan="100%">
			<div id="group_action" class="btnedit">With checked do 
			<!--
			<select class="input" name="lst_group_action">
				<option class="" value="1">Active</option>
				<option class="" value="0">Inactive</option>
				<option class="" value="-1">Delete</option>
			</select>
			-->
			<select name="f_parent_category_id" class="input ">
				<option value="0">No parent. </option>
				<?php 
				if (! empty($list_parent_category)) { 
					foreach($list_parent_category as $k => $rs) {
						// validate cannot same parent cat_id
						if($rs['category_id'] == $category_id) continue;
					?>
					<option value="<?php echo $rs['category_id']; ?>"  <?php if (isset($obj['parent_category_id']) && $rs['category_id'] == $obj['parent_category_id']) echo "selected='selected' " ?>><?php echo $rs['name']. ' - ' . $rs['category_type']; ?></option>
					<?php 
					}
				}  else {
					?>
					<option value="">No data. You have to register minimum 1 category as a parent.</option>
					<?php
				}
			?>
			</select>
			<input type="hidden" name="hdn_bulk_action" value="1">
			<button class="btn btn-default btn-sm" name="btn_group_action" value="1">Action</button></div>
		</td>
	</tr>	
		<?php
	}
	else 
	{
		?>
		<tr>
			<td colspan="100%">No data</td>
		</tr>
		<?php 
	}
	?>
	</table>
	</form>
	<?php if ( ! empty($listdata)) echo $this->common_model->common_paging($total_rows, $perpage); ?>
</div>

<script>
$(document).ready(function() {	
	
	$('#chk_parent_category_id').change(function(){
		if ($(this).is(':checked')) {
			$('#f_parent_category_id').css('visibility','visible');
		}
		else {
			$('#f_parent_category_id').css('visibility','hidden');
			$('#f_parent_category_id').find("option[value='0']").prop("selected",true);
		}
	});

	$('#f_parent_category_id').css('visibility','hidden');
	<?php if(isset($obj['parent_category_id']) && isset($category_id)) {?>
		$('#chk_parent_category_id').prop('checked',true);
		$('#chk_parent_category_id').bootstrapToggle('on');
	<?php } ?>

	<!-- Basic function -->

	$('#group_action').hide();
	$('.chkbox').click(function(){
		var count = $("[type='checkbox']:checked").length;
		if (count >= 1) {
			$('#group_action').show();
		} else if(count <= 0){
			$('#group_action').hide();
		}
	});

	
})
</script>