<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = 'Mutasi Bank';
$bread['member'] = 'Mutasi Bank';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
$do = $fund_id = NULL;

if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('fund_id')) $fund_id = $this->input->get('fund_id');

// function filter
function filter_string($str)
{
	$result = NULL;
	if (is_array($str)) {
		foreach ($str as $k => $v) {
			$result[$k] = dofilter($v);
		}
	} else {
		$result = dofilter($str);
	}
	return $result;
}

function dofilter($filter)
{
	$formula = array('"',"'",',');
	return str_replace($formula,'',$filter);
}

$newdata = array();

if ($_POST)
{
	$post = $_POST;
	if (isset($_FILES["file"]))
	{
		if ($_FILES["file"]["error"] > 0) 
		{
			$message['message'] = "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } 
		else
		{
			// Valid code
			
			// Save
			$upload_dir = __DIR__ . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR;
			$storagename = time().".csv";
			$movefile = move_uploaded_file($_FILES["file"]["tmp_name"], $upload_dir . $storagename);
			
			// debug
			if (isset($_GET['debug'])) 
			{
				echo $upload_dir . $storagename.BR;
				echo $movefile;
				die;
			}
			
			if ($movefile) 
			{
				// $message['message'] = "Stored in: " . $upload_dir . $storagename . "<br />";
				
				// Start export here 
				// $file = __DIR__ . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . 'mutasi_bca_september_2017.CSV';
				$file = $upload_dir . $storagename;

				$csv = array_map('str_getcsv', file($file));

				if ( ! empty($csv))
				{
					foreach($csv as $k => $rs) 
					{
						if (isset($rs[5]) && isset($rs[4]) && $rs[4] != '') 
						{
							$temp = $datacache = NULL;
							$temp = str_replace('/','-',$rs[0]).'-'.date('Y');
							$temp = explode('-',$temp);
							$temp = array_reverse($temp);
							$rs[0] = implode($temp,'-');
							// $rs[0] = strtotime($rs[0]);
							// $rs[0] = $rs[0].'/'.date('Y');
							$rs[5] = intval(floatval($rs[5]));
							
							$datacache['fund_id'] = 3;// rek bca
							$datacache['release_date'] = $rs[0];
							$datacache['type'] = strtoupper($rs[4]);
							$datacache['name'] = $rs[1];
							$datacache['notes'] = NULL;
							$datacache['amount'] = $rs[3];
							$datacache['last_credit'] = $rs[5];
							$datacache['creator_id'] = member_cookies('member_id');
							$datacache['creator_ip'] = getIP();
							$datacache['creator_date'] = getDatetime();
							// $datacache[''] = ;
							// debug($datacache);
							
							$newdata[] = filter_string($datacache);
						}
					}
					
					if ( ! empty($newdata)) 
					{
						// $message['message'] = "Save success " . count($newdata) . " saved <br />";
						$this->session->set_flashdata('message', getMessage(MESSAGE::SAVE));
						// $this->session->set_flashdata('message', getMessage("Save success " . count($newdata) . " saved <br />"));
						
						// Insert ignore
						$save_list = $this->fund_mutation_model->save_list($newdata);
						// debug($save_list);
						// die;
					}
				}
				// else 
				// {
					// $message['message'] = "No data";
					// $this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
				// }
			}
			else 
			{
			   // $message['message'] = "Failed upload";
			   $this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect(current_full_url());
			die;
		}
	}
}

// Clear all data
$clear_temp = base_url().'clear_temp_expense.php';
$this->curl->simple_get($clear_temp);

// BLOCK EXAMPLE
// $file = 'application/views/belajar/mutasi_bca_september_2017.csv';
// $file = __DIR__ . DIRECTORY_SEPARATOR . 'mutasi_bca_september_2017.CSV';

// $csv = array_map('str_getcsv', file($file));

// if ( ! empty($csv))
// {
	// foreach($csv as $k => $rs) 
	// {
		// if (isset($rs[5]) && isset($rs[4]) && $rs[4] != '') { 
			
			// $temp = $datacache = NULL;
			// $temp = str_replace('/','-',$rs[0]).'-'.date('Y');
			// $temp = explode('-',$temp);
			// $temp = array_reverse($temp);
			// $rs[0] = implode($temp,'-');
			// // $rs[0] = strtotime($rs[0]);
			// // $rs[0] = $rs[0].'/'.date('Y');
			// $rs[5] = intval(floatval($rs[5]));
			
			// $datacache['fund_id'] = 3;// rek bca
			// $datacache['release_date'] = $rs[0];
			// $datacache['type'] = strtoupper($rs[4]);
			// $datacache['name'] = $rs[1];
			// $datacache['notes'] = NULL;
			// $datacache['amount'] = $rs[3];
			// $datacache['last_credit'] = $rs[5];
			// // $datacache[''] = ;
			// // debug($datacache);
			
			// $newdata[] = filter_string($datacache);
		// }
	// }
	
	// // debug($newdata);
	// // die;
	
	// if (! empty($newdata)) 
	// {
		// $save_list = $this->fund_mutation_model->save_list($newdata);
		// debug($save_list);
		// die;
	// }
// }

// $list_report = $this->expense_model->get_report_all_month(array(
	// 'creator_id' => member_cookies('member_id')
// ));
$page = 1;
$limit = 0;
$i = 1;
// $offset = OFFSET;
$offset = 20;
if (is_numeric(get('page'))) $page = get('page');
if($page > 1) {
	$limit = ($page - 1 )* $offset ;
	$i = $limit+1;
}

$param = NULL;
$param['paging'] = TRUE;
$param['offset'] = $offset;
$param['limit'] = $limit;
$param['creator_id'] = member_cookies('member_id');
$param['order'] = 'release_date DESC, fund_mutation_id DESC';

$list_fund_mutation = $this->fund_mutation_model->get_list($param);
$total_rows = $list_fund_mutation['total_rows'];
$list_fund_mutation = $list_fund_mutation['data'];
// debug($list_fund_mutation);
// die;
?>

<div class="col-md-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"> </i> DESKRIPSI</b><br/>
		Fitur ini mengeksport data dari banking BCA kamu ke account.
	</div><hr/>
	
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>
	<form method="post" enctype="multipart/form-data">
		<input type="file" name="file" id="file" /><br/>
		<input class="btn btn-success" type="submit" name="submit" />
	</form>
	
	<hr/>
	<?php 
	if(!empty($list_fund_mutation))
	{
		?>
	<table class="table hover table-bordered">
	<tr class="alert bg-warning b talCnt">
		<td width="10px">#</td>
		<td width="150px">Tgl</td>
		<td>Name</td>
		<td width="10px">Type</td>
		<td width="70px">Amount</td>
		<td width="70px">Saldo</td>
	</tr>
	<?php
		foreach($list_fund_mutation as $key => $rs)
		{
		?>
	<tr class="talCnt f12">
		<td class=""><?php echo $i?></td>
		<td class=""><?php echo date('D d-m-Y',strtotime($rs['release_date']));?></td>
		<td class="talLft"><?php echo nl2br($rs['name'])?><?php if (isset($rs['notes']) && $rs['notes'] != '') echo BR.'Notes:<br/>'.$rs['notes']; ?></td>
		<td class=""><?php echo $rs['type']?></td>
		<td class="talRgt"><?php echo format_money($rs['amount'])?></td>
		<td class="talRgt"><?php echo format_money($rs['last_credit'])?></td>
	</tr>
		<?php
			$i++;
		}
	?>
	</table>
	<?php
		if (!empty($list_fund_mutation)) echo $this->common_model->common_paging($total_rows, $offset);
	
	?>
	<?php
	}
	else 
	{
		?>
	<div>No data</div>
		<?php 
	}
	?>
</div>