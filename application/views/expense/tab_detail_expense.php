<?php 
$member_id = member_cookies('member_id');
if (! $member_id) {
	echo "Get outta here"; die;
}

// REPORT
if ($this->input->get('start_date') && $this->input->get('start_date'))
{
	$start_date = date('d-m-Y',strtotime($this->input->get('start_date')));
	$end_date = date('d-m-Y',strtotime($this->input->get('end_date')));
}
else
{
	$param['start_date'] = $start_date = date("01-m-Y");
	// get end date
	// $param['end_date'] = $end_date = date("t-m-Y");

	// get current date
	$param['end_date'] = $end_date = date("d-m-Y", time());
	
	// see history by release date submit
	if ($this->input->get('release_date')) {
		$param_rd = explode('-',$get['release_date']);
		$param['start_date'] = $start_date = date("01-".$param_rd[1]."-".$param_rd[2]);
		
		// change to last day using date func
		$end_date = $param_rd[2]."-".$param_rd[1]."-1";
		$end_date = strtotime($end_date);
		$param['end_date'] = $end_date = date("t-m-Y", $end_date);
	}
}

$param['creator_id'] = member_cookies('member_id');
if (isset($start_date) && isset($end_date))
{
	$param['start_date'] = $start_date;
	$param['end_date'] = $end_date;

	$start_date_format = convert_date('D d M Y',$start_date);
	$end_date_format = convert_date('D d M Y',$end_date);
}
$param['is_detail'] = 0;
$param['is_increment'] = -1;
$param['report'] = 'expense';
$param['group_by'] = 'release_date';
$param['order_by'] = 'release_date DESC';
// $param['debug'] = 1;
$param['creator_id'] = member_cookies('member_id');

$obj_list_expense_detail = NULL;
$obj_list_expense_detail = $this->expense_model->get_list_report($param);
$obj_list_expense_detail = $obj_list_expense_detail['data'];
?>


<table class='table hover table-bordered' id='show_detail_expense' style="">
<tr>
	<td colspan="5" class="bg-info talCnt">DETAIL EXPENSE REPORT</td>
</tr>
<tr>
	<td>#</td>
	<td>Day</td>
	<td>Date</td>
	<td>Expense</td>
	<td>Income</td>
</tr>
<?php
$sum_income = $sum_expense = $total = 0;
if ( ! empty($obj_list_expense_detail)) 
{

$i = 1;
$total_expense_day = array();
foreach ($obj_list_expense_detail as $key => $rs) {
//$id = $rs['expense_id'];
?>
<tr>
	<td><?php echo $i?></td>
	<td><?php echo date('d',strtotime($rs['release_date']))?></td>
	<td><?php echo date('D d-m-Y', strtotime($rs['release_date']))?><br/>
	<?php
	$list_expense_day = array();
	$tmp1['release_date'] = $rs['release_date'];
	$tmp1['is_increment'] = -1;
	$tmp1['order_by'] = 'e.expense_id DESC';
	$tmp1['is_detail'] = 1;
	$tmp1['creator_id'] = member_cookies('member_id');
	$list_expense_day = $this->expense_model->get_list_report($tmp1);
	$list_expense_day = $list_expense_day['data'];
	if (!empty($list_expense_day))
	{
		?>
		<table class="table table-bordered table-hover wdtFul" align="right">
		<?php
		foreach($list_expense_day as $day)
		{
			// debug($day);
			$cashback = NULL;
		?>
		<tr class="bg-info">
		<td class=""><a href="<?php echo base_url().'expense/report?do=edit&expense_id='.$day['expense_id'];?>" title="Edit data"><i class="fa fa-edit fa-lg"></i></a>
			<?php
			echo '<b>'.strtoupper($day['category_name']).'</b>';
			if (isset($day['location_name'])) echo " <b class='clrRed b'><i class='fa fa-car'></i> ".$day['location_name']."</b>";
			echo ' - '.$day['title']
			?>
			<?php
			// PRINT STATUS AND DATE REMINDER HERE
			if (isset($day['is_get_notif']) && $day['is_get_notif'] == 1) 
			{
				//echo " jalan";die;

				// 15 minutes for cron
				$disable_reminder = NULL;
				if (time() >= strtotime($day['reminder_date']) && time() < (strtotime($day['reminder_date']) + 15*60))
				{
					$disable_reminder = " <a href='?do=disable_notification&expense_id=".$day['expense_id']."' class='btn btn-danger btn-xs' title='Nonaktifkan reminder ini'> Disable notif</a>";
					echo BR.BR."<i class='clrGrn b'>Reminder active</i>";
					if (isset($day['reminder_date'])) echo " on ".date('D d-m-Y H:i',$day['email_blast_date']);
				}
				elseif (time() >= strtotime($day['reminder_date']))
				{
					$disable_reminder = "";
					echo BR.BR."<i class='clrRed b'>Reminder expired</i>";
					if (isset($day['reminder_date'])) echo " on ".date('D d-m-Y H:i',$day['email_blast_date']);
				}

				//if (isset($day['reminder_date'])) echo " on ".date('D d-m-Y H:i',strtotime($day['reminder_date']));

				echo $disable_reminder;
			}
			// echo " <i> on ".date("H:i",strtotime($day['expense_creator_date']))."</i> ";

			$is_paid = "<i class='fa fa-check clrRed' title='Tagihan belum dibayar'></i>";
			if ($day['is_paid'] == 1) $is_paid = "<i class='fa fa-check-circle-o clrGrn' title='Tagihan sudah dibayar'></i>";
			// debug($day,1);
			if (isset($day['cashback']) && $day['cashback'] != '' && $day['cashback'] > 0) $cashback = $day['cashback'];

			if (isset($day['gmap_address'])) echo '<div class="text-info" style="padding:15px 0 0 10px"><i class="fa fa-map-marker"></i> '.$day['gmap_address'].'</div>';

			?>
			with <?php if (isset($day['fund_type'])) echo get_fund_type($day['fund_type']).' '.$day['fund_name']; ?>
			(<?php echo $is_paid?>)

			<?php
			// Show detail expense notes
			// debug($day);die;
			if (isset($day['expense_notes'])) {
				echo BR."Notes".BR.nl2br($day['expense_notes']);
			}
			?>
		</td>
		<td width="50px" class="talRgt"><?php echo format_money($day['amount'],'')?><?php if (isset($cashback)) echo '<div style="padding-top:12px;" class="text-success b">+' . format_money($cashback,'') . '<small>Cashback</small></div>'; ?></td>
		<td width="20px">
		<a href="<?php echo base_url().'expense/report?do=delete&expense_id='.$day['expense_id'];?>" title="Delete data"><i class="fa fa-times-circle fa-lg clrRed"></i></a>
		</td>
		</tr>
			<?php
		}
		?>
		</table>
	<?php
	}
	?>
	</td>
	<td><?php echo format_money(str_replace('.','',$rs['amount']),'')?></td>
	<td><?php if (isset($cashback)) echo format_money(str_replace('.','',$cashback),'')?></td>
</tr>
<?php
	// FILL KEY ARRAY WITH DATE FOR REPORT USE
	if (left(date('d',strtotime($rs['release_date'])),1) == 0) {
		// STRIP 0 FROM 01 to 09
		$total_expense_day[right(date('d',strtotime($rs['release_date'])),1)] = str_replace('.','',$rs['amount']);
	} else {
		// NORMAL DATE > 10 like 10,11,20
		$total_expense_day[date('d',strtotime($rs['release_date']))] = str_replace('.','',$rs['amount']);
	}

	$i++;
	if (isset($rs['amount']) && $rs['amount'] != '' && $rs['amount'] > 0) $sum_expense += str_replace('.','',$rs['amount']);
	if (isset($cashback)) $sum_income += str_replace('.','',$cashback);
} // end foreach

} // end if empty obj_list_expense_detail
else 
{
	?>
	<tr>
	<td colspan="100%">Data tidak ditampilkan saat edit</td>
	</tr>
	<?php
}
?>
<tr>
	<td colspan="3">TOTAL</td>
	<td class="text-danger b"><?php echo format_money($sum_expense) ?></td>
	<td class="text-success b"><?php echo format_money($sum_income) ?></td>
</tr>
</table>
