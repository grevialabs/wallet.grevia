<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = TREASURE;
$bread['member'] = TREASURE;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
$do = $treasure_id = NULL;

if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('treasure_id')) $treasure_id = $this->input->get('treasure_id');

// SAVE HERE
if ($_POST && isset($_POST['btn_insert']))
{
	$post = array();
	$post = $_POST;
	unset($post['btn_insert']);
	
	if (isset($post['release_date']) && $post['release_date']!='') $post['release_date'] = date('Y-m-d',strtotime($post['release_date']));
	if (isset($post['reminder_date']) && $post['reminder_date']!='') $post['reminder_date'] = date('Y-m-d',strtotime($post['reminder_date']));
	//debug($post);die;
	$insert = $this->treasure_model->save($post);
	
	if ($insert) {
		$this->session->set_flashdata('message', getMessage(MESSAGE::SAVE));
	} else {
		$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
	}
	redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
}

// UPDATE
if ($_POST && isset($_POST['btn_update']))
{
	$post = array();
	$post = $_POST;
	unset($post['btn_update']);
	
	if (isset($post['release_date']) && $post['release_date']!='') $post['release_date'] = date('Y-m-d',strtotime($post['release_date']));
	if (isset($post['reminder_date']) && $post['reminder_date']!='') $post['reminder_date'] = date('Y-m-d',strtotime($post['reminder_date']));
	
	$get['member_id'] = member_cookies('member_id');
	$get['treasure_id'] = $treasure_id;
	$obj_treasure = $this->treasure_model->get($get);
	
	if (!empty($obj_treasure)) 
	{
		// debug($post);die;
		$update = $this->treasure_model->update($treasure_id, $post);
		
		if ($update) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	}
	else
	{
		// DATA NOT FOUND
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	}
	$obj_treasure = NULL;
	
}

// DELETE TREASURE
if ($do == "delete" && is_numeric($treasure_id)) 
{
	$treasure = $this->treasure_model->get(array('treasure_id' => $treasure_id, 'creator_id' => member_cookies('member_id')));
	if (!empty($treasure)) 
	{
		$delete = $this->treasure_model->delete($treasure_id);
		if ($delete) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::DELETE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	} 
	else 
	{
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	}
}

function treasure_type($type)
{
	if ($type == 1) 
	{
		$ret = "Rekening";
	}
	else if ($type == 2) 
	{
		$ret = "Cash";
	}
	return $ret;
}

$start_date = $end_date = NULL;
$param = array();

if ($this->input->get('start_date') && $this->input->get('start_date')) 
{
	$start_date = date('d-m-Y',strtotime($this->input->get('start_date')));
	$end_date = date('d-m-Y',strtotime($this->input->get('end_date')));
}
else
{
	$param['start_date'] = $start_date = date("01-m-Y");
	$param['end_date'] = $end_date = date("t-m-Y");
}
	
$param['creator_id'] = member_cookies('member_id');
if (isset($start_date) && isset($end_date)) 
{
	$param['start_date'] = $start_date;
	$param['end_date'] = $end_date;
}
$param['is_detail'] = 0;
$param['is_increment'] = 0;
$param['report'] = 'expense';
$param['group_by'] = 'release_date';
$obj_list_expense_detail = $this->expense_model->get_list_report($param);
$obj_list_expense_detail = $obj_list_expense_detail['data'];
?>

<div class="col-md-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"> </i> DESKRIPSI</b><br/>
		Fitur ini berguna untuk mendata aset anda. (terpisah dari <i>expense tracker</i>)
	</div><hr/>
	
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>
	<?php if ($do != "insert" && $do != "edit") { ?>
	<a class="btn btn-success btn-sm" href="?do=insert">+ NEW TREASURE</a><br/><br/>
	<?php } ?>
		
	<?php 
	$do = $test_id = NULL;
	if ($this->input->get('do')) $do = $this->input->get('do');
	if ($this->input->get('test_id') && is_numeric($this->input->get('test_id'))) $test_id = $this->input->get('test_id');
	

	if ($do == 'insert' || $do == "edit")
	{
		if (isset($treasure_id)) $obj = $this->treasure_model->get(array(
			'treasure_id' => $treasure_id,
			'creator_id' => member_cookies('member_id')
		));
		// debug($obj);
		?>
		<form method="post">
		<table class='table hover table-bordered'>
			<tr>
				<td colspan="2" class="bg-info talCnt">New Aset</td>
			</tr>
			<!--
			<tr>
				<td>Type</td>
				<td>
				<input type="radio" value="1" id="radio_category1" name="f_treasure_type" <?php if (isset($obj['treasure_type']) && $obj['treasure_type'] == 1) echo "checked" ?> required /> <label for="radio_category1">Rekening</label> &nbsp;
				<input type="radio" value="2" id="radio_category2" name="f_treasure_type" <?php if (isset($obj['treasure_type']) && $obj['treasure_type'] == 2) echo "checked" ?> required /> <label for="radio_category2">Cash</label>
				</td>
			</tr>
			-->
			<tr>
				<td width="180px">Name</td>
				<td><input type="text" class="input wdtFul" name="f_name" placeholder="ex: Deposito / Tabungan" value="<?php if (isset($obj['name'])) echo $obj['name']?>" required /></td>
			</tr>
			<tr>
				<td>Nilai Aset<br/><b class="f12">*Dalam nominal rupiah</b></td>
				<td><input type="text" class="input numeric" name="f_amount" placeholder="10000" value="<?php if (isset($obj['amount'])) echo $obj['amount']?>" required /></td>
			</tr>
			<tr>
				<td>Notes<br/><b class="f12">*opsional</b></td>
				<td><textarea name="f_notes" class="input wdtFul" placeholder="catatan" rows="2"><?php if (isset($obj['notes'])) echo $obj['notes']?></textarea></td>
			</tr>
			<!--
			<tr class="hide">
				<td>Reminder Date *opsional</td>
				<td><input type="text" class="input datepicker" placeholder="Tgl pengingat" name="f_reminder_date" value="<?php if (isset($obj['reminder_date'])) echo date('d-m-Y',strtotime($obj['reminder_date']))?>"/> Email reminder will send each day, for every expense not paid</td>
			</tr>
			-->
			<tr>
				<td colspan="2" class="talCnt">
				<?php if ($do == "edit") { ?> 
				<button type="submit" name="btn_update" value="1" class="btn btn-success btn-sm">UPDATE</button></td>
				<?php } else { ?> 
				<button type="submit" name="btn_insert" value="1" class="btn btn-success btn-sm">SUBMIT</button></td>
				<?php }?>
			</tr>
		</table>
		</form>
		

	<?php 
	}
	else 
	{
	?>
		<?php
		$param['is_detail'] = 0;
		$param['order'] = ' is_increment ASC, name ASC';

		$list_category = $this->category_model->get_list($param);
		$list_category = $list_category['data'];
		
		$param2 = array();
		$param2['creator_id'] = member_cookies('member_id');
		$list_treasure = $this->treasure_model->get_list($param2);
		$list_treasure = $list_treasure['data'];
		
		?>
		<form method="post">
		
		<table class="table table-bordered">
			<tr class="bg-success">
				<td>#</td>
				<td>Name</td>
				<td>Value Amount</td>
				<!--
				<td>Due date</td>
				<td>Count date</td>
				-->
				<td>Option</td>
			</tr>
		<?php 
		if (!empty($list_treasure))
		{
			$grandtotal = 0;
			foreach ($list_treasure as $key => $rs)
			{
				$grandtotal += $rs['amount'];
				$countdown_date = NULL;
				if (isset($rs['reminder_date'])) 
				{
					$countdown_date = strtotime($rs['reminder_date']) - time();
					
					//bigger than 1 day
					
					if ($countdown_date > 2592000) 
					{
						$countdown_date = 'Berakhir dalam '.ceil($countdown_date / 2592000). ' bulan';
					}
					else if ($countdown_date > 86400) 
					{
						$countdown_date = '<span class="clrGrn b">Berakhir dalam '.ceil($countdown_date / 86400). ' hari</span>';
					}
					else if ($countdown_date > 3600) 
					{
						$countdown_date = '<span class="clrRed b">Berakhir dalam '.ceil($countdown_date / 3600). ' jam</span>';
					}
					else 
					{
						$countdown_date = '<span class="clrBlu b">Sudah berakhir </span>';
					}
				}
				
		?>
			<tr>
				<td><?php echo $key+1?></td>
				<td><?php echo $rs['name']; if (isset($rs['notes'])) echo BR.BR.'<b>Notes:</b>'.BR.nl2br($rs['notes']); ?>
				<?php 
				echo BR.BR;
				if (isset($rs['creator_date'])) echo "<div style=float:left class='b clrBlu'>Created ".BR.date('D d-m-Y H:i', strtotime($rs['creator_date']))."</div>";
				if (isset($rs['editor_date'])) echo "<div style=float:right class='b clrGrn'>Update ".BR.date('D d-m-Y H:i', strtotime($rs['editor_date']))."</div>";
				
				?>
				</td>
				<td class="talRgt"><?php echo format_money($rs['amount']); ?></td>
				<!--
				<td><?php echo $rs['reminder_date']; ?></td>
				<td><?php echo $countdown_date; ?></td>
				-->
				<td class="talCnt">
				<a class="btn btn-success btn-xs br" href="<?php echo current_url().'?do=edit&treasure_id='.$rs['treasure_id']; ?>"><?php echo UPDATE?></a> 
				<a class="btn btn-danger btn-xs br" href="<?php echo current_url().'?do=delete&treasure_id='.$rs['treasure_id']?>">Delete</a></td>
			</tr>
		<?php
			}
			?>
			<tr class="b">
				<td colspan="2">Grandtotal</td>
				<td colspan="4"><?php echo format_money($grandtotal)?></td>
			</tr>
			<?php
		}
		else 
		{
			?>
			<tr>
				<td colspan="6">No Data</td>
			</tr>
			<?php 
		}
		?>
		</table>
		<?php
	}
	?>
</div>