<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = REPORT;
$bread['member'] = REPORT;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;

$start_date = $end_date = $is_get_notif = NULL;
$param = array();

$do = $expense_id = NULL;
if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('expense_id') && is_numeric($this->input->get('expense_id'))) $expense_id = $this->input->get('expense_id');
	
if ($_GET)
{
	// DELETE EXPENSE
	if ($do == "delete" & is_numeric($expense_id)) 
	{
		$temp['expense_id'] = $expense_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj_delete = $this->expense_model->get($temp);
		
		if (!empty($obj_delete))
		{
			$obj_delete = $this->expense_model->delete($expense_id);
			if ($obj_delete)
			{
				redirect(current_url().'?delete_success=1');
			} else {
				redirect(current_url().'?delete_success=0');
			}
		}
	}
	
	// DISABLE NOTIFICATION
	if ($do == "disable_notification" & is_numeric($expense_id)) 
	{
		$temp['expense_id'] = $expense_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj_disable = $this->expense_model->get($temp);
		
		if (!empty($obj_disable))
		{
			$param_disabled = NULL;
			$param_disabled['is_get_notif'] = 0;
			$obj_disable = $this->expense_model->update($expense_id,$param_disabled);
			if ($obj_disable)
			{
				redirect(current_url().'?disable_success=1');
			} else {
				redirect(current_url().'?disable_success=0');
			}
		}
	}
}

if ($_POST)
{
	$post = $param_insert = $param_update = array();
	$post = $_POST;
	
	// INSERT
	if (isset($post['hdn_insert']))
	{
		$param_insert['category_id'] = $post['category_id'];
		$param_insert['title'] = $post['title'];
		$param_insert['amount'] = $post['amount'];
		$param_insert['notes'] = $post['notes'];
		
		// SET AS DEFAULT
		$param_insert['is_paid'] = $param_insert['is_get_notif'] = 0;
		if (isset($post['is_paid']) && $post['is_paid'] == 1) $param_insert['is_paid'] = 1;
		if (isset($post['is_get_notif']) && $post['is_get_notif'] == 1) $param_insert['is_get_notif'] = 1;
	
		if (isset($post['reminder_date'])) $param_insert['reminder_date'] = date('Y-m-d',strtotime($post['reminder_date'])).' '.$post['reminder_time'].':00:00'; 
		if (isset($post['reminder_minus_day'])) $param_insert['reminder_minus_day'] = $post['reminder_minus_day'];
		
		if (isset($post['release_date']) && $post['release_date']!='') $param_insert['release_date'] = date('Y-m-d',strtotime($post['release_date']));
		
		$param_insert['creator_id'] = member_cookies('member_id');
		$insert = $this->expense_model->save($param_insert);
		if ($insert) redirect(current_url().'?do=insert&success=1');
		
		($insert)?$message['message'] = MESSAGE::SAVE : $message['message'] = MESSAGE::ERROR;
		$message['message'] = getMessage($message['message']);
	}
	
	// UPDATE
	if (isset($post['hdn_update']) && is_numeric($expense_id))
	{
		$temp3['expense_id'] = $expense_id;
		$temp3['creator_id'] = member_cookies('member_id');
		$obj = $this->expense_model->get($temp3);
		
		if (!empty($obj))
		{
			$param_update['category_id'] = $post['category_id'];
			$param_update['release_date'] = date('Y-m-d',strtotime($post['release_date']));
			$param_update['title'] = $post['title']; 
			
			$param_update['amount'] = $post['amount'];
			$param_update['notes'] = $post['notes'];
			
			// SET AS DEFAULT
			$param_update['is_paid'] = $param_update['is_get_notif'] = 0;
			if (isset($post['is_paid']) && $post['is_paid'] == 1) $param_update['is_paid'] = 1;
			if (isset($post['is_get_notif']) && $post['is_get_notif'] == 1) $param_update['is_get_notif'] = 1;
		
			if (isset($post['reminder_date'])) $param_update['reminder_date'] = date('Y-m-d',strtotime($post['reminder_date'])).' '.$post['reminder_time'].':00:00';
			if (isset($post['reminder_minus_day'])) $param_update['reminder_minus_day'] = $post['reminder_minus_day'];
			
			$param_update['creator_id'] = member_cookies('member_id'); 
			//debug($param_update);die;
			$update = $this->expense_model->update($expense_id, $param_update);
			($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
			$message['message'] = getMessage($message['message']);
		}
		else 
		{
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}



// REPORT 
if ($this->input->get('start_date') && $this->input->get('start_date')) 
{
	$start_date = date('d-m-Y',strtotime($this->input->get('start_date')));
	$end_date = date('d-m-Y',strtotime($this->input->get('end_date')));
}
else
{
	$param['start_date'] = $start_date = date("01-m-Y");
	// get end date
	// $param['end_date'] = $end_date = date("t-m-Y");
	
	// get current date
	$param['end_date'] = $end_date = date("d-m-Y", time());
}
	
$param['creator_id'] = member_cookies('member_id');
if (isset($start_date) && isset($end_date)) 
{
	$param['start_date'] = $start_date;
	$param['end_date'] = $end_date;
}
$param['is_detail'] = 0;
$param['is_increment'] = 0;
$param['report'] = 'expense';
$param['group_by'] = 'release_date';
$param['order_by'] = 'release_date DESC';
$param['creator_id'] = member_cookies('member_id');
$obj_list_expense_detail = $this->expense_model->get_list_report($param);
$obj_list_expense_detail = $obj_list_expense_detail['data'];
?>

<div class="col-md-12">
	<h1 class=""><?php echo $MODULE?></h1><br/>
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	
	if (isset($_GET['delete_success']))
	{ 
		$str = NULL;
		if ($_GET['delete_success'] == 1) 
		{
			$str = 'Delete success';
		}
		else if ($_GET['delete_success'] == 0) 
		{
			$str = 'Delete failed';
		}
		echo message($str).BR;
	}
	
	?>
	
	<?php 
	
	if ($do == 'showresult' && is_numeric($expense_id)) 
	{
		
	}
	else 
	{
	?>
	<form method="get">
		FILTER <input type="text" class="input datepicker" placeholder="start date" name="start_date" value="<?php echo $start_date ?>" />
		<input type="text" class="input datepicker" placeholder="end date" name="end_date" value="<?php echo $end_date ?>"/>
		<button name="" value="" class="btn btn-default btn-sm">SUBMIT</button>
	</form><br/>
	
	<?php if ($do != "insert") { ?>
	<a href="?do=insert" class="btn btn-success">NEW EXPENSE</a><br/><br/>
	<?php } ?>
	<!-- FORM INPUT -->
	<?php 
	
	if ($do == "insert" || $do == "edit")
	{
		if ($do == "edit" && is_numeric($expense_id))
		{
			$temp1['creator_id'] = member_cookies('member_id');
			$temp1['expense_id'] = $expense_id;
			$obj = $this->expense_model->get($temp1);
			if (isset($obj['is_get_notif'])) $is_get_notif = $obj['is_get_notif']; 
		}

		$param_category['creator_id'] = member_cookies('member_id');
		$param_category['is_detail'] = 0;
		$param_category['order'] = ' is_increment ASC, name ASC';

		$list_category = $this->category_model->get_list($param_category);
		$list_category = $list_category['data'];
	?>
	
	<form method="post">
	<table id="table_input" class='table hover table-bordered bg-warning'>
		<tr>
			<td colspan="2" class="bg-danger talCnt">DETAIL EXPENSE REPORT BY CATEGORY</td>
		</tr>
		<tr>
			<td>Category</td>
			<td><?php 
			$i = 1;
			$total = $subtotal_expense =  0;
			if (!empty($list_category)) 
			{
				foreach ($list_category as $key => $rs) {
					if ($key > 0 && $rs['is_increment'] != $list_category[$key-1]['is_increment']) //echo "<hr>";
			?>
				<!--
				<label for="f_category_id[<?php echo $rs['category_id']?>]" style="font-weight:normal;height:15px"><div style="width:140px; float:left"><input type="radio" id="f_category_id[<?php echo $rs['category_id']?>]" name="f_category_id" value="<?php echo $rs['category_id']?>"> <?php echo $rs['name']?></div></label>
				-->
			<?php 
				}
			}
			?>
			<select name="f_category_id" class="padMed wdtFul chosen-select">
			<optgroup label="Expense">
			<?php 
			if (!empty($list_category)) 
			{
				foreach ($list_category as $key => $rs)
				{
					if ($rs['is_increment'] == 0)
					{
				?>
				<option value="<?php echo $rs['category_id']?>" <?php if (isset($obj['category_id']) && $rs['category_id'] == $obj['category_id']) echo "selected=true"; ?>><?php echo $rs['name']?></option>
				<?php
					}
				}
			}
			?>
			</optgroup>
			
			<optgroup label="Income">
			<?php 
			if (!empty($list_category)) 
			{
				foreach ($list_category as $key => $rs)
				{
					if ($rs['is_increment'] == 1)
					{
				?>
				<option value="<?php echo $rs['category_id']?>" <?php if (isset($obj['category_id']) && $rs['category_id'] == $obj['category_id']) echo "selected=true"; ?>><?php echo $rs['name']?></option>
				<?php
					}
				}
			}
			?>
			</optgroup>
			</select>
			</td>
		</tr>
		<tr>
			<td>Release Date</td>
			<td><input type="text" class="input datepicker" name="f_release_date"  placeholder="Tgl Input Expense" value="<?php if (isset($obj['release_date'])) echo date('d-m-Y',strtotime($obj['release_date']));else echo date('d-m-Y')?>" required/></td>
		</tr>
		<tr>
			<td width="180px">Expense Name</td>
			<td><input type="text" class="input wdtFul" name="f_title" placeholder="Nama Pengeluaran" value="<?php if (isset($obj['title'])) echo $obj['title']?>" required /></td>
		</tr>
		<tr>
			<td>Amount</td>
			<td><input type="text" class="input numeric" name="f_amount" placeholder="Jumlah pengeluaran" value="<?php if (isset($obj['amount'])) echo $obj['amount']?>" size="40" required /> </td>
		</tr>
		<tr>
			<td>Notes*</td>
			<td><textarea name="f_notes" class="wdtFul" placeholder="Catatan" rows="5"><?php if (isset($obj['notes'])) echo $obj['notes']?></textarea></td>
		</tr>
		<tr>
			<td><label for="f_is_paid">IsPaid</label></td>
			<td><input type="checkbox" name="f_is_paid" id="f_is_paid" value="1" <?php if (isset($obj['is_paid']) && $obj['is_paid'] == 1) echo 'checked="true"'; ?>" checked="true"/></td>
		</tr>
		<!--
		<tr>
			<td>Type</td>
			<td><input type="radio" name="f_is_increment" value="0" id="f_is_increment0"/><label for="f_is_increment0"><i class="fa fa-minus fa-4x"></i></label> <input type="radio" name="f_is_increment" id="f_is_increment1" value="1"/> <label for="f_is_increment1"><i class="fa fa-plus fa-4x"></i></label> </td>
		</tr>
		-->
		<tr>
			<td>Reminder</td>
			<td>
			 <label for="is_reminder0"><input type="radio" name="is_reminder" value="0" id="is_reminder0" <?php if (!isset($obj['is_get_notif']) || $obj['is_get_notif'] == 0) echo " checked "; ?> /> No</label> &nbsp;&nbsp;
			<label for="is_reminder1"><input type="radio" name="is_reminder" value="1" id="is_reminder1" <?php if (isset($obj['is_get_notif']) && $obj['is_get_notif'] == 1) echo " checked "; ?> /> Yes</label>
			</td>
		</tr>
		<tr class="reminder">
			<td>Email Reminder active</td>
			<td><input type="checkbox" value="1" name="f_is_get_notif" <?php if (isset($obj['is_get_notif']) && $obj['is_get_notif'] == 1) echo " checked "; ?> /></td>
		</tr>
		<tr class="reminder">
			<td>Reminder Billing Date*</td>
			<td><input type="text" class="input datepicker" name="f_reminder_date" placeholder="Tgl pengingat" size="14" value="<?php if (isset($obj['reminder_date'])) echo date('d-m-Y',strtotime($obj['reminder_date']));?>"/> 
			<select class="input" name="f_reminder_time">
			<?php 
			for ($hour=0;$hour<=24;$hour++) { 
				if ($hour <= 9) $hour = "0".$hour;
				
				// CHECK VALUE EXACT TIME
				$checked_hour = NULL;
				if (isset($obj['reminder_date']) && date('H:00',strtotime($obj['reminder_date'])) == $hour.":00") $checked_hour = " selected ";
			?>
			<option value="<?php echo $hour?>" <?php echo $checked_hour?>><?php echo $hour.':00'; ?></option>
			<?php 
			}
			?>
			</select>
			on 
			<select class="input" name="f_reminder_minus_day">
			<?php 
			$arr_day = array('0','1','2','3','4','5','6','7','8','9','10','14','21','30','60');
			
			foreach ($arr_day as $key_day => $day) { 
				$str_reminder = 'H day';
				if ($day >= 1)
					$str_reminder = 'H - '.$day.' day';
			?>
			<option value="<?php echo $day?>" <?php if (isset($obj['reminder_minus_day']) && $obj['reminder_minus_day'] == $day) echo " selected "; ?> ><?php echo $str_reminder ?></option>
			<?php 
			}
			?>
			</select>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="talCnt">
			<?php if (count($list_category) < 1) { ?>
			<div class="alert bg-danger">Ooops, anda belum memiliki kategori pengeluaran<br/>Silakan input kategori pengeluaran disini <a href="<?php echo base_url().'expense/category?do=insert';?>">Input kategori</a></div>
			<?php } else if ($do == "insert") { ?>
			<input type="hidden" name="hdn_insert" value="1" />
			<button class="btn btn-success">SAVE</button>
			<?php } else { ?>
			<input type="hidden" name="hdn_update" value="1" />
			<button class="btn btn-success">UPDATE</button>
			<?php } ?>
			</td>
		</tr>
	</table>
	</form><br/>
	
	<?php 
	}
	?>
	<!-- FORM INPUT -->
	hahaha<br/>
	<?php 
	
	$tmp_treasure = array();
	$tmp_treasure['creator_id'] = member_cookies('member_id');
	$list_treasure = $this->treasure_model->get_list($tmp_treasure);
	$list_treasure = $list_treasure['data'];
	// debug($list_treasure);
	
	if (!empty($list_treasure)) 
	{
		?>
		<table class="table table-bordered">
		<tr>
			<td>#</td>
			<td>Nama</td>
			<td>Notes</td>
		</tr>
		<?
		$total_amount_treasure = 0;
		foreach ($list_treasure as $keyt => $asset) 
		{
			?>
			<tr>
			<td><?php echo $keyt+1; ?></td>
			<td><b><?php echo get_treasure_type($asset['treasure_type']) .'</b> - '.$asset['name']?></td>
			<td><?php echo format_money($asset['amount']).HR.nl2br($asset['notes'])?></td>
			</tr>
			<?php 
			$total_amount_treasure += $asset['amount']; 
		}
		?>
		<tr>
			<td colspan="2">Total</td>
			<td><?php echo format_money($total_amount_treasure)?></td>
		</tr>
		</table>
		<?php 
	}
	
	?>
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>

	<div id="chart_round" style="width: 100%; height: 400px;"></div>
	<div id="chart_graph_daily" style="width: 100%; height: 400px;"></div>
	<br/>
	<?php 
	$param['is_detail'] = 1;
	$param['is_increment'] = 1;
	$param['is_budget'] = 1;
	$list_budget = $this->expense_model->get_list_report($param);
	$list_budget = $list_budget['data'];
	if (!empty($list_budget))
	{
	?>
		<table class="table table-bordered hover">
		<tr>
			<td colspan="3" class="bg-primary talCnt">BUDGET</td>
		</tr>
		<?php 
		//var_dump($list_budget);die;
		$i = 1;
		foreach ($list_budget as $key => $budget)
		{
			?>
		<tr>
			<td>#<?php echo $i ?></td>
			<td><?php echo $budget['category_name']?></td>
			<td><?php echo $budget['amount']?></td>
		</tr>
			<?php
		}
		?>
		<tr>
			<td colspan="2">TOTAL</td>
			<td></td>
		</tr>
			
		</table>
	<?php 
	}
	?>
	<hr>
	
	<table class="table table-bordered hover">
		<tr>
			<td colspan="4" class="bg-success talCnt">INCOME THIS MONTH</td>
		</tr>
	<?php 
	$param['group_by'] = NULL;
	$param['is_budget'] = 0;
	$param['is_increment'] = 1;
	$param['is_detail'] = 1;
	$list_income = $this->expense_model->get_list_report($param);
	$list_income = $list_income['data'];
	$total_income = 0;
	
	if (!empty($list_income))
	{
		foreach ($list_income as $key => $income)
		{
			?>
		<tr>
			<td>#<?php echo $key+1 ?></td>
			<td><?php echo date('D d-M-Y',strtotime($income['release_date']))?></td>
			<td><?php echo $income['category_name']?></td>
			<td><?php echo format_money(str_replace('.','',$income['amount']))?></td>
		</tr>
			<?php
			$total_income += str_replace('.','',$income['amount']);
		}
		?>
		<tr>
			<td colspan="3">TOTAL</td>
			<td><?php echo format_money($total_income)?></td>
		</tr>
	<?php 
	}
	else
	{
		?>
		<tr class="talCnt">
			<td colspan="4">No data</td>
		</tr>
		<?php 
	}
	?>
	</table>
	<br/><br/>
	<?php
		$obj = $this->expense_model->get_report($param);
		if (is_filled($obj['total_income']))
		{
			?>
			<?php //echo $obj['total_income']." - ".$obj['total_expense']." = ".$obj['total_revenue'] ?><hr/>
			
			<div class="alert bg-success col-sm-6 talCnt" style="font-size:35px">INCOME <i class="fa fa-dollar"></i><hr class="clrBlk">
					<?php echo format_money($obj['total_income'])?></div>
			<div class="alert bg-primary col-sm-6 talCnt" style="font-size:35px">EXPENSE <i class="fa fa-bank"></i><hr class="clrBlk">
					<?php echo format_money($obj['total_expense'])?></div>
			
			<!--
			<table class="table table-bordered">
				<tr style="font-size:40px">
					<td class="alert bg-success" width="50%"><div class="talCnt">INCOME <i class="fa fa-dollar"></i><hr class="clrBlk">
					<?php echo format_money($obj['total_income'])?></div></td>
					<td class="alert bg-primary" width="50%">
					<div class="talCnt">EXPENSE <i class="fa fa-bank"></i><hr>
					<?php echo format_money($obj['total_expense'])?></div></td>
				</tr>
			</table>
			-->
			<?php
		}
	?>
	
	<table class='table hover table-bordered'>
		<tr>
		<td colspan="4" class="bg-danger talCnt">DETAIL EXPENSE REPORT</td>
		</tr>
		<tr>
		<td>#</td>
		<td>Day</td>
		<td>Date</td>
		<td>Expense</td>
		</tr>
		<?php 
		$i = 1;
		$total = 0;
		$total_expense_day = array();
		foreach ($obj_list_expense_detail as $key => $rs) { 
		//$id = $rs['expense_id'];
		?>
		<tr>
		<td><?php echo $i?></td>
		<td><?php echo date('d',strtotime($rs['release_date']))?></td>
		<td><?php echo date('D d-m-Y', strtotime($rs['release_date']))?><br/>
		<?php 
		$list_expense_day = array();
		$tmp1['release_date'] = $rs['release_date'];
		$tmp1['is_increment'] = 0;
		$tmp1['is_detail'] = 1;
		$tmp1['creator_id'] = member_cookies('member_id');
		$list_expense_day = $this->expense_model->get_list_report($tmp1);
		$list_expense_day = $list_expense_day['data'];
		if (!empty($list_expense_day)) 
		{
			?>
			<table class="table table-bordered table-hover wdtFul" align="right">
			<?php
			foreach($list_expense_day as $day) 
			{
			?>
			<tr class="bg-warning">
			<td class=""><a href="<?php echo current_url().'?do=edit&expense_id='.$day['expense_id'];?>" title="Edit data"><i class="fa fa-edit fa-lg"></i></a> 
			<?php echo '<b>'.strtoupper($day['category_name']).'</b> - '.$day['title']?>
			<?php 
			// PRINT STATUS AND DATE REMINDER HERE
			if (isset($day['is_get_notif']) && $day['is_get_notif'] == 1) { 
				//echo " jalan";die;
				
				// 15 minutes for cron
				$disable_reminder = NULL;
				if (time() >= strtotime($day['reminder_date']) && time() < (strtotime($day['reminder_date']) + 15*60)) 
				{
					$disable_reminder = " <a href='?do=disable_notification&expense_id=".$day['expense_id']."' class='btn btn-danger btn-xs' title='Nonaktifkan reminder ini'> Disable notif</a>";
					echo BR.BR."<i class='clrGrn b'>Reminder active</i>";
					if (isset($day['reminder_date'])) echo " on ".date('D d-m-Y H:i',$day['email_blast_date']);
				}
				elseif (time() >= strtotime($day['reminder_date'])) 
				{
					$disable_reminder = "";
					echo BR.BR."<i class='clrRed b'>Reminder expired</i>";
					if (isset($day['reminder_date'])) echo " on ".date('D d-m-Y H:i',$day['email_blast_date']);
				}
				// elseif ()
				// {
					// echo BR.BR."<i class='clrRed b'>Reminder expired</i>";
				// }
				
				//if (isset($day['reminder_date'])) echo " on ".date('D d-m-Y H:i',strtotime($day['reminder_date']));
				
				echo $disable_reminder;
			}
			if ($day['is_paid'] == 1) echo " <i class='fa fa-money clrGrn' title='Tagihan sudah dibayar'></i>";
			else echo " <i class='fa fa-money clrRed' title='Tagihan belum dibayar'></i>";
			//debug($day);//die;
			
			//if (isset()) 
				//debug($rs);die;
			?>
			</td>
			<td width="50px" class="talRgt"><?php echo format_money($day['amount'],'')?></td>
			<td width="20px">
			<a href="<?php echo current_url().'?do=delete&expense_id='.$day['expense_id'];?>" title="Delete data"><i class="fa fa-times-circle fa-lg clrRed"></i></a>
			</td>
			</tr>
			<?php 
			}
			?>
			</table>
		<?php 
		}
		?>
		</td>
		<td><?php echo format_money(str_replace('.','',$rs['amount']))?></td>
		</tr>
		<?php 
			// FILL KEY ARRAY WITH DATE FOR REPORT USE
			if (left(date('d',strtotime($rs['release_date'])),1) == 0) {
				// STRIP 0 FROM 01 to 09
				$total_expense_day[right(date('d',strtotime($rs['release_date'])),1)] = str_replace('.','',$rs['amount']);
			} else {
				// NORMAL DATE > 10 like 10,11,20
				$total_expense_day[date('d',strtotime($rs['release_date']))] = str_replace('.','',$rs['amount']);
			}
			
			$i++;
			$total+= str_replace('.','',$rs['amount']);
		}

		?>
		<tr>
		<td colspan="3">TOTAL</td>
		<td><?php echo format_money($total) ?></td>
		</tr>
	</table>
	
	<?php 
	$param['is_detail'] = 0;
	$param['is_increment'] = 0;
	$param['group_by'] = 'category_name';
	$param['order_by'] = 'amount DESC';

	$list_report_by_category = $this->expense_model->get_list_report($param);
	$list_report_by_category = $list_report_by_category['data'];
	?>
	<br/>
	<br/>
	<table class='table hover table-bordered'>
		<tr>
			<td colspan="4" class="bg-danger talCnt">DETAIL EXPENSE REPORT BY CATEGORY</td>
		</tr>
		<tr>
			<td>#</td>
			<td>Date</td>
			<td>Expense</td>
			<td>Budget</td>
		</tr>
		<?php 
		$i = 1;
		$grand_total_expense = $subtotal_expense =  0;
		foreach ($list_report_by_category as $key => $rs) { 

			//CHECK IF EXPENSE EXIST
			$expense = NULL;
			foreach( $list_budget as $bgt)
			{
				if ( $bgt['category_id'] == $rs['category_id']) { $expense = $bgt['amount']; $subtotal_expense += $expense;}
			}
			
		?>
		<tr>
			<td><?php echo $i?></td>
			<td><?php echo $rs['category_name']?></td>
			<td class="talRgt"><?php echo format_money(str_replace('.','',$rs['amount']))?></td>
			<td class="talRgt"><?php if(isset($expense)) echo format_money($expense)?></td>
		</tr>
		<?php 
			$i++;
			$grand_total_expense+= str_replace('.','',$rs['amount']);
		} 
		$total_revenue = 0;
		$total_revenue = $total_income - $grand_total_expense;
		
		$bg_revenue = NULL;
		if ($total_revenue > 0) $bg_revenue = 'btn-success';
		else if ($total_revenue < 0) $bg_revenue = 'btn-danger';
		?>
		<tr>
			<td colspan="2">TOTAL EXPENSE</td>
			<td class="talRgt"><?php echo format_money($grand_total_expense) ?></td>
			<td><?php //echo format_money($subtotal_expense) ?></td>
		</tr>
		<tr>
			<td colspan="2">TOTAL INCOME</td>
			<td class="talRgt"><?php echo format_money($total_income) ?></td>
			<td><?php //echo format_money($subtotal_expense) ?></td>
		</tr>
		<tr>
			<td colspan="2">TOTAL NET REVENUE</td>
			<td class="talRgt"><div class="b btn wdtFul <?php echo $bg_revenue?>"><?php echo format_money($total_revenue) ?></div></td>
			<td><?php //echo format_money($subtotal_expense) ?></td>
		</tr>
	</table>

	<?php 
	//}
	?>
</div>
<script>
$(function () {
    $('#chart_round').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Expense Report per '
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b> => {point.y}'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} % => {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: "Brands",
            colorByPoint: true,
            data: [
			<?php 
			foreach ($list_report_by_category as $key => $rs) { 
			?>
			{	
				name: "<?php echo $rs['category_name']?>",
				y: <?php echo $rs['amount']?>
			}
				<?php if ($key+1 != count($list_report_by_category)) echo "," ?>
			<?php 
			}
			?>
			]
        }]
    });
	
	// CHART GRAPH
	<?php 
	$chart_range = $chart_range_end = NULL;
	$chart_range_end = left($end_date,2);
	
	// RANGE DATA FROM LATEST DATE TO START DATE
	for ($st = 1;$st <= $chart_range_end; $st++) 
	{
		// PRINT DATE ONLY WHEN VALUE EXIST
		if (isset($total_expense_day[$st])) {
			$chart_range .= "'".date('D d M',strtotime($st."-".right($start_date,7)))."'";
			if ($st != $chart_range_end) $chart_range.= " ,";
		}
		// if (isset($total_expense_day[$st])) {
			// $chart_range .= "'".$st." ".date('M',strtotime($st."-".right($start_date,7)))."'";
			// if ($st != $chart_range_end) $chart_range.= " ,";
		// }
	}
	
	// sort by key_date ascending so the value is on the right order
	ksort($total_expense_day);
	$total_expense_day = implode(',',$total_expense_day);
	?>
    $('#chart_graph_daily').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Report by day'
        },
        subtitle: {
            text: 'Grafik '
        },
        xAxis: {
            categories: [<?php echo $chart_range ?>]
        },
        yAxis: {
            title: {
                text: 'Amount'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: 'Expense by day',
            data: [<?php echo $total_expense_day; ?>]
        }]
    });

});
$(document).ready(function(){
	<?php if (!isset($is_get_notif) || $is_get_notif != 1) { ?>
	$("#table_input tr.reminder").hide();
	$("#table_input tr.reminder :input").attr('disabled','disabled');
	<?php } ?>
	
	$("#is_reminder1").click(function(){
		if ($(this).is(":checked")) { 
			$("#table_input tr.reminder").show();
			$("#table_input tr.reminder :input").removeAttr('disabled');
			$('input[name=f_is_get_notif]').prop('checked',true);
		}
	});
	$("#is_reminder0").click(function(){
		if ($(this).is(":checked")) { 
			$("#table_input tr.reminder").hide();
			$("#table_input tr.reminder :input").attr('disabled','disabled');
			$('input[name=f_is_get_notif]').prop('checked',false);
		}
	});
})
</script>