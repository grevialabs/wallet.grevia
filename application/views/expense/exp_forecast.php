<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = 'Financial Projection';
$bread['member'] = ABSENCE;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
$do = $absence_id = NULL;

if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('absence_id')) $absence_id = $this->input->get('absence_id');
?>

<?php 
// CHECK TODAY EXIST OR NOT
$obj_income = $this->expense_model->get_average_report(array(
	'is_increment' => 1,
	'creator_id' => member_cookies('member_id')
));

$obj_expense = $this->expense_model->get_average_report(array(
	'is_increment' => 0,
	'creator_id' => member_cookies('member_id')
));

$cashflow_description = NULL;
$cashflow_amount = 0;
if ($obj_income['total_amount'] > $obj_expense['total_amount']) 
{
	// $cashflow_amount = $obj_income['total_amount'] - $obj_expense['total_amount'];
	$cashflow_description = '<div class="alert alert-success"><b>Keuangan anda sangat baik</b>. Tingkatkan terus investasi anda dan tetap gunakan uang seperlunya.</div>';
}
else 
{
	$cashflow_description = '<div class="alert alert-danger"><b>Keuangan anda rugi</b>. Kelola keuangan anda lebih baik lagi, perbanyak tabungan dan investasi deposito.</div>';
}
$cashflow_amount = $obj_income['total_amount'] - $obj_expense['total_amount'];
// debug($obj_income);
// debug($obj_expense);
?>




<?php 
// pajak bunga depostio
$deposit_tax_percentage = 20;
if (isset($_GET['dep_tax']) && is_numeric($_GET['dep_tax']) && $_GET['dep_tax'] < 100) {
	$deposit_tax_percentage = $_GET['dep_tax'];
}

// besar bunga deposito bank
$deposit_percentage = 6;
if (isset($_GET['dep_pct']) && is_numeric($_GET['dep_pct']) && $_GET['dep_pct'] < 100) {
	$deposit_percentage = $_GET['dep_pct'];
}

// $deposit_amount = 15000000;
$deposit_amount = $cashflow_amount;
// if (isset($_GET['dep_amt']) && is_numeric($_GET['dep_amt']) && $_GET['dep_amt'] <= 10000000000) {
if (isset($_GET['dep_amt'])) {
	$deposit_amount = str_replace(',','',$_GET['dep_amt']);
	// echo "ike";
	// die;
}

$deposit_profit_monthly = 0;

$deposit_profit_monthly = $deposit_percentage/12/100 * $deposit_amount;
// simulasi bunga dipotong pajak
$deposit_profit_monthly = (100 - $deposit_tax_percentage) / 100 * $deposit_profit_monthly;

$this_month_expense = $this->expense_model->get_monthly_amount(array(
	'is_increment' => 0,
	'creator_id' => member_cookies('member_id'),
	'year' => date('Y'),
	'month' => date('m')
));
// debug($monthly_amount_expense);

$this_month_income = $this->expense_model->get_monthly_amount(array(
	'is_increment' => 1,
	'creator_id' => member_cookies('member_id'),
	'year' => date('Y'),
	'month' => date('m')
));
// debug($monthly_amount_income);
?>
<style>
#tabs{
	background:#FFF
}
.ui-tabs .ui-tabs-nav .ui-widget-header{
	background:#000 ! important
}
</style>
<div class="col-md-2">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-10">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"> </i> DESKRIPSI</b><br/>
		Dashboard berisi data proyeksi keuangan dari expense dan income yang anda input.<br/>
	</div><hr/>
	
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>

	<div id="tabs">
		<ul>
			<li><a href="#tabs-1">Keuangan bulanan</a></li>
			<li><a href="#tabs-2">Arus Kas</a></li>
			<li><a href="#tabs-3">Deposito</a></li>
		</ul>
		<div id="tabs-1">
			<!--<h3>Rata-rata keuangan bulanan anda</h3>-->
			<div class="talCnt">Rata-rata per bulan <br/>(Data anda terhitung <b><?php echo $obj_income['total_month']?></b> bulan )</div><br/>
			<div class="row">
				<div class="col-sm-6"><button class="alert alert-success wdtFul">Income anda<br/><?php if (isset($obj_income['average_amount_monthly'])) echo format_money($obj_income['average_amount_monthly']); ?></button></div>
				<div class="col-sm-6"><button class="alert alert-danger wdtFul">Expense anda<br/><?php if (isset($obj_expense['average_amount_monthly'])) echo format_money($obj_expense['average_amount_monthly']); ?></button></div>
				
				<div class="col-sm-6">Income bulan ini <br/><?php echo format_money($this_month_income['total_amount']); ?></div>
				<div class="col-sm-6">Expense bulan ini <br/><?php echo format_money($this_month_expense['total_amount']) ?></div>
				
			</div>
		</div>
		<div id="tabs-2">
			<h3>Status keuangan saat ini</h3>
			<div class="talCnt">
				Uang anda saat ini berjumlah <b><?php echo format_money($cashflow_amount)?></b>
				<?php echo $cashflow_description; ?>
			</div>
		</div>
		<div id="tabs-3">
			<h3>Simulasi Deposito</h3>
			<div class="talCnt">
				<form method="get">
					Pajak Bunga: <input type="text" class="input br numeric" name="dep_tax" value="<?php echo $deposit_tax_percentage?>" placeholder="Pajak Bunga Deposito" size="5" maxlength="2" required /><br/>
					 Bunga Deposito bank: <input type="text" class="input br numeric" name="dep_pct" value="<?php echo $deposit_percentage?>" placeholder="Persentase Bunga Deposito per tahun" size="5" maxlength="2" required /><br/>
					Jumlah setor deposito: <input type="text" class="input br numeric" name="dep_amt" value="<?php echo $deposit_amount?>" placeholder="Nominal Deposito" size="20" maxlength="15" required />
					<span class="b" id="lbl_dep_amt"></span>
					<br/>
					
					<button type="submit" class="btn btn-success">Simulasi</button>
				</form>
				
				<br/><br/>
				Jika uang anda saat ini yang berjumlah <b><?php echo format_money($deposit_amount)?></b> anda deposito kan maka setiap bulannya anda akan mendapat sebesar <?php echo format_money($deposit_profit_monthly); ?>
				
				<table class="table table-bordered">
				<tr>
					<td colspan="12">Bulan</td>
				</tr>
				<tr>
					<?php for ($i = 1; $i <= 12; $i++) { ?>
					<td><?php echo $i ?></td>
					<?php } ?>
				</tr>
				<tr>
					<?php for ($i = 1; $i <= 12; $i++) { ?>
					<td><?php echo format_money($i * $deposit_profit_monthly,''); ?></td>
					<?php } ?>
				</tr>
				</table>
			</div>
		</div>
	</div>
		
	
	
	
	
</div>
<script>
$(document).ready(function(){
	$( "#tabs" ).tabs();
	<?php if (isset($_GET['dep_tax'])) { ?>
	$('#tabs').tabs('option', 'active', 2);
	<?php } ?>
	$('input[name=dep_amt').keypress(function(){
		var dep = $(this).val();
		$('#lbl_dep_amt').html(addCommas(dep));
	})
})

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
</script>