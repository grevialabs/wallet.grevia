<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = JOURNEY;
$bread['member'] = JOURNEY;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
$do = $journey_id = NULL;

if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('journey_id')) $journey_id = $this->input->get('journey_id');

function get_hour_minute($duration) {
    $hours = floor($duration / 3600);
    $minutes = floor(($duration / 60) % 60);
    $seconds = $duration % 60;
    if ($hours != 0)
        return "$hours hour $minutes min";
    else
        return "$minutes min $seconds sec";
}

// SAVE HERE
if ($_POST && isset($_POST['start_tap']))
{
	$post = array();
	$post = $_POST;
	unset($post['start_tap']);
	
	$post['creator_id'] = member_cookies('member_id');
	$post['creator_date'] = getDatetime();

	// if (isset($post['editor_date'])) $post['editor_date'] = date('Y-m-d H:i:s');

	$insert = $this->journey_model->save($post);
	
	redirect(base_url().'expense/journey');
	
	($insert)?$message['message'] = MESSAGE::SAVE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
}

// UPDATE HERE
if ($_POST && isset($_POST['end_tap']))
{
	$post = array();
	$post = $_POST;
	unset($post['end_tap']);
	
	$post['editor_id'] = member_cookies('member_id');
	$post['editor_date'] = getDatetime();
	$post['is_finish'] = 1;
	
	// GET LAST JOURNEY AND UPDATE
	$obj_last_journey = $this->journey_model->get(array(
		'creator_id' => member_cookies('member_id'),
		'last' => TRUE
	));
	$journey_id = $obj_last_journey['journey_id'];

	$insert = $this->journey_model->update($journey_id, $post);

	redirect(base_url().'expense/journey');
	
	($insert)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
}

// // SAVE HERE
// if ($_POST && isset($_POST['btn_insert']))
// {
	// $post = array();
	// $post = $_POST;
	// unset($post['btn_insert']);
	
	// if (isset($post['release_date']) && $post['release_date']!='') $post['release_date'] = date('Y-m-d',strtotime($post['release_date']));
	// if (isset($post['reminder_date']) && $post['reminder_date']!='') $post['reminder_date'] = date('Y-m-d',strtotime($post['reminder_date']));
	// //debug($post);die;
	// $insert = $this->journey_model->save($post);
	
	// ($insert)?$message['message'] = MESSAGE::SAVE : $message['message'] = MESSAGE::ERROR;
	// $message['message'] = getMessage($message['message']);
// }

// // UPDATE
// if ($_POST && isset($_POST['btn_update']))
// {
	// $post = array();
	// $post = $_POST;
	// unset($post['btn_update']);
	
	// if (isset($post['release_date']) && $post['release_date']!='') $post['release_date'] = date('Y-m-d',strtotime($post['release_date']));
	// if (isset($post['reminder_date']) && $post['reminder_date']!='') $post['reminder_date'] = date('Y-m-d',strtotime($post['reminder_date']));
	
	// $get['member_id'] = member_cookies('member_id');
	// $get['journey_id'] = $journey_id;
	// $obj_absence = $this->journey_model->get($get);
	
	// if (!empty($obj_absence)) 
	// {
		// // debug($post);die;
		// $update = $this->journey_model->update($journey_id, $post);
		
		// ($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
		// $message['message'] = getMessage($message['message']);
		// redirect(current_url().'?success=1');
	// }
	// else
	// {
		// // DATA NOT FOUND
		// $message = MESSAGE::DATA_NOT_FOUND;
		// $message['message'] = getMessage($message);
	// }
	// $obj_absence = NULL;
	
// }

// // DELETE JOURNEY
// if ($do == "delete" && is_numeric($journey_id)) 
// {
	// $absence = $this->journey_model->get(array('journey_id' => $journey_id, 'creator_id' => member_cookies('member_id')));
	// if (!empty($treasure)) 
	// {
		// $delete = $this->journey_model->delete($journey_id);
		// redirect(current_url().'?delete_success=1');
	// } 
	// else 
	// {
		// $message['message'] = 'Data Not Found';
	// }
// }

$start_date = $end_date = NULL;
$param = array();

if ($this->input->get('start_date') && $this->input->get('start_date')) 
{
	$start_date = date('d-m-Y',strtotime($this->input->get('start_date')));
	$end_date = date('d-m-Y',strtotime($this->input->get('end_date')));
}
else
{
	$param['start_date'] = $start_date = date("01-m-Y");
	$param['end_date'] = $end_date = date("t-m-Y");
}
	
$param['creator_id'] = member_cookies('member_id');
if (isset($start_date) && isset($end_date)) 
{
	$param['start_date'] = $start_date;
	$param['end_date'] = $end_date;
}

?>

<script>
function getLocation() {
  $("#getLocation").attr("disabled", "disabled").button('refresh');
  if (navigator.geolocation) {
	  navigator.geolocation.getCurrentPosition(savePosition, errorCallback, {timeout:10000});
  } else {
	  //Geolocation is not supported by this browser
	  alert('no support');
  }
}

// handle the error here
function errorCallback(error) {
  var errorCode = error.code;
  var message = error.message;

  alert("sorry there is an error : " + message);
}

function savePosition(position) {
	  
	var d = new Date();
	var timestamp = d.getTime();

	datalat = position.coords.latitude;
	datalng = position.coords.longitude;
	text_info = $('#text_info');
	
	$.ajax({
		type	: "POST",
		url		: "<?php echo base_url()?>ajax",
		data	: "do=journey&lat="+datalat+"&lng="+datalng+"&apitoken="+timestamp,
		cache	: false,
		beforeSend: function(){
			text_info.html('<i class="fa fa-cog fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>');
		},
		success: function(data){
			// alert(data);
			if(data == "")
			{
				text_info.html("<font color='red'>Error data not saved.</font>");
			}
			else 
			{
				$('#lat_start').val(datalat);
				$('#lng_start').val(datalng);
				$('#gmap_start').val(data);
				text_info.html("success");
				$('#tap_in').click();
			}
		}
	});
}
// -------------------------------------------------------------
function getLocationUpdate() {
  if (navigator.geolocation) {
	  navigator.geolocation.getCurrentPosition(updatePosition, errorCallback, {timeout:10000});
  } else {
	  //Geolocation is not supported by this browser
	  alert('no support');
  }
}

function updatePosition(position) {
	  
	var d = new Date();
	var timestamp = d.getTime();

	datalat = position.coords.latitude;
	datalng = position.coords.longitude;
	text_info = $('#text_info');
	
	
	$.ajax({
		type	: "POST",
		url		: "<?php echo base_url()?>ajax",
		data	: "do=journey&lat="+datalat+"&lng="+datalng+"&apitoken="+timestamp,
		cache	: false,
		beforeSend: function(){
			text_info.html('<i class="fa fa-cog fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>');
		},
		success: function(data){
			// alert(data);
			if(data == "")
			{
				text_info.html("<font color='red'>Error data not saved.</font>");
			}
			else 
			{
				$('#lat_end').val(datalat);
				$('#lng_end').val(datalng);
				$('#gmap_end').val(data);
				text_info.html("success");
				$('#tap_out').click();
			}
		}
	});
}
</script>

<div class="col-md-2">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-10">
	<h1 class=""><?php echo $MODULE?></h1><br/>
	<?php if(is_filled($message['message']))echo message($message['message']).BR?>
	<?php if ($do != "insert") { ?>
	<!--
	<a class="btn btn-success btn-sm" href="?do=insert">+ NEW JOURNEY</a><br/><br/>
	-->
	<?php } ?>
		
	<?php 
	// CHECK TODAY EXIST OR NOT
	$obj_absence_today = $this->journey_model->get(array(
		'creator_date' => date('Y-m-d'),
		'creator_id' => member_cookies('member_id')
	));
	
	
	if (!empty($obj_absence_today)) 
	{
		$work_duration_estimate = $progress_bar = $normal_workhour = $normal_workhour_timestamp = NULL;
		$progress_bar = $est_work_hour = $est_work_minute = 0;
		$normal_workhour_timestamp = 9*3600; 
		$normal_workhour = 9;
		
		if (!isset($obj_absence_today['editor_date'])) 
		{
			$work_duration_estimate = time() - strtotime($obj_absence_today['creator_date']);
		}
		else 
		{
			$work_duration_estimate = strtotime($obj_absence_today['editor_date']) - strtotime($obj_absence_today['creator_date']);
		}
		
		if ($work_duration_estimate > 3600) 
		{
			$est_work_hour = floor($work_duration_estimate / 3600);
			$est_work_minute = floor(($work_duration_estimate - ($est_work_hour*3600)) /60);
		}
		else 
		{
			$est_work_minute = floor(($work_duration_estimate) /60);
		}
		$progress_bar = floor(($work_duration_estimate / $normal_workhour_timestamp) *100);
		
		// debug($progress_bar);
		?>
		<!-- SHOW JOURNEY OUT BUTTON -->
		<div class="row">
			<div class="bdrBlk bgSftGry col-sm-12">
				<h2 class="b f24 text-uppercase">TODAY JOURNEY</h2>
				<br/>
				<div class="b text-uppercase f18 clrGrn"><?php echo date('D d-m-Y H:i:s')?></div>
				<div class="row">
					<div class="col-sm-6 br-md">
						<div class="btn btn-info wdtFul">You already Tap in Today
						<br/><br/>
						<?php echo date('H:i:s',strtotime($obj_absence_today['creator_date']))?>
						</div>
					</div>
					<div class="col-sm-6 br-md">
						<?php 
						// NOT YET TAPPED OUT
						if (!isset($obj_absence_today['editor_date'])) 
						{
							?>
						<div id="text_info"></div>
						<button class="btn btn-success wdtFul" onclick="getLocationUpdate();"> <i class="fa fa-power-off fa-3x" aria-hidden="true"></i><br/> Tap OUT sekarang</button>
						
						<form method="post">
							<button id="tap_out" class="btn btn-success hide">
							<i class="fa fa-power-off fa-3x" aria-hidden="true"></i><br/>
							<input type="hidden" id="lat_end" name="f_lat_end" value=""/>
							<input type="hidden" id="lng_end" name="f_lng_end" value=""/>
							<input type="hidden" id="gmap_end" name="f_gmap_end" value=""/>
							<input type="hidden" name="f_end_tap" value="1"/>
							TAP OUT JOURNEY TODAY</button>
						</form>
						<!-- BAWAH GAPAKE -->
						<!--
						<form method="post">
							<button class="btn btn-success wdtFul" onclick="return confirm('Yakin ingin tap out absen hari ini ?')">
							<input type="hidden" name="f_editor_date" value="1" />
							<i class="fa fa-power-off fa-3x" aria-hidden="true"></i><br/><br/> TAP OUT HERE</button>
						</form>
						-->
							<?php 
						}
						else 
						{
							?>
							<div class="btn btn-info wdtFul br-md">You already Tap Out Today
								<br/><br/><br/>
								<?php echo date('H:i:s',strtotime($obj_absence_today['editor_date']))?>
							</div>
							<?php
						}
						
						$progress_class = 'progress-bar-striped active';
						if (isset($obj_absence_today['creator_date']) && isset($obj_absence_today['editor_date']))
						{	
							if ($est_work_hour >= $normal_workhour) 
							{
								$progress_class = 'progress-bar-success';
							}
							else 
							{
								$progress_class = 'progress-bar-danger';
							}
						}
						?>
					</div>
				
					<div class="col-sm-12 br-md">
						<br/>
						<div class="b f18 text-uppercase">Durasi perjalanan sementara: <?php echo $est_work_hour." jam ".$est_work_minute." menit"?>
						</div>
						
					</div>
				
				<?php 
				// SUMMARY IF TAP IN AND TAP OUT DATA EXIST
				if (isset($obj_absence_today['creator_date']) && isset($obj_absence_today['editor_date']))
				{
					$summary_absence = $color_absence = $icon_absence = NULL;
					if ($est_work_hour >= $normal_workhour) {
						$summary_absence = 'Anda pegawai sejati tepat waktu. Lanjutgan';
						// $color_absence = 'alert-success';
						$icon_absence = '<i class="fa fa-thumbs-up fa-3x bgGrn" aria-hidden="true"></i>';
					} else {
						$summary_absence = 'Nakal ya, siap2 kena SP bitches.';
						// $color_absence = 'alert-danger';
						$icon_absence = '<i class="fa fa-thumbs-down fa-3x bgGrn" aria-hidden="true"></i>';
					}
					?>
					<div class="col-sm-12">
						<br/>
						<div class="talCnt alert alert-info wdtFul"><h2 class="b f24">SUMMARY</h2>
							<?php //echo $icon_absence.BR.BR?>
							<?php //echo $summary_absence ?>
							<i class="fa fa-4x fa-motorcycle"></i><br/>
							Lama perjalanan <?php echo $est_work_hour." jam ".$est_work_minute." menit"?>
						</div>
						
					</div>
					<?php 
				}
				?>

					<div class="col-sm-12">
						<br/>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<br/>
			</div>
		</div>
		
		<?php
	}
	else 
	{
		?>
		<!-- SHOW INPUT BUTTON -->
		<div class="bdrGry bgSftGry col-sm-12">
			<h2 class="b f24 text-uppercase">TODAY JOURNEY</h2>
			<br/>
			<div id="text_info"></div>
			<form method="post">
				<input type="text" id="gmap_start" name="f_gmap_start" value="" class="input wdtFul" placeholder="Your start location.." required />
				<button type="button" id="getLocation" class="btn btn-success wdtFul" onclick="getLocationUpdate();"> <i class="fa fa-power-off fa-3x" aria-hidden="true"></i><br/> Tap In sekarang</button>
			
				<input type="hidden" id="lat_start" name="f_lat_start" value=""/>
				<input type="hidden" id="lng_start" name="f_lng_start" value=""/>
				<input type="hidden" name="f_start_tap" value="1"/>
				<button type="submit" id="tap_in" class="btn btn-success hide">
				</button>
			</form>
			<br/>
		</div>
		<div class="col-sm-12">
			<br/>
		</div>

		<?php
	}
	?>
	<?php 
	$do = $test_id = NULL;
	if ($this->input->get('do')) $do = $this->input->get('do');
	if ($this->input->get('test_id') && is_numeric($this->input->get('test_id'))) $test_id = $this->input->get('test_id');
	

	if ($do == 'insert' || $do == "edit")
	{
		if (isset($journey_id)) $obj = $this->journey_model->get(array(
			'journey_id' => $journey_id,
			'creator_id' => member_cookies('member_id')
		));
		// debug($obj);
		?>
		<!--
		<form method="post">
		<table class='table hover table-bordered'>
			<tr>
				<td colspan="2" class="bg-danger talCnt">New </td>
			</tr>
			<tr>
				<td>Type</td>
				<td>
				<input type="radio" value="1" id="radio_category1" name="f_treasure_type" <?php if (isset($obj['treasure_type']) && $obj['treasure_type'] == 1) echo "checked" ?> /> <label for="radio_category1">Rekening</label> &nbsp;
				<input type="radio" value="2" id="radio_category2" name="f_treasure_type" <?php if (isset($obj['treasure_type']) && $obj['treasure_type'] == 2) echo "checked" ?> /> <label for="radio_category2">Cash</label>
				</td>
			</tr>
			<tr>
				<td width="180px">Name</td>
				<td><input type="text" class="input wdtFul" name="f_name" placeholder="ex: Rekening BCA / Tagihan Internet" value="<?php if (isset($obj['name'])) echo $obj['name']?>" required /></td>
			</tr>
			<tr>
				<td>Amount</td>
				<td><input type="text" class="input numeric" name="f_amount" placeholder="10000" value="<?php if (isset($obj['amount'])) echo $obj['amount']?>" required /></td>
			</tr>
			<tr>
				<td>Notes</td>
				<td><textarea name="f_notes" class="wdtFul" placeholder="catatan" rows="2"><?php if (isset($obj['notes'])) echo $obj['notes']?></textarea></td>
			</tr>
			<tr>
				<td>Reminder Date *opsional</td>
				<td><input type="text" class="input datepicker" placeholder="Tgl pengingat" name="f_reminder_date" value="<?php if (isset($obj['reminder_date'])) echo date('d-m-Y',strtotime($obj['reminder_date']))?>"/> Email reminder will send each day, for every expense not paid</td>
			</tr>
			<tr>
				<td colspan="2" class="talCnt">
				<?php if ($do == "edit") { ?> 
				<button type="submit" name="btn_update" value="1" class="btn btn-success btn-sm">UPDATE</button></td>
				<?php } else { ?> 
				<button type="submit" name="btn_insert" value="1" class="btn btn-success btn-sm">SUBMIT</button></td>
				<?php }?>
			</tr>
		</table>
		</form>
		-->
		

	<?php 
	}
	else 
	{
	?>
		<?php
		$param2 = array();
		$param2['creator_id'] = member_cookies('member_id');
		$param2['order'] = 'creator_date DESC';
		$list_absence = $this->journey_model->get_list($param2);
		$list_absence = $list_absence['data'];
		
		?>
		<form method="post">
		
		<table class="table table-bordered">
			<tr class="bg-success">
				<td>#</td>
				<td>From</td>
				<td>To</td>
				<td>Duration</td>
				<!--
				<td>Option</td>
				-->
			</tr>
		<?php 
		if (!empty($list_absence))
		{
			$grandtotal = 0;
			foreach ($list_absence as $key => $rs)
			{
				$work_duration = NULL;
				if (isset($rs['editor_date'])) $work_duration = strtotime($rs['editor_date']) - strtotime($rs['creator_date']);
				?>
			<tr>
				<td><?php echo $key+1?></td>
				<?php 
				// if (isset($rs['creator_date'])) echo date('D d-m-Y H:i', strtotime($rs['creator_date']));
				// if (isset($rs['creator_date'])) echo "<div style=float:left class='b clrBlu'>Created ".BR.date('D d-m-Y H:i', strtotime($rs['creator_date']))."</div>";
				// if (isset($rs['editor_date'])) echo "<div style=float:right class='b clrGrn'>Update ".BR.date('D d-m-Y H:i', strtotime($rs['editor_date']))."</div>";
				?>
				<td><?php 
				if (isset($rs['creator_date'])) echo date('D d-m-Y H:i', strtotime($rs['creator_date'])).BR.BR;
				if (isset($rs['gmap_start'])) echo $rs['gmap_start']; 
				?></td>
				<td><?php 
				if (isset($rs['editor_date'])) echo date('D d-m-Y H:i', strtotime($rs['editor_date'])).BR.BR;
				if (isset($rs['gmap_end'])) echo $rs['gmap_end']; 
				?></td>
				<td><?php 
				// echo $work_duration; 
				if ($work_duration > 0) echo "your work for <b>".get_hour_minute($work_duration)."</b>".BR.BR;
				?></td>
				<!--
				<td class="talCnt">
				<a class="btn btn-success btn-xs br" href="<?php echo current_url().'?do=edit&journey_id='.$rs['journey_id']; ?>"><?php echo UPDATE?></a> 
				<a class="btn btn-danger btn-xs br" href="<?php echo current_url().'?do=delete&journey_id='.$rs['journey_id']?>">Delete</a>
				</td>
				-->
			</tr>
				<?php
			}
			?>
			<tr class="b">
				<td colspan="2">Grandtotal</td>
				<td colspan="4"><?php // echo format_money($grandtotal)?></td>
			</tr>
			<?php
		}
		else 
		{
			?>
			<tr>
				<td colspan="6">No Data</td>
			</tr>
			<?php 
		}
		?>
		</table>
		<?php
	}
	?>
</div>