<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;
$fund = 'Fund';
$MODULE = $PAGE = $PAGE_TITLE = $fund;
$bread['member'] = $fund;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;

$param = array();

$do = $fund_id = $base_url = $current_url = NULL;
if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('fund_id') && is_numeric($this->input->get('fund_id'))) $fund_id = $this->input->get('fund_id');
$base_url = base_url();
$current_url = current_url();

// POST HERE
if ($_POST)
{
	$post = $param_insert = $param_update = array();
	$post = $_POST;
	
	// SAVE
	if (isset($_POST['hdn_insert']))
	{
		$param_insert['creator_id'] = member_cookies('member_id');	
		$param_insert['name'] = $post['name'];
		$param_insert['fund_type'] = $post['fund_type'];
		$param_insert['shortcode'] = $post['shortcode'];
		$param_insert['balance'] = $post['balance'];
		$param_insert['notes'] = $post['notes'];
		if (isset($post['color'])) $param_insert['color'] = $post['color'];
		
		// hanya untuk kartu kredit, tgl billing bulanan
		if (isset($post['due_date'])) $param_insert['due_date'] = $post['due_date'];
		
		if (isset($post['name']) && $post['name'] != '')
		{
			$insert = $this->fund_model->save($param_insert);
			
			if ($insert) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::SAVE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect($base_url.$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		else 
		{
			$message['message'] = 'Nama harus diisi';
		}
	}
	
	// UPDATE
	if (isset($_POST['hdn_update']) && is_numeric($fund_id))
	{
		$temp = $param_update = NULL;
		$temp['fund_id'] = $fund_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj = $this->fund_model->get($temp);
		
		// hanya untuk kartu kredit, tgl billing bulanan
		if (isset($post['due_date'])) $param_update['due_date'] = $post['due_date'];
		
		if ($obj && isset($post['name']) && $post['name'] != '')
		{
			$param_update['fund_type'] = $post['fund_type'];
			$param_update['name'] = $post['name'];
			$param_update['shortcode'] = $post['shortcode'];
			$param_update['balance'] = $post['balance'];
			$param_update['notes'] = $post['notes'];
			if (isset($post['color'])) $param_update['color'] = $post['color'];
			// debug($param_update);die;
			
			$update = $this->fund_model->update($fund_id, $param_update);
			
			if ($update) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect($base_url.$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		else 
		{
			// NOT FOUND
			$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
			redirect($base_url.$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
	}
}

// DELETE
if ($_GET)
{
	if ($do == "delete" && is_numeric($fund_id))
	{
		$temp = $param_update = NULL;
		$temp['fund_id'] = $fund_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj = $this->fund_model->get($temp);
		
		if (! empty($obj))
		{
			$param_update['is_delete'] = 1;
			$delete = $this->fund_model->update($fund_id, $param_update);
			if ($delete) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::DELETE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect($base_url.$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		else
		{
			// NOT FOUND
			$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
			redirect($base_url.$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
	}
}


?>

<div class="col-md-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"></i> DESKRIPSI</b><br/>Data Jenis metode bayar yang akan muncul saat mengisi <i>expense tracker</i>
	</div><hr/>
	
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>
	
	<?php if ($do != "insert") { ?>
	<a href="?do=insert" class="btn btn-success btn-sm">+ NEW FUND</a><br/><br/>
	<?php } ?>
	
	<?php 
	$do = $test_id = NULL;
	if ($this->input->get('do')) $do = $this->input->get('do');
	if ($this->input->get('test_id') && is_numeric($this->input->get('test_id'))) $test_id = $this->input->get('test_id');

	?>
	
	<!-- table responsive end -->
	<div class="table-responsive">
	<?php 
	if ($do == "insert" || $do == "edit")
	{
		$obj = NULL;
		if ($do == "edit" && is_numeric($fund_id))
		{
			$temp['fund_id'] = $fund_id;
			$temp['creator_id'] = member_cookies('member_id');
			$obj = $this->fund_model->get($temp);
		}
		
		if ($do == "insert" || ($do == "edit" && !empty($obj)))
		{
	?>
	
	<form method="post">
	<table class='table hover table-bordered'>
		<tr>
			<td colspan="2" class="bg-success talCnt">CATEGORY</td>
		</tr>
		<tr>
			<td width="180px">Nama Kartu</td>
			<td><input type="text" class="input wdtFul" name="f_name" placeholder="Nama metode pengeluaran" value="<?php if (isset($obj['name'])) echo $obj['name']?>" required /></td>
		</tr>
		<tr>
			<td><label for="f_is_increment">Tipe</label></td>
			<td>
			<input type="radio" name="f_fund_type" value="credit card" id="credit" <?php if (isset($obj['fund_type']) && $obj['fund_type']=="credit card") echo 'checked="true"'; ?> required /> <label for="credit" class="clrGrn b"><i class="fa fa-cc"></i> Credit Card</label><br/>
			<input type="radio" name="f_fund_type" value="bank account" id="bank" <?php if (isset($obj['fund_type']) && $obj['fund_type']=="bank account") echo 'checked="true"'; ?> required /> <label for="bank" class="clrBlu b"><i class="fa fa-building"></i> Rekening Bank</label><br/>
			<input type="radio" name="f_fund_type" value="others" id="others" <?php if (isset($obj['fund_type']) && $obj['fund_type']=="others") echo 'checked="true"'; ?> required /> <label for="others" class="clrRed b"><i class="fa fa-money"></i> Others</label><br/>
			
			<!--
			<input type="checkbox" name="f_is_increment" id="f_is_increment" value="1" <?php if (isset($obj['is_increment']) && $obj['is_increment']==1) echo 'checked="true"'; ?>/><br/>
			
			*centang jika category ini berupa PEMASUKAN(penambahan), dan abaikan jika berupa EXPENSE(pengurangan) 
			-->
			</td>
		</tr>
		<tr>
			<td>
				Due Date
			</td>
			<td>
				<input type="text" class="input wdtFul" name="f_due_date" placeholder="Tgl Jatuh tempo*opsional" value="<?php if (isset($obj['due_date'])) echo $obj['due_date']?>" />
			</td>
		</tr>
		<tr>
			<td>
				Shortcode
			</td>
			<td>
				<input type="text" class="input wdtFul" name="f_shortcode" placeholder="kode shortcut untuk input expense ID, misal: gopay, dana, flazz" value="<?php if (isset($obj['shortcode'])) echo $obj['shortcode']?>" />
			</td>
		</tr>
		<tr>
			<td>
				Saldo
				<?php if (isset($obj['balance'])) echo BR.'<b>'.format_money($obj['balance']).'</b>'?>
			</td>
			<td>
				<input type="text" class="input wdtFul numeric" name="f_balance" placeholder="Saldo *opsional" value="<?php if (isset($obj['balance'])) echo $obj['balance']?>" />
			</td>
		</tr>
		<tr>
			<td>
				Color
				<?php if (isset($obj['color'])) echo BR.'<div style="width:150px;height:15px;display:block;background-color:'.($obj['color']).'">aa</div>'?>
			</td>
			<td>
				<input type="text" class="input wdtFul" name="f_color" placeholder="Warna *opsional" value="<?php if (isset($obj['color'])) echo $obj['color']?>" />
			</td>
		</tr>
		<tr>
			<td>
				Notes *<b>optional</b>
			</td>
			<td>
				<textarea class="input wdtFul" name="f_notes" placeholder="Notes"><?php if (isset($obj['notes'])) echo $obj['notes']?></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="talCnt">
			<?php if ($do == "insert") { ?>
			<input type="hidden" name="hdn_insert" value="1" />
			<button class="btn btn-success btn-sm">SAVE</button>
			<?php } else { ?>
			<input type="hidden" name="hdn_update" value="1" />
			<button class="btn btn-success btn-sm">UPDATE</button>
			<?php } ?>
			
			</td>
		</tr>
		
	</table>
	</form>
	</div>
	<br/>
	<?php 
		}
		elseif($do == "edit" && empty($obj))
		{
			echo message(getMessage(MESSAGE::NOT_FOUND));
		}
	}
	?>
	
	<?php
	$param['creator_id'] = member_cookies('member_id');
	$param['is_delete'] = 0;
	// $param['order'] = ' is_increment ASC, name ASC';

	$list_fund = $this->fund_model->get_list($param);
	$list_fund = $list_fund['data'];
	?>

	<b>Notes : </b><br/>
	Menu mutasi hanya tersedia untuk rekening Bank
	<table class="table hover table-bordered">
	<tr class="alert bg-warning b talCnt">
		<td width="10px">#</td>
		<td>Name</td>
		<td width="60px">Saldo</td>
		<td width="100px">Type</td>
		<td width="160px">Option</td>
	</tr>
	<?php 
	if(!empty($list_fund))
	{
		foreach($list_fund as $key => $rs)
		{
			// debug($rs);
			$name = NULL;
			$saldo = 0;
			
			if ($rs['fund_type'] == "cash") { 
				$name = "<i class='fa fa-money clrGrn'></i> Cash";
			} else if ($rs['fund_type'] == "credit card") {
				$name = "<i class='fa fa-credit-card clrOrg'></i> Credit Card - ".$rs['name'];
			}  else if ($rs['fund_type'] == "bank account") {
				$name = "<i class='fa fa-bank clrBlu'></i> Rekening Bank - ".$rs['name'];
				
				if (isset($rs['last_credit']) && $rs['last_credit'] > 0) 
					$saldo = '<span class="btn btn-info btn-xs"> Last saldo: <b>'.format_money($rs['last_credit']).'</b></span>';
			}  else if ($rs['fund_type'] == "others") {
				$name = "<i class='fa fa-money clrRed'></i> Others - ".$rs['name'];
			}
			
			$color = $rs['color'];
			if (isset($color)) {
				$color = '<div style="color:'.$color.';display:block;border-bottom: 3px solid '.$color.'">'.$rs['name'].'</div>';
				// $rs['name'] = '<div style="background-color:'.$color.';display:block;">'.$rs['name'].'</div>';
			}
			
			if (isset($rs['shortcode'])) $name.= '(<b>'.$rs['shortcode'].'</b>)';
			
		?>
	<tr>
		<td class="talCnt"><?php echo $key+1?></td>
		<td><?php echo $name; ?> <?php if ($rs['fund_type'] == 'bank account') { ?>
		<br/><a class="btn btn-info btn-xs" href="<?php echo $base_url.'expense/fund_mutation?fund_id='.$rs['fund_id']?>">Lihat mutasi</a>
		<?php } ?></td>
		<td class="talCnt"><?php echo $saldo?></td>
		<td class="talCnt"><?php echo $rs['fund_type'].BR.$color?></td>
		<td class="talCnt">
		<?php if ($rs['fund_type'] != "cash") { ?>
		<a href="<?php echo $current_url.'?do=edit&fund_id='.$rs['fund_id']?>"><i class="fa fa-edit fa-lg clrGrn"></i></a> &nbsp;
		<a href="<?php echo $current_url.'?do=delete&fund_id='.$rs['fund_id']?>"><i class="fa fa-times-circle fa-lg clrRed"></i></a>
		<?php } ?>
		</td>
	</tr>
		<?php 
		}
	}
	else 
	{
		?>
		<tr>
			<td colspan="100%">No data</td>
		</tr>
		<?php 
	}
	?>
	</table>
	</div>
	
</div>