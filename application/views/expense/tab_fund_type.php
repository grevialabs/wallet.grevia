<?php
$member_id = member_cookies('member_id');
if (! $member_id) {
	echo "Get outta here"; die;
}

// REPORT

$tgl_report = NULL;
$list_report_by_fund_type = $param_monthly = array();
$param_monthly['creator_id'] = member_cookies('member_id');
$tgl_report = '<b>'.date('M Y').'</b>';
if (isset($get['start_date']) && isset($get['end_date'])) 
{
	$param_monthly['start_date'] = $get['start_date'];
	$param_monthly['end_date'] = $get['end_date'];
	
	$tgl_report = '<b>'.date('D d M Y',strtotime($param_monthly['start_date'])).'</b> - <b>'.date('D d M Y',strtotime($param_monthly['end_date'])).'</b>';
}

$param_monthly['order'] = "total_transaction_count DESC, category_name ASC";
$list_report_by_fund_type = $this->expense_model->get_report_by_fund_type($param_monthly);
$list_report_by_fund_type = $list_report_by_fund_type['data'];
if ( ! empty($list_report_by_fund_type))
{
	?>
<table class="table table-striped table-hover table-bordered">
	<tr>
		<td colspan="6" class="bg-warning talCnt">List Expense by Fund Type <?php echo $tgl_report; ?></td>
	</tr>
	<tr>
		<td width="1">#</td>
		<td>Nama Fund</td>
		<td>Tgl Due Date</td>
		<td>Total Income</td>
		<td>Total Expense</td>
	</tr>
	<?php
	$total_fund_income = $total_fund_expense = NULL;
	foreach($list_report_by_fund_type as $key => $rsb)
	{
		$key++;

		// $status_paid = NULL;
		// if (isset($rsb['due_date']) && $rsb['due_date'] > 0)
		// {
			// if (date('d') >= $rsb['due_date']) {
				// $status_paid = '<i class="fa fa-check text-success"></i>';
			// } else {
				// $status_paid = '<i class="fa fa-remove text-danger"></i>';

				// $status_paid.= '- '. ceil($rsb['due_date'] - date('d')).' days';
			// }
		// }
		
		$total_fund_income+= $rsb['total_income'];
		$total_fund_expense+= $rsb['total_expense'];
		?>
	<tr>
		<td><?php echo $key; ?></td>
		<td><?php echo get_fund_type($rsb['fund_type']).' '.$rsb['fund_name']?></td>
		<td><?php if (isset($rsb['due_date'])) echo $rsb['due_date']; else echo '-'; ?></td>
		<td class="text-success b talRgt"><?php echo format_money($rsb['total_income'])?></td>
		<td class="clrRed b talRgt"><?php echo format_money($rsb['total_expense'])?></td>
	</tr>
		<?php
	}
	?>
	<tr>
		<td colspan="3">Total Cashflow <b><?php echo format_money($total_fund_income - $total_fund_expense)?></b></td>
		<td class="text-success b talRgt"><?php echo format_money($total_fund_income)?></td>
		<td class="clrRed b talRgt"><?php echo format_money($total_fund_expense)?></td>
	</tr>
</table>
	<?php
}
?>