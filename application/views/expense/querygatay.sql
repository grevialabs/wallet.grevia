SELECT COUNT(DISTINCT(DATE_FORMAT(e.release_date,'%Y-%m'))) as total_bulan_input, 
SUM(e.amount) / COUNT(DISTINCT(DATE_FORMAT(e.release_date,'%Y-%m'))) as avg_monthly
FROM wal_expense e
LEFT JOIN wal_category c ON e.category_id = c.category_id
WHERE 1 AND e.creator_id = 1 AND e.release_date IS NOT NULL AND c.is_increment = 0;