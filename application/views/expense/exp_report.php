<?php
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$base_url = base_url();
$MODULE = $PAGE = $PAGE_TITLE = REPORT;
$bread['member'] = REPORT;
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);


function isWeekend($date) {
    $weekDay = date('w', strtotime($date));
    return ($weekDay == 0 || $weekDay == 6);
}

global $page, $param, $message;

$start_date = $end_date = $start_date_format = $end_date_format = $is_get_notif = $get = NULL;
$param = array();

$do = $expense_id = NULL;
if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('expense_id') && is_numeric($this->input->get('expense_id'))) $expense_id = $this->input->get('expense_id');

if ($_GET)
{
	$get = $_GET;
	// DELETE EXPENSE
	if ($do == "delete" & is_numeric($expense_id))
	{
		$temp['expense_id'] = $expense_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj_delete = $this->expense_model->get($temp);

		if (!empty($obj_delete))
		{
			$delete = $this->expense_model->delete($expense_id);
			if ($delete) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::DELETE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			redirect($base_url.$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
	}

	// DISABLE NOTIFICATION EMAIL REMINDER
	if ($do == "disable_notification" & is_numeric($expense_id))
	{
		$temp['expense_id'] = $expense_id;
		$temp['creator_id'] = member_cookies('member_id');
		$obj_disable = $this->expense_model->get($temp);

		if (!empty($obj_disable))
		{
			$param_disabled = NULL;
			$param_disabled['is_get_notif'] = 0;
			$obj_disable = $this->expense_model->update($expense_id,$param_disabled);
			if ($obj_disable)
			{
				redirect(current_url().'?disable_success=1');
			} else {
				redirect(current_url().'?disable_success=0');
			}
		}
	}


}

if ($_POST)
{
	$post = $param_insert = $param_update = array();
	$post = $_POST;

	// INSERT
	if (isset($post['hdn_insert']))
	{
		$param_insert['category_id'] = $post['category_id'];
		$param_insert['title'] = $post['title'];
		// $param_insert['amount'] = str_replace(array(',','.'),'',$post['amount']);
		$param_insert['amount'] = preg_replace('/[^0-9]/','',$post['amount']);
		$param_insert['cashback'] = str_replace(',','',$post['cashback']);
		$param_insert['notes'] = $post['notes'];

		// SET AS DEFAULT
		$param_insert['is_paid'] = $param_insert['is_get_notif'] = $param_insert['is_routine'] = 0;
		if (isset($post['location_id'])) $param_insert['location_id'] = $post['location_id'];
		if (isset($post['fund_id'])) $param_insert['fund_id'] = $post['fund_id'];

		if (isset($post['gmap_address'])) $param_insert['gmap_address'] = $post['gmap_address'];
		if (isset($post['gmap_lat'])) $param_insert['gmap_lat'] = $post['gmap_lat'];
		if (isset($post['gmap_long'])) $param_insert['gmap_long'] = $post['gmap_long'];

		if (isset($post['is_paid']) && $post['is_paid'] == 1) $param_insert['is_paid'] = 1;
		if (isset($post['is_get_notif']) && $post['is_get_notif'] == 1) $param_insert['is_get_notif'] = 1;
		if (isset($post['is_routine']) && $post['is_routine'] == 1) $param_insert['is_routine'] = 1;

		if (isset($post['reminder_date'])) $param_insert['reminder_date'] = date('Y-m-d',strtotime($post['reminder_date'])).' '.$post['reminder_hour'].':'.$post['reminder_minute'].':00';
		if (isset($post['reminder_minus_day'])) $param_insert['reminder_minus_day'] = $post['reminder_minus_day'];

		if (isset($post['release_date']) && $post['release_date']!='') $param_insert['release_date'] = date('Y-m-d',strtotime($post['release_date']));

		$param_insert['creator_date'] = getDatetime();
		$param_insert['creator_id'] = member_cookies('member_id');

		$insert = $this->expense_model->save($param_insert);
		$ins_text = ' for ' . $post['title'] . ' IDR ' . number_format($param_insert['amount']) . ' at ' . $post['release_date'] . '';
		if ($insert) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::SAVE) . $ins_text);
			// redirect($base_url.$this->uri->segment(1).'/'.$this->uri->segment(2));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			// redirect($base_url.$this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		$url = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'?release_date=' . date('d-m-Y',strtotime($post['release_date']));
		redirect($url);
	}

	// UPDATE
	if (isset($post['hdn_update']) && is_numeric($expense_id))
	{
		$temp3['expense_id'] = $expense_id;
		$temp3['creator_id'] = member_cookies('member_id');
		$obj = $this->expense_model->get($temp3);

		if (!empty($obj))
		{
			$param_update['category_id'] = $post['category_id'];
			$param_update['release_date'] = date('Y-m-d',strtotime($post['release_date']));
			$param_update['title'] = $post['title'];

			// $param_update['amount'] = str_replace(',','',$post['amount']);
			// $param_update['amount'] = str_replace(array(',','.'),'',$post['amount']);
			$param_update['amount'] = preg_replace('/[^0-9]/','',$post['amount']);
			$param_update['cashback'] = str_replace(',','',$post['cashback']);
			$param_update['notes'] = $post['notes'];

			if (isset($post['location_id'])) $param_update['location_id'] = $post['location_id'];
			if (isset($post['fund_id'])) $param_update['fund_id'] = $post['fund_id'];
			// SET AS DEFAULT
			$param_update['is_paid'] = $param_update['is_get_notif'] = $param_update['is_routine'] = 0;
			if (isset($post['is_paid']) && $post['is_paid'] == 1) $param_update['is_paid'] = 1;
			if (isset($post['is_get_notif']) && $post['is_get_notif'] == 1) $param_update['is_get_notif'] = 1;
			if (isset($post['is_routine']) && $post['is_routine'] == 1) $param_update['is_routine'] = 1;

			if (isset($post['reminder_date'])) $param_update['reminder_date'] = date('Y-m-d',strtotime($post['reminder_date'])).' '.$post['reminder_hour'].':'.$post['reminder_minute'].':00';
			
			if (isset($post['reminder_minus_day'])) $param_update['reminder_minus_day'] = $post['reminder_minus_day'];

			$param_update['creator_id'] = member_cookies('member_id');

			$update = $this->expense_model->update($expense_id, $param_update);
			
			$ins_text = ' for ' . $post['title'] . ' IDR ' . number_format($param_update['amount']) . ' at ' . $post['release_date'] . '';

			if ($update) {
				$this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
			} else {
				$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
			}
			
			$url = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'?release_date=' . date('d-m-Y',strtotime($post['release_date']));
			redirect($url);
			// redirect(current_full_url());
		}
		else
		{
			$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
			// redirect($base_url.$this->uri->segment(1).'/'.$this->uri->segment(2));
			redirect(current_full_url());
		}
	}

	// AUTOCOMPLETE in http://localhost/wallet.grevia.com/expense/report
	if (isset($post['do']) && $post['do'] == "autocomplete" && isset($post['title']))
	{
		$param_autocomplete = $list_autocomplete = $response = NULL;
		$param_autocomplete['creator_id'] = member_cookies('member_id');
		$param_autocomplete['title'] = $post['title'];
		$param_autocomplete['limit'] = 0;
		$param_autocomplete['offset'] = 6;
		$param_autocomplete['paging'] = 1;
		$list_autocomplete = $this->expense_model->get_list_autocomplete($param_autocomplete);
		$list_autocomplete = $list_autocomplete['data'];
		if ( ! empty($list_autocomplete))
		{
			foreach($list_autocomplete as $key => $la)
			{
				$response[] = $la['title'];
			}
			echo json_encode($response);
		}
		else
		{
			// no data
			$response[] = NULL;
			echo json_encode($response);
		}
		die;
	}
}

// AUTOCOMPLETE in http://localhost/wallet.grevia.com/expense/report
// http://localhost/wallet.grevia.com/expense/report?debug=1&title=gramed
if (isset($_GET['debug']))
{
	$post['title'] = $_GET['title'];

	$param_autocomplete = $list_autocomplete = $response = NULL;
	$param_autocomplete['creator_id'] = member_cookies('member_id');
	$param_autocomplete['title'] = $post['title'];
	$param_autocomplete['limit'] = 0;
	$param_autocomplete['offset'] = 20;
	$param_autocomplete['paging'] = 1;
	$list_autocomplete = $this->expense_model->get_list_autocomplete($param_autocomplete);
	$list_autocomplete = $list_autocomplete['data'];
	if ( ! empty($list_autocomplete))
	{
		foreach($list_autocomplete as $key => $la)
		{
			$response[] = $la['title'];
		}
		echo json_encode($response);
	}
	else
	{
		$response[] = "no data";
		echo json_encode($response);;
	}
	die;
}



// REPORT
if ($this->input->get('start_date') && $this->input->get('start_date'))
{
	$start_date = date('d-m-Y',strtotime($this->input->get('start_date')));
	$end_date = date('d-m-Y',strtotime($this->input->get('end_date')));
}
else
{
	$param['start_date'] = $start_date = date("01-m-Y");
	// get end date
	// $param['end_date'] = $end_date = date("t-m-Y");

	// get current date
	$param['end_date'] = $end_date = date("d-m-Y", time());
	
	// see history by release date submit
	if ($this->input->get('release_date')) {
		$param_rd = explode('-',$get['release_date']);
		$param['start_date'] = $start_date = date("01-".$param_rd[1]."-".$param_rd[2]);
		
		// change to last day using date func
		$end_date = $param_rd[2]."-".$param_rd[1]."-1";
		$end_date = strtotime($end_date);
		$param['end_date'] = $end_date = date("t-m-Y", $end_date);
	}
}

$param['creator_id'] = member_cookies('member_id');
if (isset($start_date) && isset($end_date))
{
	$param['start_date'] = $start_date;
	$param['end_date'] = $end_date;

	$start_date_format = convert_date('D d M Y',$start_date);
	$end_date_format = convert_date('D d M Y',$end_date);
}
$param['is_detail'] = 0;
$param['is_increment'] = -1;
$param['report'] = 'expense';
$param['group_by'] = 'release_date';
$param['order_by'] = 'release_date DESC';
// $param['debug'] = 1;
$param['creator_id'] = member_cookies('member_id');
$obj_list_expense_detail = NULL;

$obj_list_expense_detail = $this->expense_model->get_list_report($param);
$obj_list_expense_detail = $obj_list_expense_detail['data'];


// get list expense by fund type
// $list_report_fund_type = $param_fund = NULL;
// $param_fund['creator_id'] = member_cookies('member_id');
// $list_report_fund_type = $this->expense_model->get_report_by_fund_type();
// $list_report_fund_type = $list_report_fund_type['data'];

// ------------------------------------
// Get div for filter data each month by year active, for shortcut filter
$str_filter_month = $str_filter_color = $str_filter_pos = NULL;
$year = date('Y');
$month = date('m');


if (isset($get['start_date']) && isset($get['end_date'])) {
	$chkdatest = explode('-',$get['start_date']);
	$chkdateed = explode('-',$get['end_date']);
	
	if ($chkdatest[1] === $chkdateed[1]) {
		$str_filter_pos = $chkdateed[1];
		$str_filter_color = 'success';
	}
}

for ($m=1;$m<=$month;$m++)
{
	if ($m == 1) $str_filter_month = '<div class="talCnt">'.$year.'</div>';
	if ($m < 10) $m = '0'.$m;
	
	$str_filter_color = 'primary';
	
	// show active month when param start_date end_date exist
	if (isset($str_filter_pos) && $str_filter_pos == $m) {
		$str_filter_color = 'success';
	} 
	
	// show active month when no param
	if (! isset($str_filter_pos)){
		if ($m == $month) $str_filter_color = 'success';
	}
	
	$start = $year.'-'.$m.'-01';
	$end = date('Y-m-t',strtotime($year.'-'.$m.''));
	$str_filter_month .= '<div class="" style=""><a style="width:60px;float:left; margin-left:15px; margin-bottom:10px" class="btn btn-'.$str_filter_color.' btn-xs clrWht" href="?start_date='.$start.'&end_date='.$end.'" >'.date('M',strtotime($year.'-'.$m.'-01')).'</a></div>';
}
$str_filter_month .= '<div class="clearfix"></div>';
?>
<!-- help: http://www.bootstraptoggle.com/ -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css?v=170812" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js?v=170812"></script>

<!-- input reset input -->
<script src="<?php echo $base_url?>asset/js/resetinput.js?v=171211"></script>
<link href="<?php echo $base_url?>asset/css/resetinput.css?v=171211" rel="stylesheet">

<div class="col-md-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"> </i> DESKRIPSI</b><br/>
		Fitur yang menampilkan seluruh ringkasan income / expense dalam grafik & diagram.
	</div><hr/>
	<?php
	// deprecated in php 7.4
	// if (is_filled($message['message']))echo message($message['message']).BR;
	if (isset($message['message']))echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;

	if (isset($_GET['delete_success']))
	{
		$str = NULL;
		if ($_GET['delete_success'] == 1)
		{
			$str = 'Delete success';
		}
		else if ($_GET['delete_success'] == 0)
		{
			$str = 'Delete failed';
		}
		echo message($str).BR;
	}

	?>

	<?php
	// $filter = NULL;
	// if (isset($_GET['start_date'])) $filter .= '?start_date='.$_GET['start_date'];
	// if (isset($_GET['end_date'])) $filter .= '&end_date='.$_GET['end_date'];
	?>
	<form method="get">
		<?php echo $str_filter_month.HR ?>
		FILTER <input type="text" class="input br datepicker" placeholder="start date" name="start_date" value="<?php echo $start_date ?>" />
		<input type="text" class="input br datepicker" placeholder="end date" name="end_date" value="<?php echo $end_date ?>"/>
		<button name="" value="" class="btn btn-default btn-md br">SUBMIT</button>
	</form><br/>

	<?php if ($do != "insert") { ?>
	<!-- <a href="?do=insert" class="btn btn-success">NEW EXPENSE</a><br/><br/> -->
	<a href="javascript:void(0)" id="new_expense" class="btn btn-info btn-sm">+ NEW EXPENSE</a><br/><br/>
	<?php } ?>
	<!-- FORM INPUT -->
	<?php

	// if ($do == "insert" || $do == "edit")
	// {
		
		// for get current month and year query if param get exist
		$qdsd = $qdsd = NULL;
		if ($do == "edit" && is_numeric($expense_id))
		{
			$temp1['creator_id'] = member_cookies('member_id');
			$temp1['expense_id'] = $expense_id;
			$obj = $this->expense_model->get($temp1);
			if (isset($obj['gmap_address'])) $gmap_address = $obj['gmap_address'];
			if (isset($obj['is_get_notif'])) $is_get_notif = $obj['is_get_notif'];
		}

		$param_category = $param_location = $param_fund = $list_category = $list_location = $list_fund = NULL;

		$param_category['creator_id'] = member_cookies('member_id');
		$param_category['total_trx_by_month_year'] = getDatetime();
		$param_category['is_parent'] = 0;
		$param_category['is_detail'] = 0;
		$param_category['is_publish'] = 1;
		
		// check date request for query total trx
		if (isset($_GET['start_date']) || isset($_GET['release_date']))
		{   
			if (isset($_GET['start_date'])) $qdsd = $_GET['start_date'];
			if (isset($_GET['release_date'])) $qdrd = $_GET['release_date'];

				if (isset($qdsd)) {
					$param_category['total_trx_by_month_year'] = date("Y-m-d",strtotime($qdsd));;
				}

				if (isset($qdrd)) {
					$param_category['total_trx_by_month_year'] = date("Y-m-d",strtotime($qdrd));
				}
			// aatertre awdawd 
			// start_date=2024-11-01
		}
		
		$param_category['order'] = ' is_increment ASC, name ASC';
		// debug($param_category,1);
		$list_category = $this->category_model->get_list($param_category);
		// debug($list_category,1);
		$list_category = $list_category['data'];

		$param_location['order'] = 'location_name ASC';
		$list_location = $this->location_model->get_list($param_location);
		$list_location = $list_location['data'];

		$param_fund['is_delete'] = 0;
		$param_fund['creator_id'] = member_cookies('member_id');
		$param_fund['order'] = 'fund_id ASC';
		$list_fund = $this->fund_model->get_list($param_fund);
		$list_fund = $list_fund['data'];

		$category_fund = NULL;
		if ( ! empty($list_fund))
		{
			foreach ($list_fund as $key => $fund)
			{
				if ($fund['fund_type'] == 'cash') $category_fund['cash'][] = $fund;
				else if ($fund['fund_type'] == 'bank account') $category_fund['bank_account'][] = $fund;
				else if ($fund['fund_type'] == 'credit card') $category_fund['credit_card'][] = $fund;
				else if ($fund['fund_type'] == 'others') $category_fund['others'][] = $fund;
			}
		}
		// debug($category_fund);

	?>
	<style>
	.panel-title a{display:block}
	</style>
	<!--
	.panel-title a => biar bisa klik all area
	
	BUAT TONGOLIN RESET BUTTON

	<div class="wdtFul" style="padding: 4px 8px; border: 1px solid black">
		<input id="input_close" class="talLft" style="width:95%;border:none;outline:none;float:left;" />
		<span class="talRgt" style="float:right;cursor:pointer" onclick="$('#input_close').val('');$('#input_close').focus()" alt="Reset input"><i class="fa fa-close"></i></span>
		<div class="clearfix"></div>
	</div>
	-->
	<?php
	
	// Get routine shortcut expense
	$obj_list_routine = $param_routine = NULL;
	$param_routine['creator_id'] = member_cookies('member_id');
	$param_routine['is_routine'] = 1;
	$obj_list_routine = $this->expense_model->get_list($param_routine);
	$obj_list_routine = $obj_list_routine['data'];
	// debug($obj_list_routine);
	
	$str_routine = $str_shortcut = NULL;
	if (! empty($obj_list_routine)) 
	{
		$str_shortcut.= "<i class='b text-success'>Shortcut</i><br/>";
		foreach($obj_list_routine as $lsr) 
		{
			
			// $str_routine[] = array(
				// 'expense_id' => $lsr['expense_id'],
				// 'category_id' => $lsr['category_id'],
				// 'amount' => $lsr['raw_amount']
			// );
			
			$str_shortcut .= '<span class="btn btn-xs freq-category pointer" onclick="set_expense_form('.$lsr['expense_id'].','.$lsr['category_id'].',\''.$lsr['title'].'\',\''.number_format($lsr['raw_amount']).'\')">'.$lsr['category_name'].' - '.$lsr['title'].' - ' .$lsr['raw_amount'].'</span> <a href="'.$base_url.'expense/report?do=edit&expense_id='.$lsr['expense_id'].'"><i class="fa fa-edit clrRed"></i></a> <span style="margin-right:10px"></span>';
		}
		// $str_routine = json_encode($str_routine);
		// debug($str_routine);
		// echo $str_shortcut;
	}
	?>
	<!-- START -->
	<form method="post" id="new_form_expense" class="form_submit">
	<div id="table_input" class='hover alert alert-info row'>
		<div class="col-sm-12 br">
			<p class="bg-info talCnt">Form Input Expense</p>
		</div>
		
		
		<div class="col-sm-12 br">
			<div class="form-header">
				Template format:<br/>
				<b>TITLE</b>.<b>TOTAL_AMOUNT</b>.<b>FUND_TYPE.CASHBACK</b>(Optional)<br/>
				example: sembako 30.65.gopay.10 <a href="javascript:void(0)" onclick="$('#f_format_input').val('sembako 30.65.gopay.10');$('#f_format_input').trigger('change')" class="btn btn-xs btn-info">Test format</a><br/>
				<input id="f_format_input" name="f_format_input" class="input wdtFul" type="text" placeholder="Format TITLE;TOTAL_AMOUNT;CATEGORY;FUND_TYPE;CASHBACK(Optional)" name="f_format_input" value="" /><br/><br/>

				<div id="lbl_format_input" style="height:50px"></div><hr/>
				
				<div class="title">Kategori<span class="b clrRed">*</span></div>

			
			<select id="f_category_id" name="f_category_id" class="padMed wdtFul chosen-select text-capitalize" required>
			<option value="">Select Expense / Income</option>
			
			
			<?php
			$i = 1;
			$total = $subtotal_expense =  0;
			if (!empty($list_category))
			{
				// tampung list bersih ke array
				$list_group_category_title = array(
					'-1' => 'expense',
					'0' => 'topup',
					'1' => 'income',
				);
				$list_group_category = NULL;
				foreach ($list_category as $key => $rs) {
					$list_group_category[$rs['is_increment']][] = $rs;
				}
				
				// looping all parent group
				foreach ($list_group_category_title as $kt => $cat_title) 
				{
					?>
					
					<optgroup class="text-capitalize" label="<?php echo $cat_title; ?>"> 
					
					<?php
					if (!empty($list_group_category[$kt]))
					{
						// fill with list data details
						foreach ($list_group_category[$kt] as $key => $rs)
						{
						?>
						<option value="<?php echo $rs['category_id']?>" data-label="<?php echo strtolower($rs['name']) ?>" <?php if (isset($obj['category_id']) && $rs['category_id'] == $obj['category_id']) echo "selected=true"; ?>>
						<b class="text-uppercase"><?php echo $cat_title ?></b> - <?php echo $rs['name']; ?> 
						(monthly <?php echo $rs['total_trx_this_month']; ?> trx 
						
						<?php 
						if (isset($qdsd)) echo " in " . $qdsd; 
						
						if (isset($qdrd)) echo " on " . date("M Y",strtotime($qdrd));
						
						// echo " in ". $param_category['total_trx_this_month'];
						
						// echo " ".$param_category['total_trx_this_year'];
						?>
						<?php 
						?>
						)
						</option>
						<?php
							
						}
					}
					?>
					
					</optgroup>
					<?php
				}
			}
			?>		
			
			</select>
			<?php
			// Show auto select expense most frequent
			$param_freq = $obj_list_frequent_expense = $str_freq_category = NULL;
			$param_freq['creator_id'] = member_cookies('member_id');
			$param_freq['paging'] = TRUE;
			$param_freq['limit'] = 0;
			$param_freq['offset'] = 10;
			$obj_list_frequent_expense = $this->expense_model->get_list_frequent_expense($param_freq);
			$obj_list_frequent_expense = $obj_list_frequent_expense['data'];

			// Show frequent category shortcut
			if ( ! empty($obj_list_frequent_expense))
			{
				foreach($obj_list_frequent_expense as $freq_category)
				{
					$str_freq_category .= '<span class="btn btn-xs freq-category pointer" onclick="update_chosen(\'f_category_id\','.$freq_category['category_id'].')">'.$freq_category['name'].'</span>&nbsp;';
				}
			}
			
			// Show auto select expense most newest
			$param_freq = $obj_list_newest_expense = $str_newest_category = NULL;
			$param_freq['creator_id'] = member_cookies('member_id');
			// $param_freq['is_increment'] = '-1'; // ga pengaruh kenapa eror
			$param_freq['newest'] = TRUE;
			$param_freq['paging'] = TRUE;
			$param_freq['limit'] = 0;
			$param_freq['offset'] = 10;
			// $param_freq['debug'] = 1;
			$param_freq['order_by'] = 'e.release_date DESC ';
			$obj_list_newest_expense = $this->expense_model->get_list_newest_expense($param_freq);
			$obj_list_newest_expense = $obj_list_newest_expense['data'];

			// Show frequent category shortcut
			if ( ! empty($obj_list_newest_expense))
			{
				foreach($obj_list_newest_expense as $freq_category)
				{
					$str_newest_category .= '<span class="btn btn-xs freq-category pointer" onclick="update_chosen(\'f_category_id\','.$freq_category['category_id'].')">'.$freq_category['name'].'</span>&nbsp;';
				}
			}
			?>
			<div>
			<?php if (isset($str_freq_category)) { ?>Top 10 Frequent<br/><?php echo $str_freq_category; ?><br/><?php } ?>
		
			<?php if (isset($str_newest_category)) { ?>Top 10 Newest<br/><?php echo $str_newest_category; ?><br/><?php } ?>
			</div>
			
			<?php if (isset($str_shortcut)) { ?>
			<?php echo $str_shortcut; ?>
			<?php } ?>
		
			</div>
		</div>		
		
		<div class="col-sm-12 br"></div>

		<!--
		<div class="col-sm-6 br">Expense Name <button type="button" class="btn btn-xs btn-default" onclick="$('#f_title').val('')">reset</button><br/>
			<input type="search" class="input wdtFul" name="f_title" id="f_title" placeholder="Nama Pengeluaran" value="<?php if (isset($obj['title'])) echo $obj['title']?>" required />
		</div>
		dibawah ini buat ujicoba doank
		-->
		<div class="col-sm-6 br">
			<div class="form-header">
				<div class="title">Nama Expense<span class="b clrRed">*</span></div>
				<div class="parent-resetinput">
					<input type="text" class="resetinput" style="width:95%;border:none;outline:none;float:left;text-align:left" name="f_title" id="f_title" placeholder="Nama Pengeluaran" value="<?php if (isset($obj['title'])) echo $obj['title']?>" tabindex="1" required />
				</div><br/>
				
				
				<div class="title">Tgl Expense<span class="b clrRed">*</span></div>
				<input type="text" class="input datepicker wdtFul" name="f_release_date" id="f_release_date"  placeholder="Tgl Input Expense" value="<?php if (isset($obj['release_date'])) echo date('d-m-Y',strtotime($obj['release_date'])); else if (isset($_GET['release_date'])) echo date('d-m-Y',strtotime($_GET['release_date'])); else echo date('d-m-Y')?>" tabindex="5" required/>
				<br/>
				<button type="button" class="btn btn-xs btn-warning " onclick="calender_move('minus')">prevday</button> &nbsp;
				<button type="button" class="btn btn-xs btn-primary" onclick="calender_move('plus')">nextday</button>
				<button type="button" class="btn btn-xs btn-default" onclick="$('#f_release_date').val('<?php echo date("d-m-Y",time()); ?>')">today</button>
				<button type="button" class="btn btn-xs btn-default" onclick="$('#f_release_date').val('<?php echo date("d-m-Y",(time()-86400)); ?>')">today-1</button> 
				<button type="button" class="btn btn-xs btn-default" onclick="$('#f_release_date').val('<?php echo date("d-m-Y",(time()-(2*86400))); ?>')">today-2</button> 
				<button type="button" class="btn btn-xs btn-default" onclick="$('#f_release_date').val('<?php echo date("d-m-Y",(time()-(3*86400))); ?>')">today-3</button>
				<button type="button" class="btn btn-xs btn-default" onclick="$('#f_release_date').val('<?php echo date("d-m-Y",(time()-(4*86400))); ?>')">today-4</button>				
			</div>
		</div>		
		
		<div class="col-sm-6 br">
			<div class="form-header">
				<div class="title">Amount<span class="b clrRed">*</span></div>
				<div class="parent-resetinput"><input type="text" class="money resetinput" id="f_amount" name="f_amount" placeholder="Jumlah pengeluaran" value="<?php if (isset($obj['amount'])) echo $obj['amount']?>" size="40" tabindex="2" required /></div><br/>
				
				<b>Cashback</b><br/>
				<input class="pointer" type="checkbox" id="chk_show_cashback" data-size="mini" data-toggle="toggle" data-on="ya" data-off=" tidak" data-onstyle="primary" tabindex="3" /><br/>
			
				<span id="f_cashback_id">
					<div class="parent-resetinput"><input type="text" class="money resetinput" id="f_cashback" name="f_cashback" placeholder="Nominal Cashback" value="<?php if (isset($obj['cashback'])) echo $obj['cashback']?>"tabindex="4" size="40" /></div>
				</span>
			</div>
		</div>
		
		<div class="col-sm-12"></div>
		
		<!--
		<tr>
			<td colspan="2">
			Gunakan alamat
			<input type="radio" name="f_map" id="no_map" checked /><label for="no_map">Tidak</label>&nbsp;&nbsp;&nbsp;
			<input type="radio" name="f_map" id="use_map" /><label for="use_map">Ya</label>
			<input id="pac-input" class="controls" type="text" name="f_gmap_address" placeholder="Lokasi kamu">
			<input type="hidden" name="gmap_long" id="long"  placeholder="longitude"/>
			<input type="hidden" name="gmap_lat" id="lat" placeholder="latitude" />

			<div id="map" class=""></div>
			</td>
		</tr>
		-->
		<div class="col-sm-12"></div>
		
		<div class="col-sm-6 br">
			
		</div>
		<div class="col-sm-6 br">
			
		</div>
			
		<div class="col-sm-12"></div>
		
		<div class="col-sm-6 br">
			<div class="form-header">
				<div class="title">Sumber dana<span class="b clrRed">*</span></div>
			<a tabindex="5" class="btn btn-info btn-xs" href="<?php echo $base_url.'expense/fund'; ?>"><i class="fa fa-money"></i> + Sumber dana baru</a>
			<br/>
			<?php
			/*
			DEPRECATED
			if ( ! empty($list_fund))
			{
				foreach($list_fund as $lf)
				{
					?>
					<input type="radio" name="f_fund_id" value="<?php echo $lf['fund_id']?>" id="fund_<?php echo $lf['fund_id']?>" <?php if ($do == "edit" && isset($obj['fund_type']) && $obj['fund_type'] == $lf['fund_type']) echo "checked" ?> <?php if ( ! isset($obj['fund_type']) && $lf['fund_type'] == 'cash') echo "checked" ?> required > <label for="fund_<?php echo $lf['fund_id']?>" /><?php echo $lf['fund_name']?></label><br/>
					<?php
				}
			}*/
			?>



			<?php
			// debug($category_fund);
			$show_collapse = NULL;
			if ($do == 'edit') {
				$show_collapse = 'in';
			}
			// debug($obj);
			
			if ( ! empty($category_fund))
			{
				$category_cash = $category_fund['cash'];
				
				// Accordion single active using these : data-parent="#accordion"
				// help : https://stackoverflow.com/questions/15696365/twitter-bootstrap-collapse-plugin-how-to-enable-multiple-groups-to-be-opened
				?>
			<!-- start accordion -->
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<!-- start #1 -->
				<div class="panel panel-success">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						<a role="button" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						  Cash
						</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
				<?php
				foreach($category_cash as $lf)
				{
					$color = NULL;
					if (isset($lf['color'])) $color = 'color:'.$lf['color'].';border-bottom: 3px solid '.$lf['color'];
					?>
					<label class="pointer" for="fund_<?php echo $lf['fund_id']?>" style="<?php echo $color ?>" />
					<input class="pointer" type="radio" name="f_fund_id" value="<?php echo $lf['fund_id']?>" id="fund_<?php echo $lf['fund_id']?>" <?php if ($do == "edit" && isset($obj['fund_type']) && $obj['fund_type'] == $lf['fund_type']) echo "checked" ?> <?php if ( ! isset($obj['fund_type']) && $lf['fund_type'] == 'cash') echo "checked" ?> data-label="cash" required > <?php echo $lf['fund_name']?></label>
					<?php
				}
				?>
						</div>
					</div>
				</div>
				<?php

				if ( ! empty($category_fund['bank_account']))
				{
					$category_bank_account = $category_fund['bank_account'];
					?>
					<!-- start #2 -->
					<div class="panel panel-info">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							  Bank Account
							</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse in <?php echo $show_collapse?>" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
				<?php
					foreach($category_bank_account as $lf)
					{
						$color = NULL;
						if (isset($lf['color'])) $color = 'color:'.$lf['color'].';border-bottom: 3px solid '.$lf['color'];
						?>
						<label class="pointer" for="fund_<?php echo $lf['fund_id']?>" style="margin-right:20px;<?php echo $color?>"  />
						<input class="pointer" type="radio" name="f_fund_id" value="<?php echo $lf['fund_id']?>" id="fund_<?php echo $lf['fund_id']?>" <?php if ($do == "edit" && isset($obj['fund_id']) && $obj['fund_id'] == $lf['fund_id']) echo "checked" ?> data-label="<?php echo ($lf['shortcode']) ? strtolower($lf['shortcode']) : strtolower($lf['fund_name']) ;?>" required > <?php echo $lf['fund_name']?></label>
						<?php
					}
				?>
							</div>
						</div>
					</div>
					<?php
				}


				if ( ! empty($category_fund['credit_card']))
				{
					$category_credit_card = $category_fund['credit_card'];
					?>
					<!-- start #3 -->
					<div class="panel panel-warning">
						<div class="panel-heading" role="tab" id="headingThree">
							<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							  Credit Card
							</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse in <?php echo $show_collapse?>" role="tabpanel" aria-labelledby="headingThree">
							<div class="panel-body">
				<?php
					foreach($category_credit_card as $lf)
					{
						$color = NULL;
						if (isset($lf['color'])) $color = 'color:'.$lf['color'].';border-bottom: 3px solid '.$lf['color'];
						?>
						<label class="pointer" for="fund_<?php echo $lf['fund_id']?>" style="margin-right:20px;<?php echo $color; ?>" />
						<input class="pointer" type="radio" name="f_fund_id" value="<?php echo $lf['fund_id']?>" id="fund_<?php echo $lf['fund_id']?>" <?php if ($do == "edit" && isset($obj['fund_id']) && $obj['fund_id'] == $lf['fund_id']) echo "checked" ?> data-label="<?php echo ($lf['shortcode']) ? strtolower($lf['shortcode']) : strtolower($lf['fund_name']) ;?>" required > <?php echo $lf['fund_name']?></label>
						<?php
					}
					?>
							</div>
						</div>
					</div>
				<?php
				}

				if ( ! empty($category_fund['others']))
				{
					$category_others = $category_fund['others'];
					?>
					<!-- start #4 -->
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingFour">
							<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
							  Others
							</a>
							</h4>
						</div>
						<div id="collapseFour" class="panel-collapse collapse in <?php echo $show_collapse?>" role="tabpanel" aria-labelledby="headingFour">
							<div class="panel-body">
				<?php
					foreach($category_others as $lf)
					{
						$color = NULL;
						if (isset($lf['color'])) $color = 'color:'.$lf['color'].';border-bottom: 3px solid '.$lf['color'];
						?>
						<label class="pointer" for="fund_<?php echo $lf['fund_id']?>" style="margin-right:20px;<?php echo $color?>" />
						<input class="pointer" type="radio" name="f_fund_id" value="<?php echo $lf['fund_id']?>" id="fund_<?php echo $lf['fund_id']?>" <?php if ($do == "edit" && isset($obj['fund_type']) && $obj['fund_id'] == $lf['fund_id']) echo "checked" ?> data-label="<?php echo ($lf['shortcode']) ? strtolower($lf['shortcode']) : strtolower($lf['fund_name']) ; ?>" required > <?php echo $lf['fund_name']?></label>
						<?php
					}
					?>
							</div>
						</div>
					</div>
				<?php
				}
				?>
			</div>
				<?php
			}
			?>
			</div>
		</div>
		
		<div class="col-sm-6 br">
			<!-- STATUSBAYAR START -->
			<div class="form-header">
				<div class="title"><label for="f_is_paid">Pembayaran</label><span class="b clrRed">*</span></div>
				<input type="checkbox" data-size="mini" data-toggle="toggle" data-on="<i class='fa fa-check'></i> Sudah dibayarkan" data-off="Belum bayar" data-onstyle="success" name="f_is_paid" id="f_is_paid" value="1" <?php if (isset($obj['is_paid']) && $obj['is_paid'] == 1) echo 'checked="true"'; ?>" checked="true" data-width="130"/>
			</div>
			<!-- STATUSBAYAR END -->
			
			<!-- CATATAN START -->
			<div class="form-header">
				<div class="title">Beri Catatan</div>
				<label class="pointer"><input class="pointer" type="checkbox" id="chk_show_notes" data-size="mini" data-toggle="toggle" data-on="ya" data-off="tidak" data-onstyle="primary" /></label><br/>
				<textarea id="f_notes" name="f_notes" class="input wdtFul" placeholder="Catatan" rows="5"><?php if (isset($obj['notes'])) echo $obj['notes']?></textarea>
			</div>
			<!-- CATATAN END -->
			
			<!-- LOCATION INPUT START -->
			<div class="form-header">
				<div class="title">Lokasi</div>
				<input class="pointer" type="checkbox" id="chk_show_location" data-size="mini" data-toggle="toggle" data-on="ya" data-off="tidak" data-onstyle="primary" />	<br/><br/>

				<span id="f_location_id">
					<select name="f_location_id" class="input padMed wdtFul br chosen-select">
					<option value="">Select Location <b class="f12">*optional</b></option>
					<?php
					if (!empty($list_location))
					{
						foreach ($list_location as $location)
						{
						?>
						<option value="<?php echo $location['location_id']?>" <?php if ($do == "edit" && isset($obj['location_id']) && $location['location_id'] == $obj['location_id']) echo "selected=true" ?> ><?php echo $location['location_name']; ?></option>
						<?php
						}
					}
					?>
					</select>			
				</span>			
			</div>			
			<!-- LOCATION INPUT END -->
			
			<!-- SHORTUCT INPUT END -->
			<div class="form-header">
				<div class="title">Simpan jadi shortcut <i class="fa fa-question-circle pointer" data-toggle="popover" data-trigger="hover" title="Fitur Shortcut" data-content="*jika ya maka akan muncul shortcut untuk expense dengan nominal tsb."></i></div>
				<input type="checkbox" name="f_is_routine" value="1" id="f_is_routine" data-size="mini" data-toggle="toggle" data-on="ya" data-off="tidak" data-onstyle="primary" <?php if ($do == "edit" && isset($obj['is_routine']) && $obj['is_routine']) echo "checked=true" ?> />
			</div>
			
			<!-- SHORTCUT INPUT START -->
			
			<!-- REMINDER INPUT START -->
			<div class="form-header">
				<div class="title">Reminder <i class="fa fa-question-circle pointer" data-toggle="popover" data-trigger="hover" title="Fitur Reminder" data-content="Fitur reminder ini akan mengirim detail expense ke alamat email anda saat tgl jatuh tempo."></i></div>
			
				<label for="is_reminder0" class="pointer"><input type="radio" class="pointer" name="is_reminder" value="0" id="is_reminder0" <?php if (!isset($obj['is_get_notif']) || $obj['is_get_notif'] == 0) echo " checked "; ?> /> No</label> &nbsp; &nbsp;<label for="is_reminder1" class="pointer"><input type="radio" class="pointer" name="is_reminder" value="1" id="is_reminder1" <?php if (isset($obj['is_get_notif']) && $obj['is_get_notif'] == 1) echo " checked "; ?> /> Yes</label>
				
				<div class="reminder-date br well bgWht">
					<label>Email Reminder active <input type="checkbox" value="1" id="f_is_get_notif" name="f_is_get_notif" <?php if (isset($obj['is_get_notif']) && $obj['is_get_notif'] == 1) echo " checked "; ?> /></label>
					
					<div>Reminder Billing Date*<br/><input type="text" class="input datepicker" name="f_reminder_date" id="f_reminder_date" placeholder="Tgl pengingat" size="14" value="<?php if (isset($obj['reminder_date'])) echo date('d-m-Y',strtotime($obj['reminder_date']));?>"/>
					
					<?php 
					$rm = $rm_hour = $rm_minute = NULL;
					// f_reminder_date
					if (isset($obj['reminder_date'])) {					
						$rm = explode(' ',$obj['reminder_date']); 
						$rm = explode(':',$rm[1]); 
						$rm_hour = $rm[0];
						$rm_minute = $rm[1];
						// debug($rm);
					}
					?>
					Hour
					<select class="input" name="f_reminder_hour" id="f_reminder_hour">
					<?php
					for ($hour=0;$hour<=23;$hour++) {
						if ($hour <= 9) $hour = "0".$hour;

						// CHECK VALUE EXACT TIME
						$checked_hour = NULL;
						if (isset($rm_hour) && $rm_hour == $hour) $checked_hour = " selected ";
					?>
					<option value="<?php echo $hour?>" <?php echo $checked_hour?>><?php echo $hour; ?></option>
					<?php
					}
					?>
					</select>
					:
					<select class="input" name="f_reminder_minute" id="f_reminder_minute">
					<?php
					for ($minute=0;$minute<=60;$minute++) {
						if ($minute <= 9) $minute = "0".$minute;

						// CHECK VALUE EXACT TIME
						$checked_minute = NULL;
						if (isset($rm_minute) && $rm_minute == $minute) $checked_minute = " selected ";
					?>
					<option value="<?php echo $minute?>" <?php echo $checked_minute?>><?php echo $minute; ?></option>
					<?php
					}
					?>
					</select>
					on
					<select class="input" name="f_reminder_minus_day">
					<?php
					$arr_day = array('0','1','2','3','4','5','6','7','8','9','10','14','21','30','60');

					foreach ($arr_day as $key_day => $day) {
						$str_reminder = 'H day';
						if ($day >= 1)
							$str_reminder = 'H - '.$day.' day';
					?>
					<option value="<?php echo $day?>" <?php if (isset($obj['reminder_minus_day']) && $obj['reminder_minus_day'] == $day) echo " selected "; ?> ><?php echo $str_reminder ?></option>
					<?php
					}
					?>
					</select>
					</div>
				</div>
			</div>
			<!-- REMINDER INPUT END -->
		</div>
				
		<div class="col-sm-12"></div>
		
		<div class="col-sm-12 talCnt br" style="padding-top: 30px">
			<?php if (count($list_category) < 1) { ?>
			<div class="alert bg-warning">Ooops, tampaknya anda belum memiliki kategori pengeluaran<br/>Silakan input kategori pengeluaran disini <a class="btn btn-info btn-xs" href="<?php echo $base_url.'expense/category?do=insert';?>" target="_blank">Input kategori</a></div>
			<?php } else if ($do == "insert" || !isset($do)) { ?>
			<div id="lbl_info_inpexp"></div>
			<input type="hidden" name="hdn_insert" value="1" />
			<button type="submit" class="btn btn-success btn-block btn-md save" id="btn_save">SAVE</button>
			<?php } else { ?>
			<input type="hidden" name="hdn_update" value="1" />
			<button type="submit" class="btn btn-success btn-block btn-md save" id="btn_update">UPDATE</button>
			<?php } ?>
		</div>

	</div>
	</form>
	<!-- END -->
	<br/>

	<?php
	// }
	?>
	<!-- FORM INPUT -->

	<script src="//code.highcharts.com/highcharts.js"></script>
	<script src="//code.highcharts.com/modules/exporting.js"></script>

	<!-- showing shortcut for all report -->
	<!-- <div id="floating-report">
		<a href="#rpt_summary" class="btn btn-lg btn-success">Report Summary</a>
		<a href="#rpt_reminder" class="btn btn-lg btn-danger">Reminder</a>
		<a href="#show_detail_expense" class="btn btn-lg btn-primary">Detail Expense</a>
		<a href="#rpt_recurring_expense" class="btn btn-lg btn-warning">Recurring Expense</a>
		<br/>
		<br/>
	</div>
	-->


	<!-- TABS PARENT START -->
	<div id="tabs" class="">
		<?php
		$url_start_end = '';
		if (isset($get['start_date']) && isset($get['end_date'])) {
			$url_start_end.= '?start_date='.$get['start_date'].'&end_date='.$get['end_date'];
		}
		?>
		<ul>
			<li class="pointer"><a href="#tabs-0" class="pointer">Dashboard</a></li>
			<li class="pointer"><a href="#tabs-1" class="pointer">Expense & Budget</a></li>
			<li class="pointer"><a href="#tabs-5" class="pointer">Income</a></li>
			<li class="pointer"><a href="#tabs-2" class="pointer">Recurring Expense</a></li>
			<li class="pointer"><a href="#tabs-3" class="pointer">Parent Category</a></li>
			<li class="pointer"><a href="#tabs-4" class="pointer">Reminder</a></li>
			
			<li class="pointer"><a href="tab-fund-type">Fund Report</a></li>
			<li class="pointer"><a href="tab-detail-expense<?php echo $url_start_end; ?>">Detail</a></li>
		</ul>
		<!--
			<li class="pointer"><a href="#tabs-" class="pointer"></a></li>
			
			<li><a href="#tabs-1">Nunc tincidunt</a></li>
		<div id="tabs-1">
			<p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
		</div>
		-->
		<div id="tabs-0">
			Choose your menu
		</div>

		<div id="tabs-1">
			<?php
			$total_income = 0;

			$param['is_detail'] = 0;
			$param['is_increment'] = -1;
			$param['group_by'] = 'category_name';
			$param['order_by'] = 'amount DESC';
			
			// activate filter
			$param['start_date'] = $start_date;
			$param['end_date'] = $end_date;

			// if (isset($get['start_date']) && isset($get['end_date'])) {
				// $param['start_date'] = $get['start_date'];
				// $param['end_date'] = $get['end_date'];
			// }
			// $param['debug'] = '1';

			$list_report_by_category = $this->expense_model->get_list_report($param);
			$list_report_by_category = $list_report_by_category['data'];
			?>
			<br/>
			<table class='table hover table-bordered' id="rpt_summary">
				<tr>
					<td colspan="5" class="bg-info talCnt">SUMMARY REPORT BY CATEGORY<?php echo BR.'From ' .$start_date_format.' to '.$end_date_format; ?></td>
				</tr>
				<tr>
					<td>#</td>
					<td>Date</td>
					<td>Expense</td>
					<td>Budget</td>
					<td>Status Budget</td>
				</tr>
				<?php
				$i = 1;
				$grand_total_expense = $subtotal_expense =  0;
				foreach ($list_report_by_category as $key => $rs) {

					//CHECK IF EXPENSE EXIST
					$expense = $budget = $overbudget = NULL;
					$expense = $rs['amount'];
					$budget = $rs['monthly_budget_amount'];
					
					if ($rs['is_increment'] == 1) {
						$total_income += $expense;
					}
					// foreach( $list_budget as $bgt)
					// {
						// if ( $bgt['category_id'] == $rs['category_id']) { $expense = $bgt['amount']; $subtotal_expense += $expense;}
					// }

				?>
				<tr>
					<td><?php echo $i?></td>
					<td><?php if (isset($rs['parent_category_name'])) echo '<b class=" clrRed">['.$rs['parent_category_name'].']</b>'; ?><?php echo $rs['category_name']?></td>
					<td class="talRgt"><?php if (isset($expense)) echo format_money($expense)?></td>
					<td class="talRgt"><?php if (isset($budget)) echo format_money($budget)?></td>
					<td class="talRgt">
					<?php
					$flag_expense = $expense_percentage = $expense_percentage_width = 100;
					$progressbar = 'progress-bar-danger';
					if (isset($expense) && $expense > 0 && $budget > 0)
					{
						$flag_expense = '<i class="fa fa-delete text-danger"></i>';
						$progressbar = 'progress-bar-danger';

						// if ($budget > 0)
						// {
							if ($expense <= $budget)
							{
								$flag_expense = '<i class="fa fa-check text-success"></i>';
								$progressbar = 'progress-bar-success';
							}
							$expense_percentage = ceil(($expense / $budget) * 100); 
							
							
							// in percentage
						// }
						// echo $flag_expense;
						if ($expense_percentage > 100) $expense_percentage_width = 100;
						else if ($expense_percentage < 5) $expense_percentage_width = 5;
						else $expense_percentage_width = $expense_percentage;

							// debug('ayam');
							// debug($expense_percentage);
							// debug('bebek');
							// debug($expense_percentage_width);
						?>
						<div class="progress">
							<div class="progress-bar <?php echo $progressbar?>" role="progressbar" aria-valuenow="<?php echo $expense_percentage ?>"
							aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $expense_percentage_width ?>%">
								<?php echo $expense_percentage ?>%
							</div>
						</div>
						<?php 
						if ($expense < $budget) { 
							echo "<span class='clrGrn f13'>Sisa budget: ". format_money($budget - $expense)."</span>";
						} else if ($expense == $budget) {  
						} else { 
							$overbudget_percentage = $expense_percentage - 100; 
							$overbudget = $expense - $budget;
							echo "<span class='clrRed f13'>Overbudget: ". format_money($overbudget)."(".$overbudget_percentage."%)</span>"; 
						}
						?>
					<?php
					}
					?>
					</td>
				</tr>
				<?php
					$i++;
					$grand_total_expense+= str_replace('.','',$rs['amount']);
				}
				$total_revenue = 0;
				$total_revenue = $total_income - $grand_total_expense;

				$bg_revenue = NULL;
				if ($total_revenue > 0) $bg_revenue = 'btn-success';
				else if ($total_revenue < 0) $bg_revenue = 'btn-danger';
				?>
				<tr>
					<td colspan="2">TOTAL EXPENSE</td>
					<td class="talRgt"><?php echo format_money($grand_total_expense) ?></td>
					<td><?php //echo format_money($subtotal_expense) ?></td>
					<td><?php //echo format_money($subtotal_expense) ?></td>
				</tr>
				<tr>
					<td colspan="2">TOTAL INCOME</td>
					<td class="talRgt"><?php echo format_money($total_income) ?></td>
					<td><?php //echo format_money($subtotal_expense) ?></td>
					<td><?php //echo format_money($subtotal_expense) ?></td>
				</tr>
				<tr>
					<td colspan="2">TOTAL NET REVENUE</td>
					<td class="talRgt" align="right"><div class="talRgt b btn wdtFul <?php echo $bg_revenue?>"><?php echo format_money($total_revenue) ?></div></td>
					<td><?php //echo format_money($subtotal_expense) ?></td>
					<td><?php //echo format_money($subtotal_expense) ?></td>
				</tr>
			</table>
		</div>
		<div id="tabs-2">
			<?php
			$list_monthly_billing_category = $param_monthly = array();
			$param_monthly['is_delete'] = 0;

			$param_monthly['start_date'] = $start_date;
			$param_monthly['end_date'] = $end_date;

			$param_monthly['creator_id'] = member_cookies('member_id');
			$param_monthly['order'] = "total_transaction_count DESC, category_name ASC";
			$list_monthly_billing_category = $this->category_model->get_list_monthly_billing_category($param_monthly);
			$list_monthly_billing_category = $list_monthly_billing_category['data'];
			if ( ! empty($list_monthly_billing_category))
			{
			?>
			<table class="table table-striped table-bordered" id="rpt_recurring_expense">
				<tr>
					<td colspan="5" class="bg-warning talCnt">List Recurring Expense<br/>menampilkan seluruh expense bulanan rutin<?php echo BR.'From ' .$start_date_format.' to '.$end_date_format; ?></td>
				</tr>
				<tr>
					<td width="1">#</td>
					<td>Nama kategori</td>
					<td>Tgl Due Date</td>
					<td>Total Transaksi<br/>bulan ini</td>
					<td>Payment</td>
				</tr>
				<?php
					// debug($list_monthly_billing_category);
					foreach($list_monthly_billing_category as $key => $rsb)
					{
						$key++;

						$status_paid = $trcolor = $trstatus = NULL;
						if (isset($rsb['due_date']) && $rsb['due_date'] > 0)
						{
							//if (date('d') >= $rsb['due_date'] || $rsb['total_transaction_count'] >= 1) {
							if ($rsb['total_transaction_count'] >= 1) {
								$status_paid = '<i class="fa fa-check text-success"></i>';
								$trcolor = 'btn-success';
								$trstatus = 'Paid';
							} else if (date('d') >= $rsb['due_date']) {
								$trcolor = 'btn-danger';
								$trstatus = "overdue";
								$status_paid = '<i class="fa fa-remove text-danger"></i>';
								$status_paid.= ' <span class="clrRed">Passed '. ceil(date('d') - $rsb['due_date']).' day</span>';
							} else {
								$trcolor = 'btn-warning';
								$trstatus = 'soon will expired';
								$status_paid = '<i class="fa fa-exclamation text-warning"></i>';
								$status_paid.= ' <span class="text-warning">expired in -'. ceil($rsb['due_date'] - date('d')).' days</span>';
							}
						}
				?>
				<tr >
					<td><?php echo $key; ?></td>
					<td>
					<div>
						<div class="pull-left"><?php echo $rsb['category_name']?></div>
						<div class="pull-right"><span class="btn btn-xs <?php echo $trcolor; ?>"><?php echo $trstatus; ?></span><br/></div>
					</div>
					<?php if (isset($rsb['notes'])) echo BR.'<div class="text-info b">Notes:'.BR.nl2br($rsb['notes'].'</div>'); ?></td>
					<td><?php if (isset($rsb['due_date'])) echo $rsb['due_date']; else echo '-'; ?></td>
					<td><?php echo $rsb['total_transaction_count']?></td>
					<td><?php echo $status_paid.BR.format_money($rsb['total_transaction_amount']); ?></td>
				</tr>
				<?php
					}
				?>
			</table>
		</div>
		<!-- PARENT REPORT START-->
		<div id="tabs-3">
			<?php
				$paramtmp = array();
				$paramtmp['creator_id'] = member_cookies('member_id');
				
				$paramtmp['start_date'] = $start_date;
				$paramtmp['end_date'] = $end_date;
				// $paramtmp['month'] = date('m');
				// $paramtmp['year'] = date('Y');
				
				$list_parent_report = $this->category_model->get_list_parent($paramtmp);
				$list_parent_report = $list_parent_report['data'];
				// $list_parent_report = $this->category_model->get_list($paramtmp);
				// debug($list_monthly_billing_category);
				$total_expense_parent = 0;
				
			?>
			<table class="table table-striped table-bordered" id="rpt_report_parent">
				<tr>
					<td colspan="5" class="bg-warning talCnt">List Parent Expense<br/>menampilkan seluruh expense Parent Category <?php echo BR.'From ' .$start_date_format.' to '.$end_date_format; ?></td>
				</tr>
				<tr>
					<td width="1">#</td>
					<td>Nama kategori</td>
					<td>Total Amount</td>
					<td>Option</td>
				</tr>
				<?php 
				$i=0;
				foreach($list_parent_report as $key => $rse) { 
					$i++;
					$total_expense_parent+=$rse['total_expense'];
					?>
				<tr >
					<td><?php echo $i; ?></td>
					<td><?php echo $rse['name']?></td>
					<td class="talRgt"><?php echo format_money($rse['total_expense']); ?></td>
					<td></td>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="2">Grandtotal</td>
					<td class="talRgt"><?php echo format_money($total_expense_parent)?></td>
				</tr>
			</table>
			
		</div>
		<!-- PARENT REPORT END -->
			
		<div id="tabs-4">
		<!-- LIST INCOMING REMINDER START -->
			<?php
			$param_reminder['group_by'] = NULL;
			$param_reminder['is_budget'] = 0;
			$param_reminder['is_detail'] = 1;
			$param_reminder['creator_id'] = member_cookies('member_id');
			$param_reminder['active_month_reminder'] = 1;
			$list_incoming_reminder = $this->expense_model->get_list_report($param_reminder);
			$list_incoming_reminder = $list_incoming_reminder['data'];
			$total_income_reminder = 0;
			?>

			<div id="toggle_incoming_reminder" class="btn btn-info talCnt wdtFul br"><i class="fa fa-arrow-down"></i> INCOMING REMINDER &nbsp;<?php if (count($list_incoming_reminder) > 0) echo "<button type='button' class='btn btn-danger'> <span class='badge b'>".count($list_incoming_reminder)."</span></button>"; ?></div><br/>

			<table class="table table-bordered hover" id="incoming_reminder" style="">
				<tr>
					<td colspan="4" class="bg-info talCnt">INCOMING REMINDER</td>
				</tr>
				<tr class="b text-uppercase bg-info">
					<td>#</td>
					<td>ReminderDate</td>
					<td>Desc</td>
					<td>Amount</td>
				</tr>
			<?php


			if (!empty($list_incoming_reminder))
			{
				foreach ($list_incoming_reminder as $key => $reminder)
				{
					$diff_date = $diff_date_day = $diff_date_hour = NULL;

					$diff_date = strtotime($reminder['reminder_date']) - time();
					$diff_date_day = floor($diff_date/(60*60*24));
					$diff_date_hour = floor($diff_date/(60*60));
					?>
				<tr class="<?php if ($diff_date_hour < 0) echo "bg-warning"; ?>">
					<td>#<?php echo $key+1 ?></td>
					<td><?php
					if ($diff_date_hour < 0)
						echo '<div class="padMed bg-danger"><i class="fa fa-truck"></i> Reminder passed</div>'.date('D d-M-Y',strtotime($reminder['reminder_date'])) . ' <br/>reminder passed ' . $diff_date_day.' day or '.$diff_date_hour .' hour';
					else
						echo '<div class="padMed bg-success"><i class="fa fa-truck"></i> Reminder active</div>'.date('D d-M-Y',strtotime($reminder['reminder_date'])) . ' <br/>reminder in ' . $diff_date_day.' day or '.$diff_date_hour .' hour';
					?></td>
					<td><?php echo '<b class="text-uppercase">'.$reminder['category_name'].'</b> - '.$reminder['title'] ?>
					<a href="<?php echo current_url().'?do=edit&expense_id='.$reminder['expense_id'];?>" title="Edit data"><i class="fa fa-edit fa-lg"></i></a>
					</td>
					<td class="talRgt"><?php echo format_money(str_replace('.','',$reminder['amount']))?></td>
				</tr>
					<?php
					$total_income_reminder += str_replace('.','',$reminder['amount']);
				}
				?>
				<tr>
					<td colspan="3">TOTAL</td>
					<td class="talRgt"><?php echo format_money($total_income_reminder)?></td>
				</tr>
			<?php
			}
			else
			{
				?>
				<tr class="talCnt">
					<td colspan="4">No data</td>
				</tr>
				<?php
			}
			?>
			</table>
			<br/>

			<!-- LIST INCOMING REMINDER END -->
		</div>
		
		<div id="tabs-5">
			<!-- LIST INCOME START -->
			<table class="table table-bordered hover">
				<tr>
					<td colspan="5" class="bg-success talCnt">INCOME THIS MONTH</td>
				</tr>
			<?php
			$param['group_by'] = NULL;
			$param['is_budget'] = 0;
			$param['is_increment'] = 1;
			$param['is_detail'] = 1;
			$param['creator_id'] = member_cookies('member_id');
			$list_income = $this->expense_model->get_list_report($param);
			// debug($list_income);
			$list_income = $list_income['data'];
			$total_income = 0;

			if (!empty($list_income))
			{
				foreach ($list_income as $key => $income)
				{
					?>
				<tr>
					<td>#<?php echo $key+1 ?></td>
					<td><?php echo date('D d-M-Y',strtotime($income['release_date']))?></td>
					<td><?php echo '<b>'.$income['category_name'].'</b> - '.$income['title'];?> <a href="<?php echo current_url().'?do=edit&expense_id='.$income['expense_id'];?>" title="Edit data"><i class="fa fa-edit fa-lg"></i></a></td>
					<td><?php echo format_money(str_replace('.','',$income['amount']))?></td>
					<td class="talCnt">

					<a href="<?php echo current_url().'?do=delete&expense_id='.$income['expense_id'];?>" title="Edit data"><i class="fa fa-times-circle fa-lg clrRed"></i></a></td>
				</tr>
					<?php
					$total_income += str_replace('.','',$income['amount']);
				}
				?>
				<tr>
					<td colspan="4">TOTAL</td>
					<td><?php echo format_money($total_income)?></td>
				</tr>
			<?php
			}
			else
			{
				?>
				<tr class="talCnt">
					<td colspan="4">No data</td>
				</tr>
				<?php
			}
			?>
			</table>
			<!-- LIST INCOME END -->
			
			
			<!-- LIST TOPUP START -->
			<table class="table table-bordered hover">
				<tr>
					<td colspan="5" class="bg-warning talCnt">TOPUP THIS MONTH</td>
				</tr>
			<?php
			$param['group_by'] = NULL;
			$param['is_budget'] = 0;
			$param['is_increment'] = 0;
			$param['is_detail'] = 1;
			$param['creator_id'] = member_cookies('member_id');
			$list_topup = $this->expense_model->get_list_report($param);
			// debug($list_topup);
			$list_topup = $list_topup['data'];
			$total_topup = 0;

			if (!empty($list_topup))
			{
				foreach ($list_topup as $key => $topup)
				{
					?>
				<tr>
					<td>#<?php echo $key+1 ?></td>
					<td><?php echo date('D d-M-Y',strtotime($topup['release_date']))?></td>
					<td><?php echo '<b>'.$topup['category_name'].'</b> - '.$topup['title'];?> <a href="<?php echo current_url().'?do=edit&expense_id='.$topup['expense_id'];?>" title="Edit data"><i class="fa fa-edit fa-lg"></i></a></td>
					<td><?php echo format_money(str_replace('.','',$topup['amount']))?></td>
					<td class="talCnt">

					<a href="<?php echo current_url().'?do=delete&expense_id='.$topup['expense_id'];?>" title="Edit data"><i class="fa fa-times-circle fa-lg clrRed"></i></a></td>
				</tr>
					<?php
					$total_topup += str_replace('.','',$topup['amount']);
				}
				?>
				<tr>
					<td colspan="4">TOTAL</td>
					<td><?php echo format_money($total_topup)?></td>
				</tr>
			<?php
			}
			else
			{
				?>
				<tr class="talCnt">
					<td colspan="4">No data</td>
				</tr>
				<?php
			}
			?>
			</table>
			<!-- LIST TOPUP END -->
		</div>
		
		<!--
		<div id="tabs-">
		</div>
		-->
		
	</div>
	<!-- TABS PARENT END -->
	
	<!-- showing shortcut for all report -->

	<div id="toggle_show_report" class="btn btn-info wdtFul br"><i class="fa fa-arrow-down"></i> SHOW REPORT</div>
	<div id="show_report" style="" class="col-sm-12 talCnt">
	<?php if ( ! empty($obj_list_expense_detail)) { ?>
		<div id="chart_round" style="width: 100%; height: 400px;"></div>
		<div id="chart_graph_daily" style="width: 100%; height: 400px;"></div>
	<?php } else { ?>
		Maaf, data expense anda belum tersedia. Silakan input data untuk melihat grafik ini.<br/><br/>
	<?php } ?>
	</div>

	<br/>
	<?php
	$param['is_detail'] = 1;
	$param['is_increment'] = 1;
	$param['is_budget'] = 1;
	// $param['debug'] = 1;
	$list_budget = $this->expense_model->get_list_report($param);
	$list_budget = $list_budget['data'];
	if (!empty($list_budget))
	{
	?>
		<table class="table table-bordered hover">
		<tr>
			<td colspan="3" class="bg-primary talCnt">BUDGET</td>
		</tr>
		<?php
		//var_dump($list_budget);die;
		$i = 1;
		foreach ($list_budget as $key => $budget)
		{
			?>
		<tr>
			<td>#<?php echo $i ?></td>
			<td><?php echo $budget['category_name']?></td>
			<td><?php echo $budget['amount']?></td>
		</tr>
			<?php
		}
		?>
		<tr>
			<td colspan="2">TOTAL</td>
			<td></td>
		</tr>

		</table>
	<?php
	}
	?>

	<br/>
	<?php
		$obj = $this->expense_model->get_report($param);
		// debug($obj);
		if (is_filled($obj['total_income']))
		{
			// $current_money = $obj['total_income'] - $obj['total_expense'];
			$current_money = 0;
			$current_money = $obj['total_income'] - $obj['total_expense'];
			if ($current_money >= 0) $current_money = '+'.$current_money;
			?>
			<?php //echo $obj['total_income']." - ".$obj['total_expense']." = ".$obj['total_revenue'] ?><hr/>

			<div class="alert bg-success col-sm-12 col-md-3 talCnt" style="font-size:28px">INCOME <i class="fa fa-dollar"></i><hr class="clrBlk">
					<?php echo format_money($obj['total_income'])?></div>
			<div class="alert bg-danger col-sm-12 col-md-3 talCnt" style="font-size:28px">TOPUP <i class="fa fa-dollar"></i><hr class="clrWht">
					<?php echo format_money($obj['total_topup'])?></div>
			<div class="alert bg-primary col-sm-12 col-md-3 talCnt"><div style="font-size:28px">EXPENSE <i class="fa fa-bank"></i><hr class="clrBlk">
					<?php echo format_money($obj['total_expense'])?></div><small class="clrWht f10">* Expense kartu kredit & topup emoney tidak dihitung</small></div>
			<div class="alert bg-warning col-sm-12 col-md-3 talCnt" style="font-size:28px">CURRENT <i class="fa fa-money"></i><hr class="clrBlk">
					<?php echo format_money($current_money)?></div>

			<!--
			<table class="table table-bordered">
				<tr style="font-size:40px">
					<td class="alert bg-success" width="50%"><div class="talCnt">INCOME <i class="fa fa-dollar"></i><hr class="clrBlk">
					<?php echo format_money($obj['total_income'])?></div></td>
					<td class="alert bg-primary" width="50%">
					<div class="talCnt">EXPENSE <i class="fa fa-bank"></i><hr>
					<?php echo format_money($obj['total_expense'])?></div></td>
				</tr>
			</table>
			-->
			<?php
		}
	?>

	<!--
	deprecated// UNUSED 27 oct
	<div id="toggle_show_detail_expense" class="btn btn-danger wdtFul br"><i class="fa fa-arrow-down"></i> SHOW DETAIL EXPENSE </div>
	for auto hide use style=display:none
	-->
	

	<?php
		}
	?>
	
	
</div>
<script>
function updateReleaseDate(r) {		
		now = moment(r).format("MM-DD-YYYY");
		$('#f_release_date').val(now);
		// console.log(now);
		// return now;
	}
	// function updateLabelInfo(pcatname) {
		// 26 okt add label div information on save
		// var incatname = pcatname;
		// var intitle = $('#f_title').val();
		// var inamount = $('#f_amount').val();
		// var indate = $('#f_release_date').val();
		// var inpaytype = $('input[name=f_fund_id]:checked').parent().text();
		// var infotext = 'Ready: ' + incatname + ' - <b>' + intitle + '</b> IDR <b>' + inamount + '</b> on <b>' + indate + '</b> with pay <b>' + inpaytype + '</b>';
		// $('#lbl_info_inpexp').html(infotext);
		// console.log(intitle);
		// console.log(inamount);
		// console.log(indate);
		// console.log(inpaytype);
	// }
	
$(function () {
	/*
	input with format
	example: 
	kuah kuning 15, pangsit kwetiau 30, buat kepu 30;65;gopay;10
	sembako 30;65;gopay
	sembako 30;65;gopay;10
	*/

	$('#f_format_input').change(function(){
		$('#lbl_format_input').html('');
		// explode by ; then 
		// #0 -> title
		// #1 -> total_amount
		// #2 -> fund_type search like 
		// #3 -> (optional cashback amount)
		
		// #X -> category search like
		// var inputs = explode(';',$(this).val());
		// var inputs = $(this).val().split(';');
		var inputraw = $(this).val();
		var inputs = $(this).val().split('.');
		var is_flag_format = true;
		
		// console.log(inputs);
		var xno, xyes, xlabel;
		xno = '<i class="fa fa-times clrRed b"></i> | ';
		xyes = '<i class="fa fa-check clrGrn b"></i> | ';
		xlabel = '';
		
		xlabel += 'title ';
		if (inputs[0]) {
			$('#f_title').val(inputraw);
			xlabel += xyes;
		}  else {
			xlabel += xno;
		}
		
		xlabel += 'amount ';
		if (inputs[1]) {
			$('#f_amount').val(inputs[1]);
			$('#f_amount').trigger('focusout');
			xlabel += xyes;
		}  else {
			xlabel += xno;
		}
		
		xlabel += 'fund_id ';
		if (inputs[2]) {
			stri = 'input[name=f_fund_id][data-label=' + inputs[2] + ']'; 
			$(stri).attr('checked',true);
			// console.log(stri);
			xlabel += xyes;
		} else {
			xlabel += xno;
		}
		// optional
		xlabel += 'cashback ';
		if (inputs[3]) {
			$("#chk_show_cashback").bootstrapToggle('on');
			$('#f_cashback').val(inputs[3]);
			xlabel += xyes;
		} else {
			$('#f_cashback').val(0);
			xlabel += xno;
		}
		
		// updateLabelInfo(inputs[2]);
		/*
		*
		var intitle = $('#f_title').val();
		var inamount = $('#f_amount').val();
		var indate = $('#f_release_date').val();
		var inpaytype = $('input[name=f_fund_id]:checked').parent().text();
		console.log(intitle);
		console.log(inamount);
		console.log(indate);
		console.log(inpaytype);
		*/
		
		// category inactive due to dynamic needs
		// if (inputs[2]) {
			// var catid;
			// // catid = $('input[name=f_category_id][data-label="' + inputs[2] + '"]').val();
			// // catid = $('#f_category_id option[data-label="' + inputs[2] + '"]').val();
			// // $('#f_category_id option[data-label="' + inputs[2] + '"]').attr('checked',true);
			// //update_chosen;
			
			// // use selector or ajax to get correct catid
			// console.log('jalan bro ' + inputs[2]);
			// console.log(catid);
			// update_chosen('f_category_id',catid);
		// } else is_flag_format = false;
		
		// $('#f_fund_type').text(inputs[1]);
		
		if (is_flag_format) {
			
			$('#lbl_format_input').addClass('alert alert-success');
			$('#lbl_format_input').append(xlabel);
			
			// time 3detik auto submit
		}
		// var dt = 
		// $('#lbl_info_inpexp').append('tester');
	});
	
	// method 1 untested
	// $('input[name=f_fund_id').each(function(k,v){
		// var vv = $(v).attr('data-label').toLowerCase();
		// if (vv == 'dana') {
			// $(this).attr('checked',true);
		// }
		// console.log(vv);
	// });
	
	// method 2 -> IT WORKS
	// $('input[name=f_fund_id][data-label=Dana').attr('checked',true);
	// $('input[name=f_fund_id][data-label=').attr('checked',true);
	
	<?php if ($do != 'edit') {  ?>
    $('#toggle_incoming_reminder').click(function(){
		$('#incoming_reminder').toggle();
		if ($('#toggle_incoming_reminder i').hasClass('fa-arrow-down')) {
			// alert('ada nih');
			$('#toggle_incoming_reminder i').removeClass('fa-arrow-down');
			$('#toggle_incoming_reminder i').addClass('fa-arrow-up');
		} else {
			$('#toggle_incoming_reminder i').removeClass('fa-arrow-up');
			$('#toggle_incoming_reminder i').addClass('fa-arrow-down');
		}
	});

	$('#toggle_show_report').click(function(){
		$('#show_report').toggle();
		if ($('#toggle_show_report i').hasClass('fa-arrow-down')) {
			// alert('ada nih');
			$('#toggle_show_report i').removeClass('fa-arrow-down');
			$('#toggle_show_report i').addClass('fa-arrow-up');
		} else {
			$('#toggle_show_report i').removeClass('fa-arrow-up');
			$('#toggle_show_report i').addClass('fa-arrow-down');
		}
	});

	// UNUSED 27 oct
	// $('#toggle_show_detail_expense').click(function(){
		// $('#show_detail_expense').toggle();
		// if ($('#toggle_show_detail_expense i').hasClass('fa-arrow-down')) {
			// alert('ada nih');
			// $('#toggle_show_detail_expense i').removeClass('fa-arrow-down');
			// $('#toggle_show_detail_expense i').addClass('fa-arrow-up');
		// } else {
			// $('#toggle_show_detail_expense i').removeClass('fa-arrow-up');
			// $('#toggle_show_detail_expense i').addClass('fa-arrow-down');
		// }
	// })

	$('#chart_round').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Expense Report per '
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b> => {point.y}'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} % => {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: "Brands",
            colorByPoint: true,
            data: [
			<?php
			foreach ($list_report_by_category as $key => $rs) {
			?>
			{
				name: "<?php echo $rs['category_name']?>",
				y: <?php echo $rs['amount']?>
			}
				<?php if ($key+1 != count($list_report_by_category)) echo "," ?>
			<?php
			}
			?>
			]
        }]
    });

	// CHART GRAPH
	<?php	

	$list_expense_byday = $tmpb = array();
	$tmpb['creator_id'] = member_cookies('member_id');
	$tmpb['is_increment'] = -1;
	
	if (isset($start_date) && isset($end_date)) 
	{
		$tmpb['release_date_start'] = $start_date;
		$tmpb['release_date_end'] = $end_date;
	}
	
	$list_expense_byday = $this->expense_model->get_list_expense_byday($tmpb);	
	$strlinedata_out = $strlinedata_in = $strline_releasedate = '';		
	// debug($list_expense_byday);
	if (! empty($list_expense_byday)) 
	{
		foreach ($list_expense_byday as $key => $rs) 
		{
			$isWeekend = 0;
			
			if (isWeekend($rs['release_date'])) $isWeekend = 1;
			
			$strline_releasedate .= "'";
			// $$strlinedata_out .= "'";
			if ($isWeekend) { 
				$strline_releasedate .= '<span style="color:red;font-weight:bolder">';
				// $strlinedata_out .= '<span style="color:red">';
			}
			
			$strlinedata_out.= $rs['total_amount'];
			$strline_releasedate .= date('D d M',strtotime($rs['release_date']));
			
			if ($isWeekend) {
				$strline_releasedate .= "</span>";
				// $$strlinedata_out .= "</span>";
			}
			$strline_releasedate .= "'";
			// $$strlinedata_out .= "'";

			if ($key+1 != count($list_expense_byday)) {
				$strline_releasedate .= ",";
				$strlinedata_out .= ",";
			}
			
		}
	}
	?>
    $('#chart_graph_daily').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Report by day'
        },
        subtitle: {
            text: 'Grafik '
        },
        xAxis: {
            categories: [<?php echo $strline_releasedate ?>],
			useHTML: true
        },
        yAxis: {
            title: {
                text: 'Amount'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
			name: 'Expense',
			data: [<?php echo $strlinedata_out; ?>]
        }
		// , {
			// name: 'Income',
			// data: [<?php echo $strlinedata_in; ?>]
		// }
		]
    });
	<?php } ?>
	
	

});
$(document).ready(function(){
	
	$( "#tabs" ).tabs(
		// {event: "mouseover"}
		);

	<?php if (!isset($is_get_notif) || $is_get_notif != 1) { ?>
	$(".reminder-date").hide();
	$(".reminder-date :input").attr('disabled','disabled');
	<?php } ?>

	$("#is_reminder1").click(function(){
		if ($(this).is(":checked")) {
			// alert('jalan1');
			$(".reminder-date").show();
			$(".reminder-date :input").removeAttr('disabled');
			$('input[name=f_is_get_notif]').prop('checked',true);
		}
	});
	$("#is_reminder0").click(function(){
		if ($(this).is(":checked")) {
			// alert('jalan0');
			$(".reminder-date").hide();
			$(".reminder-date :input").attr('disabled','disabled');
			$('input[name=f_is_get_notif]').prop('checked',false);
		}
	});

	// CHECK USE MAP
	$('#use_map').click(function(){
		$('#map').removeClass('hide');
	});
	$('#map').addClass('hide');
	$('#no_map').click(function(){
		$('#map').addClass('hide');
	})

	$('#new_expense').click(function(){
		$(this).addClass('hide');
		$('#new_form_expense').removeClass('hide');
	});
	<?php if ($do != "edit") { ?>
	$('#new_form_expense').addClass('hide');
	<?php } else { ?>
	$('#new_expense').addClass('hide');
	<?php } ?>

	$('.save').click(function(){
		// alert($('#f_category_id').find(':selected').text());
		if ($('#f_category_id').find(':selected').text() == 'Select Expense / Income'){
			alert('Kategori pengeluaran harus dipilih.');
		}
		$('#f_amount, #f_cashback').blur();
	})

	// change reminder date on radio reminder clicked
	$('#f_reminder_date').val($('#f_release_date').val());
	$('#f_release_date').change(function(){
		$('#f_reminder_date').val($(this).val());
	});

	$('#f_reminder_time').val('10');
	
	// set moment
	// var now = moment().add(1, 'days').calendar();
	// var r = $('#f_release_date').val() + 86400;
	
	// console.log(now);

	/*
	$( "input[name=f_title]" ).click(function(){
		console.log('jalan');
		var val_title = $(this).val();
		// var resp = ["itacho sushi","itc joging 10 mochi 15, somay 50, kcang ijo 12, obat demam 35, kaki 60 alfa 54 parkir 10"];
		// $("input[name=f_title]").autocomplete('option', 'source', resp);
		var somplak;
		somplak = 'ita';
		$.ajax({
			type: "POST",
			method: "POST",
			url: "<?php echo $base_url?>expense/report",
			data: "apitoken=<?php echo rand(100000000,9999999999);?>&do=autocomplete&title="+ somplak +"&creator_id=<?php echo member_cookies('member_id')?>",
			cache: false,
			dataType: "json"
			success: function(resp) {
				console.log('sukses ajaxnih');
				console.log('title ' + val_title);
				console.log(resp);
				// return resp;
				// console.log(resp);
				// var availableTags = resp;
				// $( "input[name=f_title]" ).autocomplete({
				  // source: availableTags
				// });

				$("input[name=f_title]").autocomplete('option', 'source', resp);
			}
		});
	});
	*/

	$( "input[name=f_title]" ).autocomplete({
		source: function( request, response ) {
			$.ajax({
			  type: "POST",
			  url: "<?php echo $base_url?>expense/report",
			  cache: false,
			  dataType: "json",
			  data: "apitoken=<?php echo rand(100000000,9999999999);?>&do=autocomplete&title="+ $( "input[name=f_title]" ).val() +"&creator_id=<?php echo member_cookies('member_id')?>",
			  success: function(data) {
				if (data.length > 0) {
					response(data);
                } else {
					response([]);;
				}
			  },
			  error: function () {
				response([]);
			  }
			});
		},
		// autoFocus: true,
		classes: {
			"ui-autocomplete": "clrBlu"
		},
		delay: 1000,
		minLength: 1
	});

	// run reset button plugin
	resetinput();
	
	// set auto 1000
	$('#f_amount, #f_cashback').focusout(function(){
		// alert('jalan');
		var vamount = $(this).val();
		if (vamount > 0 && vamount < 1000) {
			$(this).val(vamount + ',000');
		}
	});
	
	// activate input cashback when toggle on
	$("#chk_show_cashback").change(function(){
		if ($("#chk_show_cashback").prop('checked')) {
			$("#f_cashback").focus();
		}
	});

});

function update_chosen(select_id,value)
{
	$('#' + select_id).val(value);
	$('#' + select_id).trigger("chosen:updated");
	
}

function set_expense_form(expenseid,categoryid,title,amount)
{
	$('#f_expense_id').val(expenseid);
	$('#f_title').val(title);
	update_chosen('f_category_id',categoryid);
	$('#f_amount').val(amount);
}

// $('#chk_show_notes').change(function(){
	// if ($(this).is(':checked')) {
		// // alert('nyala nih');
		// // $('#f_notes').removeClass('hide');
		// $('#f_notes').css('visibility','visible');
	// } else {
		// // alert('ga jalan');
		// // $('#f_notes').addClass('hide');
		// $('#f_notes').css('visibility','hidden');
	// }
// });
// $('#f_notes').css('visibility','hidden');

$('#chk_show_cashback').change(function(){
	if ($(this).is(':checked')) {
		$('#f_cashback_id').css('visibility','visible');
	}
	else {
		$('#f_cashback_id').css('visibility','hidden');
	}
});
$('#f_cashback_id').css('visibility','hidden');

$('#chk_show_location').change(function(){
	if ($(this).is(':checked')) {
		$('#f_location_id').css('visibility','visible');
	}
	else {
		// if ( ! $('#f_location_id').hasClass('hide'))
		$('#f_location_id').css('visibility','hidden');
	}
});
$('#f_location_id').css('visibility','hidden');

$('#chk_show_notes').change(function(){
	if ($(this).is(':checked')) {
		$('#f_notes').show();
	} else {
		$('#f_notes').hide();
	}
});
$('#f_notes').hide();

function calcDays(date, amount, option = 'plus') {
  var tzOff = date.getTimezoneOffset() * 60 * 1000,
      t = date.getTime(),
      d = new Date(),
      tzOff2;

  if (option == 'plus') {
	t += (1000 * 60 * 60 * 24) * amount;
  } else {
	t -= (1000 * 60 * 60 * 24) * amount;
  }
  d.setTime(t);

  tzOff2 = d.getTimezoneOffset() * 60 * 1000;
  if (tzOff != tzOff2) {
    var diff = tzOff2 - tzOff;
    t += diff;
    d.setTime(t);
  }

  return d;
}

function calender_move(opt){	
	var currDate = new Date($('#f_release_date').datepicker('getDate'));
	var getd = currDate.getDate();
	if (opt == 'plus') { getd++; } else {getd--;}
	
    currDate.setDate(getd);
    $('#f_release_date').datepicker('setDate', currDate);
}

// color category on click
$('.freq-category').click(function(){
	$('.freq-category').removeClass('btn-primary')
	$(this).addClass('btn-primary');
	// console.log($(this).text());
})

</script>

<?php if ($do == "insert" || $do == "edit") { ?>
<script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

function initAutocomplete() {
  var map = new google.maps.Map(document.getElementById('map'), {
    // -6.209844902932365,106.84993743896484
	center: {lat: -6.209844902932365, lng: 106.84993743896484},
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  // Create the search box and link it to the UI element.
  document.getElementById('pac-input').value = "<?php if (isset($gmap_address)) echo $gmap_address;?>";
  var input = document.getElementById('pac-input');
  var longi = document.getElementById('long');
  var lat = document.getElementById('lat');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(longi);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(lat);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function(e) {
    searchBox.setBounds(map.getBounds());
  });

  map.addListener('click', function (e) {
		document.getElementById('lat').value = e.latLng.lat();
		document.getElementById('long').value = e.latLng.lng();
	});

  var markers = [];
  // [START region_getplaces]
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.

    var bounds = new google.maps.LatLngBounds();

    places.forEach(function(place) {

	  var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
	  document.getElementById('lat').value = place.geometry.location.lat();
	  document.getElementById('long').value = place.geometry.location.lng();
    });

    map.fitBounds(bounds);
	map.setZoom(18);
  });
  <?php
  // [END region_getplaces]
  // SERVER KY : AIzaSyA_4eVQFLKDCHOOSIkkSJRXj7WhCz_IqvQ
  // BROWSER KEY: AIzaSyBoMZNcBSuPa1MyMUFjYaHAHMqy2zYV5P0

  // JOBTALENTO
  // SERVER KY : AIzaSyB4U_wSzpo-pN1UTVpvdTYVG1RjuiVRHss
  // BROWSER KEY: AIzaSyDC3EXX-9YL4u4Ax_UWkptDY_vWLv_Nee4
  ?>

}


    </script>

	<!--
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4U_wSzpo-pN1UTVpvdTYVG1RjuiVRHss&libraries=places&callback=initAutocomplete"
         async defer></script>
	-->
		 <style>
#floating-report{
  position: fixed;
  left: 45px;
  bottom: 0;
  right: 20px;
  height:180px;
  width: 100%;
  background-color: red;
  color: white;
  text-align: center;
}
#map {
        height: 300px;
		width : 100%;
      }
.controls {
  margin-top: 10px;
  border: 1px solid transparent;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 32px;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 300px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

.pac-container {
  font-family: Roboto;
}

#long {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-top: 10px;
  margin-left: 12px;
  padding: 5px;
  text-overflow: ellipsis;
  width: 150px;
}

#lat {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-top: 10px;
  margin-left: 12px;
  padding: 5px;
  text-overflow: ellipsis;
  width: 150px;
}
#type-selector {
  color: #fff;
  background-color: #4d90fe;
  padding: 5px 11px 0px 11px;
}

#type-selector label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}
		 </style>
<?php } ?>
<style>
.freq-category{
	border: 1px solid #808483;

	line-height: 190%;
	background-color: #E9FDF6;
	margin-bottom: 4px;
}

.freq-category:hover{
	background-color: #477187;
	color: #fff;
}

.form-header{
	background-color: #F8F8F9; 
	
	padding:10px; 
	border: 1px solid #FEB600; 
	border-radius: 5px; 
	margin: 7px 0px 8px 0px;
}
.form-header > .title{
	color: #838C95; 
	font-weight:bolder;
	margin: 3px 0px 3px 0px;
}
</style>
