<?php
global $PAGE, $PAGE_TITLE, $BREADCRUMB , $PAGE_HEADER;
 
if (!isset($PAGE)) $PAGE = DEFAULT_PAGE_TITLE . ' - '.DEFAULT_PAGE_DESCRIPTION;
if (!isset($PAGE_TITLE)) $PAGE_TITLE = $PAGE;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!--<meta name="google-site-verification" content="NM1j6BG3n_-7PcXeKwQnS6R-JABiEe2PL7Lm7M_Tbjo" />-->
	<meta charset="utf-8">
	<title><?php echo $PAGE_TITLE ?></title>
	<link rel="shortcut icon" href="<?php echo base_url()?>favicon.ico?v=201706221" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url()?>favicon.ico?v=201706221" type="image/x-icon">
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!-- Latest compiled and minified CSS 
		<link rel="stylesheet" href="http://localhost/ci/assets/css/bootstrap.min.css">
		-->
	
	<?php if ( ! is_internal()) {?>
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-56560352-2', 'auto');
	ga('send', 'pageview');

	</script>
	<?php } ?>

	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.css?v=201706221"/>
	
	<link rel='stylesheet' href='<?php echo base_url(); ?>asset/css/ui-lightness/jquery-ui-1.10.4.css?v=201706221' />

	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css?v=201706221"/>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css.map?v=201706221"/>
	<!--<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css"/>-->
	
    <link href="<?php echo base_url()?>asset/css/font-awesome.css?v=201706221" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-social.css?v=201706221"/>
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/style.css?v=201706221"/>
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/animate.css"?v=201706221/>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js?v=201706221"></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery-ui-1.10.4.js?v=201706221'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery.validate.js?v=201706221'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/ui/jquery.ui.core.js?v=201706221'></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/bootstrap.js?v=201706221"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/scrollReveal.js?v=201706221"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.backstretch.min.js?v=201706221"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/lazyload/jquery.lazyload.js?v=1.9.1"></script>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/colorbox.css?v=201706221" />
	<script src="<?php echo base_url()?>asset/js/jquery.colorbox.js?v=201706221"></script>
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery.shiftcheckbox.js?v=201706221"></script>
	<script type="text/javascript">
	$(document).ready(function(){	
		$(function() {	
			//$.backstretch("http://dl.dropbox.com/u/515046/www/garfield-interior.jpg");
			$("img.lazy").lazyload({ effect: "fadeIn" });
			
			$('[data-toggle="popover"]').popover();
			//$('.popover').popover(options);
			$( ".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true,
				showAnim: "slideDown",
				yearRange: '1950:+10' 
			});
		});
		
		/* INSERT class parentcheckbox in wrapper checkbox to activate */
		$('.parentcheckbox').shiftcheckbox({
			checkboxSelector : ':checkbox',
			//selectAll        : $('.chkbox '),
			ignoreClick      : 'a',
		});
	});
	window.sr = new scrollReveal();
	</script>
	
</head>
<?php 
global $fetch_class, $fetch_method;
$fetch_class = $this->router->fetch_class();
$fetch_method = $this->router->fetch_method();
function show_active_menu($menu_title) 
{
	global $fetch_class, $fetch_method;
	$str = '';
	if ($fetch_class == 'modular') 
	{
		if ($fetch_method == $menu_title) 
		{
			$str = 'btn btn-info';
			$str = '<span class="sr-only">(current)</span>';
		}
	}
	return $str;
}

?>
<body>
<?php include_once 'navheader.php';?>

<div class="container">	
	<div class="row">
			<?php if (isset($BREADCRUMB)) echo $BREADCRUMB.'' ?><br/><br/><br/>
		<div class="col-md-12" style="min-height:500px">
			<?php if (isset($PAGE_HEADER)) echo '<h2>'.$PAGE_HEADER.'</h2>' ?>
			<!--<h1>Selamat datang</h1>
			<div class="alert alert-warning"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Error:</span>&nbsp;Email / Password tidak valid.</div>-->
			<?php echo $CONTENT; ?>
		</div>
	</div>
</div>

<?php include_once 'navfooter.php';?>

<script>
$(document).ready( function() {
	
	function togglebox(){
		if ($('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',true);
		}
		if (!$('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',false);
		}
	}
	
	$('#group_action').hide();
	$('.chkbox').click(function(){
        var count = $("[type='checkbox']:checked").length;
		if (count >= 1) {
             $('#group_action').show();
        } else if(count <= 0){
            $('#group_action').hide();
        }
    });

});
</script>
</body>
</html>