<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = 'Task Schedule';
$bread['member'] = 'Task Schedule';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
$do = $task_id = $obj = $task_project_id = NULL;

// if ($this->input->get('do')) $do = $this->input->get('do');
// if ($this->input->get('task_id')) $task_id = $this->input->get('task_id');

if ($this->uri->segment(3)) $do = $this->uri->segment(3);
if ($this->uri->segment(4)) $task_id = $this->uri->segment(4);
if (isset($_GET['task_project_id'])) $task_project_id = $_GET['task_project_id']; 

function get_hour_minute($duration) {
    $hours = floor($duration / 3600);
    $minutes = floor(($duration / 60) % 60);
    $seconds = $duration % 60;
    if ($hours != 0)
        return "$hours hour $minutes min";
    else
        return "$minutes min $seconds sec";
}

$arr_priority = get_priority();
$arr_priority_clr = get_priority_clr();

// load data on edit
if (isset($do) && is_numeric($task_id)) 
{
	$tmp = NULL;
	$tmp['task_id'] = $task_id;
	$obj = $this->task_model->get($tmp);
}

// SAVE HERE
if ($_POST && isset($_POST['btn_insert']))
{
	$post = $param = array();
	$post = $_POST;
	
	if (isset($post['task_project_id'])) $param['task_project_id'] = $post['task_project_id'];

	$param['title'] = $post['title'];
	$param['description'] = $post['description'];
	$param['priority'] = $post['priority'];
	$param['input_date'] = date('Y-m-d 00:00:00',strtotime($post['input_date']));
	
	$param['due_date'] = NULL;
	if (isset($post['due_date'])) $param['due_date'] = date('Y-m-d 00:00:00',strtotime($post['due_date']));
	
	$param['est_workday'] = $post['est_workday'];
	$param['est_workhour'] = $post['est_workhour'];
	$param['est_workmin'] = $post['est_workmin'];
	$param['notes'] = $post['notes'];
	
	$param['is_done'] = 0;
	if (isset($post['is_done']) && $param['is_done'] == 1) $param['is_done'] = 1;
	
	$param['creator_id'] = member_cookies('member_id');
	
	if (isset($post['notes'])) $param['notes'] = $post['notes'];

	$insert = $this->task_model->save($param);
	
	if ($insert) {
		$this->session->set_flashdata('message', getMessage(MESSAGE::SAVE));
	} else {
		$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
	}
	redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	die;
}

// UPDATE
if ($_POST && isset($_POST['btn_update']))
{
	$param = $post = array();
	$post = $_POST;
	
	if (isset($post['task_project_id'])) $param['task_project_id'] = $post['task_project_id'];

	$param['title'] = $post['title'];
	$param['description'] = $post['description'];
	$param['priority'] = $post['priority'];
	
	$param['due_date'] = NULL;
	if (isset($post['due_date']) && $post['due_date'] != '') $param['due_date'] = date('Y-m-d 00:00:00',strtotime($post['due_date']));
	
	$param['est_workday'] = $post['est_workday'];
	$param['est_workhour'] = $post['est_workhour'];
	$param['est_workmin'] = $post['est_workmin'];
	$param['notes'] = $post['notes'];
	
	$param['is_done'] = 0;
	if (isset($post['is_done']) && $post['is_done'] == 1) $param['is_done'] = 1;
	
	// Find data exist
	$get['task_id'] = $task_id;
	$get['creator_id'] = member_cookies('member_id');
	$obj_task = $this->task_model->get($get);
	
	if (!empty($obj_task)) 
	{
		// debug($post);die;
		$update = $this->task_model->update($task_id, $param);
		
		if ($update) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/edit/'.$task_id);
	}
	else
	{
		// DATA NOT FOUND
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/edit/'.$task_id);
	}
	$obj_task = NULL;
	
}

// UPDATE
// if ($_POST && isset($_POST['btn_update']))
// {
	// $post = array();
	// $post = $_POST;
	// unset($post['btn_update']);
	
	// if (isset($post['release_date']) && $post['release_date']!='') $post['release_date'] = date('Y-m-d',strtotime($post['release_date']));
	// if (isset($post['reminder_date']) && $post['reminder_date']!='') $post['reminder_date'] = date('Y-m-d',strtotime($post['reminder_date']));
	
	// $get['member_id'] = member_cookies('member_id');
	// $get['task_id'] = $task_id;
	// $obj_absence = $this->task_model->get($get);
	
	// if (!empty($obj_absence)) 
	// {
		// // debug($post);die;
		// $update = $this->task_model->update($task_id, $post);
		
		// if ($update) {
			// $this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
		// } else {
			// $this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		// }
		// redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	// }
	// else
	// {
		// // DATA NOT FOUND
		// $this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		// redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	// }
	// $obj_absence = NULL;
	
// }

// DELETE ABSENCE
if ($do == "delete" && is_numeric($task_id)) 
{
	$absence = $this->task_model->get(array('task_id' => $task_id, 'creator_id' => member_cookies('member_id')));
	if (!empty($treasure)) 
	{
		$delete = $this->task_model->delete($task_id);
		if ($delete) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::DELETE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	} 
	else 
	{
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	}
}

$start_date = $end_date = NULL;
$param = array();

if ($this->input->get('start_date') && $this->input->get('start_date')) 
{
	$start_date = date('d-m-Y',strtotime($this->input->get('start_date')));
	$end_date = date('d-m-Y',strtotime($this->input->get('end_date')));
}
else
{
	$param['start_date'] = $start_date = date("01-m-Y");
	$param['end_date'] = $end_date = date("t-m-Y");
}
	
$param['creator_id'] = member_cookies('member_id');
if (isset($start_date) && isset($end_date)) 
{
	$param['start_date'] = $start_date;
	$param['end_date'] = $end_date;
}

// Get data member 
$obj_member = $this->member_model->get(array(
	'creator_id' => member_cookies('member_id')
));

// ON CREATE
$obj_task_project = $tp1 = NULL;
if (isset($task_project_id)) 
{
	$tp1 ['task_project_id'] = $task_project_id;
	$obj_task_project = $this->task_project_model->get($tp1);
}

// ON EDIT
if (isset($obj['task_project_id'])) 
{
	$tp1 ['task_project_id'] = $obj['task_project_id'];
	$obj_task_project = $this->task_project_model->get($tp1);
}

// debug($obj);
?>

<!-- help: http://www.bootstraptoggle.com/ -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css?v=170812" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js?v=170812"></script>

<div class="col-md-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"> </i> DESKRIPSI</b><br/>
		Fitur ini berguna untuk mencatat todolist pekerjaan anda.<br/>
	</div><hr/>
	
	<?php 
	if (isset($message['message']))echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>	
	
	<div class="col-sm-12">
		<a href="<?php echo base_url()?>member/task" class="btn btn-info btn-xs btnBack"><i class="fa fa-arrow-left"></i> Back</a><br/><br/>
		<style>
		.btnBack{width: 100px; padding: 4px 3px}
		.btnSubmit{width: 100px; padding: 4px 3px}
		</style>
		<form method="post" id="form_task" class="">
			<div class="row">
				<?php if (isset($obj_task_project['name'])) { ?>
				<div class="col-sm-3">Nama Projek <span class="clrRed b">*</span></div>
				<div class="col-sm-9 br"><input type="text" class="input br wdtFul bgGry" placeholder="cth: mengerjakan modul API" value="<?php if (isset($obj_task_project['name'])) echo $obj_task_project['name'] ?>" readonly />
				<input type="hidden" name="f_task_project_id" value="<?php if (isset($obj_task_project['task_project_id'])) echo $obj_task_project['task_project_id'] ?>" />
				</div>
				<?php } ?>
				
				<div class="col-sm-3">Nama tugas <span class="clrRed b">*</span></div>
				<div class="col-sm-9 br"><input type="text" id="f_title" name="f_title" class="input br wdtFul" placeholder="cth: mengerjakan modul API" required /></div>
				
				<div class="col-sm-3">Priority</div>
				<div class="col-sm-9">
				<fieldset>
				<?php 
					foreach ($arr_priority as $keyprior => $prior) {
					?>
					<input type="radio" id="f_priority_<?php echo $keyprior ?>" name="f_priority" class="" value="<?php echo $keyprior?>"> 
					
					<label for="f_priority_<?php echo $keyprior?>" class="pointer multi-radio btn btn-sm <?php echo $arr_priority_clr[$keyprior]?>"><?php echo $prior?></label>
					<span style="padding-right: 18px"></span>
					<?php 
					}
				?>
				<br/>
				<br/>
				</fieldset>
				</div>
				<style>
				
				
				</style>
				
				<div class="col-sm-3">Deskripsi <span class="clrRed b">*</span></div>
				<div class="col-sm-9"><textarea id="f_description" name="f_description" class="input br wdtFul" placeholder="Deskripsi" rows="5"></textarea></div>
				
				<div class="col-sm-3">Tgl pengerjaan <span class="clrRed b">*</span></div>
				<div class="col-sm-9"><input type="text" id="f_input_date" name="f_input_date" class="input br w200 datepicker" placeholder="Tanggal" required /></div>
				
				<div class="col-sm-3">Tgl deadline</div>
				<div class="col-sm-9"><input type="text" name="f_due_date" id="f_due_date" class="input br w200 datepicker" placeholder="Tanggal" />  &nbsp; <button type="button" class="btn btn-reset btn-info btn-sm" onclick="$('#f_due_date').val('')">reset</button></div>
				
				<div class="col-sm-3">Estimasi waktu pengerjaan<span class="clrRed b">*</span></div>
				<div class="col-sm-9">
					<input type="text" id="f_est_workday" name="f_est_workday" class="input br w200 numeric" placeholder="hari" maxlength="3" /> hari
					<input type="text" id="f_est_workhour" name="f_est_workhour" class="input br w200 numeric" placeholder="jam" maxlength="2" style="margin-left:15px" /> jam 
					<input type="text" id="f_est_workmin" name="f_est_workmin" class="input br w200 numeric" placeholder="menit" maxlength="2" style="margin-left:15px" /> menit
				</div>
				
				<div class="col-sm-3">Notes</div>
				<div class="col-sm-9"><textarea id="f_notes" name="f_notes" class="input br wdtFul" placeholder="Notes"></textarea></div>
				
				<div class="col-sm-3">Status</div>
				<div class="col-sm-9"><input type="checkbox" data-size="mini" data-toggle="toggle" data-on="<i class='fa fa-check'></i> Selesai" data-off="Belum selesai" data-onstyle="success" name="f_is_done" id="f_is_done" value="1" data-width="100" <?php if (isset($obj['is_done']) && $obj['is_done'] == 1) echo "checked='true'"?> /><br/><br/></div>
				
				<div class="col-sm-12">
				<?php if ($do == 'create') { ?>
				<button type="submit" id="btn_insert" name="btn_insert" class="btn btn-success btn-sm btnSubmit">Submit</button>
				<?php } else if ($do == 'edit' && isset($task_id)) { ?>
				<button type="submit" id="btn_update" name="btn_update" class="btn btn-success btn-sm btnSubmit">Submit</button>
				<?php } ?>
				</div>
			</div>
		</form>
		<br/>
		
	</div>
</div>
<script>
$(document).ready(function(){
	$('#btn_insert').click(function(){
		// alert('berhasil');
	});
	$('input[name="f_input_date"]').val('<?php echo date('d-m-Y') ?>');
	
	<?php 
	// debug($obj);
	// remove required and add readonly
	if (isset($obj['input_date'])) {
		echo "$('#f_input_date').val('".date('d-m-Y',strtotime($obj['input_date']))."');";
		echo "$('#f_input_date').prop('disabled',true);";
	} 
	
	// if (isset($obj['priority'])) echo "$('#f_priority').val('".$obj['priority']."');";
	if (isset($obj['title'])) echo "$('#f_title').val('".$obj['title']."');";
	if (isset($obj['description'])) echo "$('#f_description').val('".$obj['description']."');";
	if (isset($obj['due_date'])) echo "$('#f_due_date').val('".date('d-m-Y',strtotime($obj['due_date']))."');";
	if (isset($obj['notes'])) echo "$('#f_notes').val('".$obj['notes']."');";
	
	if (isset($obj['est_workday'])) echo "$('#f_est_workday').val('".$obj['est_workday']."');";
	if (isset($obj['est_workhour'])) echo "$('#f_est_workhour').val('".$obj['est_workhour']."');";
	if (isset($obj['est_workmin'])) echo "$('#f_est_workmin').val('".$obj['est_workmin']."');";
	
	// if (isset($obj['priority'])) echo "$('input[name=f_priority] option[value=".$obj['priority']."]').attr('selected','selected');";	
	// if (isset($obj['priority'])) echo "$('input[name=f_priority] option[value=".$obj['priority']."]').attr('checked',true);";	
	if (isset($obj['priority'])) echo "$('#f_priority_".$obj['priority']."').attr('checked',true);";	
	
	// cannot work
	// if (isset($obj['is_done']) && $obj['is_done'] == 1) echo "$('#f_is_done').attr('checked','checked');";	
	?>
	
});

</script>