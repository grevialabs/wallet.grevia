<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = 'Task';
$bread['member'] = 'Task Schedule';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
$do = $task_project_id = $show = $order = NULL;

if ($this->input->get('show')) $show = $this->input->get('show');
if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('task_project_id')) $task_project_id = $this->input->get('task_project_id');

// SAVE HERE AJAX
if ($_POST && isset($_POST['ajax'])) 
{
	$post = $param = $tmp = $obj = array();
	$post = $_POST;
	unset($post['ajax']);
	
	$tmp['creator_id'] = member_cookies('member_id');
	$tmp['task_id'] = $post['task_id'];
	$obj = $this->task_model->get($tmp);
	
	$message = 'failed';
	if (! empty($obj)) {
		
		$param['title'] = $post['title'];
		$param['editor_id'] = member_cookies('member_id');
		$param['editor_date'] = getDateTime();
		$update = $this->task_model->update($post['task_id'],$param);
		if ($update) $message = 'Update success';
	} else {
		
	}
	echo $message;
	die;
}

// DELETE ABSENCE
if ($do == "delete" && is_numeric($task_project_id)) 
{
	$absence = $this->absence_model->get(array('task_project_id' => $task_project_id, 'creator_id' => member_cookies('member_id')));
	if (!empty($treasure)) 
	{
		$delete = $this->absence_model->delete($task_project_id);
		if ($delete) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::DELETE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	} 
	else 
	{
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	}
}

$param['creator_id'] = member_cookies('member_id');
$param['order'] = ' input_date ASC, due_date ASC';

// filter by today
if (isset($_GET['order']) && $_GET['order'] == 'today') {
	$param['today'] = 1;
}



$obj_project = $uri_project_id = NULL;

// $list_task_project
if (isset($task_project_id)) 
{
	$param['task_project_id'] = $task_project_id;
	
	$obj_project = $this->task_project_model->get(array('task_project_id' => $task_project_id));
	$uri_project_id = "&task_project_id=".$task_project_id;
}
else 
{
	$list_task_project = $tmp1 = NULL;
	$tmp1['status'] = 1;
	$tmp1['creator_id'] = member_cookies('member_id');
	$list_task_project = $this->task_project_model->get_list($tmp1);
	$list_task_project = $list_task_project['data'];
}

$arr_show = array(
	'all' => 9,
	'done' => 1,
	'undone' => 0,
	// 'today' => 5,
);

if (isset($show)) 
	$param['is_done'] = $arr_show[$show];
else 
	$param['is_done'] = 0;

$list_task_done = $this->task_model->get_list($param);
$list_task_done = $list_task_done['data'];

$get_priority = get_priority();
$get_priority_clr = get_priority_clr();
?>

<div class="col-md-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"> </i> DESKRIPSI</b><br/>
		Fitur ini berguna untuk mencatat todolist pekerjaan bb anda.<br/>
	</div><hr/>
	
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>	
	
	<div class="col-sm-12">
		<?php if (isset($task_project_id))  { ?>
		<a href="<?php echo base_url().'member/task_project' ?>" class="btn btn-info btn-sm" ><i class="fa fa-arrow-left"></i> Back to List project</a>
		
		<?php } else { ?>
		
		<a href="<?php echo base_url()?>member/task_project/create" class="btn btn-success btn-sm"><i class="fa fa-folder"></i> &nbsp; New <?php echo $MODULE ?></a> &nbsp; &nbsp;
		<?php } ?>
		<br/><br/>

		<?php 
		if (! empty($list_task_project))
		{ 
			?>
			<div class="talRgt">
			<h3>List Project</h3>
			<?php
			$arr_btnclr = array('warning','danger','primary','success');
			foreach ($list_task_project as $project) {
			$rand_btnclr = mt_rand(0,3);
				?>
				<a class="btn btn-sm btn-<?php echo $arr_btnclr[$rand_btnclr]?>" href="<?php echo base_url().'member/task_project?task_project_id='.$project['task_project_id']?>"><?php echo $project['name'] ?></a> &nbsp;&nbsp;|&nbsp;&nbsp;
				<?php
			}
			?>
			</div>
			<?php
		}
		?>
		
		<?php echo (isset($obj_project['name'])) ? "<h2 class='b'>Project ".$obj_project['name']."</h2>" : '';?>
		
		<a href="<?php echo base_url().'member/task/create?'.$uri_project_id ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>&nbsp; Add new <?php echo $MODULE ?></a>
		
		<?php 		
		// $current_url = currentPageUrl();
		$current_url = current_url();
		$pget = $pget_done = $pget_undone = $get = NULL;
		if (! empty($_GET)) 
		{
			$pget = $get = $_GET;
			if (isset($pget['show'])) unset($pget['show']);
			foreach ($pget as $k => $gg) {
				$get[$k] = $gg;
			}
			$pget_done = $pget_undone = $get;
		}
		$pget_done['show'] = 'done';
		$pget_undone['show'] = 'undone';
		
		$current_url_all = $current_url;
		if (isset($get['task_project_id'])) $current_url_all .= '?'.http_build_query($get);
		$current_url_done = $current_url.'?'.http_build_query($pget_done);
		$current_url_undone = $current_url.'?'.http_build_query($pget_undone);
		// debug($get);
		// contain ?
		// if (strpos($current_url,'?') !== FALSE) $current_url .= '&'; else $current_url .= '?';
		
		$active_filter = 'btn-primary';
		$btnPrimary = 'btn-primary';
		$btnDefault = 'btn-default';
		?>
		<div class="talRgt"> Show data
		<a class="btn  <?php echo (! isset($get['task_project_id']) && ! isset($get['show']) || isset($get['task_project_id']) && count($get) == 1) ? $btnPrimary : $btnDefault; ?> btn-sm w200 br" href="<?php echo $current_url_all;?>">All</a> 
		<a class="btn  btn-sm w200 br <?php echo (isset($get['show']) && $get['show'] == 'done') ? $btnPrimary : $btnDefault; ?>" href="<?php echo $current_url_done;?>">Done</a>
		<a class="btn btn-sm w200 br <?php echo (isset($get['show']) && $get['show'] == 'undone') ? $btnPrimary : $btnDefault; ?>" href="<?php echo $current_url_undone;?>">Undone</a>
		</div>
		
		<div class="b sup"><span class="clrRed">Notes</span>: * click on title name to edit instantly</div>
		<table class="table hover table-bordered">
		<tr class="alert bg-warning b talCnt">
			<td width="10px">#</td>
			<td>Title</td>
			<td width="20px">Priority</td>
			<td width="120px">Project</td>
			<td width="150px">Due Date</td>
			<td width="">Option</td>
		</tr>
		<?php 
		if (!empty($list_task_done))
		{
			
			foreach($list_task_done as $key => $rs)
			{
				$done = '<i class="fa fa-truck clrRed" title="not done yet"></i> <span class="clrRed">Undone</span>';
				if ($rs['is_done']) $done = '<i class="fa fa-truck fa-flip-horizontal clrGrn" title="done"></i> <span class="clrGrn">Done</span>';
				// debug($rs);
			?>
		<tr data-task-id="<?php echo $rs['task_id']?>">
			<td class="talCnt"><?php echo $key+1?></td>
			<td>
			<div class="edit_title" id="edit_title_<?php echo $rs['task_id']?>"><?php echo $rs['title'] ?></div> <br/>
			<a class="btn btn-info btn-xs" href="<?php echo $current_url.'/edit/'.$rs['task_id']?>">Detail</a><br/>
			<?php echo $done?><br/>
			Last update: <?php echo (isset($rs['editor_date'])) ? getDateFormat(DATE_FORMAT,$rs['editor_date']) : '-'; ?>
			</td>
			<td class="talCnt"><div class="btn btn-xs <?php echo $get_priority_clr[$rs['priority']]?>"><?php echo $get_priority[$rs['priority']]; ?></div></td>
			<td class="talCnt"><?php echo $rs['project_name']; ?></td>
			<td class="talCnt"><?php echo (isset($rs['due_date'])) ? getDateFormat(DATE_FORMAT,$rs['due_date']) : '-'; ?></td>
			<td class="talCnt">
			<a href="<?php echo $current_url.'/edit/'.$rs['task_id']?>"><i class="fa fa-edit fa-lg clrGrn"></i></a> &nbsp;
			<a href="<?php echo $current_url.'?do=delete&task_id='.$rs['task_id']?>"><i class="fa fa-times-circle fa-lg clrRed"></i></a>
			</td>
		</tr>
			<?php 
			}
		}
		else 
		{
			?>
			<tr>
				<td colspan="100%">No data</td>
			</tr>
			<?php 
		}
		?>
		</table>
		
		
	</div>
</div>
<script>
$(document).ready(function(){
	
	$('.edit_title').click(function(){
		
		// set state edit only once
		if ($(this).data('is_edit') == 1) return false;
		
		$(this).data('is_edit',1);
		
		var title = $(this).html();
		var task_id = $(this).closest('tr').data('task-id');
		
		$(this).html('');
		$(this).html('<input type="text" class="ajax_edit_title" id="ajax_edit_title" value="' + title + '" style="width:99%; padding:8px 8px; border:1px solid #DBDBEA; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;" onfocusout="ajax_update(' + task_id + ',this)" />');
		
		// var title_new = $('#ajax_edit_title').html();
		
		$('#ajax_edit_title').focus();
		// $(this).trigger('focusout');
		$(this).focusout(function(){
			$(this).html('');
			$(this).data('is_edit',0);

		});
	});	
	
	// accept enter button on edit 
	$('body').on('keypress', '#ajax_edit_title', function(e) {
		if (e.which == 13) {
			var task_id = $(this).closest('tr').data('task-id');
			ajax_update(task_id,this);
		}
	});
	
		// var task_id = $(this).closest('tr').data('task-id');
	// $('body').on('focusout', '#ajax_edit_title', function() {
		// ajax_update(task_id,this);
	// });
	
});

function ajax_update(task_id, obj)
{
	// alert('update id :' + task_id + ', val: ' + obj.value);
	$('#edit_title_' + task_id ).html(obj.value);
	
	dodelete = false;
	paramdata = "ajax=1&creator_id=" + <?php echo member_cookies('member_id')?> + "&task_id=" + task_id + "&title=" + obj.value;
	
	$.ajax({
		type: "POST",
		data: paramdata,
		cache: false,
		success: function(resp) {

			// $('#loading-response').html('<div class="alert alert-info talCnt"><i class="fa fa-cog fa-spin fa-lg fa-fw"></i> Mohon menunggu</div>');
			setTimeout(function(){
				if (resp == "success") {
					
					if (dodelete) {
						action_message = 'Data telah terhapus.';
					} else {
						action_message = 'Data telah tersimpan.';
					}						

					is_action_success = true;
					
					// untuk redirect 
					// setTimeout(function(){
						// location.reload();
					// }, 2000)
					
				} else if (resp == "failed") {
					action_message = 'Mohon maaf, data gagal disimpan.';
				} else {
					action_message = 'Mohon maaf, terjadi kesalahan, mohon ulangi beberapa saat lagi.';
				}
				// alert(action_message);
				// $('#loading-response').html('<div class="alert alert-info talCnt">' + action_message + '</div>');
				
				
			}, 1000);
			
			// setTimeout(function(){
				// if (is_action_success) {
					// $('#loading-response').html('');
					// $('#modalForm').modal('hide');
					// show_balloon_bottom(action_message,12);
					
				// } else {
					// // do save
					// if (!todo_id || todo_id == null) 
						// $('.btn_action[value=dosubmit]').show();						
					// else 
						// $('.btn_action').show();
				// }
				
			// }, 2000);
		}
	});
}

</script>