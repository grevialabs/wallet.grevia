	<?php
	global $current_segment;
	if ($this->uri->segment(2)) $current_segment = $this->uri->segment(2);
	
	function setting_active($page) 
	{
		global $current_segment;
		$str = '';
		if ($current_segment === $page)
		{
			$str = 'active';
		}
		return $str;
	}
	
	?>
	<div class="lnkWht">
		<a class="bg-primary padMed <?php echo setting_active('profile')?>" href="<?php echo base_url().'member/setting'?>"><i class="fa fa-person-text fa-fw"></i>&nbsp;<?php echo MENU_CHANGE_PASSWORD?></a>
		<a class="bg-primary padMed <?php echo setting_active('profile')?>" href="<?php echo base_url().'member/profile'?>"><i class="fa fa-person-text fa-fw"></i>&nbsp;<?php echo MENU_MY_PROFILE?></a>
	</div><br/>