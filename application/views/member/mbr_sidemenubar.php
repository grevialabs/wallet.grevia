	<?php
	global $current_segment;
	if ($this->uri->segment(2)) $current_segment = $this->uri->segment(2);
	
	function member_active($page) 
	{
		global $current_segment;
		$str = '';
		if ($current_segment === $page)
		{
			$str = 'active';
		}
		return $str;
	}
	
	function admin_active($page) 
	{
		global $current_segment;
		$str = '';
		if ($current_segment === $page)
		{
			$str = 'active';
		}
		return $str;
	}
	?>
	Hola, <?php echo member_cookies('fullname')?><hr>

	<div class="panel panel-primary">
		<div class="list-group">
			<?php if (is_admin()) {?>
			<a class="list-group-item bgRed clrWht" href="<?php echo base_url().'admin/dashboard'?>"><i class="fa fa-file-text fa-fw"></i>&nbsp; Adminarea</a>
			<a class="list-group-item bgPch" href="<?php echo base_url().'expense/report'?>"><i class="fa fa-money fa-fw"></i>&nbsp; Expense</a>
			<?php } ?>
			<a class="list-group-item <?php echo member_active('test')?>" href="<?php echo base_url().'member/test'?>"><i class="fa fa-file-text fa-fw"></i>&nbsp; <?php echo MENU_TEST?></a>
			<a class="list-group-item <?php echo member_active('history_test')?>" href="<?php echo base_url().'member/history_test'?>"><i class="fa fa-th fa-fw"></i>&nbsp; <?php echo MENU_HISTORY_TEST?></a>
			<a class="list-group-item <?php echo member_active('setting').member_active('profile')?>" href="<?php echo base_url().'member/setting'?>"><i class="fa fa-cog fa-fw"></i>&nbsp; <?php echo MENU_MY_SETTING?></a>
		</div>
	</div>