<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = 'Task Project';
$bread['member'] = 'Task Project';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
$do = $task_project_id = $obj = NULL;

// if ($this->input->get('do')) $do = $this->input->get('do');
// if ($this->input->get('task_project_id')) $task_project_id = $this->input->get('task_project_id');

if ($this->uri->segment(3)) $do = $this->uri->segment(3);
if ($this->uri->segment(4)) $task_project_id = $this->uri->segment(4);

// load data on edit
if (isset($do) && is_numeric($task_project_id)) 
{
	$tmp = NULL;
	$tmp['task_project_id'] = $task_project_id;
	$obj = $this->task_project_model->get($tmp);
}

// SAVE HERE
if ($_POST && isset($_POST['btn_insert']))
{
	$post = $param = array();
	$post = $_POST;
	
	$param['name'] = $post['name'];
	$param['status'] = 0;
	if (isset($post['status']) && $param['status'] == 1) $param['status'] = 1;
	
	$param['creator_id'] = member_cookies('member_id');
	
	if (isset($post['notes'])) $param['notes'] = $post['notes'];

	$insert = $this->task_project_model->save($param);
	
	if ($insert) {
		$this->session->set_flashdata('message', getMessage(MESSAGE::SAVE));
	} else {
		$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
	}
	redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	die;
}

// UPDATE
if ($_POST && isset($_POST['btn_update']))
{
	$param = $post = array();
	$post = $_POST;
	
	$param['name'] = $post['name'];
	$param['status'] = 0;
	if (isset($post['status']) && $post['status'] == 1) $param['status'] = 1;
	
	// Find data exist
	$get['task_project_id'] = $task_project_id;
	$get['creator_id'] = member_cookies('member_id');
	$obj_task = $this->task_project_model->get($get);
	
	if (!empty($obj_task)) 
	{
		// debug($post);die;
		$update = $this->task_project_model->update($task_project_id, $param);
		
		if ($update) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/edit/'.$task_project_id);
	}
	else
	{
		// DATA NOT FOUND
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/edit/'.$task_project_id);
	}
	$obj_task = NULL;
	
}

// UPDATE
// if ($_POST && isset($_POST['btn_update']))
// {
	// $post = array();
	// $post = $_POST;
	// unset($post['btn_update']);
	
	// if (isset($post['release_date']) && $post['release_date']!='') $post['release_date'] = date('Y-m-d',strtotime($post['release_date']));
	// if (isset($post['reminder_date']) && $post['reminder_date']!='') $post['reminder_date'] = date('Y-m-d',strtotime($post['reminder_date']));
	
	// $get['member_id'] = member_cookies('member_id');
	// $get['task_project_id'] = $task_project_id;
	// $obj_absence = $this->task_project_model->get($get);
	
	// if (!empty($obj_absence)) 
	// {
		// // debug($post);die;
		// $update = $this->task_project_model->update($task_project_id, $post);
		
		// if ($update) {
			// $this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
		// } else {
			// $this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		// }
		// redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	// }
	// else
	// {
		// // DATA NOT FOUND
		// $this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		// redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	// }
	// $obj_absence = NULL;
	
// }

// DELETE
if ($do == "delete" && is_numeric($task_project_id)) 
{
	$obj = $this->task_project_model->get(array('task_project_id' => $task_project_id, 'creator_id' => member_cookies('member_id')));
	if (!empty($obj)) 
	{
		$delete = $this->task_project_model->delete($task_project_id);
		if ($delete) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::DELETE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	} 
	else 
	{
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	}
}

// Get data member 
$obj_member = $this->member_model->get(array(
	'creator_id' => member_cookies('member_id')
));

// echo "mantapss";die;
?>

<!-- help: http://www.bootstraptoggle.com/ -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css?v=170812" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js?v=170812"></script>

<div class="col-md-2">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-10">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"> </i> DESKRIPSI</b><br/>
		Fitur ini berguna untuk mencatat todolist pekerjaan anda.<br/>
	</div><hr/>
	
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>	
	
	<div class="col-sm-12">
		<a href="<?php echo base_url()?>member/task" class="btn btn-info btn-sm"><i class="fa fa-arrow-left"></i> Back</a><br/><br/>
		
		<form method="post" id="form_task" class="">
			<div class="row">
				<div class="col-sm-3">Nama Projek <span class="clrRed b">*</span></div>
				<div class="col-sm-9 br"><input type="text" id="f_name" name="f_name" class="input br wdtFul" placeholder="nama projek" required /></div>
				
				<div class="col-sm-3">Notes</div>
				<div class="col-sm-9"><textarea id="f_notes" name="f_notes" class="input br wdtFul" placeholder="Notes"></textarea></div>
				
				<div class="col-sm-3">Status</div>
				<div class="col-sm-9"><input type="checkbox" data-size="mini" data-toggle="toggle" data-on="<i class='fa fa-check'></i> Aktif" data-off="Non aktif" data-onstyle="success" name="f_status" id="f_status" value="1" data-width="130" <?php if (isset($obj['status']) && $obj['status'] == 1) echo "checked='true'"?> /><br/></div>
				
				<div class="col-sm-12">
				<?php if ($do == 'create') { ?>
				<button type="submit" id="btn_insert" name="btn_insert" class="btn btn-success btn-sm">Save</button>
				<?php } else if ($do == 'edit' && isset($task_project_id)) { ?>
				<button type="submit" id="btn_update" name="btn_update" class="btn btn-success btn-sm">Update</button>
				<?php } ?>
				</div>
			</div>
		</form>
		<br/>
		
	</div>
</div>

<script>
$(document).ready(function(){
	$('#btn_insert').click(function(){
		// alert('berhasil');
	});
	$('input[name="f_input_date"]').val('<?php echo date('d-m-Y') ?>');
	
	<?php 
	// debug($obj);
	// remove required and add readonly 
	
	if (isset($obj['name'])) echo "$('#f_name').val('".$obj['name']."');";
	if (isset($obj['notes'])) echo "$('#f_notes').val('".$obj['notes']."');";
	
	?>
	
});

</script>