<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER;

$PAGE_HEADER = NULL;
$PAGE = $MODULE = CHANGE_PASSWORD;
$bread['member'] = 'Member';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

global $page, $param, $message;

if ($_POST) {
	$tmp['member_id'] = member_cookies('member_id');
	$obj = $this->member_model->get($tmp);
	if (!empty($obj)) {
		
		$old_password = filter(post('f_old_password'));
		$new_password = filter(post('f_new_password'));
		$confirm_new_password = filter(post('f_confirm_new_password'));
		
		if (encrypt($old_password) == $obj['thepassword']) {
			if ($new_password == $confirm_new_password) {
				$new_password = encrypt($confirm_new_password);
				
				$update = $this->member_model->update($tmp['member_id'],array('thepassword' => $new_password));
				($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
				$message['message'] = getMessage($message['message']);
			} else {
				$message['message'] = 'Password baru tidak sesuai';
			}
		} else {
			$message['message'] = 'Password lama salah';
		}
	} else {
		$message['message'] = getMessage(MESSAGE::NOT_FOUND);
	}
}

?>

<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1 class=""><?php echo $MODULE?></h1><br/>
	
	<?php echo $SETTINGMENUBAR ?>
	
	<?php if(is_filled($message['message']))echo message($message['message']).BR?>
	
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<label for='f_OldPassword' class='col-sm-2'><?php echo OLD_PASSWORD?></label>
			<div class='col-sm-10'><input type='password' class='form-control' name='f_OldPassword' id='f_old_password' placeholder=' <?php echo INPUT.' '.OLD_PASSWORD?>' required></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_NewPassword' class='col-sm-2'><?php echo NEW_PASSWORD?></label>
			<div class='col-sm-10'><input type='password' class='form-control' name='f_NewPassword' id='f_new_password' placeholder='<?php echo INPUT.' '.NEW_PASSWORD?>' required></div>
		</div>		
		<div class='form-group form-group-sm'>
			<label for='f_ConfirmNewPassword' class='col-sm-2'><?php echo CONFIRM_NEW_PASSWORD?></label>
			<div class='col-sm-10'><input type='password' class='form-control' name='f_ConfirmNewPassword' id='f_confirm_new_password' placeholder='<?php echo INPUT.' '.CONFIRM_NEW_PASSWORD?>' required></div>
		</div>
		<div class='form-group' style='padding-left:15px'>
			<button class='btn btn-success'><?php echo UPDATE?></button>
		</div>
	</form>
</div>