<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = 'Task Schedule';
$bread['member'] = 'Task Schedule';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
$do = $absence_id = $order = NULL;

if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('absence_id')) $absence_id = $this->input->get('absence_id');

function get_priority($idx) {
    $arr_priority = array(
		'1' => 'Low',
		'2' => 'Medium',
		'3' => 'High',
	);
	return $arr_priority[$idx];
}

$arr_absent_status = array(
	'1' => 'Low',
	'2' => 'Medium',
	'3' => 'High',
);

// SAVE HERE AJAX
if ($_POST && isset($_POST['ajax'])) 
{
	$post = $param = $tmp = $obj = array();
	$post = $_POST;
	unset($post['ajax']);
	
	$tmp['creator_id'] = member_cookies('member_id');
	$tmp['task_id'] = $post['task_id'];
	$obj = $this->task_model->get($tmp);
	
	$message = 'failed';
	if (! empty($obj)) {
		
		$param['title'] = $post['title'];
		$param['editor_id'] = member_cookies('member_id');
		$update = $this->task_model->update($post['task_id'],$param);
		if ($update) $message = 'success';
	} else {
		
	}
	echo $message;
	die;
}

// DELETE ABSENCE
if ($do == "delete" && is_numeric($absence_id)) 
{
	$absence = $this->absence_model->get(array('absence_id' => $absence_id, 'creator_id' => member_cookies('member_id')));
	if (!empty($treasure)) 
	{
		$delete = $this->absence_model->delete($absence_id);
		if ($delete) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::DELETE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	} 
	else 
	{
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	}
}

$start_date = $end_date = NULL;
$param = array();

if ($this->input->get('start_date') && $this->input->get('start_date')) 
{
	$start_date = date('d-m-Y',strtotime($this->input->get('start_date')));
	$end_date = date('d-m-Y',strtotime($this->input->get('end_date')));
}
else
{
	$param['start_date'] = $start_date = date("01-m-Y");
	$param['end_date'] = $end_date = date("t-m-Y");
}
	
$param['creator_id'] = member_cookies('member_id');
if (isset($start_date) && isset($end_date)) 
{
	$param['start_date'] = $start_date;
	$param['end_date'] = $end_date;
}

// Get data member 
$obj_member = $this->member_model->get(array(
	'creator_id' => member_cookies('member_id')
));
?>

<div class="col-md-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"> </i> DESKRIPSI</b><br/>
		Fitur ini berguna untuk mencatat todolist pekerjaan anda.<br/>
	</div><hr/>
	
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>	
	
	<div class="col-sm-12">
		<a href="<?php echo base_url()?>member/task/create" class="btn btn-success btn-sm">New <?php echo $MODULE ?></a><br/><br/>
		
		<?php
		$param['creator_id'] = member_cookies('member_id');
		// $param['is_detail'] = 0;
		$param['order'] = ' input_date ASC, due_date ASC';
		
		// filter by today
		if (isset($_GET['order']) && $_GET['order'] == 'today') {
			$param['today'] = 1;
		}

		// $param['is_done'] = 1;
		$list_task_done = $this->task_model->get_list($param);
		$list_task_done = $list_task_done['data'];
		
		// $param['is_done'] = 0;
		$list_task_undone = $this->task_model->get_list($param);
		$list_task_undone = $list_task_undone['data'];
		
		?>
		<a class="btn btn-primary btn-sm w200 br" href="<?php echo base_url().'member/task/report';?>">Show Chart</a>
		<a class="btn btn-primary btn-sm w200 br" href="<?php echo base_url().'member/task';?>">Show all data</a>
		<a class="btn btn-primary btn-sm w200 br" href="<?php echo base_url().'member/task?order=today';?>">Show today only</a><br/>
		
		<h3 class="">Done Task</h3>
		<table class="table hover table-bordered">
		<tr class="alert bg-warning b talCnt">
			<td width="10px">#</td>
			<td>Title</td>
			<td width="20px">Priority</td>
			<td width="150px">Due Date</td>
			<td width="120px">Option</td>
		</tr>
		<?php 
		if (!empty($list_task_done))
		{
			foreach($list_task_done as $key => $rs)
			{
				$done = '<i class="fa fa-truck clrRed" title="not done yet"></i> <span class="clrRed">Undone</span>';
				if ($rs['is_done']) $done = '<i class="fa fa-truck fa-flip-horizontal clrGrn" title="done"></i> <span class="clrGrn">Done</span>';
			?>
		<tr data-task-id="<?php echo $rs['task_id']?>">
			<td class="talCnt"><?php echo $key+1?></td>
			<td><div class="edit_title" id="edit_title_<?php echo $rs['task_id']?>"><?php echo $rs['title'] ?></div> <br/>
			<a class="btn btn-info btn-sm" href="<?php echo current_url().'/edit/'.$rs['task_id']?>">Detail</a>
			<?php echo $done?></td>
			<td class="talCnt"><?php echo get_priority($rs['priority']); ?></td>
			<td class="talCnt"><?php echo $rs['due_date']; ?></td>
			<td class="talCnt">
			<a href="<?php echo current_url().'/edit/'.$rs['task_id']?>"><i class="fa fa-edit fa-lg clrGrn"></i></a> &nbsp;
			<a href="<?php echo current_url().'?do=delete&task_id='.$rs['task_id']?>"><i class="fa fa-times-circle fa-lg clrRed"></i></a>
			</td>
		</tr>
			<?php 
			}
		}
		else 
		{
			?>
			<tr>
				<td colspan="100%">No data</td>
			</tr>
			<?php 
		}
		?>
		</table>
		
	</div>
</div>
<script>
$(document).ready(function(){
	
	$('.edit_title').click(function(){
		
		// set state edit only once
		if ($(this).data('is_edit') == 1) return false;
		
		$(this).data('is_edit',1);
		
		var title = $(this).html();
		var task_id = $(this).closest('tr').data('task-id');
		
		$(this).html('');
		$(this).html('<input type="text" class="ajax_edit_title" id="ajax_edit_title" value="' + title + '" style="width:99%; padding:8px 8px; border:1px solid #DBDBEA; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;" onfocusout="ajax_update(' + task_id + ',this)" />');
		
		var title_new = $('#ajax_edit_title').html();
		
		$(this).focusout(function(){
			$(this).html('');
			$(this).data('is_edit',0);

		});
	});
	
	
	$('body').on('keypress', '#ajax_edit_title', function(e) {
		if (e.which == 13) {
			var task_id = $(this).closest('tr').data('task-id');
			ajax_update(task_id,this);
		}
	});
	
		// var task_id = $(this).closest('tr').data('task-id');
	// $('body').on('focusout', '#ajax_edit_title', function() {
		// ajax_update(task_id,this);
	// });
	
});

function ajax_update(task_id, obj)
{
	// alert('update id :' + task_id + ', val: ' + obj.value);
	$('#edit_title_' + task_id ).html(obj.value);
	
	dodelete = false;
	paramdata = "ajax=1&creator_id=" + <?php echo member_cookies('member_id')?> + "&task_id=" + task_id + "&title=" + obj.value;
	
	$.ajax({
		type: "POST",
		data: paramdata,
		cache: false,
		success: function(resp) {

			// $('#loading-response').html('<div class="alert alert-info talCnt"><i class="fa fa-cog fa-spin fa-lg fa-fw"></i> Mohon menunggu</div>');
			setTimeout(function(){
				if (resp == "success") {
					
					if (dodelete) {
						action_message = 'Data telah terhapus.';
					} else {
						action_message = 'Data telah tersimpan.';
					}						

					is_action_success = true;
					
					// untuk redirect 
					// setTimeout(function(){
						// location.reload();
					// }, 2000)
					// 
				} else if (resp == "failed") {
					action_message = 'Mohon maaf, data gagal disimpan.';
				} else {
					action_message = 'Mohon maaf, terjadi kesalahan, mohon ulangi beberapa saat lagi.';
				}
				// alert(action_message);
				// $('#loading-response').html('<div class="alert alert-info talCnt">' + action_message + '</div>');
				
				
			}, 1000);
			
			// setTimeout(function(){
				// if (is_action_success) {
					// $('#loading-response').html('');
					// $('#modalForm').modal('hide');
					// show_balloon_bottom(action_message,12);
					
				// } else {
					// // do save
					// if (!todo_id || todo_id == null) 
						// $('.btn_action[value=dosubmit]').show();						
					// else 
						// $('.btn_action').show();
				// }
				
			// }, 2000);
		}
	});
}

</script>