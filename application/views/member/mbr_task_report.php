<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = 'Task Schedule';
$bread['member'] = 'Task Schedule';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;
$do = $absence_id = NULL;

if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('absence_id')) $absence_id = $this->input->get('absence_id');

// SAVE HERE AJAX
if ($_POST && isset($_POST['ajax'])) 
{
	$post = $param = $tmp = $obj = array();
	$post = $_POST;
	unset($post['ajax']);
	
	$tmp['creator_id'] = member_cookies('member_id');
	$tmp['task_id'] = $post['task_id'];
	$obj = $this->task_model->get($tmp);
	
	$message = 'failed';
	if (! empty($obj)) {
		
		$param['title'] = $post['title'];
		$param['editor_id'] = member_cookies('member_id');
		$update = $this->task_model->update($post['task_id'],$param);
		if ($update) $message = 'success';
	} else {
		
	}
	echo $message;
	die;
}

// SAVE HERE
if ($_POST && (isset($_POST['in_date']) || isset($_POST['out_date'])))
{
	$post = array();
	$post = $_POST;
	unset($post['btn_insert']);
	
	$post['title'] = $post['title'];
	$post['description'] = $post['description'];
	$post['priority'] = $post['description'];
	$post['input_date'] = $post['input_date'];
	if (isset($post['due_date'])) $post['due_date'] = $post['notes'];
	
	$post['is_done'] = 0;
	
	$post['creator_id'] = member_cookies('member_id');
	if (isset($post['absent_status'])) $post['absent_status'] = $post['absent_status'];
	
	if (isset($post['notes'])) $post['notes'] = $post['notes'];

	$insert = $this->task_model->save($post);
	
	redirect(base_url().'member/task');
	
	($insert)?$message['message'] = MESSAGE::SAVE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
}

// DELETE ABSENCE
if ($do == "delete" && is_numeric($absence_id)) 
{
	$absence = $this->absence_model->get(array('absence_id' => $absence_id, 'creator_id' => member_cookies('member_id')));
	if (!empty($treasure)) 
	{
		$delete = $this->absence_model->delete($absence_id);
		if ($delete) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::DELETE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	} 
	else 
	{
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	}
}

$start_date = $end_date = NULL;
$param = array();

if ($this->input->get('start_date') && $this->input->get('start_date')) 
{
	$start_date = date('d-m-Y',strtotime($this->input->get('start_date')));
	$end_date = date('d-m-Y',strtotime($this->input->get('end_date')));
}
else
{
	$param['start_date'] = $start_date = date("01-m-Y");
	$param['end_date'] = $end_date = date("t-m-Y");
}
	
$param['creator_id'] = member_cookies('member_id');
if (isset($start_date) && isset($end_date)) 
{
	$param['start_date'] = $start_date;
	$param['end_date'] = $end_date;
}

// Get data member 
$obj_member = $this->member_model->get(array(
	'creator_id' => member_cookies('member_id')
));
?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/xrange.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
	
<div class="col-md-12">
	<h1 class=""><?php echo $MODULE?></h1>
	<div>
		<b><i class="fa fa-inbox b"> </i> DESKRIPSI</b><br/>
		Fitur ini berguna untuk mencatat todolist pekerjaan anda.<br/>
	</div><hr/>
	
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>	
	
	<div class="col-sm-12">
		<a href="<?php echo base_url()?>member/task" class="btn btn-info btn-sm">Back</a><br/><br/>
		
		<div id="chart"></div>
		
		<?php
		$param['creator_id'] = member_cookies('member_id');
		// $param['is_detail'] = 0;
		$param['order'] = ' input_date ASC, due_date ASC';

		$list_task = $this->task_model->get_list($param);
		$list_task = $list_task['data'];
		?>

		<table class="table hover table-bordered">
		<tr class="alert bg-warning b talCnt">
			<td width="10px">#</td>
			<td>Title</td>
			<td width="20px">Priority</td>
			<td width="150px">Due Date</td>
			<td width="120px">Option</td>
		</tr>
		<?php 
		if (!empty($list_task))
		{
			foreach($list_task as $key => $rs)
			{
				$done = '<i class="fa fa-truck  clrRed" title="not done yet"></i>';
				if ($rs['is_done']) $done = '<i class="fa fa-truck fa-flip-horizontal clrGrn" title="done"></i>';
			?>
		<tr data-task-id="<?php echo $rs['task_id']?>">
			<td class="talCnt"><?php echo $key+1?></td>
			<td><div class="edit_title" id="edit_title_<?php echo $rs['task_id']?>"><?php echo $rs['title'] ?></div> <?php echo $done?></td>
			<td class="talCnt"><?php echo get_priority($rs['priority']); ?></td>
			<td class="talCnt"><?php echo $rs['due_date']; ?></td>
			<td class="talCnt">
			<!--
			<a href="<?php echo current_url().'/edit/'.$rs['task_id']?>"><i class="fa fa-edit fa-lg clrGrn"></i></a> &nbsp;
			<a href="<?php echo current_url().'?do=delete&task_id='.$rs['task_id']?>"><i class="fa fa-times-circle fa-lg clrRed"></i></a>
			-->
			</td>
		</tr>
			<?php 
			}
		}
		else 
		{
			?>
			<tr>
				<td colspan="100%">No data</td>
			</tr>
			<?php 
		}
		?>
		</table>
		
	</div>
</div>
<?php 
$strchart = $tmpchart = NULL;
if (! empty($list_task)) 
{
	foreach ($list_task as $key => $task) {
		$tmpchart[] = $task['title'];
	}

	$tmpchart = implode($tmpchart);
} 
else
{
	$tmpchart[] = 'No Project Currently';
}
$strchart = "'" . $tmpchart . "'";
// debug($strchart);
?>
<script>
Highcharts.chart('chart', {
	chart: {
		type: 'xrange'
	},
	title: {
		text: 'Roadmap project'
	},
	xAxis: {
		type: 'datetime'
	},
	yAxis: {
		title: {
			text: ''
		},
		categories: [<?php echo implode('","',$strchart) ?>],
		reversed: true
	},
	series: [{
		name: 'Project 1',
		// pointPadding: 0,
		// groupPadding: 0,
		borderColor: 'gray',
		pointWidth: 20,
		data: [{
			x: Date.UTC(2014, 10, 21),
			x2: Date.UTC(2014, 11, 2),
			y: 0,
			partialFill: 0.79
		}, {
			x: Date.UTC(2014, 11, 2),
			x2: Date.UTC(2014, 11, 5),
			y: 1
		}, {
			x: Date.UTC(2014, 11, 8),
			x2: Date.UTC(2014, 11, 9),
			y: 2
		}, {
			x: Date.UTC(2014, 11, 9),
			x2: Date.UTC(2014, 11, 19),
			y: 1
		}, {
			x: Date.UTC(2018, 04, 10),
			x2: Date.UTC(2018, 05, 23),
			y: 2
		}],
		dataLabels: {
			enabled: true
		}
	}]

});

$(document).ready(function(){
	
});

</script>