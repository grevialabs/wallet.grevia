<!--
<div class="jumbotron bgBluLight" style="margin-bottom:0;padding:35px 10px">
	<div class="container">
		<div class="col-md-3 lnkLogo">
		<a class="fntNml"><i class="fa fa-comments fa-2x"></i> <span class="clrWht">Navigation</span></a><br/>

			<a href="<?php echo base_url()?>about">About <?php echo show_active_menu('about')?></a><br/>
			<a href="<?php echo base_url()?>contact">Contact <?php echo show_active_menu('contact')?></a>
		</div>
		
		<div class="col-md-3 lnkLogo">
		<a class="fntNml"><i class="fa fa-comments fa-2x"></i> <span class="clrWht">Navigation</span></a><br/>
		<a class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i>&nbsp;</a>
		<a class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i>&nbsp;</a>
		</div>
		<div class="col-md-6 talCnt lnkWht"></div>
		<div class="clearfix"></div>
	</div>
</div>
-->

<?php if ($this->uri->segment(2) != 'admin') { ?>
<!--
<div class="jumbotron bgTopFooter" style="margin-bottom:0; padding-bottom:0; padding: 20px">
	<div class="container clrLgtGry" style="padding: 20px">
		<div class="col-sm-2 talLft lnkBlk">
			<img class="img-responsive wdtFul" src="<?php echo base_url()?>asset/images/logo-grevia.png"/>
		</div>
		<div class="col-sm-3 talLft ">
	Grevia adalah <strong>Grup Evangelist Internet Indonesia</strong> yang memiliki visi meningkatkan jumlah pengusaha di bidang teknologi informasi khususnya digital kreatif dan technopreneur di Indonesia.<br/><br/>
		</div>
		<div class="col-sm-2 talLft f12 lnkLgtGry">
			 &nbsp;
		</div>
		<div class="col-sm-1 f12 lnkLgtGry" style="padding-left:25px"><span class="b">Navigation</span><br/>
		<a href="<?php echo base_url()?>article">Article<br/>
		<a href="<?php echo base_url()?>about">About<br/>
		<a href="<?php echo base_url()?>contact">Contact</a><br/>
		</div>
		<div class="col-sm-1 talLft clrWht"></div>
		<div class="col-sm-3 talLft clrWht">
			<div class="well well-sm clrBlk" id="divSubscribe">
				<div class="br">Daftar Email anda ke newsletter</div>
				<input class="input br wdtFul" type="text" name="t_EmailSubscribe" id="t_EmailSubscribe" placeholder="Email anda..."/><br/>
				<button class="btn btn-info wdtFul subscribe">Submit</button>
			</div><br/>
			<a href="<?php echo current_url().'?lang=en'?>"/>English</a>
			<a href="<?php echo current_url().'?lang=id'?>"/>Indonesian</a>
		</div>
	</div>
</div>
-->
<?php } ?>

<div class="talCnt" style="margin-bottom:0; padding:25px; border-top:1px solid #DBDBEA">
	<div class="container">
		<div class="col-md-12">
			<span class="fntNml">&copy; <?php echo date('Y',time())?></span>
			<span class="">Powered by</span> <a class="lnkBlu" href="http://www.grevialabs.com" target="_blank">Grevialabs</a>
			<!-- <br/>Page rendered in <strong>{elapsed_time}</strong> seconds {memory_usage}-->
		</div>
		<!--
		<div class="col-md-6 talRgt">
			
			<a href="<?php echo base_url()?>about">About<?php echo show_active_menu('about')?></a>
			&nbsp;<a>|</a>&nbsp;
			<a href="<?php echo base_url()?>contact">Contact<?php echo show_active_menu('contact')?></a>
			
		</div>
		-->
	</div>
</div>

<script>
function IsEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!regex.test(email)) {
		return false;
	}else{
		return true;
	}
  }
  
function togglebox(){
	if ($('.togglebox').prop('checked')) {
		$('.chkbox').prop('checked',true);
	}
	if (!$('.togglebox').prop('checked')) {
		$('.chkbox').prop('checked',false);
	}
}

function confirmDelete() {
	return confirm('<?php DELETE.' '. DATA.'. '.SELECTED ?> ?');	
}

function sompla()
{
	var txt;
	var r = confirm("Press a button!");
	if (r == true) {
		return true;
	}
	return false;
}

function doConfirm() {
	return confirm('<?php echo CONFIRM.' '.ACTION?> ?');	
}
$(".subscribe").click(function(){
	var the_name = $('#t_name_subscribe').val();
	var the_email = $('#t_email_subscribe').val();
	if (IsEmail(the_email)) {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url()?>ajax",
			data: "apitoken=<?php echo rand(100000000,9999999999);?>&do=subscribe&name="+ the_name +"&email=" + the_email,
			cache: false,
			success: function(resp) {
				if (resp == "<?php echo INFO_SAVE_SUCCESS?>") {
					$('#divSubscribe').html('<div class=""><i class="fa fa-info"></i> &nbsp;<?php echo INFO_EMAIL_SUBSCRIBE_SUCCESS?></div>');
					$('#form_register').html('<div class="bg-primary padMed"><i class="fa fa-info"></i> &nbsp;<?php echo INFO_EMAIL_SUBSCRIBE_SUCCESS?></div>');
				} else {
					if (resp == "<?php echo INFO_EMAIL_SUBSCRIBE_EXIST?>") {
						$('#t_email_subscribe').focus();
					} 
					alert(resp);
				}
			}
		});
	} else {
		$('#t_email_subscribe').focus();
		alert('Email tidak valid.');
	}
});

// PRINT BACK TO TOP BUTTON
var back_button;
back_button = '<a href="#" class="back-to-top"><i class="fa fa-arrow-up fa-lg"></i></a>';
$('body').prepend(back_button);

// SCROLL TO TOP BUTTON
var amountScrolled = 200;

$(window).scroll(function() {
	if ( $(window).scrollTop() > amountScrolled ) {
		$('a.back-to-top').fadeIn('slow');
	} else {
		$('a.back-to-top').fadeOut('slow');
	}
});

// CLICK TRIGGER
$('a.back-to-top').click(function() {
	$('html, body').animate({
		scrollTop: 0
	}, 700);
	return false;
});

$(document).ready(function(){
	// show_balloon('bisanih bro');
});

function show_balloon(str, css = 'info')
{
	// Set modal info
	$('#balloon_content').html(str);
	$('#balloon').addClass('alert alert-' + css);
	$('#balloon').fadeIn()
	.css({right:-400, top:'50%'})
	.animate({right:15, top:'50%'}, 1000, function(){
		// callback
		
	});
	
	window.setTimeout(function() {
		$('#balloon').fadeIn()
		.animate({right:-400}, 1000, function(){
			
		});
	}, 4500);
}
</script>

<div id="balloon" class="" style="width:300px; position:fixed; display: none" >
	<span class="close" data-dismiss="alert">&times;</span>
	<span id="balloon_content"></span>
</div>

<!-- Start modal block -->
<div id="modalLoading" class="modal fade" role="dialog" style="padding-top:200px;z-index:9999;background-color:rgba(0, 0, 0, 0.4)">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
		<div class="talCnt" style="padding:25px 10px">
			<i class="fa fa-cog fa-spin fa-3x fa-fw"></i><br/>
			Mohon menunggu.
		</div>
      </div>
    </div>

  </div>
</div>
<!-- End modal block -->

<script>
function modal_loading_block()
{
	$('#modalLoading').modal({
		show: 'false',
		keyboard: false,
		backdrop: 'static'
	});
}
</script>