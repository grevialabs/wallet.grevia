<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER;
global $param, $message;

$PAGE_HEADER = NULL;
$PAGE = $MODULE = REPORT;
$bread['company'] = 'Company';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

$participant_id = $do = $test_id = NULL;
if ($this->input->get('participant_id')) $participant_id = $this->input->get('participant_id'); 
if ($this->input->get('do')) $do = $this->input->get('do'); 
if ($this->input->get('test_id')) $test_id = $this->input->get('test_id');
	
if ($_POST)
{
	// export pdf
	if (isset($_POST['export_pdf']) && $_POST['export_pdf'])
	{
		$content = NULL;
		
		$list_history_test = $this->history_test_model->get_list_history_test(array('participant_id' => $participant_id));
		$list_history_test = $list_history_test['data'];
		
		if (!empty($list_history_test) && isset($test_id))
		{
			foreach($list_history_test as $k => $history_test )
			{			
				if ($history_test['test_code'] == 'disc-1') 
				{
					// FIND RESULT BY EACH
					$temp1['history_test_id'] = $history_test['history_test_id'];
					$temp1['participant_id'] = $participant_id;
					$obj_result = $this->history_test_model->get_disc1_result($temp1);
					$participant = $this->participant_model->get(array('participant_id' => $participant_id));
					if (!empty($obj_result['data']))
					{
						$disc_strength = array();
						$disc_weak = array();
						
						$disc_strength['d'] = $disc_strength['i'] = $disc_strength['s'] = $disc_strength['c'] = $disc_weak['d'] = $disc_weak['i'] = $disc_weak['s'] = $disc_weak['c'] = 0;

						foreach ($obj_result['data'] as $key => $rs)
						{
							if ($rs['the_order'] == 'd+') $disc_strength['d'] = $rs['show_count'];
							if ($rs['the_order'] == 'i+') $disc_strength['i'] = $rs['show_count'];
							if ($rs['the_order'] == 's+') $disc_strength['s'] = $rs['show_count'];
							if ($rs['the_order'] == 'c+') $disc_strength['c'] = $rs['show_count'];
							
							if ($rs['the_order'] == 'd-') $disc_weak['d'] = $rs['show_count'];
							if ($rs['the_order'] == 'i-') $disc_weak['i'] = $rs['show_count'];
							if ($rs['the_order'] == 's-') $disc_weak['s'] = $rs['show_count'];
							if ($rs['the_order'] == 'c-') $disc_weak['c'] = $rs['show_count'];
						}
						$disc_str = $disc_strength['d'].$disc_strength['i'].$disc_strength['s'].$disc_strength['c'];
						$disc_wk = $disc_weak['d'].$disc_weak['i'].$disc_weak['s'].$disc_weak['c'];
						
						$d['count'] = $disc_strength['d'] + $disc_weak['d'];
						$i['count'] = $disc_strength['i'] + $disc_weak['i'];
						$s['count'] = $disc_strength['s'] + $disc_weak['s'];
						$c['count'] = $disc_strength['c'] + $disc_weak['c'];
						
						$d['percent'] = $d['count'] / 40 * 100;
						$i['percent'] = $i['count'] / 40 * 100;
						$s['percent'] = $s['count'] / 40 * 100;
						$c['percent'] = $c['count'] / 40 * 100;
						
						$content = '
						<br/><br/><img src="'.base_url().'asset/images/logo-jobtalento.PNG"/><br/><b>Your Online Psychotest Solution</b><br/><br/>
						<table class="table " border = "1" cellpadding="4" cellspacing="0" width="540px" >
						<tr class="b">
							<td colspan="2">
							<b>'.strtoupper(PARTICIPANT_DATA).'</b>
							</td>
						</tr>
						<tr class="b">
							<td width="150px">'.NAME.'</td>
							<td width="390px">'.$participant['name'].'</td>
						</tr>
						<tr class="b">
							<td>'.EMAIL.'</td>
							<td>'.$participant['email'].'</td>
						</tr>
						<tr class="b">
							<td>'.GENDER.'</td>
							<td>'.get_gender($participant['gender']).'</td>
						</tr>
						<tr class="b">
							<td>'.COMPANY.'</td>
							<td>'.$participant['company_name'].'</td>
						</tr>
						<tr class="b">
							<td>'.JOB_POSITION.'</td>
							<td>'.$participant['job_position'].'</td>
						</tr>
						</table>
						<br/><br/>
						
						<table class="table " border = "1" cellpadding="4" cellspacing="0" width="540px" style="text-align:center">
						<tr class="b">
							<td colspan="4" width="160px">STRENGTH</td>
							<td width="20px">+</td>
							<td colspan="4" width="160px">WEAKNESS</td>
							<td width="200px">RESULT</td>
						</tr>
						<tr>
							<td>'.$disc_strength['d'].'</td>
							<td>'.$disc_strength['i'].'</td>
							<td>'.$disc_strength['s'].'</td>
							<td>'.$disc_strength['c'].'</td>
							<td>&nbsp;</td>
							<td>'.$disc_weak['d'].'</td>
							<td>'.$disc_weak['i'].'</td>
							<td>'.$disc_weak['s'].'</td>
							<td>'.$disc_weak['c'].'</td>
							<td valign="top" rowspan="2">
								<table class="table table-bordered" border="1" width="190px">
								<tr>
									<td class="b">D</td>
									<td>'.$d['count'].'</td>
									<td>'.$d['percent'].'%</td>
								</tr>
								<tr>
									<td class="b">I</td>
									<td>'.$i['count'].'</td>
									<td>'.$i['percent'].'%</td>
								</tr>							
								<tr>
									<td class="b">S</td>
									<td>'.$s['count'].'</td>
									<td>'.$s['percent'].'%</td>
								</tr>							
								<tr>
									<td class="b">C</td>
									<td>'.$c['count'].'</td>
									<td>'.$c['percent'].'%</td>
								</tr>
								</table>
							</td>
						</tr>
						<tr class="b" style="font-weight:bold">
							<td>D</td>
							<td>I</td>
							<td>S</td>
							<td>C</td>
							<td>&nbsp;</td>
							<td>D</td>
							<td>I</td>
							<td>S</td>
							<td>C</td>
						</tr>
						</table>
						
						<br><br/>
						
						<table class="table table-bordered" border = "1" cellpadding="4" cellspacing="0" width="550px">
						<tr class="b" style="text-align:center">
							<td width="30px">No</td>
							<td width="130px">TIPE KEPRIBADIAN</td>
							<td width="190px">STRENGTH CHARACTERISTIC</td>
							<td width="190px">WEAK CHARACTERISTIC</td>
						</tr>
						<tr>
							<td rowspan="10">1</td>
							<td class="talCnt">Dominance Style</td>
							<td>Suka mengendalikan Lingkungan</td>
							<td>Pemimpin yang otoriter / demanding</td>
						</tr>
						<tr>
							<td class="talCnt">Kekuasaan</td>
							<td>Suka menggerakkan orang di sekitar mereka</td>
							<td>Kurang kesabaran & empati terhadap bawahan</td>
						</tr>
						<tr>
							<td class="talCnt">'.$d['count'].'</td>
							<td>Pribadi yang to the point</td>
							<td>Bila termotivasi negatif akan menjadi pembangkang</td>
						</tr>
						<tr>
							<td class="fntHdr talCnt" rowspan="7">'.$d['percent'].'%</td>
							<td>Tidak bertele-tele</td>
							<td>Cepat bosan terhadap rutinitas</td>
						</tr>
						<tr>
							<td>Senang mengambil peran penting</td>
							<td>Kurang suka hal yang detail</td>
						</tr>
						<tr>
							<td>Pembuat keputusan</td>
							<td></td>
						</tr>
						<tr>
							<td>Problem solver</td>
							<td></td>
						</tr>
						<tr>
							<td>menyukai posisi sebagai leader</td>
							<td></td>
						</tr>
						<tr>
							<td>Orang yang punya visi / visioner</td>
							<td></td>
						</tr>
						<tr>
							<td>menyukai tantangan & berani mengambil resiko</td>
							<td></td>
						</tr>
						<tr>
							<td rowspan="9">2</td>
							<td class="talCnt">Influence Style</td>
							<td>Suka Bergaul dengan orang lain</td>
							<td>Tidak suka bekerja sendirian / lebih suka bekerja dengan orang lain</td>
						</tr>
						<tr>
							<td class="talCnt">Pengaruh</td>
							<td>Ekstrovert</td>
							<td>Tidak suka pekerjaan / tugas yang menuntut ketelitian</td>
						</tr>
						<tr>
							<td class="talCnt">'.$i['count'].'</td>
							<td>Senang berada pada pertemanan yang luas</td>
							<td></td>
						</tr>
						<tr>
							<td class="fntHdr talCnt" rowspan="6">'.$i['percent'].'%</td>
							<td>Empati terhadap orang lain tinggi</td>
							<td></td>
						</tr>
						<tr>
							<td>Mudah melibatkan perasaan ketika sedang beraktivitas</td>
							<td></td>
						</tr>
						<tr>
							<td>Optimis</td>
							<td></td>
						</tr>
						<tr>
							<td>Antusias</td>
							<td></td>
						</tr>
						<tr>
							<td>Sifat dasar riang</td>
							<td></td>
						</tr>
						<tr>
							<td>Dapat menjadi seorang promotor untuk gagasan baru</td>
							<td></td>
						</tr>

						<tr>
							<td rowspan="8">3</td>
							<td class="talCnt">Steadiness Style</td>
							<td>Introvert</td>
							<td>Kurang menyukai perubahan yang radikal dan bersifat mendadak</td>
						</tr>
						<tr>
							<td class="talCnt">Kemantapan</td>
							<td>Reserve</td>
							<td>Terpaku pada sistem yang sudah berjalan</td>
						</tr>
						<tr>
							<td class="talCnt">'.$s['count'].'</td>
							<td>Quiet</td>
							<td>Kurang terdorong melakukan inovasi yang bersifat radikal</td>
						</tr>
						<tr>
							<td class="fntHdr talCnt" rowspan="5">'.$s['percent'].'%</td>
							<td>Suka melakukan sesuatu secara sistematis</td>
							<td>ketika sedang demotivasi akan menjadi orang yang kaku, resisten dan kemudian akan melakukan perlawanan secara pasif</td>
						</tr>
						<tr>
							<td>Teratur & bertahap</td>
							<td></td>
						</tr>
						<tr>
							<td>Suka sesuatu yang berjalan dengan konsisten</td>
							<td></td>
						</tr>
						<tr>
							<td>Pribadi yang sabar dan dapat diandalkan</td>
							<td></td>
						</tr>
						<tr>
							<td>Loyalitas tinggi</td>
							<td></td>
						</tr>
						
						<tr>
							<td rowspan="7">4</td>
							<td class="talCnt">Conscientiousness Style</td>
							<td>Pribadi yang menekankan akurasi dan ketelitian yang tinggi</td>
							<td>Terlalu fokus pada keteraturan</td>
						</tr>
						<tr>
							<td class="talCnt">Ketelitian / Kecermatan</td>
							<td>Menyukai sesuatu yang direncanakan dengan matang & bersifat menyeluruh</td>
							<td>Skeptis terhadap gagasan baru yang radikal</td>
						</tr>
						<tr>
							<td class="talCnt">'.$c['count'].'</td>
							<td>Suka dengan pekerjaan yang mengacu pada prosedur & standar operasi baku</td>
							<td>Tidak suka menerima perubahan yang mendadak</td>
						</tr>
						<tr>
							<td class="fntHdr talCnt" rowspan="2">'.$c['percent'].'%</td>
							<td>Seorang pribadi pemikir yang kritis</td>
							<td>Jika sedang termotivasi secara negatif akan menjadi sinis atau sangat kritis</td>
						</tr>
						<tr>
							<td>Memiliki analisa untuk memastikan akurasi</td>
							<td></td>
						</tr>
						
						</table>';
					}
				}
				else if ($history_test['test_code'] == "2")
				{
					// disini
				}
			}
		}		
		
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Jobtalento');
		$pdf->SetTitle('Personality Report');
		$pdf->SetSubject('Personality Report');
		$pdf->SetKeywords('Jobtalento','Psychotest','Online Psychotest');

		// set default header data
		//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

		// // set header and footer fonts
		// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// // set default monospaced font
		// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// // set margins
		// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// // set auto page breaks
		// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// // set image scale factor
		// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('dejavusans', '', 10);

		// add a page
		$pdf->AddPage();

		// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
		// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
		
		// output the HTML content

		//echo $content;die;
		$pdf->writeHTML($content, true, false, true, false, '');
		
		//Close and output PDF document
		// I => untuk output ke browser on the fly
		// D => untuk download
		$pdf->Output('report_'.$participant['name'].'_'.$participant['participant_id'].'.pdf', 'D');

		die;
		//============================================================+
		// END OF FILE
		//============================================================+
	}
}
?>

<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1 class=""><?php echo $MODULE?></h1><br/>
	
	<?php if(is_filled($message['message']))echo message($message['message']).BR?>
	
	<?php 

	if (is_numeric($participant_id) && !isset($do))
	{
		$obj = $this->participant_model->get( array(
			'participant_id' => $participant_id,
			'company_id' => company_cookies('company_id')
		) );
			
		if (!empty($obj))
		{
			// SHOW HISTORY RESULT
			$temp = array();
			$temp['participant_id'] = $participant_id;
			$obj_list_type_test = $this->test_model->get_list_type_test_participant($temp);
			$obj_list_type_test = $obj_list_type_test['data'];
			?>
			<div class="table-responsive">
			<table class="table table-hover table-bordered">
			<tr>
				<td><?php echo NAME?></td>
				<td><?php echo $obj['name']?></td>
			</tr>
			<tr>
				<td><?php echo EMAIL?></td>
				<td><?php echo $obj['email']?></td>
			</tr>
			<tr>
				<td><?php echo GENDER?></td>
				<td><?php echo get_gender($obj['gender'])?></td>
			</tr>
			</table>
			</div>
			<br/>
			<?php
			if (!empty($obj_list_type_test)) 
			{
				?>
				<div class="table-responsive">
				<table class="table table-hover table-bordered">
				<tr>
					<td>#</td>
					<td><?php echo TEST_NAME ?></td>
					<td><?php echo TEST_SCORE?></td>
					<td><?php echo TEST_DATE?></td>
					<td><?php echo TEST_RESULT?></td>
				</tr>
				<?php
				foreach ($obj_list_type_test as $key => $test)
				{
					// GET RESULT BY GENERIC TEST
					// $param = array();
					// $param['test_id'] = $test['test_id'];
					// $param['participant_id'] = $participant_id;
					// $obj_result = $this->participant_model->get_test_result_score($param);
				
					// GET RESULT BY HISTORY TEST STARTDATE END DATE
					$param = array();
					$param['test_id'] = $test['test_id'];
					$param['participant_id'] = $participant_id;
					$obj_result = $this->history_test_model->get_history_test($param);
					
					?>
				<tr>
					<td width="1"><?php echo $key + 1?></td>
					<td width="300px"><?php echo $test['title']?></td>
					<td> <?php //echo $obj_result['total_score']?></td>
					<td><?php if(isset($obj_result['test_date'])) echo date(DATE_FORMAT, strtotime($obj_result['test_date'])); else echo '-';?></td>
					<td>
					
					<?php 
					if (!empty($obj_result) && isset($obj_result['start_time'])) {
						//echo $obj_result['result'];
						if (isset($obj_result['end_time'])) 
						{
							?>
						<a href="<?php echo current_url()?>?do=detail&participant_id=<?php echo $participant_id?>&test_id=<?php echo $test['test_id']?>">detail result</a>
							<?php
						}
						else
						{
							echo "Test belum selesai dijalani";
						}
						
					} else if (empty($obj_result) || $obj_result == 0) {
						echo DATA_NOT_FOUND;
					}
					?></td>
				</tr>
					<?php
				}

				?>
				</table>
				</div>
				<?php
			}
			else 
			{
				echo DATA_NOT_FOUND;
			}
		}
		else 
		{
			echo DATA_NOT_FOUND;
		}
	}
	else if ($do == 'detail' && is_numeric($test_id))
	{
		// show history report here
		// split by generic and disc-1
		
		// SHOW ALL HISTORY TEST
		
		$list_history_test = $this->history_test_model->get_list_history_test(array(
		'participant_id' => $participant_id,
		'test_id' => $test_id
		));
		$list_history_test = $list_history_test['data'];
		
		if (!empty($list_history_test) && isset($test_id))
		{
			foreach($list_history_test as $k => $history_test )
			{			
				// result disc-1
				if (isset($history_test['test_code']) && $history_test['test_code'] == 'disc-1') 
				{
					
					// FIND RESULT BY EACH
					$temp1['history_test_id'] = $history_test['history_test_id'];
					$temp1['participant_id'] = $participant_id;
					$obj_result = $this->history_test_model->get_disc1_result($temp1);
					//$obj_result = $obj_result['data'];
					if (!empty($obj_result['data']))
					{
						$disc_strength = array();
						$disc_weak = array();
						
						$disc_strength['d'] = $disc_strength['i'] = $disc_strength['s'] = $disc_strength['c'] = $disc_weak['d'] = $disc_weak['i'] = $disc_weak['s'] = $disc_weak['c'] = 0;

						foreach ($obj_result['data'] as $key => $rs)
						{
							if ($rs['the_order'] == 'd+') $disc_strength['d'] = $rs['show_count'];
							if ($rs['the_order'] == 'i+') $disc_strength['i'] = $rs['show_count'];
							if ($rs['the_order'] == 's+') $disc_strength['s'] = $rs['show_count'];
							if ($rs['the_order'] == 'c+') $disc_strength['c'] = $rs['show_count'];
							
							if ($rs['the_order'] == 'd-') $disc_weak['d'] = $rs['show_count'];
							if ($rs['the_order'] == 'i-') $disc_weak['i'] = $rs['show_count'];
							if ($rs['the_order'] == 's-') $disc_weak['s'] = $rs['show_count'];
							if ($rs['the_order'] == 'c-') $disc_weak['c'] = $rs['show_count'];
						}
						$disc_str = $disc_strength['d'].$disc_strength['i'].$disc_strength['s'].$disc_strength['c'];
						$disc_wk = $disc_weak['d'].$disc_weak['i'].$disc_weak['s'].$disc_weak['c'];
						
						$d['count'] = $disc_strength['d'] + $disc_weak['d'];
						$i['count'] = $disc_strength['i'] + $disc_weak['i'];
						$s['count'] = $disc_strength['s'] + $disc_weak['s'];
						$c['count'] = $disc_strength['c'] + $disc_weak['c'];
						
						$d['percent'] = $d['count'] / 40 * 100;
						$i['percent'] = $i['count'] / 40 * 100;
						$s['percent'] = $s['count'] / 40 * 100;
						$c['percent'] = $c['count'] / 40 * 100;
						
						
						$participant = $this->participant_model->get(array('participant_id' => $participant_id));
						?>
						
						<form method="post">
						<button type="text" name="export_pdf" class="btn btn-success" value="1"><?php echo EXPORT_PDF?></button>
						</form><br/><br/>
						
						<div class="table-responsive">
						<table class="table table-bordered" >
						<tr class="b">
							<td colspan="2">
							<b><?php echo strtoupper(PARTICIPANT_DATA)?></b>
							</td>
						</tr>
						<tr class="b">
							<td width="150px"><?php echo NAME; ?></td>
							<td width="390px"><?php echo $participant['name']; ?></td>
						</tr>
						<tr class="b">
							<td><?php echo EMAIL; ?></td>
							<td><?php echo $participant['email']; ?></td>
						</tr>
						<tr class="b">
							<td><?php echo GENDER; ?></td>
							<td><?php echo get_gender($participant['gender']); ?></td>
						</tr>
						<tr class="b">
							<td><?php echo COMPANY; ?></td>
							<td><?php echo $participant['company_name']; ?></td>
						</tr>
						<tr class="b">
							<td><?php echo JOB_POSITION; ?></td>
							<td><?php echo $participant['job_position']; ?></td>
						</tr>
						</table>
						</div>
						
						<br/>
						
						<div class="table-responsive">
						<table class="table ">
						<tr class="b">
							<td colspan="4">STRENGTH</td>
							<td>+</td>
							<td colspan="4">WEAKNESS</td>
							<td>RESULT</td>
						</tr>
						<tr>
							<td><?php echo $disc_strength['d']?></td>
							<td><?php echo $disc_strength['i']?></td>
							<td><?php echo $disc_strength['s']?></td>
							<td><?php echo $disc_strength['c']?></td>
							<td>&nbsp;</td>
							<td><?php echo $disc_weak['d']?></td>
							<td><?php echo $disc_weak['i']?></td>
							<td><?php echo $disc_weak['s']?></td>
							<td><?php echo $disc_weak['c']?></td>
							<td valign="top" rowspan="2">
								<table class="table table-bordered">
								<tr>
									<td class="b">D</td>
									<td><?php echo $d['count']?></td>
									<td><?php echo $d['percent']?>%</td>
								</tr>
								<tr>
									<td class="b">I</td>
									<td><?php echo $i['count']?></td>
									<td><?php echo $i['percent']?>%</td>
								</tr>							
								<tr>
									<td class="b">S</td>
									<td><?php echo $s['count']?></td>
									<td><?php echo $s['percent']?>%</td>
								</tr>							
								<tr>
									<td class="b">C</td>
									<td><?php echo $c['count']?></td>
									<td><?php echo $c['percent']?>%</td>
								</tr>
								</table>
							</td>
						</tr>
						<tr class="b">
							<td>D</td>
							<td>I</td>
							<td>S</td>
							<td>C</td>
							<td>&nbsp;</td>
							<td>D</td>
							<td>I</td>
							<td>S</td>
							<td>C</td>
						</tr>
						</table>
						</div>
						
						<hr>
						<div class="table-responsive">
						<table class="table table-bordered">
						<tr class="b">
							<td>No</td>
							<td>TIPE KEPRIBADIAN</td>
							<td>STRENGTH CHARACTERISTIC</td>
							<td>WEAK CHARACTERISTIC</td>
						</tr>
						<tr>
							<td rowspan="10">1</td>
							<td class="talCnt">Dominance Style</td>
							<td>Suka mengendalikan Lingkungan</td>
							<td>Pemimpin yang otoriter / demanding</td>
						</tr>
						<tr>
							<td class="talCnt">Kekuasaan</td>
							<td>Suka menggerakkan orang di sekitar mereka</td>
							<td>Kurang kesabaran & empati terhadap bawahan</td>
						</tr>
						<tr>
							<td class="talCnt"><?php echo $d['count']?></td>
							<td>Pribadi yang to the point</td>
							<td>Bila termotivasi negatif akan menjadi pembangkang</td>
						</tr>
						<tr>
							<td class="fntHdr talCnt" rowspan="7"><?php echo $d['percent']?>%</td>
							<td>Tidak bertele-tele</td>
							<td>Cepat bosan terhadap rutinitas</td>
						</tr>
						<tr>
							<td>Senang mengambil peran penting</td>
							<td>Kurang suka hal yang detail</td>
						</tr>
						<tr>
							<td>Pembuat keputusan</td>
							<td></td>
						</tr>
						<tr>
							<td>Problem solver</td>
							<td></td>
						</tr>
						<tr>
							<td>menyukai posisi sebagai leader</td>
							<td></td>
						</tr>
						<tr>
							<td>Orang yang punya visi / visioner</td>
							<td></td>
						</tr>
						<tr>
							<td>menyukai tantangan & berani mengambil resiko</td>
							<td></td>
						</tr>
						<!-- INFLUENCE START -->
						<tr>
							<td rowspan="9">2</td>
							<td class="talCnt">Influence Style</td>
							<td>Suka Bergaul dengan orang lain</td>
							<td>Tidak suka bekerja sendirian / lebih suka bekerja dengan orang lain</td>
						</tr>
						<tr>
							<td class="talCnt">Pengaruh</td>
							<td>Ekstrovert</td>
							<td>Tidak suka pekerjaan / tugas yang menuntut ketelitian</td>
						</tr>
						<tr>
							<td class="talCnt"><?php echo $i['count']?></td>
							<td>Senang berada pada pertemanan yang luas</td>
							<td></td>
						</tr>
						<tr>
							<td class="fntHdr talCnt" rowspan="6"><?php echo $i['percent']?>%</td>
							<td>Empati terhadap orang lain tinggi</td>
							<td></td>
						</tr>
						<tr>
							<td>Mudah melibatkan perasaan ketika sedang beraktivitas</td>
							<td></td>
						</tr>
						<tr>
							<td>Optimis</td>
							<td></td>
						</tr>
						<tr>
							<td>Antusias</td>
							<td></td>
						</tr>
						<tr>
							<td>Sifat dasar riang</td>
							<td></td>
						</tr>
						<tr>
							<td>Dapat menjadi seorang promotor untuk gagasan baru</td>
							<td></td>
						</tr>
						<!-- INFLUENCE END -->
						
						<!-- STEADINESS START -->
						<tr>
							<td rowspan="8">3</td>
							<td class="talCnt">Steadiness Style</td>
							<td>Introvert</td>
							<td>Kurang menyukai perubahan yang radikal dan bersifat mendadak</td>
						</tr>
						<tr>
							<td class="talCnt">Kemantapan</td>
							<td>Reserve</td>
							<td>Terpaku pada sistem yang sudah berjalan</td>
						</tr>
						<tr>
							<td class="talCnt"><?php echo $s['count']?></td>
							<td>Quiet</td>
							<td>Kurang terdorong melakukan inovasi yang bersifat radikal</td>
						</tr>
						<tr>
							<td class="fntHdr talCnt" rowspan="5"><?php echo $s['percent']?>%</td>
							<td>Suka melakukan sesuatu secara sistematis</td>
							<td>ketika sedang demotivasi akan menjadi orang yang kaku, resisten dan kemudian akan melakukan perlawanan secara pasif</td>
						</tr>
						<tr>
							<td>Teratur & bertahap</td>
							<td></td>
						</tr>
						<tr>
							<td>Suka sesuatu yang berjalan dengan konsisten</td>
							<td></td>
						</tr>
						<tr>
							<td>Pribadi yang sabar dan dapat diandalkan</td>
							<td></td>
						</tr>
						<tr>
							<td>Loyalitas tinggi</td>
							<td></td>
						</tr>
						<!-- STEADINESS END -->
						
						<!-- CONSCIENTIOUSNESS START -->
						<tr>
							<td rowspan="7">4</td>
							<td class="talCnt">Conscientiousness Style</td>
							<td>Pribadi yang menekankan akurasi dan ketelitian yang tinggi</td>
							<td>Terlalu fokus pada keteraturan</td>
						</tr>
						<tr>
							<td class="talCnt">Ketelitian / Kecermatan</td>
							<td>Menyukai sesuatu yang direncanakan dengan matang & bersifat menyeluruh</td>
							<td>Skeptis terhadap gagasan baru yang radikal</td>
						</tr>
						<tr>
							<td class="talCnt"><?php echo $c['count']?></td>
							<td>Suka dengan pekerjaan yang mengacu pada prosedur & standar operasi baku</td>
							<td>Tidak suka menerima perubahan yang mendadak</td>
						</tr>
						<tr>
							<td class="fntHdr talCnt" rowspan="2"><?php echo $c['percent']?>%</td>
							<td>Seorang pribadi pemikir yang kritis</td>
							<td>Jika sedang termotivasi secara negatif akan menjadi sinis atau sangat kritis</td>
						</tr>
						<tr>
							<td>Memiliki analisa untuk memastikan akurasi</td>
							<td></td>
						</tr>
						<!-- CONSCIENTIOUSNESS END -->
						
						</table>
						</div>
						
						<?php
						//echo 'disc strength: '.$disc_str.'<br> disc weak'.$disc_wk;
					}
				}
				else if ($history_test['test_code'] == 'disc-2')
				{
					// FIND RESULT BY EACH
					$temp1['history_test_id'] = $history_test['history_test_id'];
					$temp1['participant_id'] = $participant_id;
					$obj_result = $this->history_test_model->get_disc1_result($temp1);
					//$obj_result = $obj_result['data'];
					if (!empty($obj_result['data']))
					{
						$disc_strength = array();
						$disc_weak = array();
						
						$disc_strength['d'] = $disc_strength['i'] = $disc_strength['s'] = $disc_strength['c'] = $disc_strength['n'] = $disc_weak['d'] = $disc_weak['i'] = $disc_weak['s'] = $disc_weak['c'] = $disc_weak['n'] = 0;

						foreach ($obj_result['data'] as $key => $rs)
						{
							if ($rs['the_order'] == 'd+') $disc_strength['d'] = $rs['show_count'];
							if ($rs['the_order'] == 'i+') $disc_strength['i'] = $rs['show_count'];
							if ($rs['the_order'] == 's+') $disc_strength['s'] = $rs['show_count'];
							if ($rs['the_order'] == 'c+') $disc_strength['c'] = $rs['show_count'];
							if ($rs['the_order'] == 'n+') $disc_strength['n'] = $rs['show_count'];
							
							if ($rs['the_order'] == 'd-') $disc_weak['d'] = $rs['show_count'];
							if ($rs['the_order'] == 'i-') $disc_weak['i'] = $rs['show_count'];
							if ($rs['the_order'] == 's-') $disc_weak['s'] = $rs['show_count'];
							if ($rs['the_order'] == 'c-') $disc_weak['c'] = $rs['show_count'];
							if ($rs['the_order'] == 'n-') $disc_weak['n'] = $rs['show_count'];
							
							$total_plus = $disc_strength['d'] + $disc_strength['i'] + $disc_strength['s'] + $disc_strength['c'] + $disc_strength['n'];
							$total_minus = $disc_weak['d'] + $disc_weak['i'] + $disc_weak['s'] + $disc_weak['c'] + $disc_weak['n'];
							
						}
						// $disc_str = $disc_strength['d'].$disc_strength['i'].$disc_strength['s'].$disc_strength['c'];
						// $disc_wk = $disc_weak['d'].$disc_weak['i'].$disc_weak['s'].$disc_weak['c'];
						
						$d['balance'] = $disc_strength['d'] - $disc_weak['d'];
						$i['balance'] = $disc_strength['i'] - $disc_weak['i'];
						$s['balance'] = $disc_strength['s'] - $disc_weak['s'];
						$c['balance'] = $disc_strength['c'] - $disc_weak['c'];
						$n['balance'] = $disc_strength['n'] + $disc_weak['n'];
						
						$participant = $this->participant_model->get(array('participant_id' => $participant_id));
					?>
					<div class="table-responsive">
						START NIH <br/>
						<table class="table table-bordered">
						<tr>
							<td></td>
							<td>+</td>
							<td>-</td>
							<td>Balance</td>
						</tr>
						<tr>
							<td>D</td>
							<td><?php echo $disc_strength['d']?></td>
							<td><?php echo $disc_weak['d']?></td>
							<td><?php echo $d['balance']?></td>
						</tr>
						<tr>
							<td>I</td>
							<td><?php echo $disc_strength['i']?></td>
							<td><?php echo $disc_weak['i']?></td>
							<td><?php echo $i['balance']?></td>
						</tr>
						<tr>
							<td>S</td>
							<td><?php echo $disc_strength['s']?></td>
							<td><?php echo $disc_weak['s']?></td>
							<td><?php echo $s['balance']?></td>
						</tr>
						<tr>
							<td>C</td>
							<td><?php echo $disc_strength['c']?></td>
							<td><?php echo $disc_weak['c']?></td>
							<td><?php echo $c['balance']?></td>
						</tr>
						<tr>
							<td>*</td>
							<td><?php echo $disc_strength['n']?></td>
							<td><?php echo $disc_weak['n']?></td>
							<td><?php echo $n['balance']?></td>
						</tr>
						<tr>
							<td>Total</td>
							<td><?php echo $total_plus?></td>
							<td><?php echo $total_minus?></td>
							<td></td>
						</tr>
						</table>
						</div>
						
						<?php 
						// START 1.1
						$graph1 = NULL;
						if ($disc_strength['d'] >= 0 && $disc_strength['d'] <= 2)
						{
							$graph1 .= 1;
						}
						else if ($disc_strength['d'] >= 3 && $disc_strength['d'] <= 6)
						{
							$graph1 .= 2;
						}
						else if ($disc_strength['d'] >= 7 && $disc_strength['d'] <= 12)
						{
							$graph1 .= 3;
						}
						else if ($disc_strength['d'] >= 13 && $disc_strength['d'] <= 20)
						{
							$graph1 .= 4;
						}
						
						// ------------------------------
						// START 1.2 
						
						if ($disc_strength['i'] >= 0 && $disc_strength['i'] <= 1)
						{
							$graph1 .= 1;
						}
						else if ($disc_strength['i'] >= 2 && $disc_strength['i'] <= 3)
						{
							$graph1 .= 2;
						}
						else if ($disc_strength['i'] >= 4 && $disc_strength['i'] <= 6)
						{
							$graph1 .= 3;
						}
						else if ($disc_strength['i'] >= 7 && $disc_strength['i'] <= 17)
						{
							$graph1 .= 4;
						}
						
						// ------------------------------
						// START 1.3
						if ($disc_strength['s'] >= 0 && $disc_strength['s'] <= 1)
						{
							$graph1 .= 1;
						}
						else if ($disc_strength['s'] >= 2 && $disc_strength['s'] <= 3)
						{
							$graph1 .= 2;
						}
						else if ($disc_strength['s'] >= 5 && $disc_strength['s'] <= 9)
						{
							$graph1 .= 3;
						}
						else if ($disc_strength['s'] >= 10 && $disc_strength['s'] <= 19)
						{
							$graph1 .= 4;
						}
						
						//----------------------------
						// START 1.4
						if ($disc_strength['c'] >= 0 && $disc_strength['c'] <= 1)
						{
							$graph1 .= 1;
						}
						else if ($disc_strength['c'] >= 2 && $disc_strength['c'] <= 3)
						{
							$graph1 .= 2;
						}
						else if ($disc_strength['c'] >= 4 && $disc_strength['c'] <= 6)
						{
							$graph1 .= 3;
						}
						else if ($disc_strength['c'] >= 7 && $disc_strength['c'] <= 14)
						{
							$graph1 .= 4;
						}
						?>
						
						<div>
						GRAPH 1 - Kepibadian di lingkungan normal<br/>
						<?php echo $graph1; ?>
						
						<table class="table table-bordered">
						<tr>
							<td></td>
							<td>+</td>
							<td>-</td>
							<td>Balance</td>
						</tr>
						</table><br/><br/>
						<?php
						
						//----------------------------------------------------------------------------------------------------
						?>
						
						<br/><br/>
						GRAPH 2 - Kepribadian saat dalam tekanan / under pressure
						<?php 
						$graph2 = NULL;
						
						// START 2.1
						if ($disc_weak['d'] >= 12 && $disc_weak['d'] <= 21)
						{
							$graph2 .= 1;
						}
						else if ($disc_weak['d'] >= 6 && $disc_weak['d'] <= 11)
						{
							$graph2 .= 2;
						}
						else if ($disc_weak['d'] >= 3 && $disc_weak['d'] <= 5)
						{
							$graph2 .= 3;
						}
						else if ($disc_weak['d'] >= 0 && $disc_weak['d'] <= 2)
						{
							$graph2 .= 4;
						}
						
						// ------------------------------
						// START 2.2 
						
						if ($disc_weak['i'] >= 8 && $disc_weak['i'] <= 19)
						{
							$graph2 .= 1;
						}
						else if ($disc_weak['i'] >= 5 && $disc_weak['i'] <= 7)
						{
							$graph2 .= 2;
						}
						else if ($disc_weak['i'] >= 2 && $disc_weak['i'] <= 4)
						{
							$graph2 .= 3;
						}
						else if ($disc_weak['i'] >= 0 && $disc_weak['i'] <= 1)
						{
							$graph2 .= 4;
						}
						
						// ------------------------------
						// START 2.3
						if ($disc_weak['s'] >= 10 && $disc_weak['s'] <= 19)
						{
							$graph2 .= 1;
						}
						else if ($disc_weak['s'] >= 7 && $disc_weak['s'] <= 9)
						{
							$graph2 .= 2;
						}
						else if ($disc_weak['s'] >= 3 && $disc_weak['s'] <= 6)
						{
							$graph2 .= 3;
						}
						else if ($disc_weak['s'] >= 0 && $disc_weak['s'] <= 2)
						{
							$graph2 .= 4;
						}
						
						//----------------------------
						// START 2.4
						if ($disc_weak['c'] >= 11 && $disc_weak['c'] <= 15)
						{
							$graph2 .= 1;
						}
						else if ($disc_weak['c'] >= 7 && $disc_weak['c'] <= 10)
						{
							$graph2 .= 2;
						}
						else if ($disc_weak['c'] >= 3 && $disc_weak['c'] <= 6)
						{
							$graph2 .= 3;
						}
						else if ($disc_weak['c'] >= 0 && $disc_weak['c'] <= 2)
						{
							$graph2 .= 4;
						}
						
						echo '<br>'.$graph2.'';
						?>
						<br/><br/>
						
						<table class="table table-bordered">
						<tr>
							<td></td>
							<td>+</td>
							<td>-</td>
							<td>Balance</td>
						</tr>
						</table><br/><br/>
						
						GRAPH 3 - Kepribadian Asli<br/>
						
						<?php 
						$graph3 = NULL;
						
						// START 3.1
						if ($d['balance'] >= -21 && $d['balance'] <= -10)
						{
							$graph3 .= 1;
						}
						else if ($d['balance'] >= -9 && $d['balance'] <= 0)
						{
							$graph3 .= 2;
						}
						else if ($d['balance'] >= 1 && $d['balance'] <= 9)
						{
							$graph3 .= 3;
						}
						else if ($d['balance'] >= 10 && $d['balance'] <= 20)
						{
							$graph3 .= 4;
						}
						
						//---------------------------------------------------------------------
						// START 3.2
						
						// START 3.1
						if ($i['balance'] >= -19 && $i['balance'] <= -6)
						{
							$graph3 .= 1;
						}
						else if ($i['balance'] >= -5 && $i['balance'] <= -1)
						{
							$graph3 .= 2;
						}
						else if ($i['balance'] >= 0 && $i['balance'] <= 3)
						{
							$graph3 .= 3;
						}
						else if ($i['balance'] >= 4 && $i['balance'] <= 17)
						{
							$graph3 .= 4;
						}
						
						// --------------------------------------------------------------
						// START 3.3
						if ($s['balance'] >= -19 && $s['balance'] <= -8)
						{
							$graph3 .= 1;
						}
						else if ($s['balance'] >= -7 && $s['balance'] <= -1)
						{
							$graph3 .= 2;
						}
						else if ($s['balance'] >= 0 && $s['balance'] <= 5)
						{
							$graph3 .= 3;
						}
						else if ($s['balance'] >= 7 && $s['balance'] <= 19)
						{
							$graph3 .= 4;
						}
						
						//--------------------------------------------------------------------
						// START 3.4
						if ($c['balance'] >= -15 && $c['balance'] <= -8)
						{
							$graph3 .= 1;
						}
						else if ($c['balance'] >= -7 && $c['balance'] <= -3)
						{
							$graph3 .= 2;
						}
						else if ($c['balance'] >= -2 && $c['balance'] <= 1)
						{
							$graph3 .= 3;
						}
						else if ($c['balance'] >= 2 && $c['balance'] <= 14)
						{
							$graph3 .= 4;
						}
						
						echo $graph3;
						?>
						<table class="table table-bordered">
						<tr>
							<td></td>
							<td>+</td>
							<td>-</td>
							<td>Balance</td>
						</tr>
						</table><br/><br/>
						
						Description<br/>
						
						<b>Influence : </b><br/><br/>
						Dalam lingkungan anda, anda sangat berfokus dengan mempengaruhi atau mengajak orang lain. <br/><br/>
						
						Anda terkenal dengan berhubungan dengan semua jenis orang dan senang membuat impresi. Anda seringkali mencari peluang untuk memunculkan antusiasme dan meraih popularitas. Anda menyelesaikan pekerjaan dengan bantuan orang lain, dan pengakuan secara sosial sangat penting bagi anda. Cara mengambil keputusan anda biasanya cenderung berdasarkan fakta dan kejadian bukan dari kenekatan.<br/><br/>
						
						Anda memerlukan kebebasan dalam berekspresi dan kendali yang strategis. DI luar tempat kerja, anda sangat menikmati aktivitas grup dimana anda bisa bertemu orang baru dan meningkatkan pertemanan. Anda menghargai kemampuan anda dalam menyampaikan ide mereka dan mereka senang melibatkan anda saat berdiskusi.<br/><br/>
						
						Mereka yang dominan di area ini, sebaiknya memiliki prioritas dan tenggat waktu untuk berbagi. Mereka juga harus fokus dalam membuat keputusan yang objektif. Ketika melakukan delegasi, orang dengan Influence yang tinggi, terkadang peduli terhadap menghargai kebebasan pihak lain dan bernegosiasi. Mereka  mengambil resiko 
						<br/><br/>
						</div>
					<?php 
					}
				}
			}
		}
		else
		{
			echo DATA_NOT_FOUND;
		}
	}
	else
	{
	?>
	
	<form class='form-horizontal' role='form' method='post'>
		<?php 
		$obj_list = $this->participant_model->get_list(array(
			'company_id' => company_cookies('company_id'),
		));
		$obj_list = $obj_list['data'];
		if (!empty($obj_list)) 
		{
			?>
			<div class="table-responsive">
			<table class="table table-hover">
			<tr>
				<td>#</td>
				<td>Participant</td>
				<td>Email</td>
				<td>Take test</td>
				<td>Option</td>
			</tr>
			<?php
			
			foreach($obj_list as $key => $obj)
			{
				
			?>
			
			<tr>
				<td><?php echo $key+1 ?></td>
				<td><?php echo $obj['name'] ?></td>
				<td><?php echo $obj['email'] ?></td>
				<td>-<?php //echo $obj['email'] ?></td>
				<td><a href="<?php echo current_url().'?participant_id='.$obj['participant_id']?>" class="btn btn-primary"><?php echo REPORT.' '.DETAIL ?></a> </td>
				
			</tr>
			<?php 
			}
			?>
			</table>
			</div>
			<?php
		}
		else
		{
			?>
			No Data
			<?php
		}
		?>
	</form>
	<?php 
	}
	?>
</div>