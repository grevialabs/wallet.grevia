<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER;

$PAGE_HEADER = NULL;
$PAGE = $MODULE = PARTICIPANT;
$bread['company'] = 'Company';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

global $page, $param, $message;

$participant_id = $keyword = NULL;

if (is_numeric($this->input->get('participant_id'))) $participant_id = $this->input->get('participant_id');
if ($this->input->get('keyword')) $keyword = filter($this->input->get('keyword'));

if (!is_numeric($participant_id)) $participant_id = NULL;
		
// UPDATE PROFILE participant 
if ($_POST && isset($_POST['btnUpdateProfile'])) {
	$tmp['participant_id'] = $this->input->get('participant_id');
	$obj = $this->participant_model->get($tmp);
	if (!empty($obj) && $obj['company_id'] == company_cookies('company_id') && !empty($_POST['test_id'])) {
			
		$param = NULL;
		$param['name'] = filter(post('name'));
		// NOT ALLOWED TO CHANGE EMAIL
		//$param['email'] = filter(post('email'));
		$param['gender'] = filter(post('gender'));
		$param['notes'] = filter(post('notes'));
		$param['expired_test_date'] = date('Y-m-d H:i:s',strtotime(filter(post('expired_test_date'))));
		
		$arrtest = array();
		if (post('test_id')) $arrtest = post('test_id');
			
		$update = $this->participant_model->update($participant_id, $param);
		
		if (!empty($arrtest)) {
			foreach ($arrtest as $key => $val) {
			$tmp = array();
			$tmp['test_id'] = $key;
			$tmp['participant_id'] = $participant_id;
			$tmp['is_active'] = 0;
			if (isset($val) && $val==1) $tmp['is_active'] = 1;
			$update_test = $this->test_model->save_type_test($tmp);
			}
		}
		
		($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
		$message['message'] = getMessage($message['message']);
			
	} else {
		$message['message'] = getMessage(MESSAGE::NOT_FOUND);
	}
}

// EMAIL BLAST
//if (isset($_POST['btnEmail']))
if (post('btnEmail'))
{
	$arr_email_blast = array();
	$arr_email_blast = post('btnEmail');
	$arr_name = post('names');
	$arr_email = post('emails');
	$arr_token = post('tokens');
	$arr_expired_test_dates = post('expired_test_dates');
	foreach ($arr_email_blast as $key => $val)
	{
		// IF EMAIL PARTICIPANT NOT EXIST, DON'T BLAST
		//if (!isset($val['email'])) return false;
		
		// EMAIL BLAST HERE
		$headers = "From: Jobtalento Team<noreply@jobtalento.com> \r\n";
		$headers .= "Reply-To: \r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$subject = "Psychotest Invitation from ".company_cookies('name');
		$message_email = "
		<html>
		<head></head>
		<body>
		Dear ".ucwords($arr_name[$key]).",<br/><br/>Congratulations, you are selected as a candidate from <b>".company_cookies('name')."</b> and invited to participate in online recruitment program from us. Please click link below ".base_url()."participant_test?token=".$arr_token[$key]." or <a href='".base_url()."participant_test?token=".$arr_token[$key]."'>click here</a> to start your online assesment test.<br/><br/>
		
		Please note that these test will expired in ".date('D, d M Y H:i',strtotime($arr_expired_test_dates[$key]))."
		<br/><br/>
		Best Regards<br/>
		<b>Jobtalento team</b> on behalf ".company_cookies('name').".<br/>
		www.jobtalento.com
		</body>
		</html>
		
		";
		$to = $arr_email[$key];
		
		$send = FALSE;
		if (!is_internal())
		{	
			$send = mail($to,$subject,$message_email,$headers);
		}
		
		
		//$this->load->library('email');
		
		// $config['smtp_host'] = 'mail.grevia.com';
		// $config['smtp_user'] = 'rusdi.grevia';
		// $config['smtp_pass'] = 'rusdi!@#$2012';
		// $config['validate'] = true;
		// $config['smtp_port'] = 25;
		// $config['smtp_crypto'] = 'tls';
		// $config['protocol'] = 'smtp';
		// $config['mailtype'] = 'html';
		// $this->email->initialize($config);
		// $this->email->from('rusdi@grevia.com','Jobtalento Team');
		// if (!empty($to)) $this->email->to($to);
		// $this->email->subject($subject);
		// $this->email->message($message);
		// $send = $this->email->send();
		
		// $config['smtp_host'] = 'smtp.gmail.com';
		// $config['smtp_user'] = 'grevianet@gmail.com';
		// $config['smtp_pass'] = 'debian!@#$2012';
		// $config['validate'] = true;
		// $config['smtp_port'] = 465;
		// $config['smtp_crypto'] = 'ssl';
		// $config['protocol'] = 'smtp';
		// $config['mailtype'] = 'html';

		// $this->email->initialize($config);
		// $this->email->from('grevianet@gmail.com','Jobtalento Team');
		// if (!empty($to)) $this->email->to($to);
		// $this->email->subject($subject);
		// $this->email->message($message);
		// $send = $this->email->send();
		
		//$config['smtp_host'] = '$this->load->library('email');
							
		// $config['smtp_host'] = 'garuda.lakupon.com';
		// $config['smtp_user'] = 'noreply@lakupon.com';
		// $config['smtp_pass'] = 'bLast!!!';
		// $config['validate'] = true;
		// $config['smtp_port'] = 465;
		// $config['smtp_crypto'] = 'ssl';
		// $config['protocol'] = 'smtp';
		// $config['mailtype'] = 'html';
		// $to = 'rusdi@lakupon.com';
		// $subject = 'coba nih bisa';
		// $message = 'Ini tester ya bro';
		// $this->email->initialize($config);
		// $this->email->from('noreply@lakupon.com','Lakupon Team');
		// if (!empty($to)) $this->email->to($to);
		// if (!empty($cc)) $this->email->cc($cc);
		// if (!empty($bcc)) $this->email->bcc($bcc);
		// $this->email->subject($subject);
		// $this->email->message($message);
		// $send = $this->email->send();
		
		$message = array();
		if ($send)
		{
			$message['message'] = 'Email Blast Success';
		}
		else
		{
			$message['message'] = 'Email Blast Error';
			if (is_internal()) $message['message'] = $message_email;
		}
	}
}

$param['company_id'] = company_cookies('company_id');

$obj_list = $this->order_model->get_list($param);
$obj_list = $obj_list['data'];
?>

<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1 class=""><?php echo $MODULE?></h1><br/>
	<?php

	//	SHOW QUOTA
	$quota_avail = 0;
	$obj_quota = $this->company_model->get_quota( array('company_id' => company_cookies('company_id')) );
	if (!empty($obj_quota)) $quota_avail = $obj_quota['after_quota'];
	
	?>	
	<?php echo YOU_HAVE_QUOTA ?> <b><?php echo $quota_avail?></b> <?php echo PARTICIPANT?><br/><br/>
	<?php if ($quota_avail > 0) { ?>
	<a href="<?php echo base_url()."company/test_candidate?do=insert";?>" class="btn btn-success"><?php echo ADD_PARTICIPANT?></a>
	<?php } else { ?>
	<a href="<?php echo base_url().'company/order'; ?>" class="btn btn-danger"><i class="fa fa-exclamation"></i> <?php echo PLEASE_BUY_QUOTA_FIRST?></a>
	<?php } ?>
	<br/><br/>
	
	<?php if(is_filled($message['message']))echo message($message['message']).BR?>
	
	<?php 
	$participant_id = NULL;
	
	$participant_id = $do = NULL;
	if ($this->input->get('participant_id')) $participant_id = $this->input->get('participant_id'); 
	if ($this->input->get('do')) $do = $this->input->get('do'); 
		
	if (is_numeric($participant_id))
	{
			$obj_participant = $this->participant_model->get( array(
			'participant_id' => $participant_id,
			'company_id' => company_cookies('company_id')
		) );
			
		if (!empty($obj_participant))
		{
			$expired_test_date = NULL;
			if (isset($obj_participant['expired_test_date'])) 
				$expired_test_date = date('d-m-Y H:i',strtotime($obj_participant['expired_test_date']));
		?>
		<form method="post">
		<div class="table-responsive">
		<table class="table table-bordered">
			<tr>
				<td colspan="2" class="alert bg-primary">Ubah Profile</td>
			</tr>
			<tr>
				<td>ParticipantID</td>
				<td><?php echo $participant_id?></td>
			</tr>
			<tr>
				<td><?php echo NAME ?></td>
				<td><input type="text" class="input wdtFul" name="f_name" value="<?php echo $obj_participant['name'] ?>" /></td>
			</tr>
			<tr>
				<td><?php echo PASSWORD ?></td>
				<td><input type="text" class="input wdtFul" name="f_password" value="<?php //echo $obj_participant['name'] ?>" disabled /></td>
			</tr>
			<tr>
				<td><?php echo TOKEN ?></td>
				<td><input type="text" class="input wdtFul" name="f_token" value="<?php echo $obj_participant['token'] ?>" disabled /></td>
			</tr>
			<tr>
				<td><?php echo GENDER?></td>
				<td>
				<select name="f_gender" class="input wdtFul">
					<option value="1" <?php if($obj_participant['gender'] == 1) echo "selected" ?> ><?php echo MALE ?></option>
					<option value="2" <?php if($obj_participant['gender'] == 2) echo "selected" ?> ><?php echo FEMALE ?></option>
				</select>
				</td>
			</tr>
			<tr>
				<td width="300px"><?php echo CREATE_ACCOUNT_DATE ?></td>
				<td><?php echo date(DATE_FORMAT, strtotime($obj_participant['creator_date']))?></td>
			</tr>
			<tr>
				<td><?php echo EMAIL?></td>
				<td><input type="text" class="input wdtFul" name="f_email" value="<?php echo $obj_participant['email']?>"disabled /></td>
			</tr>
			<tr>
				<td><?php echo NOTES ?></td>
				<td><textarea name="f_notes" rows="5" class="wdtFul"><?php echo $obj_participant['notes']?></textarea></td>
			</tr>
			<tr>
				<td colspan="2" class="alert bg-primary" id="type_test">Tipe tes</td>
			</tr>
			<tr>
				<td><?php echo EXPIRED_TEST_DATE ?></td>
				<td><input type="text" class="input wdtFul date" name="f_expired_test_date" value="<?php echo $expired_test_date?>"/></td>
			</tr>
			<tr>
				<td><?php echo TEST_TYPE ?></td>
				<td class="valTop">
				<?php 
				// SINI 
				$param['is_publish'] = 1;
				$param['is_company'] = 1;
				$param['participant_id'] = $participant_id;
				$obj_list_type_test_participant = $this->test_model->get_list_type_test_participant($param);
				$obj_list_type_test_participant = $obj_list_type_test_participant['data'];
				
				// SAVE ACTIVE TEST PARTICIPANT
				$arr_active_test = array();
				if (!empty($obj_list_type_test_participant)) {
					foreach ($obj_list_type_test_participant as $rs_test)
					{
						if ($rs_test['is_active']) $arr_active_test[] = $rs_test['test_id'];
					}
				}
				
				$obj_list_type_test = $this->test_model->get_list_type_test($param);
				$obj_list_type_test = $obj_list_type_test['data'];
				
				if (!empty($obj_list_type_test)) 
				{
					echo AVAILABLE_TEST.':'.BR;
					?>
					
					<?php
					foreach($obj_list_type_test as $key => $test)
					{
						?>
						<input name="test_id[<?php echo $test['test_id']?>]" type="hidden" value="0" />
						<input type="checkbox" name="test_id[<?php echo $test['test_id']?>]" id="f_test_<?php echo $key?>" value="1" 
						<?php if ($do =="editprofile" && !empty($arr_active_test) && in_array($test['test_id'],$arr_active_test)) echo ' checked '; ?> /> 
						<label for="f_test_<?php echo $key?>"><?php echo $test['title']?></label><br/>
						<?php
						
					}
					//echo HR;
				}
				?>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="talCnt"><button class="btn btn-success" name="btnUpdateProfile" value="1"><?php echo UPDATE?></button></td>
			</tr>
		</table>
		</div>
		</form>
		
		
		<?php
		
		}
		else 
		{
			?>
			No Data
			<?php
		}
	}
	else
	{
		$obj_list_participant = $this->participant_model->get_list(array(
			'company_id' => company_cookies('company_id'),
			'keyword' => $keyword,
			'is_delete' => 0,
		));
		$obj_list_participant = $obj_list_participant['data'];
		if (!empty($obj_list_participant)) 
		{
			?>
			<!-- START SEARCH -->
			<table class="table table-bordered bg-warning">
			<tr>
				<td colspan="6" class="talLft">
				<form method="get">
				<?php echo FILTER ?> : <input name="keyword" class="input" type="text" placeholder="<?php echo KEYWORD?>" value="<?php echo $keyword ?>"/>
				<button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-search fa-fw"></i> <?php echo SEARCH; ?></button></form>
				</td>
			</tr>
			</table>
			<!-- END SEARCH -->
			
			Showing <?php echo count($obj_list_participant) ?> record<br/>
			<form class='form-horizontal' role='form' method='post'>
			<div class="table-responsive">
			<table class="table table-hover">
			<tr>
				<td><input type="checkbox" /></td>
				<td>#</td>
				<td>Participant</td>
				<td>Email</td>
				<td>Test</td>
				<td width="50%">Option</td>
			</tr>
			<?php
			
			foreach($obj_list_participant as $key => $obj)
			{
			?>
			
			<tr>
				<td><input type="checkbox" /></td>
				<td><?php echo $key+1 ?></td>
				<td><?php echo $obj['name'] ?></td>
				<td><?php echo $obj['email'] ?></td>
				<td>-<?php //echo $obj['email'] ?></td>
				<td>
				<form method="post">
				<a href="<?php echo base_url().'company/report?participant_id='.$obj['participant_id']?>" class="btn btn-primary"><?php echo REPORT.' '.DETAIL ?></a> 
				<a href="<?php echo base_url().'company/participant?do=editprofile&participant_id='.$obj['participant_id']?>" class="btn btn-warning"><?php echo EDIT.' '.PROFILE ?></a> 
				<input type="hidden" name="names[<?php echo $obj['participant_id']?>]" value="<?php echo $obj['name']; ?>" />
				<input type="hidden" name="emails[<?php echo $obj['participant_id']?>]" value="<?php echo $obj['email']; ?>" />
				<input type="hidden" name="tokens[<?php echo $obj['participant_id']?>]" value="<?php echo $obj['token']; ?>" />
				<input type="hidden" name="expired_test_dates[<?php echo $obj['participant_id']?>]" value="<?php echo $obj['expired_test_date']; ?>" />
				<button name="btnEmail[<?php echo $obj['participant_id']?>]" value="1" class="btn btn-success"><?php echo EMAIL_BLAST_TEST ?></button>
				</form>
				</td>
				
			</tr>
			<?php 
			}
			?>
			</table>
			</div>
			</form>
			<?php
		}
		else
		{
			?>
			No Data
			<?php
		}
	}
	?>
</div>