<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER;

$PAGE_HEADER = NULL;
$PAGE = $MODULE = BILLING;
$bread['company'] = 'Company';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

global $page, $param, $message;

$order_id = NULL;
if ($this->input->get('order_id')) $order_id = $this->input->get('order_id');

//update
if ($_POST && isset($order_id)) {
	$obj = $this->order_model->get(array('order_id' => $order_id));
	if (!empty($obj)) {
		$tmp = array();
		$tmp['confirm_payment_notes'] = filter(post('confirm_payment_notes'));
		$tmp['confirm_payment_amount'] = filter(post('confirm_payment_amount'));
		$tmp['confirm_payment_type'] = filter(post('confirm_payment_type'));
		$tmp['confirm_payment_date'] = filter(post('confirm_payment_date'));
				
		if (!empty($tmp)) {
			
			$tmp['payment_status'] = '2';
			$tmp['confirm_payment_date'] = date('Y-m-d H:i:s',strtotime($tmp['confirm_payment_date']));

			$update = $this->order_model->update($order_id, $tmp);
			($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
			$message['message'] = getMessage($message['message']);

		} else {
			$message['message'] = 'Seluruh Field harus diisi.';
		}
	} else {
		$message['message'] = getMessage(MESSAGE::NOT_FOUND);
	}
}

$param = array();
$param['company_id'] = company_cookies('company_id');

$obj_list = $this->order_model->get_list($param);
$obj_list = $obj_list['data'];
?>

<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1 class=""><?php echo $MODULE?></h1><br/>
		
	<?php if(is_filled($message['message']))echo message($message['message']).BR?>
	
	<?php 
	$order_id = $do = NULL;
	if ($this->input->get('order_id')) $order_id = $this->input->get('order_id'); 
	if ($this->input->get('do')) $do = $this->input->get('do'); 
		
	if (is_numeric($order_id))
	{
		$obj_order = $this->order_model->get( array(
			'order_id' => $order_id,
			'company_id' => company_cookies('company_id')
		) );
			
		if (!empty($obj_order))
		{
		?>
		<div class="padMed bg-success">Anda sudah konfirmasi</div>
		<form method="post">
		<div class="table-responsive">
		<table class="table table-bordered">
		<tr>
			<td>Payment Type</td>
			<td>BCA 527.0590.278 Rusdi Karsandi<input type="hidden" name="f_confirm_payment_type" value="1"/></td>
		</tr>
		<tr>
			<td>Kode Order</td>
			<td><?php if (isset($order_id)) echo $order_id ?></td>
		</tr>
		<tr>
			<td>Tgl bayar</td>
			<td><input type="text" class="input datepicker hasDatepipcker wdtFul" name="f_confirm_payment_date" value="" placeholder="Tgl bayar"/></td>
		</tr>
		<tr>
			<td>Total yang harus dibayar</td>
			<td class="b"><?php echo format_money($obj_order['grandtotal'])?></td>
		</tr>
		<tr>
			<td>Total Terbayar</td>
			<td><input type="text" class="input wdtFul" name="f_confirm_payment_amount" value="" placeholder="" /></td>
		</tr>
		<tr>
			<td>Notes</td>
			<td><textarea placeholder="<?php //echo NOTES?>" name="f_confirm_payment_notes" class="wdtFul" rows="5"><?php if(isset($aa))$aa?></textarea></td>
		</tr>
		<tr>
			<td></td>
			<!--<td><button type="submit" value="<?php echo SUBMIT?>" class="btn btn-success" /></td>-->
			<td><button type="submit" class="btn btn-success"><?php echo SUBMIT?></button></td>
		</tr>
		</table>
		</div>
		</form>
		
		<!--
		<table class="table table-hover table-bordered">
			<tr>
				<td>OrderID</td>
				<td><?php echo $obj_order['order_id']?></td>
			</tr>
			<tr>
				<td>Invoice Code</td>
				<td><?php if (isset($obj_order['invoice_code'])) echo $obj_order['invoice_code'] ;else echo NOT_READY_YET?></td>
			</tr>
			<tr>
				<td width="300px">Date</td>
				<td><?php echo date(DATE_FORMAT, strtotime($obj_order['creator_date']))?></td>
			</tr>
			<tr>
				<td><?php echo PAYMENT_STATUS?></td>
				<td><?php echo order_payment_status($obj_order['payment_status'])?></td>
			</tr>
			<tr>
				<td>Detail</td>
				<td><?php echo $obj_order['detail']?></td>
			</tr>
			<tr>
				<td>Notes</td>
				<td><?php echo $obj_order['notes']?></td>
			</tr>
		</table>
		-->
		
		<!-- DETAIL INVOICE START -->
		<!--
		<table class="table table-hover">
		<tr>
			<td>#</td>
			<td><?php echo PRODUCT_NAME ?></td>
			<td><?php echo QUANTITY ?></td>
			<td><?php echo PRICE ?></td>
			<td><?php echo SUBTOTAL ?></td>
		</tr>
		<?php
		$param = array(
			'order_id' => $order_id,
			'creator_id' => company_cookies('company_id'),
		);
		$obj_list_order_detail = $this->order_model->get_list_detail($param);
		$obj_list_order_detail = $obj_list_order_detail['data'];
		
		$subtotal = $subprice = $discount = $tax = 0;
		foreach ($obj_list_order_detail as $key => $obj)
		{
			$price = $obj['price'];
			$subprice = $obj['quantity'] * $obj['price'];
			
			?>
			<tr>
				<td><?php echo $key+1 ?></td>
				<td><?php echo $obj['product_name'].BR.$obj['description']?></td>
				<td><?php echo $obj['quantity']?></td>
				<td><?php echo format_money($obj['price'])?></td>
				<td class="talRgt"><?php echo format_money($subprice) ?></td>
			</tr>
			<?php
			$subtotal+= $subprice;
		}
		?>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td><?php echo SUBTOTAL?></td>
				<td class="talRgt"><?php echo format_money($subtotal) ?></td>
			</tr>
			<?php 
			if (isset($discount) && $discount > 0) 
			{
				?>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td><?php echo DISCOUNT?></td>
				<td class="talRgt"><?php echo $discount?></td>
			</tr>
			<?php 
			}
			?>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td><?php echo TAX?></td>
				<td class="talRgt"><?php echo $tax = format_money($subtotal*0.1) ?></td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td>Grandtotal</td>
				<td class="talRgt"><?php echo format_money($obj_order['grandtotal'])?></td>
			</tr>
		</table>
		-->
		<!-- DETAIL INVOICE END -->
		<?php
		}
		else 
		{
			?>
			No Data
			<?php
		}
	}
	?>
</div>