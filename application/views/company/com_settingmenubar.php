	<?php
	global $current_segment;
	if ($this->uri->segment(2)) $current_segment = $this->uri->segment(2);
	
	function setting_active($page) 
	{
		global $current_segment;
		$str = '';
		if ($current_segment === $page)
		{
			$str = 'active';
		}
		return $str;
	}
	
	?>
	<div class="">
		<a class="btn bg-primary padMed <?php echo setting_active('profile')?>" href="<?php echo base_url().'company/setting'?>"><i class="fa fa-cog"></i> <?php echo MENU_CHANGE_PASSWORD?></a>
		<a class="btn bg-primary padMed <?php echo setting_active('profile')?>" href="<?php echo base_url().'company/profile'?>"><i class="fa fa-user"></i> <?php echo MENU_MY_PROFILE?></a>
	</div><br/>