<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER;

$PAGE_HEADER = NULL;
$PAGE = $MODULE = NOTIFICATION;
$bread['company'] = 'Company';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

$notification_id = $do = $read = NULL;

if ($this->input->get('do')) $do = $this->input->get('do');
if ($this->input->get('read')) $read = $this->input->get('read');
if ($this->input->get('notification_id')) $notification_id = $this->input->get('notification_id');

if ($this->input->get('delete_success'))$message['message'] = getMessage(MESSAGE::DELETE);
if ($this->input->get('success'))$message['message'] = getMessage(MESSAGE::UPDATE);
//if ($this->input->get('notification_id')) $notification_id = $this->input->get('notification_id');

// UPDATE STATUS
if ((isset($_POST['btn_delete']) || isset($_POST['btn_unread'])) && $this->input->post('notification_id')) 
{
	$get_notification_id = $this->input->post('notification_id');
	$obj_notification = $this->notification_model->get(array('notification_id' => $get_notification_id));
	$company_id = company_cookies('company_id');
	
	if (isset($obj_notification['notification_id']) && $obj_notification['company_id'] == $company_id) 
	{
		$param = array();
		
		if ($_POST['btn_delete']) $param['is_delete'] = 1;
		if ($_POST['btn_unread']) $param['is_read'] = 0;
		
		$update = $this->notification_model->update($get_notification_id, $param);
		($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
		$message['message'] = getMessage($message['message']);
		
		if ($_POST['btn_delete']) redirect(base_url().'company/notification?delete_success=1');
		if ($_POST['btn_unread']) redirect(base_url().'company/notification?success=1');
	}	
}

// // DELETE
// if ((isset($_POST['btn_delete']) || isset($_POST['btn_unread'])) && $this->input->post('notification_id')) 
// {
	// $get_notification_id = $this->input->post('notification_id');
	// $obj_notification = $this->notification_model->get(array('notification_id' => $get_notification_id));
	// $company_id = company_cookies('company_id');
	
	// if (isset($obj_notification['notification_id']) && $obj_notification['company_id'] == $company_id) 
	// {
		// $param = array();
		
		// if ($_POST['btn_delete']) $param['is_delete'] = 1;
		
		// $update = $this->notification_model->update($get_notification_id, $param);
		// ($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
		// $message['message'] = getMessage($message['message']);
		
		// redirect(base_url().'company/notification?delete_success=1');
	// }	
// }


if (isset($read)) $param['is_read'] = $read;

// GET LIST NOTIFICATION
$obj_list_notification = $param = array();
if (isset($read)) $param['is_read'] = $read;
$param['is_delete'] = 0;
$param['company_id'] = company_cookies('company_id');
$obj_list_notification = $this->notification_model->get_list($param);
$obj_list_notification = $obj_list_notification['data'];

?>
<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1><?php echo $MODULE?></h1><br/>
	
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	?>
	<?php 
	if(!isset($notification_id)) 
	{
		$bg_active = 'bg-success';
		$bg_inactive = 'bg-warning';
		?>
		<a href="<?php echo current_url() ?>" class="btn <?php if (!isset($read)) echo $bg_active; else echo $bg_inactive?>">All</a>  &nbsp;
		<a href="<?php echo current_url() ?>?read=true" class="btn <?php if ($read === 'true') echo $bg_active; else echo $bg_inactive?>">Read</a>  &nbsp;
		<a href="<?php echo current_url() ?>?read=false" class="btn <?php if ($read === 'false') echo $bg_active; else echo $bg_inactive?>">Unread</a> <br/> <br/>

		<div class="table-responsive">
		<table class="table table-bordered btn-sm">
		<?php
		if (!empty($obj_list_notification))
		{
			foreach ($obj_list_notification as $key => $notif)
			{
				?>
				<tr class="<?php if (!$notif['is_read']) echo 'bg-warning'; ?>">
					<td width="5px"><input type="checkbox" name="" value="" /></td>
					<td width="180px"><?php echo ' '.date('D, d M Y H:i',strtotime($notif['creator_date'])) ?></td>
					<td><a href="<?php echo current_url().'?notification_id='.$notif['notification_id']; ?>"><?php echo $notif['subject'] ?></a> <?php if (!$notif['is_read']) echo '<sup class="bgRed clrWht b">&nbsp;new&nbsp;&nbsp;</sup>'; ?></td>
					<td><?php echo nl2br($notif['message']) ?></td>
					<td class="talCnt" width="150px">
					<form method="post">
						<a class="btn btn-success" href="<?php echo current_url().'?notification_id='.$notif['notification_id']; ?>"><?php echo DETAIL ?></a> 
						<input type="hidden" name="notification_id" value="<?php echo $notif['notification_id']?>"/>
						<button class="btn btn-danger" name="btn_delete" value="1"><i class="fa fa-remove"></i></button>
					</form>
					</td>
				</tr>
				<?php
			}
		}
		else
		{
			echo DATA_NOT_FOUND;
		}
		?>
			
		</table>
		</div>
		<?php
	}
	elseif (isset($notification_id))
	{
		$tmp1['notification_id'] = $notification_id;
		$tmp1['company_id'] = company_cookies('company_id');
		$obj_notification = $this->notification_model->get($tmp1);
		if (isset($obj_notification['company_id']))
		{
		?>
			<div class="">
				<form method="post">
					<a class="btn btn-default" href="<?php echo base_url().'company/notification'?>"><?php echo BACK?></a>
					<button type="submit" name="btn_unread" value="1" class="btn btn-default btn-bg"><i class="fa fa-file-o"></i> <?php echo MARK_UNREAD ?></button>
					
					<input type="hidden" name="notification_id" value="<?php echo $notification_id?>"/>
					<button type="submit" name="btn_delete" value="1" class="btn btn-danger btn-bg"><i class="fa fa-remove"></i> <?php echo DELETE ?></button>
					
				</form>
			</div>
			<br/>
			<?php echo date('D d M Y H:i', strtotime($obj_notification['creator_date'])); ?><hr>
			Subject : <?php echo $obj_notification['subject'] ?><br/><br/>
			<br/>
			<?php echo $obj_notification['message'] ?>
		
		<?php
		}
	}
	else
	{
		echo YOU_DONT_HAVE_NOTIFICATION_UNREAD;
	}
	?>
</div>