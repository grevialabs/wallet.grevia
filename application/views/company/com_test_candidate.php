<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER;

$PAGE_HEADER = NULL;
$PAGE = $MODULE = TEST;
$bread['company'] = 'Company';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

global $page, $param, $message;

// SAVE HERE FOR INSERT USER AND TEST AVAILABLE
if ($_POST)
{
	$arr_test = array();
	$arr_test = post('test_id');
	$email = post('email');
	$job_position = post('job_position');
	$name = post('name');
	//$expired_test_date = post('expired_test_date');
	$expired_test_date = NULL;
	$gender = post('gender');
	
	if (isset($email))
	{
		// CHECK IF EMAIL EXIST
		$obj_participant = $this->participant_model->get( array('email' => $email) );
		
		if (empty($obj_participant))
		{
			//get latest quota
			$tempquota['company_quota'] = TRUE;
			$tempquota['company_id'] = company_cookies('company_id');
			$obj_quota = $this->quota_model->get($tempquota);
			
			// CHECK QUOTA COMPANY 
			if (!empty($obj_quota) && $obj_quota['after_quota'] >= 0)
			{
				// INSERT QUOTA AND REDUCE QUOTA
				$param = array();
				$param['company_id'] = company_cookies('company_id');
				$param['current_quota'] = $obj_quota['after_quota'];
				$param['increase_quota'] = 0;
				$param['decrease_quota'] = 1;
				$param['after_quota'] = $obj_quota['after_quota'] - 1;
				$param['remarks'] = 'update '.time();
				$insert = $this->quota_model->save($param);
				
				// INSERT PARTICIPANT HERE
				if (!isset($expired_test_date)) $expired_test_date = date('Y-m-d H:i:s',strtotime("+2month",time()));
				
				$temp = array();
				$temp['company_id'] = company_cookies('company_id');
				$temp['name'] = $name;
				$temp['gender'] = $gender;
				$temp['job_position'] = $job_position;
				$temp['expired_test_date'] = $expired_test_date;
				$temp['email'] = $email;
				$temp['token'] = md5(time().$email);
				$this->participant_model->save($temp);
				
				$last_participant = $this->participant_model->get(array('last' => TRUE, 'company_id' => company_cookies('company_id')));
				
				// INSERT LIST TYPE TEST HERE
				if (!empty($arr_test))
				{
					foreach ($arr_test as $key => $test) 
					{
						$param = array(
							'test_id' => $key,
							'participant_id' => $last_participant['participant_id'],
							'is_active' => 1
						);
						$this->test_model->save_type_test($param);
					}
				}
				
				($insert)?$message['message'] = MESSAGE::SAVE : $message['message'] = MESSAGE::ERROR;
				$message['message'] = getMessage($message['message']);
			}
			else 
			{
				// QUOTA NOT ENOUGH OR NOT FOUND
				$message['message'] = QUOTA_NOT_ENOUGH;
			}
		}
		
	}
	
}

$do = NULL;
if (isset($_GET['do'])) $do = $_GET['do'];

$quota_avail = 0;
$obj_quota = $this->company_model->get_quota( array('company_id' => company_cookies('company_id')) );
if (!empty($obj_quota)) $quota_avail = $obj_quota['after_quota'];

?>

<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1 class=""><?php echo $MODULE?></h1><br/>
	
	<?php if(is_filled($message['message']))echo message($message['message']).BR?>
	
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'>
			<?php 
			if (!isset($do))
			{
				?>
			Anda masih memiliki quota <b><?php echo $quota_avail?></b> kandidat<br/><br/>
			<a href="?do=insert" class="btn btn-success">Tambah user</a>
			<br/><br/> atau import csv <input type="file" name="import" value="Import Csv"/><br/>
				<?php 
			}
			if ($do == "insert" || $do == "edit")
			{
				$param['is_publish'] = 1;
				$param['is_company'] = 1;
				$obj_list_test = $this->test_model->get_list_test($param);
				
				$obj_list_test = $obj_list_test['data'];
				
				if (!empty($obj_list_test)) 
				{
					echo AVAILABLE_TEST.':'.BR;
					?>
					
					<?php
					foreach($obj_list_test as $key => $test)
					{
						?>
						<input type="checkbox" name="f_test_id[<?php echo $test['test_id'] ?>]" id="f_test_<?php echo $key?>" value="" <?php if ($do == "edit" && $test['is_active']) echo 'checked'?> /> <label for="f_test_<?php echo $key?>"><?php echo $test['title']?></label><br/>
						<?php
						
					}
					echo HR;
				}
				
				?>
				
				
				<div class="col-sm-6"><?php echo NAME.BR?><input type="text" class="input wdtFul br" placeholder="<?php echo NAME ?>" name="f_name" /></div>
				<div class="col-sm-6"><?php echo EMAIL.BR?><input type="text" class="input wdtFul br" placeholder="<?php echo EMAIL ?>" name="f_email" /></div>
				
				<div class="col-sm-6">
				<?php echo GENDER.BR ?>
				<select name="f_gender" id='f_gender' placeholder="Gender" title="<?php echo SELECT_GENDER?>" class="form-control" required>
					<option value=""><?php echo SELECT_GENDER; ?></option>
					<option value="1" <?php if(isset($_POST['gender']) && $_POST['gender'] == 1)echo 'selected'?>><?php echo MALE?></option>
					<option value="2" <?php if(isset($_POST['gender']) && $_POST['gender'] == 2)echo 'selected'?>><?php echo FEMALE?></option>
				</select>
				</div>
				<div class="col-sm-6"><?php echo JOB_POSITION.BR?><input type="text" class="input wdtFul br" placeholder="<?php echo JOB_POSITION ?>" name="f_job_position" /></div>
				
				<!--
				<div class="col-sm-6"><?php echo EXPIRED_TEST_DATE.BR?><input type="text" class="input wdtFul br" placeholder="<?php echo EXPIRED_TEST_DATE ?>" name="f_expired_test_date" /></div>
				<div class="col-sm-6"></div>
				-->
				
				<div class="clearfix"></div><br/>
				
				<div class='' style='padding-left:15px'>
			<button class='btn btn-success' value="btnInsert"><?php echo SUBMIT?></button>
		</div>
				
				<?php
			}		
			?>
			</div>
		</div>

	</form>
</div>