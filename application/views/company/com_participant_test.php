<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$MODULE = $PAGE = $PAGE_TITLE = MEMBER.' '.TEST;
$bread['company'] = 'Company';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);

global $page, $param, $message;

$obj_list = $this->test_model->get_list_test(array('is_publish' => 1));
$obj_list = $obj_list['data'];
?>

<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1 class=""><?php echo $MODULE?></h1><br/>
	<?php if(is_filled($message['message']))echo message($message['message']).BR?>
	
	<?php 
	$do = $test_id = NULL;
	if ($this->input->get('do')) $do = $this->input->get('do');
	if ($this->input->get('test_id') && is_numeric($this->input->get('test_id'))) $test_id = $this->input->get('test_id');
	
	if ($do == 'showresult' && is_numeric($test_id)) 
	{
		$param['test_id'] = $this->input->get('test_id');
		$param['member_id'] = member_cookies('member_id');
		$data = $this->test_model->get_test_result_score($param);
		if (!empty($data['test_name']))
		{			
	?>
	Test : <br/><?php echo $data['test_name'] ; ?><br/><br/>
	Test Description : <br/><?php echo $data['test_description'] ; ?><br/><br/>
	Hasil skor : <br/><?php echo $data['total_score'] ; ?><br/><br/>
	Hasil test anda : <br/><?php echo $data['result'] ; ?><br/><br>
	<a href="<?php echo base_url().'member/test'?>" class="btn btn-info btn-sm"><?php echo BACK ?></a>
	
	<?php
		}
		else 
		{
			echo DATA_NOT_FOUND;
		}
	} 
	else 
	{
	?>
	<table class='table hover'>
		<tr>
		<td>#</td>
		<td>Test</td>
		<td>Opsi</td>
		</tr>
		<?php 
		$i = 1;
		$url = base_url().'member/take_test';
		foreach ($obj_list as $key => $rs) { 
		$id = $rs['test_id'];
		?>
		<tr>
		<td><?php echo $i?></td>
		<td><?php echo $rs['title']?></td>
		<td class="talRgt">
			<a class="btn btn-success btn-sm" href="<?php echo $url.'?test_id='.$id; ?>" title="Mulai tes ini" alt="Mulai tes ini"><i class="clrWht fa fa-pencil-square-o fa-1x"></i> Mulai tes ini</a> 
		</td>
		</tr>
		<?php 
			$i++;
		} 
		?>
	</table>
	<hr>
	<sub>catatan: Anda hanya bisa mengikuti tes sekali dalam setahun.</sub>
	<?php 
	}
	?>
</div>