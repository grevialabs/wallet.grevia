<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER;

$PAGE_HEADER = NULL;
$PAGE = $MODULE = BILLING;
$bread['company'] = 'Company';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

global $page, $param, $message;

$get_payment_status = NULL;
if ($this->input->get('payment_status'))
{
	$get_payment_status = $this->input->get('payment_status');
}

if (isset($get_payment_status) && is_numeric($get_payment_status)) 
{
	$param['payment_status'] = $get_payment_status;
}

$allowed_order = array('creator_date','total');
$allowed_orderby = array('asc','desc');

$order = "creator_date";
$orderby = 'desc';
if ($this->input->get('order') && in_array($this->input->get('order'),$allowed_order)) 
{
	// ASC DESC
	if ($this->input->get('orderby') && in_array($this->input->get('orderby'),$allowed_orderby)) 
	{
		$orderby = $this->input->get('orderby');
	}
	
	$param['order'] = $this->input->get('order');
	$param['orderby'] = $orderby;
}
$param['company_id'] = company_cookies('company_id');

// $param['offet'] = 2;
// $param['limit'] = 0;
$param['paging'] = TRUE;

$obj_list = $this->order_model->get_list($param);
$obj_list = $obj_list['data'];
?>

<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1 class=""><?php echo $MODULE?></h1><br/>
		
	<?php if(is_filled($message['message']))echo message($message['message']).BR?>
	
	<?php 
	$order_id = NULL;
	if ($this->input->get('order_id') && $this->input->get('do')) $order_id = $this->input->get('order_id'); 
		
	if (is_numeric($order_id))
	{
		$obj_order = $this->order_model->get( array(
			'order_id' => $order_id,
			'company_id' => company_cookies('company_id')
		) );
			
		if (!empty($obj_order))
		{
		?>
		<table class="table table-hover table-bordered">
			<tr>
				<td>OrderID</td>
				<td><?php echo $obj_order['order_id']?></td>
			</tr>
			<tr>
				<td>Invoice Code</td>
				<td><?php if (isset($obj_order['invoice_code'])) echo $obj_order['invoice_code'] ;else echo NOT_READY_YET?></td>
			</tr>
			<tr>
				<td width="300px">Date</td>
				<td><?php echo date(DATE_FORMAT, strtotime($obj_order['creator_date']))?></td>
			</tr>
			<tr>
				<td><?php echo PAYMENT_STATUS?></td>
				<td><?php echo order_payment_status($obj_order['payment_status'])?></td>
			</tr>
			<tr>
				<td>Detail</td>
				<td><?php echo $obj_order['detail']?></td>
			</tr>
			<tr>
				<td>Notes</td>
				<td><?php echo $obj_order['notes']?></td>
			</tr>
		</table>
		
		<!-- DETAIL INVOICE START -->
		<table class="table table-hover">
		<tr>
			<td>#</td>
			<td><?php echo PRODUCT_NAME ?></td>
			<td><?php echo QUANTITY ?></td>
			<td><?php echo PRICE ?></td>
			<td><?php echo SUBTOTAL ?></td>
		</tr>
		<?php
		$param = array(
			'order_id' => $order_id,
			'creator_id' => company_cookies('company_id'),
		);
		$obj_list_order_detail = $this->order_model->get_list_detail($param);
		$obj_list_order_detail = $obj_list_order_detail['data'];
		
		$subtotal = $subprice = $discount = $tax = 0;
		foreach ($obj_list_order_detail as $key => $obj)
		{
			$price = $obj['price'];
			$subprice = $obj['quantity'] * $obj['price'];
			
			?>
			<tr>
				<td><?php echo $key+1 ?></td>
				<td><?php echo $obj['product_name'].BR.$obj['description']?></td>
				<td><?php echo $obj['quantity']?></td>
				<td><?php echo format_money($obj['price'])?></td>
				<td class="talRgt"><?php echo format_money($subprice) ?></td>
			</tr>
			<?php
			$subtotal+= $subprice;
		}
		?>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td><?php echo SUBTOTAL?></td>
				<td class="talRgt"><?php echo format_money($subtotal) ?></td>
			</tr>
			<?php 
			if (isset($discount) && $discount > 0) 
			{
				?>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td><?php echo DISCOUNT?></td>
				<td class="talRgt"><?php echo $discount?></td>
			</tr>
			<?php 
			}
			?>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td><?php echo TAX?></td>
				<td class="talRgt"><?php echo $tax = format_money($subtotal*0.1) ?></td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td>Grandtotal</td>
				<td class="talRgt"><?php echo format_money($obj_order['grandtotal'])?></td>
			</tr>
		</table>
		<!-- DETAIL INVOICE END -->
		<?php
		}
		else 
		{
			echo NO_DATA;
		}
	}
	else
	{
	?>
	
	<form class='form-horizontal' role='form' method='post'>
		<?php 
		$arr_payment_status = order_payment_status('get');
		$arr_bg = array('1','2','3','5','7');
		$bg = 'bg-success';
		if (!$this->input->get('payment_status')) $bg = 'btn-success clrWht';
		echo '<a class="btn btn-sm clrBlk b alert '.$bg.'" href="'.current_url().'">'.ALL.'</a> ';

		$bg = 'bg-success';
		foreach ($arr_payment_status as $key => $val)
		{
			if (in_array($this->input->get('payment_status'),$arr_bg) && $this->input->get('payment_status') == $key) 
				$bg = 'btn-success clrWht';
			else
				$bg = 'bg-success';
			echo '<a class="btn btn-sm clrBlk b alert '.$bg.'" href="?payment_status='.$key.'">'.$val.'</a> ';
		}
		echo "<br/>";
		
		if (!empty($obj_list)) 
		{
			if ($this->input->get('orderby') && $orderby == "desc") $orderby = "asc";
			else $orderby = "desc";
			?>
			<div class="table-responsive">
			<table class="table table-hover">
			<tr>
				<td>#</td>
				<td>OrderCode</td>
				<td><a href="<?php echo current_url().'?order='.$order.'&orderby='.$orderby; ?>">Date</a></td>
				<td>Status</td>
				<td>Total</td>
				<td>Option</td>
			</tr>
			<?php
			foreach($obj_list as $key => $obj)
			{
			?>
			
			<tr>
				<td><?php echo $key+1 ?></td>
				<td><?php echo $obj['order_id'] ?></td>
				<td><?php echo date(DATE_FORMAT,strtotime($obj['creator_date'])) ?></td>
				<td><?php echo order_payment_status($obj['payment_status']) ?></td>
				<td><?php echo format_money($obj['grandtotal']); ?></td>
				<td><a href="<?php echo current_url().'?do=detail&order_id='.$obj['order_id']?>" class="btn btn-primary"><?php echo DETAIL ?></a> 
				<?php 
				if ($obj['payment_status'] != 5)
				{
					?><a href="<?php echo base_url().'company/billing_confirmation?order_id='.$obj['order_id']; ?>" class="btn btn-success"><?php echo CONFIRM ?></a>
				<?php
				}
				?></td>
				
			</tr>
			<?php 
			}
			?>
			</table>
			</div>
			<?php
		}
		else
		{
			?>
			No Data
			<?php
		}
		?>
	</form>
	<?php 
	}
	?>
</div>