<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER;

$PAGE_HEADER = NULL;
$PAGE = $MODULE = PRODUCT;
$bread['company'] = 'Company';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

global $page, $param, $message;

if ($_POST) {
	$list_product_id = $list_qty = $list_price = array();
	$is_valid = FALSE;
	
	if (!empty($_POST['price'])) 
	{
		$list_price = $_POST['price'];
	}
	
	if (!empty($_POST['product_id'])) 
	{
		$list_product_id = $_POST['product_id'];
	}
	
	if (!empty($_POST['qty'])) 
	{
		$list_qty = $_POST['qty'];
	}
	
	// CHECK CART VALIDATION HERE
	$subtotal = 0;
	foreach ($list_product_id as $key => $val)
	{
		// ADD ITEM IF QTY > 0
		if (isset($list_qty[$key]) && $list_qty[$key] > 0) 
		{
			$is_valid = TRUE;
			$subtotal+= $list_qty[$key] * $list_price[$key];
		}
	}
	
	$order_id = $this->order_model->get_new_order_code();
	
	if ($is_valid)
	{
		// INSERT TO DB HERE
		// SAVE HEADER HERE
		$param = array();
		$param['order_id'] = $order_id;
		$param['company_id'] = company_cookies('company_id');
		//$param['detail'] = '';
		$param['subtotal'] = $subtotal;
		$param['tax'] = 0.1 * $subtotal;
		$param['grandtotal'] = $subtotal + $param['tax'];
		$param['payment_status'] = 1; //DEFAULT 1 not paid
		$param['notes'] = NULL;
		$this->order_model->save($param);
				
		$obj_invoice = $this->order_model->get( array('order_id' => $order_id) );
		foreach ($list_product_id as $key => $val)
		{
			// ADD ITEM IF QTY > 0
			if (isset($list_qty[$key]) && $list_qty[$key] > 0) 
			{
				// SAVE HEADER HERE
				$param = array();
				$param['order_id'] = $obj_invoice['order_id'];
				$param['product_id'] = $list_product_id[$key];
				$param['price'] = $list_price[$key];
				$param['quantity'] = $list_qty[$key];
				$this->order_model->save_detail($param);
			}
		}
		//redirect('company/billing?do=detail&invoice_id='.$obj_invoice['invoice_id']);
		redirect('company/billing?do=detail&order_id='.$obj_invoice['order_id']);
	}
	else
	{
		$message['message'] = "Please Fill quantity";
	}
	
}

$obj_list = $this->product_model->get_list();
$obj_list = $obj_list['data'];

?>

<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1 class=""><?php echo $MODULE?></h1><br/>
	
	<?php if(is_filled($message['message']))echo message($message['message']).BR?>
	
	<form class='form-horizontal' role='form' method='post'>
		<?php 
		if (!empty($obj_list)) 
		{
			?>
			<div class="table-responsive">
			<table class="table table-hover">
			<tr>
				<td>#</td>
				<td>Product Code</td>
				<td>Product</td>
				<td>Quota</td>
				<td>Price</td>
				<td>Option</td>
			</tr>
			<?php
			foreach($obj_list as $key => $obj)
			{
			?>
			
			<tr>
				<td><?php echo $key+1 ?></td>
				<td><?php echo $obj['product_code'] ?>
				<input type="hidden" name="f_product_id[]" value="<?php echo $obj['product_id']?>"/></td>
				<input type="hidden" name="f_price[]" value="<?php echo $obj['normal_price']?>"/></td>
				<td><?php echo $obj['name'].'<br/><br/>'.$obj['description'] ?></td>
				<td><?php echo $obj['participant_credit'] ?></td>
				<td><?php echo format_money($obj['normal_price']); ?></td>
				<td><input type="text" name="f_qty[]" size="2" maxlength="3" /></td>
			</tr>
			<?php 
			}
			?>
			<tr>
				<td colspan="6" class="talRgt"><input type="submit" class="wdtFul btn btn-success" value="<?php echo ORDER?>" onclick="return confirm('<?php echo CONFIRMATION_ORDER?> ?')"/></td>
			</tr>
			</table>
			</div>
			<?php
		}
		?>
	</form>
</div>