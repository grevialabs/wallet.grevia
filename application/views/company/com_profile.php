<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER;

$PAGE_HEADER = NULL;
$PAGE = $MODULE = PROFILE;
$bread['company'] = 'Company';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

if (isset($_POST['btnUpdate'])) {
	$is_subscribe = 0;
	
	if (isset($_POST['is_subscribe'])) {
		$is_subscribe = 1;
	}
	
	$company_id = company_cookies('company_id');
	$param = array(
		'name' => $_POST['name'],
		'address' => $_POST['address'],
		'pic_prefix' => $_POST['pic_prefix'],
		'pic_name' => $_POST['pic_name'],
		'pic_phone' => $_POST['pic_phone'],
		'pic_mobile' => $_POST['pic_mobile'],
		'is_subscribe' => $is_subscribe
	);
	
	$update = $this->company_model->update($company_id, $param);
	($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
}

if ($this->uri->segment(3)) { 
	$data = $this->company_model->get(array('company_id' => $this->uri->segment(3)));
}
if (is_company()) {
	$data = $this->company_model->get(array('company_id' => company_cookies('company_id')));
}

?>
<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1><?php echo $MODULE?></h1><br/>
	
	<?php echo $SETTINGMENUBAR ?>
	
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	?>
	<?php 
	if(!empty($data)) {
		$obj = $data;

	?>
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<label for='f_FullName' class='col-sm-2'><?php echo COMPANY_NAME?></label>
			<div class='col-sm-5'><input type='text' class='form-control' name='f_name' id='f_name' placeholder='<?php echo FILL.' '.COMPANY_NAME ?>' value='<?php echo $obj['name']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_email' class='col-sm-2'><?php echo EMAIL?></label>
			<div class='col-sm-10'><input type='email' class='form-control' name='f_email' id='f_email' placeholder='<?php echo FILL.' '.EMAIL?>' value='<?php echo $obj['email']?>' disabled></div>
		</div>		
		<div class='form-group form-group-sm'>
			<label for='f_address' class='col-sm-2'><?php echo COMPANY_ADDRESS?></label>
			<div class='col-sm-10'><textarea class='form-control' style="min-height:105px" name='f_address' id='f_address' placeholder='<?php echo FILL.' '.COMPANY_ADDRESS?>' ><?php echo $obj['address']?></textarea></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_pic_name' class='col-md-2'><?php echo PIC.' '.NAME?>aa</label>
			<div class='col-md-2'>
			<select name="f_pic_prefix" class="form-control wdtFul">
			<option <?php if ($obj['pic_prefix'] == "Mr.") echo "selected" ?> >Mr.</option>
			<option <?php if ($obj['pic_prefix'] == "Ms.") echo "selected" ?> >Ms.</option>
			</select></div>
			<div class='col-md-8'><input type='text' class='form-control' name='f_pic_name' id='f_pic_name' placeholder='<?php echo FILL.' '.NAME?>' value='<?php echo $obj['pic_name']?>'></div>
		</div>	
		<div class='form-group form-group-sm'>
			<label for='f_pic_phone' class='col-sm-2'><?php echo PIC.' '.PHONE?></label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_pic_phone' id='f_pic_phone' placeholder='<?php echo FILL.' '.PHONE?>' value='<?php echo $obj['pic_phone']?>' ></div>
		</div>	
		<div class='form-group form-group-sm'>
			<label for='f_pic_mobile' class='col-sm-2'><?php echo PIC.' '.MOBILE?></label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_pic_mobile' id='f_pic_mobile' placeholder='<?php echo FILL.' '.MOBILE?>' value='<?php echo $obj['pic_mobile']?>' ></div>
		</div>	
		<!--
		<div class='form-group form-group-sm'>
			<label for='f_Facebook' class='col-sm-2'>Facebook</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_Facebook' id='f_Facebook' placeholder='Enter Facebook' value='<?php //echo $obj['Facebook']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_Twitter' class='col-sm-2'>Twitter</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_Twitter' id='f_Twitter' placeholder='Enter Twitter' value='<?php //echo $obj['Twitter']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_AboutMe' class='col-sm-2'><?php echo ABOUT_ME?></label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_AboutMe' id='f_AboutMe' placeholder='Enter AboutMe' value='<?php// echo $obj['AboutMe']?>'></div>
		</div>
		-->
		<div class='form-group form-group-sm'>
			<label for='f_is_subscribe' class='col-sm-2'><?php echo SUBSCRIBE_NEWSLETTER?></label>
			<div class='col-sm-10 checkbox'>
				<label><input type='checkbox' name='f_is_subscribe' id='f_is_subscribe' value='1' <?php if ($obj['is_subscribe']) echo 'checked'?>></label>
			</div>
		</div>
		<div class='form-group padLeft'>
			<button class='btn btn-success' name='btnUpdate' value='1'><?php echo UPDATE?></button>
		</div>

		<!--
		<p><input type="file" name="f_image"/><br/>
		<?php if(is_filled($image)) echo '<img src="'.base_url().'/data/image/'.$image.'"/>'?>
		</p>
		-->
	</form>
	<!--<form class='form-horizontal'><div class='form-group padLeft'><button class="btn btn-danger" onclick="return confirm('apakah anda yakin ? \n Aksi ini akan menghapus permanen akun anda. ');"><?php echo DELETE.' '.ACCOUNT?></button></div></form>-->
	<?php
	}
	?>
</div>