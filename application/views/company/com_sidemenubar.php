	<?php
	global $current_segment;
	if ($this->uri->segment(2)) $current_segment = $this->uri->segment(2);
	
	function member_active($page) 
	{
		global $current_segment;
		$str = '';
		if ($current_segment === $page)
		{
			$str = 'active';
		}
		return $str;
	}
	
	function admin_active($page) 
	{
		global $current_segment;
		$str = '';
		if ($current_segment === $page)
		{
			$str = 'active';
		}
		return $str;
	}
	
	if ($this->input->get('notification_id')) $notification_id = $this->input->get('notification_id');

	// UPDATE IF OPEN

	if (isset($notification_id))
	{
		$tmp = array();
		$tmp['company_id'] = company_cookies('company_id');
		$tmp['notification_id'] = $notification_id;
		$obj_notification = $this->notification_model->get($tmp);
		
		if (isset($obj_notification['is_read']) && $obj_notification['is_read'] == 0)
		{
			// UPDATE TO READ
			$tmp['is_read'] = 1;
			$update = $this->notification_model->update($notification_id,$tmp);
		}
	}
	
	// SHOW NOTIFICATION UNREAD IN SIDEBAR
	$param['company_id'] = company_cookies('company_id');
	$param['is_read'] = 0;
	$param['is_delete'] = 0;
	$obj_list_notification = $this->notification_model->get_list($param);
	$obj_list_notification = $obj_list_notification['data'];
	
	$notif = 0;
	if (!empty($obj_list_notification)) $notif = count($obj_list_notification);
	
	$bg_notif = NULL;
	if ($notif > 0) $bg_notif = '&nbsp; &nbsp;<sub class="bgRed clrWht"> new &nbsp;</sub>';
	?>
	Hola, <?php echo company_cookies('name')?><hr>

	<div class="panel panel-primary">
		<div class="list-group">
			<a class="list-group-item <?php echo member_active('notification')?>" href="<?php echo base_url().'company/notification'?>"><i class="fa fa-bell fa-fw"></i>&nbsp; <?php echo MENU_NOTIFICATION?>
			<i><?php echo $notif?></i><?php echo $bg_notif?></a>
			<a class="list-group-item <?php echo member_active('order')?>" href="<?php echo base_url().'company/order'?>"><i class="fa fa-pencil-square-o fa-fw"></i>&nbsp; <?php echo MENU_ORDER_PRODUCT?></a>
			<a class="list-group-item <?php echo member_active('billing').''.member_active('billing_confirmation')?>" href="<?php echo base_url().'company/billing?payment_status=1'?>"><i class="fa fa-files-o"></i>&nbsp; <?php echo MENU_COMPANY_BILLING?></a>
			<a class="list-group-item <?php echo member_active('participant').member_active('test_candidate').member_active('report')?>" href="<?php echo base_url().'company/participant'?>"><i class="fa fa-user fa-fw"></i>&nbsp; <?php echo MENU_COMPANY_PARTICIPANT?></a>
			<!--<a class="list-group-item <?php echo member_active('test_candidate')?>" href="<?php echo base_url().'company/test_candidate'?>"><i class="fa fa-file-text fa-fw"></i>&nbsp; <?php echo MENU_COMPANY_TEST_CANDIDATE?></a>
			<a class="list-group-item <?php echo member_active('report')?>" href="<?php echo base_url().'company/report'?>"><i class="fa fa-bar-chart"></i>&nbsp; <?php echo MENU_COMPANY_REPORT?></a>-->
			<a class="list-group-item <?php echo member_active('setting').member_active('profile')?>" href="<?php echo base_url().'company/setting'?>"><i class="fa fa-cog fa-fw"></i>&nbsp; <?php echo MENU_MY_SETTING?></a>
		</div>
	</div>