<?php 
if (!empty($data))
{
	if( !is_internal() ){ 
?>
<!-- FB API FOR COMMENTS -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=359223105905";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- FB API FOR COMMENTS -->
<?php 
	}
} 
?>

<div class="col-sm-9 col-md-9">
<?php
if (!empty($data))
{
	$friendlyUrl = current_url();
	
	$articleCategoryID = array();
	//var_dump($articleCategory);
	if (strpos($data['ArticleCategoryID'],',') !== FALSE) {
		$articleCategoryID = explode(',', $data['ArticleCategoryID']);
		foreach ($articleCategoryID as $key => $val) 
		{
			$obj = NULL;
			$obj = $this->articlecategory_model->get(array('ArticleCategoryID' => $val));
			$articleCategory[] = array( 'CategoryName' => $obj['CategoryName'], 'Slug' => $obj['Slug'] );
		}
	} else {
		$obj = NULL;
		$obj = $this->articlecategory_model->get(array($data['ArticleCategoryID']));
		$articleCategory[] = array( 'CategoryName' => $obj['CategoryName'], 'Slug' => $obj['Slug'] );
	}
	
	?>
	<h1><?php echo $data['Title']?></h1>
	
	<div>
	<?php 
	
	foreach ($articleCategory as $key => $val) {
		echo ' <a class="btn btn-xs btn-info" href="'.base_url().'articlecategory/'.$val['Slug'].'" alt="Artikel Kategori '.$val['CategoryName'].'" title="Artikel Kategori '.$val['CategoryName'].'"><span class="clrWht">'.$val['CategoryName'].'</span></a> ';
	}
	?>
	</div><br/>
	
	<div>
	<img class="img-responsive wdtFul" src="<?php echo base_url()?>asset/images/article/<?php echo $data['Image']?>"/><br/>
	<?php if (isset($data['ImageDescription'])) { ?>
	<div class="clrGry talRgt"><?php echo $data['ImageDescription']?></div><br/>
	<?php } ?>
	<?php 
	// SOCIAL
	$pagecontent = '';
	$pagecontent.= "<table cellpadding='0' cellspacing='15' style='padding: 10px'>";

	if (is_filled($data['ShortUrl'])) {
		//$pagecontent.= "<tr><td width='300px' colspan='2'><label for='divShortUrl' title='Copy short url'><img src='".base_url()."/asset/images/url_icon.png' height='30px' width='30px'/>shortUrl</label> <input id='divShortUrl' title='Copy short url' style='font-size:12px' onClick='this.select();' class='fntBld' readonly='readonly' size='11' value='".$data['ShortUrl']."'></td></tr>";
	}
	$pagecontent.= "<tr><td style='padding-right:10px' valign='top'>
	<!-- start fb share button-->
	<iframe src='http://www.facebook.com/plugins/like.php?href=". urlencode($friendlyUrl) ."&width&layout=button_count&action=like&show_faces=false&share=true&height=21&appId=".FACEBOOK_APP_ID ."' width='145px' height='25px' style='float:left; border:none'></iframe>
	<!-- // fb share button-->
	</td>
	<td style='padding-right:10px' valign='top'>
	<!-- start twitter share-->
	<a href='https://twitter.com/share' class='twitter-share-button' data-via='greviacom' data-related='greviacom'>Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='//platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','twitter-wjs');</script>
	<!-- // twitter share-->
	</td>
	<td>
		<!-- start google+ share -->
		<div class='g-plus' style='width:100px;height:22px' data-action='share' data-annotation='bubble' data-height='24' data-href='".$friendlyUrl."'></div>

		<!-- Place this tag after the last share tag. -->
		<script type='text/javascript'>
		  (function() {
			var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			po.src = 'https://apis.google.com/js/platform.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		  })();
		</script>
		<!-- end google+ share -->
	</td></tr>";
	$pagecontent.= "</table>";
	echo $pagecontent;
	
	//QUOTE HERE
	if (isset($data['Quote'])) {
		echo "<div class='divQuote'>".$data['Quote']."</div>";
	}
	?>
	<p><?php echo html_entity_decode($data['Content'])?></p>
	</div>
	
	<?php if (!is_internal()) { ?>
	<div class="clrBlk fntHdr fntBld">Comments</div>
	<!-- COMMENTS BY FACEBOOK -->
	<div class="fb-comments" data-href="<?php echo $friendlyUrl?>" data-width="620"></div><br/>
	<span class="clrWht fntBld"> <fb:comments-count href=<?php echo $friendlyUrl?>/></fb:comments-count> </span>
	<!-- COMMENTS BY FACEBOOK -->
	<?php 
	}
	?>
	<style>
	.authorpic {
		width: 100px;
		height: 100px;
		border-radius: 150px;
		-webkit-border-radius: 150px;
		-moz-border-radius: 150px;
		background: url(<?php echo base_url().'asset/images/member/'.$data['AuthorImage']?>) no-repeat;
	}
	p {
		font-size: 16px
	}
	</style>
	<?php
	$pagecontent = "<div class=' round'>
		<div class='b clrRed fntBld f18 '>AUTHOR</div>
		<table cellpadding='5'>
		<tr>
			<td class=' talCnt'><div class='authorpic'></div></td>
			<td class=' valTop'><img src='".base_url()."asset/images/Icon-Twitter-19x19.gif'/> &nbsp;<a href='http://twitter.com/".$data['Twitter']."' target='_blank'>@".$data['Twitter']."</a><br/><br/>".$data['AboutMe']."</td>
		</tr>
		</table>
		</div><br/>";
	
	/* TAG HERE */
	$list_tag = explode(',',$data['Tag']);
	$pagecontent.= "<div class='b clrLgtBlu fntBld f18'>TAG</div>";
	$pagecontent.= "<div id='tag-article' class='tag-article'>";
	$tags = "";
	foreach ($list_tag as $val) {
		$tags.= "<a class='btn btn-warning' href='".base_url()."search?q=".urlencode($val)."'>#".$val."</a> ";
	}  
	$pagecontent.= $tags."</div><br/>";
		
	/* RELATED ARTICLE HERE */
	$tag_keyword = str_replace(",","|",$data['Tag']);
	
	$param = array(
		'Tag' => $tag_keyword,
		'IsPublish' => '1',
		'NotArticleID' => $ArticleID,
		'order' => 'a.CreatorDateTime DESC',
		'paging' => TRUE,
		'limit' => 0,
		'offset' => 5
	);
	
	$tmpdata = $this->article_model->get_list($param);
	$list_related_article = $tmpdata['data'];

	if (!empty($list_related_article)) 
	{
		$pagecontent.= "<div class='b clrOrg f18'>ARTIKEL TERKAIT</div>";
		$pagecontent.= "<div id='related-article' class='lnkBlk'><ul>";
		foreach($list_related_article as $tmp) {
			$pagecontent.= "<li><a href='".base_url()."article/".$tmp['ArticleID']."/".$tmp['Slug']."?utm_source=article&utm_campaign=related' alt='Artikel ".$tmp['Title']."' title='Artikel ".$tmp['Title']."'>".$tmp['Title']."</a></li>";
		}
		$pagecontent.= "</ul></div><br/>";
	}
	echo $pagecontent;
}
?>
</div>

<div class="col-sm-3 col-md-3">
<?php echo $SIDEBAR?>
</div>