<?php 
global $PAGE_TITLE;

$PAGE_TITLE = PARTICIPANT_TEST.' - '.DEFAULT_PAGE_TITLE;

$token = $obj = NULL;
if ($this->input->get('token')) $token = $this->input->get('token') ;
if (!empty($token))
{
	$obj = $this->participant_model->get(array( 'token' => $token ));
	$list_type_test = array();
	if (!empty($obj))
	{
		$list_type_test = $this->test_model->get_list_type_test_participant(
			array(
			'participant_id' => $obj['participant_id'],
			'is_active' => 1
			)
		);
		$list_type_test = $list_type_test['data'];
	}
}
?>
<div class="col-sm-2">
&nbsp;
</div>
<div class='col-sm-8'>
	<h1 class="title-header">&nbsp;<?php echo PARTICIPANT_TEST ?></h1><hr/>
	<div class="errLog"></div>
	
	<?php 
	if (!empty($obj))
	{
	?>
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'>
				<div class="talCnt">Dear <?php echo ucwords($obj['name'])?>,<br/><br/><?php echo YOU_WILL_FOLLOW_TEST_FROM; ?> <b><?php echo $obj['company_name']?></b><br/><br/><?php echo PLEASE_TAKE_TEST_IN_TIME; ?>.<br/><br/></div>
			
			<?php 
			if (!empty($list_type_test))
			{
				?>
				<table class="table table-hover">
				<tr>
					<td>Tes</td>
					<td>Expired Date</td>
					<td>Telah diikuti</td>
					<td>Opsi</td>
				</tr>
				<?php
				
				foreach ($list_type_test as $key => $val)
				{
			?>
			<tr>
				<td><?php echo $val['title']?></td>
				<td><?php echo date('D, d M Y H:i',strtotime($obj['expired_test_date']))?></td>
				<td>
				<?php 
				$paramx['participant_id'] = $obj['participant_id'];
				$paramx['test_id'] = $val['test_id'];
				$paramx['is_taken'] = TRUE;
				
				$total_history = $this->history_test_model->get_list_history_test_participant($paramx);
				$total_history = COUNT($total_history['data']);
				echo $total_history;
				?>
				</td>
				<td>
				<?php 
				
				if (time() < strtotime($obj['expired_test_date']) && $total_history < 1)
				{
					?>
				<a onclick="return confirm('<?php echo CONFIRM ?> ?')" class="btn btn-success" href="<?php echo base_url().'take_test?token='.$obj['token'].'&test_id='.$val['test_id']?>">Ikut tes</a>
					<?php
				}
				else if($total_history > 0)
				{
					?>
					<div class="btn btn-danger"><?php echo TEST_ALREADY_TAKEN; ?></div>
					<?php
				}
				else
				{
					?>
					<div class="btn btn-danger"><?php echo YOUR_TIME_IS_UP; ?></div>
					<?php
				}
				?>
				</td>
				
			</tr>
			<?php 
				}
				?>
				</table>
				<?php
			}
			?>
			</div>
		</div>
		
	</form>
	
	<?php
	}
	else 
	{
	?>
		Ooops... salah kamar.
	<?php
	}
	if (isset($gInfo)) echo print_message($gInfo);
	?>
</div>
<div class="col-sm-2">
&nbsp;
</div>
