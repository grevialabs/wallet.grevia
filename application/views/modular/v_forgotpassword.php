<?php 
global $PAGE_TITLE, $MODULE;

$MODULE = FORGOT_PASSWORD;
$PAGE_TITLE = $MODULE.' - '.DEFAULT_PAGE_TITLE;

/*|
  | FORGOT_PASSWORD
*/

$do = $email = $code = NULL;

if (isset($_GET['do'])) $do = $_GET['do'];
if (isset($_GET['email']) && is_valid_email($_GET['email'])) $email = $_GET['email'];
if (isset($_GET['code']) && is_alphanumeric($_GET['code'])) $code = $_GET['code'];

$post_email = $this->input->post('email');
if ($_POST && isset($post_email)) 
{
	$obj_member = $this->member_model->get(array('email' => $post_email));
	if (!empty($obj_member) && isset($obj_member['member_id'])) 
	{
		$expired_reset_date = date('Y-m-d H:i:s', time() + 86400); //strtotime($obj_member['']);
		$update = $this->member_model->update($obj_member['member_id'], array('forgot_pass_expired' => $expired_reset_date));
		
		$str_email = 'Dear '.$obj_member['first_name'].', <br/><br/>';
		$str_email.= 'Untuk melakukan reset password di situs www.duitlo.com , silakan klik ke sini <a href="'.base_url().'resetpassword?do=reset&email='.urlencode($obj_member['email']).'&code='.$obj_member['active_code'].'">Reset password</a> atau '.base_url().'resetpassword?do=reset&email='.urlencode($obj_member['email']).'&code='.$obj_member['active_code'].' dalam waktu 1 x 24 jam. Jika anda tidak merasa melakukan aksi ini, mohon abaikan email ini.<br/><br/>
		Salam Hangat,<br/>
		<b>Tim Duitlo</b> -	http://www.duitlo.com<br/>
		Situs perencana keuangan finansial kamu.
		';
		
		$gInfo = '';
		$to = $obj_member['email'];
		if(!is_internal())
		{
			// SEND EMAIL HERE
			//$to = $obj_member['email'];
			$subject = "Reset Password Akun Duitlo.com";
			$headers = "From: Duitlo Team.<noreply@duitlo.com> \r\n";
			$headers .= "Reply-To: \r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			$send = mail($to,$subject,$str_email,$headers);
			
			$gInfo.= 'Email reset Password berhasil dikirim ke <b>'.$to.'</b> ';
		}else{
			$gInfo.= 'Email reset Password berhasil dikirim ke <b>'.$to.'</b> '.br().$str_email;
		}
	} 
	else 
	{
		$gInfo = DATA_NOT_FOUND;
	}
}

?>
<div class='col-md-3'>
</div>
<div class='col-md-6'>
	<h1 class="title-header">&nbsp;<?php echo $MODULE?></h1><hr/>
	<form method="post" id="frmReg" class="form-group">
		<div class="errReg"></div>
		<?php if (isset($gInfo))echo message($gInfo); ?>
		
		<div class="form-group form-group-md br">
			<div>
			<input type="email" name="f_email" class="input wdtFul br" value="<?php if (isset($post_email)) echo $post_email; ?>" required />
			<button class="btn btn-success" type="submit" name="forgot_password" value="1" />SUBMIT</button>
			</div>
			
		</div>
		<div class='clearfix'></div><br/>
	</form>
</div>
<div class='col-md-3'>
</div>