<div class="col-md-9">
<?php

//$data = (array) json_decode($data);
if (!empty($data))
{
	$str = "";
	$total_rows = $data['total_rows'];
	$list_data = $data['data'];
	?>
	<div class="row">
	<?php
	$i = 0;
	if (is_numeric($page) && $page > 0) 
	{
		$i = ($page - 1) * $offset;
	}
	foreach($list_data as $key => $rs)
	{
		$rs = (array) $rs;
		$i += 1;
		$title = $rs['Title'];
		$image = base_url().'asset/images/article/'.$rs['Image'] ;
		$url = base_url().'article/'.$rs['ArticleID'].'/'.$rs['Slug'];
		$alt = "Artikel ".$rs['Title'];
		?>
		<div class="col-md-6">
		<img data-original="<?php echo $image ?>" src="<?php echo $image; ?>" class="lazy img-responsive wdtFul"/>
		<a class="fntMd" href="<?php echo $url ?>" alt="<?php echo $alt?>" title="<?php echo $alt?>"><?php echo $title?></a><br/><br/>
		<?php if(isset($rs['ShortDescription'])) echo $rs['ShortDescription'].'</br>'; ?><br/>
		<div class="talRgt lnkBlu" style="padding:5px"><a href="<?php echo $url?>" alt="<?php echo $alt?>" title="<?php echo $alt?>">Baca selengkapnya</a></div>
		<div style="border-top: 3px #0089C9 solid"></div><br/>
		</div>
		<?php if($i%2 == 0) { ?>
		<div class="clearfix"></div>
		<?php
		}		
	}
	?>
	</div>
	<?php
	if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
}


?>
</div>
<div class="col-md-3 ">
	<?php echo $SIDEBAR_RIGHT?>
</div>
