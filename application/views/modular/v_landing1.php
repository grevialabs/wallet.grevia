<div class="jumbotron bgBlurry">
	<div class="container">
		<div class="row" style="min-height:250px">
			<div class="col-xs-4">
				<i class="fa fa-database fa-5x pull-left"></i>
				Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
			</div>
			<div class="col-xs-4">
			<i class="fa fa-car fa-5x pull-left"></i>
			Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit ametLorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit ametLorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit ametLorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
			</div>
			
			<div class="col-xs-4">
				<i class="fa fa-camera-retro fa-5x pull-left"></i> Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit ametLorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit ametLorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit ametLorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit ametLorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
			</div>
			<hr>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<!-- 
TEMPORARY COMMENT

<div class="col-md-3" class="padMed">
<?php if (isset($SIDEBAR)) echo $SIDEBAR; ?>
<blockquote class='reverse-block bg-warning'>
Quotes
<div style="background:#fff;color:#000">-- Anonym</div>
</blockquote>
<br/>

<div class="bg-primary padLrg">Subscribe<br/><input class="btn-xs clrBlk wdtFul" type="text" name="f_email_subscribe"/><br style="margin-bottom:10px"/>
<input class="btn btn-info wdtFul" type="submit" value="Submit"/></div>

</div>
<div class="clearfix"></div>
-->

<!-- NEW LINE -->
<div class="container">
	<div class="rows">
		<div class="col-xs-2">
			<i class="fa fa-database fa-5x pull-left"></i>
			Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
		</div>
		<div class="col-xs-2">
		<i class="fa fa-car fa-5x pull-left"></i>
		Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
		</div>
		
		<div class="col-xs-2">
			<i class="fa fa-camera-retro fa-5x pull-left"></i> Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
		</div>
		
		<div class="col-xs-2">
			<i class="fa fa-database fa-5x pull-left"></i>
			Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
		</div>
		<div class="col-xs-2">
		<i class="fa fa-car fa-5x pull-left"></i>
		Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
		</div>
		
		<div class="col-xs-2">
			<i class="fa fa-camera-retro fa-5x pull-left"></i> Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
		</div>
	</div>
</div>

<div class="container">
	<div class="rows">
		<div class="col-xs-3">
			<span class="fa-stack fa-4x">
			<i class="fa fa-circle fa-stack-2x"></i>
			<i class="fa fa-flask fa-stack-1x text-primary"></i>
			</span><br/>
			<div class="b talCnt">Email</div><br/>
			<div>Lorem ipsum dolor sit amet</div>
		</div>
		<div class="col-xs-3">
			<span class="fa-stack fa-4x">
			<i class="fa fa-circle fa-stack-2x"></i>
			<i class="fa fa-shield fa-stack-1x text-primary"></i>
			</span>
			<div class="b talCnt">Email</div><br/>
			<div>Lorem ipsum dolor sit amet</div>
		</div>
		<div class="col-xs-3">
			<div class="b talCnt">
				<span class="fa-stack fa-4x">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-compass fa-stack-1x text-primary"></i>
				</span><br/>
				<span class="upper">Email</span><br/>
				<div>Lorem ipsum dolor sit amet</div>
			</div>
		</div>
		<div class="col-xs-3">
			<span class="fa-stack fa-4x">
			<i class="fa fa-circle fa-stack-2x"></i>
			<i class="fa fa-cloud fa-stack-1x text-primary"></i>
			</span>
			<div class="b talCnt">Email</div><br/>
			<div>Lorem ipsum dolor sit amet</div>
		</div>
	</div>
</div>
<div class="clearfix"></div><br/>