<?php 
global $PAGE_TITLE;

$PAGE_TITLE = LOGIN.' '.COMPANY.' - '.DEFAULT_PAGE_TITLE;

if (is_member()) redirect(base_url().'member/profile');
if (is_company()) redirect(base_url().'company/profile');

/*|
  | LOGIN
*/
if ($this->input->post('hdnLogin')) 
{
	$gInfo = '';
	$email = filter(post('l_email'));
	$password = filter(post('l_password'));
	if(isset($email) && isset($password))
	{
		$company = $this->company_model->get(array('email' => $email));
		if(!empty($company))
		{
			if($company['thepassword'] == encrypt($password))
			{
				$encrypt = encrypt($company['company_id']."#".$company['name']."#".$company['email']."#".$company['is_admin']);
				
				// 24 hour expired cookie on default
				$expiredCookie = time() + 24 * 3600;
				if (post('l_RememberMe')) $expiredCookie = time() + 30 * 86400;
				
				setcookie('hash_company', $encrypt, $expiredCookie, base_url());

				// redirect to reference url if exist
				if ($this->input->get('url')) 
				{
					$url = $this->input->get('url');
					redirect(base_url().urldecode($url));
				}
				else
				{
					redirect('company/profile');
				}
			}
			else
			{
				$gInfo = EMAIL.' / '.PASSWORD.' '.NOT_VALID;
			}
		}
		else
		{
			$gInfo = EMAIL.' '.NOT_FOUND;
		}
	}
}
?>
<div class="col-sm-3">
&nbsp;
</div>
<div class='col-sm-6'>
	<h1 class="title-header">&nbsp;<?php echo LOGIN.' '.COMPANY?></h1><hr/>
	<div class="errLog"></div>
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'><input type='email' class='form-control' name='l_email' id='l_email' placeholder='<?php echo EMAIL?>' value='<?php if ($this->input->post('l_email')) echo filter(post('l_email'))?>' required></div>
		</div>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'><input type='password' class='form-control' name='l_password' id='l_password' placeholder='<?php echo PASSWORD?>' required></div>
		</div>
		<div class='form-group form-group-md'>
			<div class='col-sm-1 checkbox ' style='padding-left:30px'>
				<label><input type='checkbox' name='l_RememberMe' id='l_RememberMe' value='1' /></label>
			</div>
			<div class='col-sm-11' style='padding-top:8px'><label for='l_RememberMe'><?php echo INFO_REMEMBER_ME?></label></div>
		</div>
		<div class='form-group'>
			<div class='col-sm-12'>
				<div class='talRgt'><a href="<?php echo base_url()?>forgotpassword"><?php echo MENU_FORGOT_PASSWORD?></a> | <a href="<?php echo base_url()?>register"><?php echo MENU_REGISTER?></a></div>
			<input type="hidden" name="hdnLogin" value="1"/><button type="submit" value="" class="btn btn-success"/><?php echo LOGIN?></button></div>
		</div>
	</form>
	
	<?php
	if (isset($gInfo)) echo print_message($gInfo);
	?>
</div>
<div class="col-sm-3">
&nbsp;
</div>
