<?php 
global $BREADCRUMB, $PAGE, $PAGE_TITLE, $MODULE, $PAGE_HEADER, $SUBPAGE_HEADER;

$PAGE_HEADER = NULL;
$PAGE = $MODULE = TEST;
$bread['test'] = 'Test';
$BREADCRUMB = $this->common_model->breadcrumb($bread, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

global $page, $param, $message;

$message = array();
$message['message'] = $token = $test_id = $obj_history = NULL;
$is_valid = FALSE;
if ($this->input->get('token')) $token = $this->input->get('token');
if ($this->input->get('test_id') && is_numeric($this->input->get('test_id'))) $test_id = $this->input->get('test_id');

if (isset($token) && isset($test_id))
{	
	// INSERT TO HISTORY TEST FIRST

	// GET PARTICIPANT
	// $participant = $this->participant_model->get(array('token' => $token));

	// if (!empty($participant))
	// {
		// // CHECK IF EXIST HISTORY TEST WITH NO end_date THEN GET THE LATEST HISTORY_TEST+ID
		// // IF NOT EXIST THEN INSERT ONE
		
		// $tmp = array();
		// $obj_list_history_test = $this->history_test->get_list_history_test($tmp);
		// $obj_list_history_test = $obj_list_history_test['data'];
		
		// // NO RECORD HISTORY TEST => INSERT ONE
		// if (!empty($obj_list_history_test))
		// {
			// $param = array();
			// $param['participant_id'] = $participant['participant_id'];
			// $param['test_id'] = $test_id;
			// $param['start_time'] = $start_time;
			// $this->
			// $insert = $this->history_test_model->save_history_test($param);
		// }
		// else
		// {
			// // 
		// }
		
	// }
	$participant = $this->participant_model->get(array('token' => $token));
	
	// VALID
	if (!empty($participant))
	{
		//VALIDATE NO POSTBACK TEST TAKEN
		$tmp = array();
		$tmp['participant_id'] = $participant['participant_id'];
		$tmp['test_id'] = $test_id;
		$obj_history = $this->history_test_model->get_history_test($tmp);
	
		if (!empty($obj_history))
		{
			$message['message'] = "Anda sudah pernah mengikuti tes ini.";
			//continue;
		}
	}
	
	if (isset($_POST['btnSubmit'])) 
	{
		$list_test_detail_id = $_POST['test_detail_id'];
		$list_test_detail_option_id = $_POST['opt'];
		// var_dump($list_test_detail_option_id);
		// // foreach ($list_test_detail_option_id as $key_id)
		// // {
			// // echo $key_id."<br/>";
		// // }
		// die;
		
		//START TEST
		$start_time = $_POST['starting'];
		
		// GET PARTICIPANT
		$insert = FALSE;
		
		//-------------------------------------------------------
		// echo count($list_test_detail_id);
		// die;
		// var_dump($list_test_detail_id);
		// echo "<hr>";

		// var_dump($list_test_detail_option_id);
		// echo "<hr>";
		// die;
		// //var_dump($obj);die;
		// // INSERT history_detail
		// for ($i = 0; $i < count($list_test_detail_id); $i++)
		// {
			// if (isset($list_test_detail_id[$i]) && isset($list_test_detail_option_id[$i]))
			// {
				// if ($test_id == "5")
				// {
					// // IF DISC-2 THEN ACCEPT MULTIPLE VALUES FOR SINGLE ESSAY
					// foreach ($list_test_detail_option_id as $key_id)
					// {
						// $param = array(
						// //'history_test_id' => $obj['history_test_id'],
						// 'test_detail_id' => $list_test_detail_id[$i],
						// 'test_detail_option_id' => $key_id,
						// 'participant_id' => $participant['participant_id']
						// );
						// $save_test_detail = $this->history_test_model->save_history_test_detail($param);
					// }
				// }
				// else
				// {
					// $param = array(
					// 'history_test_id' => $obj['history_test_id'],
					// 'test_detail_id' => $list_test_detail_id[$i],
					// 'test_detail_option_id' => $list_test_detail_option_id[$i],
					// 'participant_id' => $participant['participant_id']
					// );
					// $save_test_detail = $this->history_test_model->save_history_test_detail($param);

				// }
			// }
		// }
		// die;
		//-------------------------------------------------------
		
		if (!empty($participant)) 
		{
			//VALIDATE NO POSTBACK TEST TAKEN
			$tmp = array();
			$tmp['participant_id'] = $participant['participant_id'];
			$tmp['test_id'] = $test_id;
			$obj_history = $this->history_test_model->get_history_test($tmp);
			
			// IF IS ON TYPE TEST THEN VALID
			// $this->test_model->participant_type_test();
			// if ()
			// {
				
			// }
			
			// INSERT IF TOKEN VALID AND TEST IS NOT TAKEN
			if (empty($obj_history)) 
			{
				//INSERT HEADER
				$param['participant_id'] = $participant['participant_id'];
				$param['test_id'] = $test_id;
				$param['start_time'] = $start_time;
				$param['end_time'] = current_date_time();
				$insert = $this->history_test_model->save_history_test($param);
				
				if ($insert)
				{
					// INSERT NOTIFICATION FOR COMPANY
					$tmp = array();
					$tmp['company_id'] = $participant['company_id'];
					$tmp['subject'] = 'New Result for '.$participant['name'].' - '.$participant['email'];
					$tmp['is_read'] = 0;
					$tmp['is_delete'] = 0;
					$tmp['message'] = 'Peserta '.$participant['name'].' dengan email '.$participant['email'].' telah mengikuti test '.' .<br/><br/> Silakan klik link ini untuk melihat hasil nya.';
					$save = $this->notification_model->save($tmp);
				}
			}
			else
			{
				// USER WANT TO POST BACK, REDIRECT TO thank you
				redirect(base_url().'thankyou_test');
			}
		}
		
		if ($insert && !empty($participant))
		{	
			$param = NULL;
			$param['participant_id'] = $participant['participant_id'];
			$param['last'] = 1;
			$obj = $this->history_test_model->get_history_test($param);
			
			// INSERT history_detail
			for ($i = 0; $i <= count($list_test_detail_id); $i++)
			{
				if (isset($list_test_detail_id[$i]) && isset($list_test_detail_option_id[$i]))
				{
					$param = array(
					'history_test_id' => $obj['history_test_id'],
					'test_detail_id' => $list_test_detail_id[$i],
					'test_detail_option_id' => $list_test_detail_option_id[$i],
					'participant_id' => $participant['participant_id']
					);
					$save_test_detail = $this->history_test_model->save_history_test_detail($param);

				}
			}
			//die;
			$message['message'] = 'Congrats !!!';
			redirect(base_url().'thankyou_test');
		}
		
	}
}
$param['test_id'] = $test_id; 
$obj_test = $this->test_model->get_test(array("test_id" => $test_id));

//$param['is_publish'] = 1; 
$obj_list = array();
if (isset($test_id))
{
	$obj_list = $this->test_model->get_list_test_detail($param);
	$obj_list = $obj_list['data'];
}
// in seconds
$timer = $obj_test['timer'] * 60;
?>

<style>
<!--

-->
</style>

<div class="col-sm-2">
	<?php //echo $SIDEMENUBAR?>		
</div>

<div class="col-sm-10">
	<h1 class=""><?php echo $MODULE?></h1><br/>
	<?php if(is_filled($message['message']))echo message($message['message']).BR?>
	<?php 
	
	if (!empty($obj_list) && !isset($message['message']) && $obj_test['test_code'] == 'disc-1') 
	{
		// GENERAL TEST LAYOUT
	?>
	
	Waktu tersisa : <span id="countdown" class="timer f18"></span>
	<form method="post" id="form_test">
	<div class="table-responsive">
	<table class='table hover'>
		<tr>
		<td>#</td>
		<td>Test</td>
		</tr>
		<?php 
		$i = 1;
		$url = current_url();
		// var_dump($obj_list);
		// die;
		foreach ($obj_list as $keyq => $obj_question)
		{ 
			$id = $obj_question['test_id'];
		?>
		<tr>
		<td><?php echo $i?></td>
		<td><div class="br"><?php echo $obj_question['detail_description']?></div>
		<div class="">
		<input type="hidden" name="test_detail_id[]" value="<?php echo $obj_question['test_detail_id']?>"/>
		<?php 
			$obj_list_option = $this->test_model->get_list_test_detail_option(
				array('test_detail_id' => $obj_question['test_detail_id'])
			);
			$obj_list_option = $obj_list_option['data'];
			foreach ($obj_list_option as $keyo => $option) 
			{
				?>
				<input id="opt<?php echo $i.$keyo?>" type="radio" name="opt[<?php echo $keyq?>]" value="<?php echo $option['test_detail_option_id']?>"> <label for="opt<?php echo $i.$keyo?>"><?php echo $option['answer_detail']?></label><br/>
				<?php 
			}
			?>
		</div>
		</td>
		</tr>
		<?php 
			$i++;
		}
		?>
	</table>
	</div>
	<input type="hidden" name="f_starting" value="<?php echo current_date_time()?>"/>
	<button class="btn btn-sm btn-success" type="submit" name="btnSubmit" id="btnSubmit" value="1"><?php echo SUBMIT?></button>
	</form>
	<?php
	}
	else if (!empty($obj_list) && $obj_test['test_code'] == 'disc-2')
	{
		// TEST DISC Version 2 / DISC-2
	
	if (isset($obj_test['detail_instruction'])) echo "<div>INSTRUKSI<br/><br/>".$obj_test['detail_instruction']."</div><hr/>";
	?>

	Waktu tersisa : <span id="countdown" class="timer f18"></span>
	<form method="post" id="form_test">
	<table class='table hover'>
		<tr>
		<td>#</td>
		<td>Test</td>
		</tr>
		<?php 
		$i = 1;
		$url = current_url();
		// var_dump($obj_list);
		// die;
		$the_order = 0;
		foreach ($obj_list as $keyq => $obj_question)
		{ 
			$id = $obj_question['test_id'];
			
			//if ($the_order > 0) $the_order += ;
			$question = $list_question = NULL;
			//$question = explode(';',$obj_question['detail_description']);
			$question = $obj_question['detail_description'];
			$question = str_replace(';','<br/>',$question);
			$list_question = explode(';',$obj_question['detail_description']);
		?>
		<tr>
		<td><?php echo $i?></td>
		<td>
			<div class="table-responsive">
			<table class="table table-bordered">
			<tr>
				<td><div style="padding:0px">&nbsp;</div></td>
			</tr>
			<?php
			foreach ($list_question as $quest)
			{
				?>
				<tr cellspacing="90px">
					<td class=""><div class="padMed"><?php echo $quest; ?></div></td>
				</tr>
				<?php
			}
			?>
			</table>
			</div>
		</td>
		<td>
			<div class="">
				<input type="hidden" name="test_detail_id[]" value="<?php echo $obj_question['test_detail_id']?>"/>
				<?php 
				$obj_list_option = $this->test_model->get_list_test_detail_option(
					array('test_detail_id' => $obj_question['test_detail_id'])
				);
				$obj_list_option = $obj_list_option['data'];
				?>
				<table class="table table-bordered talCnt">
				<tr>
				<td>+</td>
				</tr>
				<tr>
				<?php
				foreach ($obj_list_option as $keyo => $option) 
				{
					if ($keyo < 4) 
					{
					?>
					<tr><td>
					<label for="opt<?php echo $keyq.$keyo.'a'?>"><?php echo $option['answer_detail']?><i class="fa fa-thumbs-up fa-2x clrGrn"></i>
					<input id="opt<?php echo $keyq.$keyo.'a'?>" type="radio" name="opt[<?php echo $the_order?>]" value="<?php echo $option['test_detail_option_id']?>" size="35" onclick="theclick(this)"> 
					</label><br/>
					</tr></td>
					<?php
					}
				}
				?>
				</tr>
				</table>
				<!--  -->
			</div>
		</td>
		<td>
			<div>
				<input type="hidden" name="test_detail_id[]" value="<?php echo $obj_question['test_detail_id']?>"/>
				<?php 
				$obj_list_option = $this->test_model->get_list_test_detail_option(
					array('test_detail_id' => $obj_question['test_detail_id'])
				);
				$obj_list_option = $obj_list_option['data'];
				?>
				
				<table class="table table-bordered talCnt">
				<tr>
				<td>-</td>
				</tr>
				<tr>
				<?php
				$the_order+= 1;
				foreach ($obj_list_option as $keyo => $option) 
				{
					if ($keyo >= 4) 
					{
						$keyo -= 4;
					?>
					<tr><td>
					<label for="opt<?php echo $keyq.$keyo.'b'?>"><?php echo $option['answer_detail']?>
					<i class="fa fa-thumbs-down fa-2x clrRed"></i>
					<input id="opt<?php echo $keyq.$keyo.'b'?>" type="radio" name="opt[<?php echo $the_order?>]" value="<?php echo $option['test_detail_option_id']?>" onclick="theclick(this)"/>
					</label><br/>
					</tr></td>
					<?php 
					}
				}
				$the_order+= 1;
				?>
				</tr>
				</table>
				<!--  -->
			</div>
		</td>
		</tr>
		<?php 
			$i++;
		}
		?>
	</table>
	<input type="hidden" name="f_starting" value="<?php echo current_date_time()?>"/>
	<button class="btn btn-sm btn-success" type="submit" name="btnSubmit" id="btnSubmit" value="1"><?php echo SUBMIT?></button>
	</form>
	<?php
	}
	else if(isset($message['message']))
	{
		//echo "<div class='bg bg-danger'>".$message['message']."</div>";
	}
	else
	{
		echo DATA_NOT_FOUND;
	}
	?>
	<hr>
</div>
<script>
<?php 
if (!isset($timer) || $timer <= 0) $timer = 25*60;
?>
function theclick(elem)
{
	var element_id = elem.id;
	section = element_id.substring(5,6); // return a, b
	element = element_id.substring(0,4); // return opt0
	element_order = element_id.substring(4,5); // return 0 1 2 3
	if (section == 'a')
	{
		// CHECK IF SECTION B CHECKED TOO THEN
		//check_active = document.getElementById("opt00a").checked = false;
		if (document.getElementById(element + element_order + "b").checked)
		{
			document.getElementById(element + element_order + "b").checked = false;
		}
	}
	
	if (section == 'b')
	{
		if (document.getElementById(element + element_order + "a").checked)
		{
			document.getElementById(element + element_order + "a").checked = false;
		}
	}
}

var upgradeTime = <?php echo $timer?>;
var seconds = upgradeTime;
function timer() {
    var days        = Math.floor(seconds/24/60/60);
    var hoursLeft   = Math.floor((seconds) - (days*86400));
    var hours       = Math.floor(hoursLeft/3600);
    var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
    var minutes     = Math.floor(minutesLeft/60);
    var remainingSeconds = seconds % 60;
    if (remainingSeconds < 10) {
        remainingSeconds = "0" + remainingSeconds; 
    }
    document.getElementById('countdown').innerHTML = hours + ":" + minutes + ":" + remainingSeconds;
    if (seconds == 0) {
        clearInterval(countdownTimer);
        document.getElementById('countdown').innerHTML = "Completed";
		//document.forms["form_test"].submit();
		document.getElementById("btnSubmit").click();
    } else {
        seconds--;
    }
}
var countdownTimer = setInterval('timer()', 1000);
</script>