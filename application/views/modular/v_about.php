<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = ABOUT_US;
$BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;
$PAGE_HEADER = $PAGE;
?>
<div class="jumbotron bgPch" >
	<div class="container">
		<div class="rows min-height-250">
			<div class="col-sm-12">
			<?php if (isset($BREADCRUMB)) echo $BREADCRUMB.'' ?>
			<?php if (isset($PAGE_HEADER)) echo '<h2>'.$PAGE_HEADER.'</h2>' ?>
			Grevia Wallet adalah situs yang membantu mencatat dan melacak setiap pengeluaran kamu. Kami menyiapkan berbagai macam report agar kamu bisa mereview dan mempersiapkan anggaran pengeluaran kamu setiap bulannya.<br/><br/>
			Saat ini Grevia Wallet masih rilis untuk beta dan sudah bisa digunakan.<br/><br/>
			Silakan hubungi kami <a href="<?php echo base_url()?>contact">disini</a><hr>
			
			<h2>Mengapa harus mengatur keuangan</h2>
			Money Budgeting adalah sebuah tindakan untuk merencanakan atau mengalokasikan ke mana uang hasil jerih payah Anda akan berlabuh. Karena sebanyak apapun penghasilan yang Anda terima tanpa pengolahan yang baik, percuma saja. Hidup akan menjadi lebih tenang tenang dan tak kesulitan masalah keuangan di masa mendatang, bila Anda pandai mengatur keuangan Anda.<br/><br/>
			
			<h2>Apa saja langkahnya ?</h2>
			
			Langkah pertama untuk mengatur keuangan adalah 
			<ol>
			<li>menentukan posting - posting utama. Perkirakan apa saja biaya yang sering Anda keluarkan, dan berapa jumlahnya. Misalnya dalam tiap bulan ada fix cost yang anda keluarkan, perkiraan biaya listrik, cicilan, biaya trasnport, asuransi, dan lain lain. </li>
			<li>Tetapkan berapa besar uang yang Anda akan tabung, kemudian sisanya bisa Anda alokasikan untuk makanan, hiburan, weekend atau kebutuhan diluar kebutuhan primer.</li> 
			</ol>
			
			<h2>Bagaimana Expense Tracker membantu anda</h2>
			Website ini bertujuan untuk membantu Anda dalam membuat budget keuangan, mencatat semua jenis pengeluaran dan juga mencatat pendapatan yang Anda terima. Ini akan sangat membantu Anda untuk mengontrol secara pribadi pengeluaran, semua bisa Anda lakukan secara online. Anda bisa membuat kategori tersendiri untuk jenis biaya atau penggeluaran, selain itu Anda juga bisa memasukkan lokasi dimana pengeluaran itu dilakukan.
			
			</div>
		</div>
	</div>
</div>