<div class="col-md-9">
<?php
if (!empty($data))
{
	$str = "";
	$total_rows = $data['total_rows'];
	$list_data = $data['data'];
	
	$topic = $list_data[0]['Topic'];
	?>
	<h3><?php echo $topic; ?></h3>
	<?php 
	if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset , array('is_show_total_row' => TRUE));
	?>
	<table class="table table-hover">
	<?php
	$i = 0;
	if (is_numeric($page) && $page > 0) 
	{
		$i = ($page - 1) * $offset;
	}
	foreach($list_data as $key => $rs)
	{
		$i += 1;
		$note_ishelpful = '';
		if ($rs['IsHelpful']) $note_ishelpful = '<div class="b i" align="right"><span class="glyphicon glyphicon-flag" aria-hidden="true"></span> Marked as helpful</div><br/>';
		?>
	<tr class="<?php if ($rs['IsHelpful']) echo 'bg-info' ?>">
		<td width="160px">#<?php echo $i; ?><br/>by <span class="b"><a href="<?php echo base_url().'member/profile/'.$rs['MemberID']?>"><?php echo $rs['FullName']?></a></span></td>
		<td><div style="min-height:100px"><?php echo $note_ishelpful .' On '.date('d M Y h:s', strtotime($rs['CreatorDateTime'])).'<br/><br/>'. html_entity_decode($rs['Content'])?></div></td>
	</tr>
		<?php 
	}
	?>
	</table>
	<?php
	if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset , array('is_show_total_row' => TRUE));
}
?>
</div>

<div class="col-md-3 bg-primary" align="">
Related Article
<div style="">-- Anonym</div>
</div>

<div>
	<form>
		<textarea name=""></textarea><br/>
		<input type="submit" class="btn btn-submit" />
	</form>
</div>