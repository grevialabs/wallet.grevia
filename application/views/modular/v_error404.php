<?php 
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = NOT_FOUND;
$BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;
$PAGE_HEADER = $PAGE;
?>
<div class='talCnt'>
	<br/><br/>
	<i class="fa fa-frown-o fa-5x" aria-hidden="true"></i>
	<p>
	Oopss... Halaman yang anda cari tidak ditemukan.
	</p>
</div>