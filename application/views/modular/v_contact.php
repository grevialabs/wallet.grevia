<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = CONTACT;
$BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;;
$PAGE_HEADER = $PAGE;

$gInfo = '';
if($this->input->post('hdnSubmit') == '1'){
	
	$name = post('tName');
	$email = filter($this->input->post('tEmail'));
	$subject = filter($this->input->post('tSubject'));
	$message = nl2br(htmlentities($this->input->post('tMessage')));
	if(!is_filled($name)){
		$gInfo .= "Name must be filled.";
	}elseif(!is_filled($email)){
		$gInfo .= "Email must be filled.";
	}elseif(!is_filled($subject)){
		$gInfo .= "Subject must be filled.";
	}elseif(!is_filled($message)){
		$gInfo .= "Message must be filled.";
	}else{
		$message = '
		<html>
		<body>
		<table style="padding:15px" cellpadding="10" cellspacing="5" border="1">
		<tr>
			<td width="80px">Name</td>
			<td width="10px"> </td>
			<td>'.$name.'</td>
		</tr>
		<tr>
			<td width="80px">Email</td>
			<td width="10px"> </td>
			<td>'.$email.'</td>
		</tr>
		<tr>
			<td width="80px">Subject</td>
			<td width="10px"> </td>
			<td>'.$subject.'</td>
		</tr>
		<tr>
			<td width="80px">Name</td>
			<td width="10px"> </td>
			<td>'.$message.'</td>
		</tr>
		</table>
		</body>
		</html>
		';
		$to = 'rusdi@grevia.com';
		$headers = "From: Grevia Wallet<wallet@grevia.com> \r\n";
		//$headers = "Cc: rusdi.karsandi@gmail.com \r\n";
		$headers .= "Reply-To: \r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		if (!is_internal()) 
		{
			$send = mail($to,$subject,$message,$headers);
			if ($send) $gInfo = "Thank you for your email, we will respond in 2 x 24 hour.";
			else $gInfo = "Sorry, error connection. Please try again.";
		} else {
			$gInfo = $message;
		}
		
	}
}

?>
<div class="jumbotron bgPch" >
	<div class="container">
		<div class="rows min-height-250">
			<div class="col-sm-12">
			<?php if (isset($BREADCRUMB)) echo $BREADCRUMB.'' ?>
			<?php if (isset($PAGE_HEADER)) echo '<h2>'.$PAGE_HEADER.'</h2>' ?>
			
			Silakan mengisi form dibawah ini untuk menghubungi tim kami. <i class="fa fa-question-circle pointer" data-toggle="popover" data-trigger="hover" title="Panduan Mengisi" data-content="Silakan mengisi seluruh field dengan lengkap"></i>
				<br/><br/><?php if (is_filled($gInfo)) echo message($gInfo)."";?>
				<form method="post">
				<input type="text" name="tName" class="input wdtFul" placeholder="Name" required/><br/><br/>
				<input type="text" name="tEmail" class="input wdtFul" class="input round bdrGry" placeholder="Email" required/><br/><br/>
				<input type="text" name="tSubject" class="input wdtFul" placeholder="Subject" value="<?php if (isset($_GET['subject'])) echo $_GET['subject']?>" required/><br/><br/>
				<textarea placeholder="Message here..." name="tMessage" rows="8" class="input wdtFul" required></textarea><br/><br/>
				<input type="hidden" name="hdnSubmit" value="1"/>
				<input type="submit" class="btn btn-success" value="<?php echo SUBMIT?>"/>
				</form><br/>
	
			</div>
		</div>
	</div>
</div>