<?php 

/* start paging */
$page = 1;
if (isset($_GET['page'])) $page = $_GET['page'];
if (OFFSET) $offset = OFFSET;
//$offset = 30;
$param['offset'] = $offset;

/* end paging */

$q = '';
if ($this->input->get("q")) $q = mysql_real_escape_string(urldecode($this->input->get("q")));
$str = "";

if (isset($q))
{
	$param = array('keyword' => $q);
	$data = $this->article_model->get_list($param);
	$obj_list_article = $data['data'];
	if( !empty($obj_list_article) ){
		/* AUTO REDIRECT TO ARTICLE IF ONLY 1 RESULT */
		// if( count($obj_list_article) == 1 ){
			// foreach($obj_list_article as $obj){
				// redirect( friendly_url("/showarticle.php?aid=".$obj['article_id()."&title=".$obj['title()) );
			// }
		// }
		$i = 1;

		$str.= '<div class="bg-info padLrg"><i class="fa fa-info"></i>&nbsp; Ditemukan '.$data['total_rows'].' '.$q.' artikel terkait keyword <span class="b">'. $q .' </span> </div>';
		foreach ($obj_list_article as $obj) {
			if ($obj['is_publish']) {
				/* COLORING BORDER ON TOP */
				$bgcolor = "";
				if($i > 1)$bgcolor = " bdrTopGry";
				$friendly_url = base_url()."showarticle/".$obj['article_id']."/".$obj['slug'];
				
				if (is_internal()) {
					//OFFLINE
					$imageUrl = base_url()."asset/plugin/autothumbnail/index.php?src=staging/asset/images/article/".$obj['image']."&width=340";
				} else {
					// ONLINE
					$imageUrl = base_url()."asset/plugin/autothumbnail/index.php?src=asset/images/article/".$obj['image']."&width=340";
					//http://www.grevia.com/plugin/autothumbnail/index.php?src=data/article/2013.jpg&width=500
				}
				$str.= "
				<div class='$bgcolor'><br/>
				<div class='col-md-5'><a href='".$friendly_url."'><img data-original='".$imageUrl."' class='lazy img-responsive wdtFul' style='min-height:250px'/></a><br/>
				</div>
				<div class='col-md-7 padLrg'>
				<a href='".$friendly_url."' class='b clrBlk f18'>".$obj['title']."</a><br/><br/>
				<span class='fntBld fntMed'>".$obj['short_description']."</span>
				</div></div><br/>
				<div class='clearfix'></div>
				";
				$i++;
			}
		}
	}else{
		$str.= "Tag artikel tidak ditemukan.";
	}
}else{
	$str.= "Tag artikel tidak ditemukan.";
}
?>
<div class="col-xs-9">
<?php
echo $str;
?>
</div>
<div class="col-xs-3">
	<?php echo $SIDEBAR_RIGHT; ?>
</div>
