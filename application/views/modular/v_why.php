<section class="clrWht b bgmoonlight" style="padding-bottom:35px">
	<div class="container ">
		<div class="rows bitter" style="margin-top:125px;">
			<div class="col-sm-12">
				<br/><h1 class="wow fadeInUp talCnt padNon marNon">Pelacak keuangan</h1>
				<h2 class="wow fadeInUp talCnt i padNon marNon">Expense Tracker</h2>
				
				<div class="col-sm-2"></div>
				<div class="col-sm-2 talRgt wow bounceInLeft">
					<i class="fa fa-laptop fa-5x"></i>
				</div>
				<div class="col-sm-8 wow bounceInRight talLft">
					<h3 class="padNon marNon">Aplikasi pencatat keuangan personal anda</h3>
				</div>
				
				<div class="col-sm-12 wow bounceInRight talCnt">
					<a class="btn btn-info" href="<?php echo base_url().'register'?>">Registrasi sekarang</a>
				</div>
			</div>
			
			<div class="col-sm-12"><br/></div>
			
			
		</div>
	</div>
</section>

<div class="container">
	<div class="rows bitter" style="min-height:250px;margin-top:45px">
		<div class="col-sm-4">
			<div class="b talCnt wow fadeInUp">
				<span class="fa-stack fa-4x">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-desktop fa-stack-1x text-primary"></i>
				</span><br/>
				<p class="b">Catat semua histori Finansial kamu.</p>
			</div>
			<div class="subindex wow fadeInUp">Data kamu akan akan kami olah dan tersimpan di histori Finansial.</div>
		</div>
		<div class="col-sm-4">
			<div class="b talCnt wow fadeInUp">
				<span class="fa-stack fa-4x">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-tachometer fa-stack-1x clrGrn"></i>
				</span>
				<p class="b">Pengontrol Keuangan.</p>
			</div>
			<div class="subindex wow fadeInUp">Kamu bisa mengukur semua pengeluaran & pemasukan kamu secara mendetail.</div>
		</div>
		<div class="col-sm-4">
			<div class="b talCnt wow fadeInUp">
				<span class="fa-stack fa-4x">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-calendar fa-stack-1x text-warning"></i>
				</span><br/>
				<p class="b upper">Pengingat tagihan jatuh tempo</p>
			</div>
			<div class="subindex wow fadeInUp">Semua tagihan kamu tidak akan pernah telat dibayar lagi dengan fitur <b>Reminder</b> kami.</div>
		</div>
		
		<div class="col-sm-12">
			<br/><br/>
		</div>
		
		<div class="col-sm-2">
		</div>
		<div class="col-sm-4">
			<div class="b talCnt wow fadeInUp">
				<span class="fa-stack fa-4x">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-line-chart fa-stack-1x clrWht"></i>
				</span><br/>
				<p class="b">Laporan pengeluaran per kategori yang mendetail.</p>
			</div>
			<div class="subindex wow fadeInUp">Laporan lengkap mendetail agar kamu bisa mengambil keputusan Finansial yang akurat.</div>
		</div>
		<div class="col-sm-4">
			<div class="b talCnt wow fadeInUp">
				<span class="fa-stack fa-4x">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-cloud-upload fa-stack-1x text-primary"></i>
				</span><br/>
				<p class="b">Akses laporan keuangan di Cloud.</p>
			</div>
			<div class="subindex wow fadeInUp">Input dan lihat semuanya melalui aplikasi Grevia Wallet.</div>
		</div>
		<div class="col-sm-2">
		</div>
		
		<div class="col-sm-12">
			<br/><br/>
			<h2>Mengapa harus mengatur keuangan</h2>
			Money Budgeting adalah sebuah tindakan untuk merencanakan atau mengalokasikan ke mana uang hasil jerih payah Anda akan berlabuh. Karena sebanyak apapun penghasilan yang Anda terima tanpa pengolahan yang baik, percuma saja. Hidup akan menjadi lebih tenang tenang dan tak kesulitan masalah keuangan di masa mendatang, bila Anda pandai mengatur keuangan Anda.<br/><br/>
			
			<h2>Apa saja langkahnya ?</h2>
			
			Langkah pertama untuk mengatur keuangan adalah 
			<ol>
			<li>menentukan posting - posting utama. Perkirakan apa saja biaya yang sering Anda keluarkan, dan berapa jumlahnya. Misalnya dalam tiap bulan ada fix cost yang anda keluarkan, perkiraan biaya listrik, cicilan, biaya trasnport, asuransi, dan lain lain. </li>
			<li>Tetapkan berapa besar uang yang Anda akan tabung, kemudian sisanya bisa Anda alokasikan untuk makanan, hiburan, weekend atau kebutuhan diluar kebutuhan primer.</li> 
			</ol>
			
			<h2>Bagaimana Expense Tracker membantu anda</h2>
			Website ini bertujuan untuk membantu Anda dalam membuat budget keuangan, mencatat semua jenis pengeluaran dan juga mencatat pendapatan yang Anda terima. Ini akan sangat membantu Anda untuk mengontrol secara pribadi pengeluaran, semua bisa Anda lakukan secara online. Anda bisa membuat kategori tersendiri untuk jenis biaya atau penggeluaran, selain itu Anda juga bisa memasukkan lokasi dimana pengeluaran itu dilakukan.
		</div>
	</div>
</div>
<!--
<div class="container">
	<div class="">
		<div class="row">
			<div class="col-sm-6">
				<h2 class="wow fadeInUp">Mengapa kami</h2><br/>
				<ul class="wow bounceInUp">
					<li>Lihat pos pengeluaran anda per-kategori</li>
					<li>Laporan detail pemasukan & pengeluaran anda</li>
					<li>Simulasi keuangan anda secara akurat</li>
					<li>Fitur reminder untuk mengingat jatuh tempo tagihan anda</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<h2 class="wow fadeInUp">FAQ</h2><br/>
				<ul class="wow bounceInUp">
					<li>Auto generate report</li>
					<li>Fasten recruitment process</li>
					<li>Anytime test</li>
				</ul>
			</div>
			
			<div class="col-sm-4">
				<i class="fa fa-shopping-bag"></i>
			</div>
			<div class="col-sm-4">
				<i class="fa fa-bar-chart"></i>
			</div>
			<div class="col-sm-4">
				<i class="fa fa-calculator"></i>
			</div>
		</div>
	</div>
</div>

<div class="rows bitter" style="min-height:250px;margin-top:45px">
	<div class="col-sm-12">
		<div class="rows">
			<div class=" col-sm-6">
				<div class="b">
					<span class="fa-stack fa-4x">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-flask fa-stack-1x text-primary"></i>
					</span><br/>
				</div>
			</div>
			<div class="col-sm-4 col-sm-offset-2">		
				<div class="subindex">Setelah registrasi, kamu bisa mengikuti berbagai tes untuk mencari tahu kelebihan dan kekurangan personal kamu. Setelah registrasi, kamu bisa mengikuti berbagai tes untuk mencari tahu kelebihan dan kekurangan personal kamu. Setelah registrasi, kamu bisa mengikuti berbagai tes untuk mencari tahu kelebihan dan kekurangan personal kamu.</div>
			</div>
		</div>
	</div>
	
	<div class="col-sm-12">
		<div class="rows">
			<div class="col-sm-offset-2 col-sm-4">
				<p class="b">Isi tes di Job Talento</p>
				<div class="subindex">JobTalento hadir untuk membantu kamu menemukan passion dan berkarir di bidang yang kamu sukai. JobTalento hadir untuk membantu kamu menemukan passion dan berkarir di bidang yang kamu sukai. JobTalento hadir untuk membantu kamu menemukan passion dan berkarir di bidang yang kamu sukai.</div>
			</div>
			<div class="col-sm-4 col-sm-offset-2">		
				<div class="b">
					<span class="fa-stack fa-4x">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-tasks fa-stack-1x text-primary"></i>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
-->
<!--
<div class="container">
	<div class="rows bitter" style="min-height:250px;margin-top:45px">
		<h2>FAQ</h2>
		<div class="col-sm-4">
		
			<div class="spoiler">+ Apakah hasil tes Jobtalento dijamin akurat ?</div>
			<p>- Ya, kami berusaha memberikan hasil yang paling sesuai dengan tes yang telah anda ikuti.</p>
		
			<ul>
				<li>+ Siapakah pembuat alat tes & bagaimana track record mereka ?</li>
				<li>- Kami adalah psikolog dengan sertifikasi xxx.</li>
			</ul>
			<ul>
				<li>+ Apakah saya dijamin mendapat pekerjaan jika sudah melakukan tes di Jobtalento ?</li>
				<li>- Kami hanya bisa menyarankan blablabla.</li>
			</ul>
		</div>
		
	</div>
</div>
-->
<div class="clearfix"></div><br/>