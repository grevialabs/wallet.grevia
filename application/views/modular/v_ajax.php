<?php
if ($_POST && is_filled($_POST['do'])) {
	
	$str = '';
	$do = $_POST['do'];
	
	// SUBSCRIBE
	if ($do == "subscribe" && is_filled($_POST['email'])) {
		$tmp['name'] = $_POST['name'];
		$tmp['email'] = $_POST['email'];
		
		if (!$this->subscribe_model->get($tmp)) {
			$save = $this->subscribe_model->save($tmp);
			if ($save) $str = INFO_SAVE_SUCCESS;
			else $str = INFO_ERROR_OCCURED;
		} else {
			$str = INFO_EMAIL_SUBSCRIBE_EXIST;
		}
	} else if($do == "journey" && is_filled($_POST['lat']) && is_filled($_POST['lng'])) {
		//
		$lat = $_POST['lat'];
		$lng = $_POST['lng'];

		// $url = sprintf("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyA_4eVQFLKDCHOOSIkkSJRXj7WhCz_IqvQ&latlng=%s,%s", $lat, $lng);
		$url = sprintf("https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s", $lat, $lng);

		$content = file_get_contents($url); // get json content

		$metadata = json_decode($content, true); //json decoder

		if(count($metadata['results']) > 0) {
			// for format example look at url
			// https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452
			$result = $metadata['results'][0];

			// save it in db for further use
			echo $result['formatted_address'];

		}
		else {
			// no results returned
			echo "kosong cuy";
		}
	} else if($do == "journeytest" && is_filled($_GET['lat']) && is_filled($_GET['lng'])) {
		//
		$lat = $_GET['lat'];
		$lng = $_GET['lng'];

		$url = sprintf("https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&key=AIzaSyB4U_wSzpo-pN1UTVpvdTYVG1RjuiVRHss", $lat, $lng);
		// $url = sprintf("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyA_4eVQFLKDCHOOSIkkSJRXj7WhCz_IqvQ&latlng=%s,%s", $lat, $lng);
		// $url = sprintf("https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s", $lat, $lng);

		$content = file_get_contents($url); // get json content

		$metadata = json_decode($content, true); //json decoder

		if(count($metadata['results']) > 0) {
			// for format example look at url
			// https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyA_4eVQFLKDCHOOSIkkSJRXj7WhCz_IqvQ&latlng=40.714224,-73.961452
			$result = $metadata['results'][0];

			// save it in db for further use
			echo $result['formatted_address'];

		}
		else {
			// no results returned
			echo "kosong cuy";
		}
	} else {
		$str = INFO_DATA_INVALID;
	}
} else {
	header_status(404);
	header_status(200);
	die;
}
//$str = 'die';
echo $str;
die;
?>