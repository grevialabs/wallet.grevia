<?php 
global $PAGE_TITLE;

$PAGE_TITLE = REGISTER.' - '.DEFAULT_PAGE_TITLE;

if (is_member()) redirect(base_url().'expense/report');


/*|
  | REGISTER
*/
if ($_POST) {
	$gInfoReg = NULL;
	if(post('hdnRegister') == 1) {
		
		if(!is_filled($_POST['first_name'])) {
			$gInfoReg.= NAME.' '.MUST_FILLED;
		} else if(!is_filled($_POST['email'])) {
			$gInfoReg.= EMAIL.' '.MUST_FILLED;
		} else if(!is_filled($_POST['password'])) {
			$gInfoReg.= PASSWORD.' '.MUST_FILLED;
		} else if(!is_filled($_POST['gender'])) {
			$gInfoReg.= PLEASE_SELECT.' '.NAME;
		} else if(!is_filled($_POST['confirm_password'])) {
			$gInfoReg.= 'Konfirmasi password harus diisi';
		} else {
			// var_dump($_POST);
			// die;
			$objMember = NULL;
			$objMember = $this->member_model->get(
				array('email' => $_POST['email'])
			);
			if (empty($objMember)) {
				$email = $_POST['email'];
				$full_name = $_POST['first_name'].' '.$_POST['last_name'];
				$active_code = genRandomString(30);
					
				$param = array(
					'first_name' => $_POST['first_name'],
					'last_name' => $_POST['last_name'],
					'email' => $_POST['email'],
					'thepassword' => encrypt($_POST['password']),
					'gender' => $_POST['gender'],
					'dob' => $_POST['dob'],
					'active_code' => $active_code,
					'is_admin' => 0,
					'is_subscribe' => 1,
					'is_active' => 0,
					'is_deleted' => 0,
				);
				
				$save = $this->member_model->save($param);
				$save = TRUE;
				if($save){
					
					
					// benerin lagi nih
					$email_member ='<html><head></head><body>';
					$email_member.='<div style="padding:15px">'.DEAR.' '.$full_name.',<br/><br/>'.YOU_GOT_THIS_EMAIL_BECAUSE_YOU_REGISTER_IN.' '.DEFAULT_PAGE_TITLE.'.';
					$email_member.= PLEASE_CLICK.' <a href="'.base_url().'activation?do=submit&email='.$email.'&code='.$active_code.'">'.THIS_LINK.'</a> '.OR_COPY_PASTE_THIS_URL_TO_BROWSER.' '.base_url().'activation?do=submit&email='.$email.'&code='.$active_code.' '.TO_CONFIRM_YOUR_ACCOUNT.'<br/><br/>';
					$email_member.='<p>'.WARM_REGARDS.'.<br/>'.JOBTALENTO_TEAM.'.</p></div></body></html>';
					if (!is_internal()) {
						// SEND EMAIL HERE
						$to = $email;
						$subject = ACTIVATION_ACCOUNT_MEMBER;
						$headers = "From: ".JOBTALENTO_TEAM.".<noreply@jobtalento.com> \r\n";
						$headers .= "Reply-To: \r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
						$send = mail($to,$subject,$email_member,$headers);
					} else {
						unset($_POST);
						$gInfoReg.= REGISTRATION_SUCCESS.' '.PLEASE_LOGIN.' '.$email_member;
					}
					
					$member = $this->expense_model->get_member(array('email' => $email));
					$encrypt = encrypt($member['member_id']."#".$member['first_name']." ".$member['last_name']."#".$member['email']."#".$member['is_admin']);
				
					// DEFAULT one month login
					$expiredCookie = time() + 30 * 86400;
					
					// 24 hour expired cookie on default
					//$expiredCookie = time() + 24 * 3600;
					//if (post('l_RememberMe')) $expiredCookie = time() + 30 * 86400;
					
					setcookie('hash', $encrypt, $expiredCookie, base_url());
					
					redirect(base_url().'expense/report');
					
				} else {
					$gInfoReg = REGISTRATION_FAIL_PLEASE_TRY_AGAIN;
				}
			}else{
				$gInfoReg = EMAIL.' <b>'.$_POST['email'].'</b> '. HAS_REGISTERED.' '.PLEASE_USE_ANOTHER_EMAIL;
			}
		}
	}
}
?>
<div class="col-sm-3">
&nbsp;
</div>
<div class='col-sm-6'>
	<h1 class="title-header">&nbsp;<?php echo REGISTRATION?></h1><hr/>
	<div class='col-sm-2'>
		<div id='fblogin'><fb:login-button scope="public_profile,email" size="large" onlogin="checkLoginState();"></fb:login-button></div>
	</div>
	<!--
	<div class='col-sm-2'>
		<div id="gConnect">
			<button class="g-signin"
				data-scope="https://www.googleapis.com/auth/plus.profile.emails.read"
				data-requestvisibleactions="http://schemas.google.com/AddActivity"
				data-clientId="<?php //echo GOOGLE_CLIENT_ID ?>"
				data-callback="onSignInCallback"
				data-theme="dark"
				data-cookiepolicy="single_host_origin">
			</button>
		</div>
	</div>
	-->
	<div class='clearfix'></div>
	
	<form method="post" id="frmReg" class="form-group">
		<div class="errReg"></div>
		<?php // if (isset($gInfoReg))echo message($gInfoReg); ?>
		<div class="input-group margin-bottom-sm br">
			<span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
			<div class='col-sm-6 padNon' style="padding-right:5px">
			<input class="form-control" type="text" placeholder="<?php echo FIRST_NAME?>" name="f_first_name" date-toggle="senpai" value='<?php if (isset($_POST['first_name'])) echo $_POST['first_name']?>' required/>
			</div>
			<div class='col-sm-6 padNon' >
			<input class="form-control" type="text" placeholder="<?php echo LAST_NAME?>" name="f_last_name" date-toggle="senpai" value='<?php if (isset($_POST['last_name'])) echo $_POST['last_name']?>' required/>
			</div>
		</div>
		<?php // if (isset($gInfoReg))echo message($gInfoReg); ?>

		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
		  <input class="form-control" type="email" placeholder="<?php echo EMAIL?>" name="f_email" value='<?php if(isset($_POST['email'])) echo $_POST['email']?>' required/>
		</div>
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
		  <input class="form-control" type="password" placeholder="<?php echo PASSWORD?>" name="f_password" minlength="4" required/>
		</div>
		
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
		  <input class="form-control" type="password" placeholder="<?php echo CONFIRM.' '.PASSWORD?>" name="f_confirm_password" minlength="4" required/>
		</div>
		
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-neuter fa-fw"></i></span>
		  <select name="f_gender" id='f_gender' placeholder="Gender" title="<?php echo SELECT_GENDER?>" class="form-control" required>
			<option value="1" <?php if(isset($_POST['gender']) && $_POST['gender'] == 1)echo 'selected'?>><?php echo MALE?></option>
			<option value="2" <?php if(isset($_POST['gender']) && $_POST['gender'] == 2)echo 'selected'?>><?php echo FEMALE?></option>
		  </select>
		</div>
		
		<div class="form-group form-group-md padTop10 padNon br" >
			<div class="col-sm-12 padTop10 br talCnt"><input type='hidden' name='hdnRegister' value='1'/><button value="Register" class="btn btn-info wdtFul"/><?php echo REGISTER?></button></div>
		</div>
		<div class='clearfix'></div><br/>
		
		<?php if (isset($gInfoReg))echo print_message($gInfoReg); ?>
		
	</form>
</div>
<div class="col-sm-3">
&nbsp;
</div>
