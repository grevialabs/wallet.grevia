<?php 
global $PAGE_TITLE;

$PAGE_TITLE = LOGIN.' - '.DEFAULT_PAGE_TITLE;

if (is_member()) redirect(base_url().'expense/report');

/*|
  | LOGIN
*/
if ($this->input->post('hdnLogin')) 
{
	$gInfo = '';
	$email = filter(post('l_email'));
	$password = filter(post('l_password'));
	// debug($email);
	// die;
	if(isset($email) && isset($password))
	{
		$member = $this->expense_model->get_member(array('email' => $email));
		
		if(!empty($member))
		{
			if($member['thepassword'] == encrypt($password))
			{
				$encrypt = encrypt($member['member_id']."#".$member['first_name']." ".$member['last_name']."#".$member['email']."#".$member['is_admin']);
				
				// debug($encrypt);
				
				// DEFAULT one year login
				$expiredCookie = time() + 12 * 30 * 86400;
				
				// 24 hour expired cookie on default
				//$expiredCookie = time() + 24 * 3600;
				//if (post('l_RememberMe')) $expiredCookie = time() + 30 * 86400;
				
				setcookie('hash', $encrypt, $expiredCookie, '/');

				// redirect to reference url if exist
				if ($this->input->get('url')) 
				{
					$url = $this->input->get('url');
					redirect(base_url().urldecode($url));
				}
				else
				{
					redirect('expense/report');
				}
			}
			else
			{
				$gInfo = EMAIL.' / '.PASSWORD.' '.NOT_VALID;
			}
		}
		else
		{
			$gInfo = EMAIL.' '.NOT_FOUND;
		}
	}
}
?>
<div class="col-sm-4">
&nbsp;
</div>
<div class='col-sm-4'>
	<h1 class="title-header">&nbsp;<?php echo LOGIN?> Expense</h1><hr/>
	<div class="errLog"></div>
	<form class='form-horizontal form_submit' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<div class='col-sm-2'>
				&nbsp;
			</div>
			<div class='col-sm-8'><?php echo EMAIL?><br/>
			<input type='email' class='form-control' name='l_email' id='l_email' placeholder='<?php echo EMAIL?>' value='<?php if ($this->input->post('l_email')) echo filter(post('l_email'))?>' required></div>
			<div class='col-sm-2'>
				&nbsp;
			</div>
		</div>
		<div class='form-group form-group-sm'>
			<div class='col-sm-2'>
				&nbsp;
			</div>
			<div class='col-sm-8'>
			<?php echo PASSWORD?><br/>
			<input type='password' class='form-control' name='l_password' id='l_password' placeholder='<?php echo PASSWORD?>' required></div>
			<div class='col-sm-2'>
				&nbsp;
			</div>
		</div>
		<div class='form-group'>
			<div class='col-sm-2'>
				&nbsp;
			</div>
			<div class='col-sm-8'>
			
				<div class='talRgt'><a href="<?php echo base_url()?>forgotpassword"><?php echo MENU_FORGOT_PASSWORD?></a> | <a href="<?php echo base_url()?>register"><?php echo MENU_REGISTER?></a></div><br/>
			
				<input type="hidden" name="hdnLogin" value="1"/><button type="submit" value="" class="btn btn-success" style="width:150px"/><?php echo LOGIN?></button>
			</div>
			<div class='col-sm-2'>
				&nbsp;
			</div>
		</div>
	</form>
	
	<?php
	if (isset($gInfo)) echo print_balloon_message($gInfo);
	?>
</div>
<div class="col-sm-4">
&nbsp;
</div>
