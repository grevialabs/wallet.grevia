<div class="col-md-9">
<?php
//$data = (array) json_decode($data);
if (!empty($data))
{
	$str = "";
	$total_rows = $data['total_rows'];
	$list_data = $data['data'];
	?>
	<table class="table table-hover">
	<tr>
		<td width=1>#</td>
		<td>Title</td>
		<td>Viewed</td>
	</tr>
	<?php
	$i = 0;
	if (is_numeric($page) && $page > 0) 
	{
		$i = ($page - 1) * $offset;
	}
	foreach($list_data as $key => $rs)
	{
		$rs = (array) $rs;
		$i += 1;
		$title = $rs['Title'];
		$slug = str_replace(' ','-',strtolower($title));
		?>
		<tr>
		<td><?php echo $i ; ?></td>
		<td><a href="<?php echo base_url().'showarticle/'.$rs['ArticleID'].'/'.$slug ?>"><?php echo $title?></a></td>
		<td><?php echo $rs['View']; ?></td>
		</tr>
		<?php 
	}
	?>
	</table>
	<?php
	if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
}


?>
</div>
<div class="col-md-3 ">

	<div class="list-group">
		<a class="list-group-item <?php if(!$this->uri->segment(2) || $this->uri->segment(2) == 'all') echo 'active';?>" href="<?php echo base_url()?>articlecategory/all"><i class="fa fa-home fa-fw"></i>&nbsp; All</a>
	  <?php 
	  $list_article_category = $this->articlecategory_model->get_list();
	  $list_article_category = $list_article_category['data'];
	  foreach($list_article_category as $rsr) 
	  {
	  ?>
	  <a class="list-group-item <?php if($this->uri->segment(2) && $this->uri->segment(2) == $rsr['Slug']) echo 'active';?>" href="<?php echo base_url().'articlecategory/'.$rsr['Slug'];?>"><i class="fa fa-home fa-fw"></i>&nbsp; <?php echo $rsr['CategoryName']?></a>
	  <?php } ?>
	</div>

</div>
