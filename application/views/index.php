<?php
global $PAGE, $PAGE_TITLE, $BREADCRUMB , $PAGE_HEADER;
 
if (!isset($PAGE)) $PAGE = DEFAULT_PAGE_TITLE . ' - '.DEFAULT_PAGE_DESCRIPTION;
if (!isset($PAGE_TITLE)) $PAGE_TITLE = $PAGE;

$base_url = base_url();
$vrs = '202207171';


global $current_segment;
if ($this->uri->segment(2)) $current_segment = $this->uri->segment(2);


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="google-site-verification" content="NM1j6BG3n_-7PcXeKwQnS6R-JABiEe2PL7Lm7M_Tbjo" />
	<meta charset="utf-8">
	<title><?php echo $PAGE_TITLE ?></title>
	<link rel="shortcut icon" href="<?php echo $base_url?>favicon.ico?v=<?php echo $vrs; ?>" type="image/x-icon">
	<link rel="icon" href="<?php echo $base_url?>favicon.ico?v=201706221" type="image/x-icon">
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!-- Latest compiled and minified CSS 
		<link rel="stylesheet" href="http://localhost/ci/assets/css/bootstrap.min.css">
		-->

	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/bootstrap.css?v=201706221"/>
	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/font-awesome-animation.min.css?v=20160708"/>
		
	<!-- Include required JS files -->
	<link rel='stylesheet' href='<?php echo $base_url; ?>asset/css/ui-lightness/jquery-ui-1.10.4.css?v=201706221' />

	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/bootstrap-theme.css?v=201706221"/>
	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/bootstrap-theme.css.map?v=201706221"/>
	<!--<link rel="stylesheet" href="<?php echo $base_url?>asset/css/bootstrap-theme.min.css"/>-->
	
    <link href="<?php echo $base_url?>asset/css/font-awesome.css?v=201706221" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/bootstrap-social.css?v=201706221"/>
    <link rel="stylesheet" href="<?php echo $base_url?>asset/css/style.css?v=<?php echo $vrs; ?>"/>
    <link rel="stylesheet" href="<?php echo $base_url?>asset/css/animate.css?v=201706221"/>

	<script type="text/javascript" src='<?php echo $base_url; ?>asset/js/jquery.1.11.2.min.js?v=201706221'></script>
	<script type="text/javascript" src='<?php echo $base_url; ?>asset/js/jquery-ui-1.10.4.js?v=201706221'></script>
	<script type="text/javascript" src='<?php echo $base_url; ?>asset/js/jquery.validate.js?v=201706221'></script>
	<script type="text/javascript" src='<?php echo $base_url; ?>asset/js/ui/jquery.ui.core.js?v=201706221'></script>
	<script type="text/javascript" src="<?php echo $base_url; ?>asset/js/bootstrap.js?v=201706221"></script>
	<script type="text/javascript" src="<?php echo $base_url; ?>asset/js/scrollReveal.js?v=201706221"></script>
	<script type="text/javascript" src="<?php echo $base_url; ?>asset/js/jquery.backstretch.min.js?v=201706221"></script>
	<!-- <script type="text/javascript" src="<?php echo $base_url; ?>asset/js/lazyload/jquery.lazyload.js?v=1.9.1"></script> -->
	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/colorbox.css?v=201706221" />
	<script src="<?php echo $base_url?>asset/js/jquery.colorbox.js?v=201706221"></script>
	<script src="<?php echo $base_url?>asset/js/wow.min.js?v=201706221"></script>
	<script src="<?php echo $base_url?>asset/js/moment.min.js?v=201706221"></script>
	<script src="<?php echo $base_url?>asset/js/jquery.maskMoney.min.js?v=20190214" type="text/javascript"></script>
	
	<script type="text/javascript">
	$(document).ready(function(){	
		$(function() {	
			//$.backstretch("http://dl.dropbox.com/u/515046/www/garfield-interior.jpg");
			// $("img.lazy").lazyload({ effect: "fadeIn" });
			
			$('[data-toggle="popover"]').popover();
			// $('.popover').popover(options);
			$( ".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true,
				showAnim: "slideDown",
				yearRange: '1950:+10' 
			});
			
			$(".numeric").keypress(function(e){
				if (e.which != 8 && e.which != 13 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					return false;
				}
			});
			
			
		});
		
	});
	
	window.sr = new scrollReveal();
	new WOW().init();
	
	</script>
	
	<link rel="stylesheet" href="<?php echo $base_url.'asset/css/chosen/'?>chosen.min.css?v=201706221">
	<style type="text/css" media="all">
	/* fix rtl for demo */
	.chosen-rtl .chosen-drop { left: -9000px; }
	</style>
</head>
<?php 
global $fetch_class, $fetch_method;
$fetch_class = $this->router->fetch_class();
$fetch_method = $this->router->fetch_method();
function show_active_menu($menu_title) 
{
	global $fetch_class, $fetch_method;
	$str = '';
	if ($fetch_class == 'modular') 
	{
		if ($fetch_method == $menu_title) 
		{
			$str = 'btn btn-info';
			$str = '<span class="sr-only">(current)</span>';
		}
	}
	return $str;
}

?>
<body>
<?php include_once 'navheader.php';?>

<div class="container">	
	<div class="row">
		<div class="col-md-12" style="min-height:500px"><br/>
			<?php if (isset($BREADCRUMB)) echo $BREADCRUMB.'' ?>
			<?php if (isset($PAGE_HEADER)) echo '<h2>'.$PAGE_HEADER.'</h2>' ?>
			<!--<h1>Selamat datang</h1>
			<div class="alert alert-warning"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Error:</span>&nbsp;Email / Password tidak valid.</div>-->
			<?php echo $CONTENT; ?>
		</div>
	</div>
</div>

<?php include_once 'navfooter.php';?>

<script src="<?php echo $base_url.'/asset/js/'?>chosen.jquery.min.js?v=201706221" type="text/javascript"></script>
<script type="text/javascript">
var config = {
  '.chosen-select'           : {allow_single_deselect:true, 
								
								},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}

$('.money').maskMoney({precision:0, thousand:',',allowNegative: false});

function togglebox(){
	if ($('.togglebox').prop('checked')) {
		$('.chkbox').prop('checked',true);
	}
	if (!$('.togglebox').prop('checked')) {
		$('.chkbox').prop('checked',false);
	}
}

function doConfirm(str = 'delete') {
	if (str == 'delete') {
		str = 'Yakin menghapus data ini ?'
	} else 
		str = 'Yakin melakukan aksi ini ?'
	return confirm(str)
}

function resubmit(target, obj){
	var url = obj.value;
	window.location = target + url;
}
</script>
</body>
</html>