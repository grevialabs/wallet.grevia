<?php 
$base_url = base_url().'expense';
$base_url_member = base_url().'member';
// $base_url = base_url();
?>
<!-- Sidebar start -->
<!--
<div>
	<a data-toggle="collapse" href="<?php echo $base_url.'/report'?>" id="menu-toggle"><span class="fa-stack fa-lg pull-left"><i class="fa fa-cloud-download fa-stack-1x "></i></span>Showmenu</a>
</div><hr/>
-->
<div id="sidebar-wrapper">
	<ul class="sidebar-nav nav-pills nav-stacked" id="menu">
		<li>
			<a href="#" class=" collapsed in" data-toggle="collapse" id="menu-toggle">
				<span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
			</a>
			<!--
			<a href="#" class="navbar-toggle collapse in" data-toggle="collapse" id="menu-toggle-20">
				<span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
			</a>
			-->
		</li>
		<li class="<?php echo member_active('expense/dashboard').member_active('dashboard')?>">
			<a href="<?php echo $base_url.'/dashboard'?>"><span class="fa-stack fa-lg pull-left"><i class="fa fa-dashboard fa-stack-1x "></i></span> Dashboard </a>
		</li>
		<!--
		example for accordion child menu
		<li>
			<a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span>Shortcut</a>
			<ul class="nav-pills nav-stacked" style="list-style-type:none;">
				<li><a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span>link1</a></li>
				<li><a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span>link2</a></li>
			</ul>
		</li>
		-->
		<li class="<?php echo member_active('expense/calendar').member_active('calendar')?>">
			<a href="<?php echo $base_url.'/calendar'?>"><span class="fa-stack fa-lg pull-left"><i class="fa fa-calendar fa-stack-1x "></i></span>Calendar</a>
		</li>
		<li class="<?php echo member_active('expense/report').member_active('report')?>">
			<a href="<?php echo $base_url.'/report'?>"><span class="fa-stack fa-lg pull-left"><i class="fa fa-cloud-download fa-stack-1x "></i></span>Report</a>
		</li>
		<li class="<?php echo member_active('expense/storage').member_active('storage')?>">
			<a href="<?php echo $base_url.'/storage'?>"><span class="fa-stack fa-lg pull-left"><i class="fa fa-inbox fa-stack-1x "></i></span>Storage</a>
		</li>
		<li class="<?php echo member_active('expense/search').member_active('search')?>">
			<a href="<?php echo $base_url.'/search'?>"> <span class="fa-stack fa-lg pull-left"><i class="fa fa-cart-plus fa-stack-1x "></i></span>Search</a>
		</li>
		<li class="<?php echo member_active('expense/category').member_active('category')?>" >
			<a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span>Masterdata <i class="fa fa-caret-down clrWht"></i></a>
			<ul class="nav-pills nav-stacked" style="list-style-type:none;">
				<li class="<?php echo member_active('expense/category').member_active('category')?>"><a href="<?php echo $base_url.'/category'?>"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span>Category</a></li>
				<li class="<?php echo member_active('expense/treasure').member_active('treasure')?>"><a href="<?php echo $base_url.'/treasure'?>"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span>Treasure</a></li>
				
				<li class="<?php echo member_active('expense/fund').member_active('fund')?>"><a href="<?php echo $base_url.'/fund'?>"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span>Fund</a></li>
			</ul>
		</li>
	</ul>
</div>
<!-- Sidebar end -->