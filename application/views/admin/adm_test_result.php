<?php 
global $PAGE, $PAGE_TITLE, $PAGE_HEADER;
global $param,$message,$get_test_id,$get_test_result_id,$show;

$PAGE = 'Admin Test Result';
$PAGE_HEADER = 'Admin Test Result<hr>';
$PAGE_TITLE = $PAGE;

$do = $get_id = $update_test = '';
$offset = OFFSET;
//$offset = 1;
$page = 1;

if ($this->input->get('page') && $this->input->get('page') > 1) $page = $this->input->get('page');
if (isset($_GET['do'])) $do = $_GET['do'];
if (isset($_GET['test_id'])) $get_test_id = $_GET['test_id'];
if (isset($_GET['test_result_id'])) $get_test_result_id = $_GET['test_result_id'];

$list_allow_show = array('header', 'detail', 'result');
if (isset($_GET['show']) && in_array($_GET['show'], $list_allow_show)=== TRUE) $show = $_GET['show']; 

/*
  | SAVE 
*/
if (post('btnInsert')) {
	
	// // INSERT TEST RESULT
	// if () 
	// {
		// $list_result_test_id = $_POST['result_test_id'];
		// $list_score_description = $_POST['score_description'];
		// $list_result_id = $_POST['test_result_id'];
		// $list_min_score = $_POST['min_score'];
		// $list_max_score = $_POST['max_score'];
		// for ($i = 0; $i < count($list_result_id); $i++)
		// {
			// $param = array(
				// 'test_id' => $list_result_test_id[$i],
				// 'score_description' => $list_score_description[$i],
				// 'min_score' => $list_min_score[$i],
				// 'max_score' => $list_max_score[$i]
			// );
			// $update_test_result = $this->test_model->update_test_result($list_result_id[$i], $param);
		// }
		// ($update_test_result)?$message['message_test_result'] = MESSAGE::UPDATE : $message['message_test_result'] = MESSAGE::ERROR;
		// $message['message_test_result'] = getMessage($message['message_test_result']);
	// } else {
		// $message['message'] = getMessage(MESSAGE::NOT_FOUND);
	// }
}

/*
  | UPDATE 
*/
if (post('btnUpdate')) {

	// UPDATE TEST RESULT
	$list_test_id = $_POST['test_id'];
	$list_score_description = $_POST['score_description'];
	$list_result_id = $_POST['test_result_id'];
	$list_min_score = $_POST['min_score'];
	$list_max_score = $_POST['max_score'];
	for ($i = 0; $i < count($list_result_id); $i++)
	{
		$param = array(
			'test_id' => $list_test_id[$i],
			'score_description' => $list_score_description[$i],
			'min_score' => $list_min_score[$i],
			'max_score' => $list_max_score[$i]
		);
		$update_test_result = $this->test_model->update_test_result($list_result_id[$i], $param);
	}
	($update_test_result)?$message['message_test_result'] = MESSAGE::UPDATE : $message['message_test_result'] = MESSAGE::ERROR;
	$message['message_test_result'] = getMessage($message['message_test_result']);
}

/*
  | DELETE 
*/
if ($do == "delete") {
	
	if (is_numeric($get_test_id)) {
		$delete = $this->member_model->delete($get_test_id);
		if ($delete) {
			($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
			$message['message'] = getMessage($message['message']);
		} else {
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}

/*
  | GROUP ACTION 
  | CHECKED BOX
*/
if (isset($_POST['btn_group_action'])) {
	if ($_POST['lst_group_action'] == "delete") {
		if (!empty($_POST['chkbox'])) { 
			$delete = false;
			// foreach (post('chkbox') as $key => $val) {
				// $delete = $this->test_model->delete($val);
			// }
			
			if ($delete) {
				($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
				$message['message'] = getMessage($message['message']);
			} else {
				$message['message'] = getMessage(MESSAGE::NOT_FOUND);
			}
			
		}
	}
}

// SEARCH
if (get('keyword')) {
	$param['keyword'] = get('keyword');
}

$param['paging'] = TRUE;
$param['offset'] = $offset;
$data = $this->test_model->get_list_test($param);

$total_rows = $data['total_rows'];
$list_data = $data['data'];
?>

<?php 

?>
<div class="col-xs-12">
<?php echo $SIDEMENUBAR ?>
</div>

<div class="col-xs-12">
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="talLft"><a href="<?php echo base_url()?>admin/<?php echo $MODULE?>/insert" class="btn btn-success br"><i class="fa fa-plus"></i>&nbsp; New <? echo $MODULE?></a></div><br/>
		<?php } ?>
	</div>
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="br talRgt">
			<form method="get">
			<input class="input input-sm" type="text" name="keyword" value="<?php if (get('keyword')) echo get('keyword')?>" placeholder="Keyword" />
			<button class="btn btn-info btn-sm" > <i class="fa fa-search-minus"></i> Filter</button>
			</form>
		</div>
		<?php } ?>
	</div>
	<div class="clearfix"></div>
	
	<?php if (isset($message['message'])) echo message($message['message'])?>
		
	<?php
	if ((!is_filled($do) && !is_filled($get_test_id)) || $do == "delete")
	{
		if (!empty($data))
		{
			$str = "";
			$total_rows = $data['total_rows'];
			$list_data = $data['data'];
			?>
			
			<form method="post">
			<table class="table table-hover table-striped table-responsive">
			<tr class="b">
				<td width=1><input type="checkbox" class="chkbox togglebox" onclick="togglebox()" /></td>
				<td width=1>#</td>
				<td>Title</td>
				<td>Detail Description</td>
				<td>CreatorID</td>
				<td>CreatorDate</td>
				<td class="talRgt" width="90px">Option</td>
			</tr>
			<?php
			$i = 0;
			if (is_numeric($page) && $page > 0) 
			{
				$i = ($page - 1) * $offset;
			}
			foreach($list_data as $key => $rs)
			{
				$rs = (array) $rs;
				$id = $rs['test_id'];
				$i += 1;
				$url = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
				?>
				<tr>
				<td><input type="checkbox" name="chkbox[]" id="chkbox[]" class="chkbox" value="<?php echo $id?>" /></td>
				<td><?php echo $i ; ?></td>
				<td><?php echo $rs['title']; ?></td>
				<td><?php echo $rs['detail_description']; ?></td>
				<td><?php echo $rs['creator_id']; ?></td>
				<td><?php echo $rs['creator_date']; ?></td>
				<td class="talRgt"><a href="<?php echo $url.'/edit/'.$id; ?>" title="Edit data" alt="Edit data"><i class="clrBlu fa fa-pencil-square-o fa-2x"></i></a> 
				<a href="<?php echo $url.'/delete/'.$id; ?>" onclick="return confirm('Yakin menghapus data ini ?')"><i class="clrRed fa fa-times fa-2x" title="Delete data" alt="Delete data"></i></a></td>
				</tr>
				<?php 
			}
			?>
			<tr>
				<td colspan="100%">
					<div id="group_action">With checked do <select class="input" name="lst_group_action"><option class="" value="delete">Delete</option></select>
					<button class="btn btn-default btn-sm" name="btn_group_action" id="btn_group_action"value="1">Action</button>
					</div>
				</td>
			</tr>
			</table>
			</form>
			<br/>
			
		<?php
			if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
		}
		else 
		{
			echo 'No data Exist';
		}
	}
	
	//if (isset($do) || isset($get_id))
	else
	{
		?>
		<form method="post">
			<h2 class=""><?php echo $MODULE?></h2>
			<?php 
			if (!empty($obj_test))
			{
				?>
				<div class="padLrg">
					<div class="col-sm-3 b">Title</div>
					<div class="col-sm-9"><?php echo $obj_test['title']?></div>
					
					<div class="col-sm-3 b">Detail Description</div>
					<div class="col-sm-9"><?php echo $obj_test['detail_description']?></div>
					
					<div class="clearfix"></div>
				</div>
				<?php
			}
			?>
			
			<!-- START TEST RESULT -->
			<?php 
			if (isset($message['message_test_result'])) echo print_message($message['message_test_result']).'<br/>';
			
			$obj_list_test_result = $this->test_model->get_list_test_result(array('test_id' => $get_test_id));
			$obj_list_test_result = $obj_list_test_result['data'];
			if (!empty($obj_list_test_result)) 
			{				
				foreach ($obj_list_test_result as $obj)
				{
					?>
					<div class="col-sm-2">
						Scoring (min to max)
					</div>
					<div class="col-sm-3">
						<input type="text" name="f_min_score[]" size="2" value="<?php echo $obj['min_score']?>"/> - 
						<input type="text" name="f_max_score[]"  size="2" value="<?php echo $obj['max_score']?>"/><br/>
					</div>
					
					<div class="col-sm-2">
						<?php echo SCORE_DESCRIPTION ?>
					</div>
					<div class="col-sm-5">
						<textarea name="f_score_description[]" class="wdtFul" rows="5"><?php echo $obj['score_description']?></textarea>
						<input type="hidden" name="f_test_id[]" value="<?php echo $obj['test_id']?>"/>
						<input type="hidden" name="f_test_result_id[]" value="<?php echo $obj['test_result_id']?>"/>
					</div>
					<div class="clearfix"></div>
					<hr/>
				<?php
				}
			} else {
				echo DATA_NOT_FOUND.br(2);
			}
			
			?>
			<!-- END TEST RESULT -->
			<button class="btn btn-success" name="btnUpdate" value="1"><?php echo UPDATE?></button><br/><br/>
		</form>
	<?php
	}
	?>
</div>