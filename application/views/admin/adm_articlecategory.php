<?php 
global $param,$message,$getArticleCategoryID;

$do = $get_id = '';
$offset = OFFSET;
//$offset = 1;
$page = 1;
if ($this->input->get('page') && $this->input->get('page') > 1) $page = $this->input->get('page');
if ($this->uri->segment(3)) $do = $this->uri->segment(3);
if ($this->uri->segment(4)) $get_id = $getArticleCategoryID = $this->uri->segment(4);

/*
  | SAVE 
*/
if (post('f_insert')) {
	
	$categoryName = filter( post("f_CategoryName") );
	$description = filter( post("f_Description") );
	$isPublish = 0;
	if (post('f_IsPublish') == 1 ) $isPublish = 1 ;
	
	if (is_filled($categoryName)) {
		if (post('f_IsPublish') == 1 ) $isPublish = 1 ;else $isPublish = 0;
		
		$param = array(
			'CategoryName' => $categoryName,
			'Description' => $description,
			//'IsPublish' => $isPublish
		);
		
		$save = $this->articlecategory_model->save($param);
		
		($save)?$message['message'] = MESSAGE::SAVE:$message['message'] = MESSAGE::ERROR;
		$message['message'] = getMessage($message['message']);
	} else {
		$message['message'] = getMessage(MESSAGE::NOT_FOUND);
	}
}

/*
  | UPDATE 
*/
if (post('update')) {
	$categoryName = filter( post("CategoryName") );
	$description = filter( post("Description") );
	$isPublish = 0;
	if (post('IsPublish') == 1 ) $isPublish = 1 ;
	if (post('update') && is_numeric($getArticleCategoryID)) {
		$param['ArticleCategoryID'] = $getArticleCategoryID;
		$objArticleCategory = $this->articlecategory_model->get($param);
		if (!empty($objArticleCategory)) {
			$param = array(
				'CategoryName' => $categoryName,
				'Description' => $description
			);

			$update = $this->articlecategory_model->update($getArticleCategoryID, $param);
			
			($update)?$message['message'] = MESSAGE::UPDATE:$message['message'] = MESSAGE::ERROR;
			$message['message'] = getMessage($message['message']);
		} else {
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}

/*
  | DELETE 
*/
if ($do == "delete") {
	
	if (is_numeric($getArticleCategoryID)) {
		$delete = $this->articlecategory_model->delete($getArticleCategoryID);
		if ($delete) {
			($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
			$message['message'] = getMessage($message['message']);
		} else {
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}

/*
  | GROUP ACTION 
  | CHECKED BOX
*/
if (isset($_POST['btn_group_action'])) {
	if ($_POST['lst_group_action'] == "delete") {
		if (!empty($_POST['chkbox'])) { 
			$delete = false;
			foreach (post('chkbox') as $key => $val) {
				$delete = $this->articlecategory_model->delete($val);
			}
			
			if ($delete) {
				($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
				$message['message'] = getMessage($message['message']);
			} else {
				$message['message'] = getMessage(MESSAGE::NOT_FOUND);
			}
			
		}
	}
	//pre($_POST,TRUE);
}

// SEARCH
if (get('keyword')) {
	$param['CategoryName'] = get('keyword');
}

$param['paging'] = TRUE;
$param['offset'] = $offset;
$data = $this->articlecategory_model->get_list($param);

$total_rows = $data['total_rows'];
$list_data = $data['data'];
?>

<?php 

?>
<div class="col-xs-12">
<?php echo $SIDEMENUBAR ?>
</div>

<div class="col-xs-12">
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="talLft"><a href="<?php echo base_url()?>admin/articlecategory/insert" class="btn btn-success br"><i class="fa fa-plus"></i>&nbsp; Insert New Category</a></div><br/>
		<?php } ?>
	</div>
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="br talRgt">
			<form method="get">
			<input class="input input-sm" type="text" name="keyword" value="<?php if (get('keyword')) echo get('keyword')?>" placeholder="Keyword" />
			<button class="btn btn-info btn-sm" > <i class="fa fa-search-minus"></i> Filter</button>
			</form>
		</div>
		<?php } ?>
	</div>
	<div class="clearfix"></div>
	
	<?php if (isset($message['message'])) echo message($message['message'])?>
		
	<?php
	if ((!is_filled($do) && !is_filled($getArticleCategoryID)) || $do == "delete")
	{
		if (!empty($data))
		{
			$str = "";
			$total_rows = $data['total_rows'];
			$list_data = $data['data'];
			?>
			
			<form method="post">
			<table class="table table-hover table-striped table-responsive">
			<tr class="b">
				<td width=1><input type="checkbox" class="chkbox togglebox" onclick="togglebox()" /></td>
				<td width=1>#</td>
				<td>Article Category Name</td>
				<td class="talRgt">Option</td>
			</tr>
			<?php
			$i = 0;
			if (is_numeric($page) && $page > 0) 
			{
				$i = ($page - 1) * $offset;
			}
			foreach($list_data as $key => $rs)
			{
				$rs = (array) $rs;
				$id = $rs['ArticleCategoryID'];
				$i += 1;
				?>
				<tr>
				<td><input type="checkbox" name="chkbox[]" id="chkbox[]" class="chkbox" value="<?php echo $id?>" /></td>
				<td><?php echo $i ; ?></td>
				<td><?php echo $rs['CategoryName']; ?></td>
				<td class="talRgt"><a href="<?php echo $this->uri->segment(2).'/edit/'.$id; ?>" title="Edit data" alt="Edit data"><i class="clrBlu fa fa-pencil-square-o fa-2x"></i></a> 
				<a href="<?php echo $this->uri->segment(2).'/delete/'.$id; ?>" onclick="return confirm('Yakin menghapus data ini ?')"><i class="clrRed fa fa-times fa-2x" title="Delete data" alt="Delete data"></i></a></td>
				</tr>
				<?php 
			}
			?>
			<tr>
				<td colspan="100%">
					<div id="group_action">With checked do <select class="input" name="lst_group_action"><option class="" value="delete">Delete</option></select>
					<button class="btn btn-default btn-sm" name="btn_group_action" id="btn_group_action"value="1">Action</button>
					</div>
				</td>
			</tr>
			</table>
			</form>
			<br/>
			
		<?php
			if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
		}
		else 
		{
			echo 'No data Exist';
		}
	}
	
	//if (isset($do) || isset($get_id))
	else
	{
		$obj = array();
		if ($do != 'insert')
		{
			$tmp['ArticleCategoryID'] = $get_id;
			$obj = $this->articlecategory_model->get($tmp);
		}
	?>
	<?php if ($do == "edit") echo "<div class='fntLg'>Edit ".$MODULE."</div><br>"; ?>
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<label for='f_CategoryName' class='col-sm-2'>CategoryName</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_CategoryName' id='f_CategoryName' placeholder='Enter CategoryName' value='<?php if (!empty($obj)) echo $obj['CategoryName']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_Slug' class='col-sm-2'>Description</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_Description' id='f_Description' placeholder='Enter Description' value='<?php if (!empty($obj)) echo $obj['Description']?>'></div>
		</div>
		
		<div class='form-group form-group-sm col-sm-12'>
		<?php if ($do == 'insert') { ?>
		<button class='btn btn-success' name='f_insert' value='1'><?php echo SAVE?></button>
		<?php } else if ($do == 'edit'){ ?>
		<button class='btn btn-success' name='f_update' value='1'><?php echo UPDATE?></button>
		<?php } ?>
		</div>
		<div class="clearfix"></div>
	</form>
	<?php
	}
	?>
</div>
<script>
$(document).ready( function() {
	
	function togglebox(){
		if ($('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',true);
		}
		if (!$('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',false);
		}
	}
	
	$('#group_action').hide();
	$('.chkbox').click(function(){
        var count = $("[type='checkbox']:checked").length;
		if (count >= 1) {
             $('#group_action').show();
        } else if(count <= 0){
            $('#group_action').hide();
        }
    });

});
</script>