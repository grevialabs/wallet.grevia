<?php 
global $PAGE, $PAGE_TITLE, $PAGE_HEADER;
global $param,$message,$get_order_id;

$PAGE = 'Admin Order';
$PAGE_HEADER = 'Admin Order<hr>';
$PAGE_TITLE = $PAGE;

$do = $get_id = '';
$offset = OFFSET;
//$offset = 1;
$page = 1;
if ($this->input->get('page') && $this->input->get('page') > 1) $page = $this->input->get('page');
if ($this->uri->segment(3)) $do = $this->uri->segment(3);
if ($this->uri->segment(4)) $get_id = $get_order_id = $this->uri->segment(4);

/*
  | CANCEL ORDER
*/
if (post('btnCancel')) {
	// 7 - CANCEL
	$order_id = post('order_id');
	$update = $this->order_model->update($order_id, array('payment_status' => 7));
}

/*
  | ISSUE ORDER
*/

if (post('btnIssue')) {
	
	// 5 - ISSUE
	$order_id = post('order_id');
	$company_id = post('company_id');
	
	$obj_order = NULL;
	$obj_order = $this->order_model->get(array('order_id' => $order_id));
	// CHECK IF ORDER Exist
	if (!empty($obj_order))
	{
		if ($obj_prder['payment_status'] != 5)
		{
			
			// CREATE INVOICE (UPDATE ORDER)
			$invoice_code = $this->order_model->get_new_id('INV');
			$update = $this->order_model->update($order_id, array('invoice_code' => $invoice_code,'payment_status' => 5));
			
			// CREATE QUOTA
			// LOOP ORDER DETAIL
			$order_detail = array();
			$order_detail = $this->order_model->get_list_detail(array('order_id' => $order_id));
			$order_detail = $order_detail['data'];
			if (!empty($order_detail))
			{
				foreach ($order_detail as $key => $val)
				{
					$obj_quota = $this->quota_model->get(array('company_id' => $company_id));
					
					$temp = array();
					$temp['company_id'] = $company_id;
					$temp['current_quota'] = $obj_quota['after_quota'];
					$temp['increase_quota'] = $val['quantity'] * $val['participant_credit'];
					$temp['decrease_quota'] = 0;
					$temp['after_quota'] = $obj_quota['after_quota'] + ($val['quantity'] * $val['participant_credit']);
					$temp['remarks'] = 'Issue '.$order_id.' on '.date('D d M Y, H:i',time());
					$insert = $this->quota_model->save($temp);
				}
				redirect(current_url().'?success=1');
			}
			else
			{
				// no order detail, is it bug ???
			}
		}
		else
		{
			// ERROR TRANSACTION STATUS IS ISSUED ALREADY ???
		}
	}
	else
	{
		// NO DATA 
	}
}

/*
  | SAVE 
*/
if (post('btnInsert')) {
	
	var_dump($_POST);die;
	$name = filter( post("f_Name") );
	$email = filter( post("f_Email") );
	$DOB = filter( post("f_DOB") );
	$isActive = 0;
	if (post('f_IsActive') == 1 ) $isActive = 1 ;
	
	if (is_filled($name)) {
		
		$param = array(
			'Name' => $name,
			'Email' => $email,
			'DOB' => $DOB,
			'IsActive' => $isActive,
			//'IsPublish' => $isPublish
		);
		
		$save = $this->member_model->save($param);
		
		($save)?$message['message'] = MESSAGE::SAVE:$message['message'] = MESSAGE::ERROR;
		$message['message'] = getMessage($message['message']);
	} else {
		$message['message'] = getMessage(MESSAGE::NOT_FOUND);
	}
}

/*
  | UPDATE 
*/
if (post('btnUpdate')) {
	
	$is_subscribe = 0;
	$dob = NULL;
	if (isset($_POST['IsSubscribe'])) {
		$is_subscribe = 1;
	}
	if (isset($_POST['DOB'])) {
		$tempdob = explode('-',$_POST['DOB']);
		$dob = $tempdob[2].'-'.$tempdob[1].'-'.$tempdob[0];
	}
	$member_id = member_cookies('MemberID');
	$param = array(
		'FullName' => $_POST['FullName'],
		'Gender' => $_POST['Gender'],
		'DOB' => $dob,
		'Facebook' => $_POST['Facebook'],
		'Twitter' => $_POST['Twitter'],
		'AboutMe' => $_POST['AboutMe'],
		'IsSubscribe' => $is_subscribe,
	);
	
	$update = $this->member_model->update($getMemberID, $param);
	($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
	
	// if (post('f_update') && is_numeric($getMemberID)) {
		// $param['ArticleCategoryID'] = $getMemberID;
		// $objArticleCategory = $this->member_model->get($param);
		// if (!empty($objArticleCategory)) {
			// $param = array(
				// 'CategoryName' => $categoryName,
				// 'Description' => $description
			// );

			// $update = $this->member_model->update($getMemberID, $param);
			
			// ($update)?$message['message'] = MESSAGE::UPDATE:$message['message'] = MESSAGE::ERROR;
			// $message['message'] = getMessage($message['message']);
		// } else {
			// $message['message'] = getMessage(MESSAGE::NOT_FOUND);
		// }
	// }
}

/*
  | DELETE 
*/
if ($do == "delete") {
	
	if (is_numeric($getMemberID)) {
		$delete = $this->member_model->delete($getMemberID);
		if ($delete) {
			($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
			$message['message'] = getMessage($message['message']);
		} else {
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}

/*
  | GROUP ACTION 
  | CHECKED BOX
*/
if (isset($_POST['btn_group_action'])) {
	if ($_POST['lst_group_action'] == "delete") {
		if (!empty($_POST['chkbox'])) { 
			$delete = false;
			foreach (post('chkbox') as $key => $val) {
				$delete = $this->member_model->delete($val);
			}
			
			if ($delete) {
				($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
				$message['message'] = getMessage($message['message']);
			} else {
				$message['message'] = getMessage(MESSAGE::NOT_FOUND);
			}
			
		}
	}
}

// SEARCH
if (get('keyword')) {
	$param['keyword'] = get('keyword');
}

$get_payment_status = NULL;
if ($this->input->get('payment_status'))
{
	$get_payment_status = $this->input->get('payment_status');
}

if (isset($get_payment_status) && is_numeric($get_payment_status)) 
{
	$param['payment_status'] = $get_payment_status;
}

$param['paging'] = TRUE;
$param['offset'] = $offset;
$data = $this->order_model->get_list($param);

$total_rows = $data['total_rows'];
$list_data = $data['data'];
?>

<?php 

?>
<div class="col-xs-12">
<?php echo $SIDEMENUBAR ?>
</div>

<div class="col-xs-12">
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="talLft"><a href="<?php echo base_url()?>admin/<?php echo $MODULE?>/insert" class="btn btn-success br"><i class="fa fa-plus"></i>&nbsp; New <? echo $MODULE?></a></div><br/>
		<?php } ?>
	</div>
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="br talRgt">
			<form method="get">
			<input class="input input-sm" type="text" name="keyword" value="<?php if (get('keyword')) echo get('keyword')?>" placeholder="Keyword" />
			<select class="input input-sm" name="payment_status">
				<?php 
				$list_payment_status = order_payment_status('get');
				// var_dump($list_payment_status);
				// die;
				foreach ($list_payment_status as $key => $val) 
				{ ?>
				<option value="<?php echo $key?>" <?php if ($this->input->get('order_status') && $this->input->get('order_status') == $key) echo 'selected'?> /><?php echo order_payment_status($key)?></option>
				<?php 
				} 
				?>
			</select>
			<button class="btn btn-info btn-sm" > <i class="fa fa-search-minus"></i> Filter</button>
			</form>
		</div>
		<?php } ?>
	</div>
	<div class="clearfix"></div>
	
	<?php if (isset($message['message'])) echo message($message['message'])?>
		
	<?php
	if ((!is_filled($do) && !is_filled($get_order_id)) || $do == "delete")
	{
		if (!empty($data))
		{
			$str = "";
			$total_rows = $data['total_rows'];
			$list_data = $data['data'];
			
			?>
			
			<form method="post">
			<table class="table table-hover table-striped table-responsive">
			<tr class="b">
				<td width=1><input type="checkbox" class="chkbox togglebox" onclick="togglebox()" /></td>
				<td width=1>#</td>
				<td>Order ID</td>
				<td>Company Name</td>
				<td>GrandTotal</td>
				<td>Status</td>
				<td>CreateDate</td>
				<td>LastUpdate</td>
				<td class="talRgt">Option</td>
			</tr>
			<?php
			$i = 0;
			if (is_numeric($page) && $page > 0) 
			{
				$i = ($page - 1) * $offset;
			}
			foreach($list_data as $key => $rs)
			{
				$rs = (array) $rs;
				$id = $rs['order_id'];
				$i += 1;
				$url = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
				?>
				<tr>
				<td><input type="checkbox" name="chkbox[]" id="chkbox[]" class="chkbox" value="<?php echo $id?>" /></td>
				<td><?php echo $i ; ?></td>
				<td><?php echo $rs['order_id']; ?></td>
				<td><?php echo $rs['company_name']; ?></td>
				<td><?php echo format_money($rs['grandtotal']); ?></td>
				<td><?php echo order_payment_status($rs['payment_status']); ?></td>
				<td><?php echo $rs['creator_date']; ?></td>
				<td><?php echo $rs['editor_date']; ?></td>
				<td class="talRgt">
				
				<form method="post">
				<?php if ($rs['payment_status'] != 5) { ?>
				<!-- ISSUE HERE -->
				<input type="hidden" name="f_order_id" value="<?php echo $rs['order_id']?>"/>
				<input type="hidden" name="f_company_id" value="<?php echo $rs['company_id']?>"/>
				<button class="btn btn-success btn-sm" name="btnIssue" value="1"><?php echo ISSUE?></button>
				<!-- ISSUE HERE -->
				<?php } ?>
				
				<!-- CANCEL HERE -->
				<button class="btn btn-danger btn-sm" name="btnCancel" value="1"><?php echo CANCEL?></button>
				<!-- CANCEL HERE -->
				</form>
				
				<!--
				<a href="<?php echo $url.'/edit/'.$id; ?>" title="Edit data" alt="Edit data"><i class="clrBlu fa fa-pencil-square-o fa-2x"></i></a> 
				<a href="<?php echo $url.'/delete/'.$id; ?>" onclick="return confirm('Yakin menghapus data ini ?')"><i class="clrRed fa fa-times fa-2x" title="Delete data" alt="Delete data"></i></a>
				-->
				</td>
				</tr>
				<?php 
			}
			?>
			<tr>
				<td colspan="100%">
					<div id="group_action">With checked do <select class="input" name="lst_group_action"><option class="" value="delete">Delete</option></select>
					<button class="btn btn-default btn-sm" name="btn_group_action" id="btn_group_action"value="1">Action</button>
					</div>
				</td>
			</tr>
			</table>
			</form>
			<br/>
			
		<?php
			if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
		}
		else 
		{
			echo 'No data Exist';
		}
	}
	
	//if (isset($do) || isset($get_id))
	
	?>
</div>
<script>
$(document).ready( function() {
	
	function togglebox(){
		if ($('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',true);
		}
		if (!$('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',false);
		}
	}
	
	$('#group_action').hide();
	$('.chkbox').click(function(){
        var count = $("[type='checkbox']:checked").length;
		if (count >= 1) {
             $('#group_action').show();
        } else if(count <= 0){
            $('#group_action').hide();
        }
    });

});
</script>