<?php 
global $PAGE, $PAGE_TITLE, $PAGE_HEADER;
global $param,$message,$get_test_id,$show;

$PAGE = 'Admin Tes';
$PAGE_HEADER = 'Admin Tes<hr>';
$PAGE_TITLE = $PAGE;

$do = $get_id = $update_test = '';
$offset = OFFSET;
//$offset = 1;
$page = 1;
if ($this->input->get('page') && $this->input->get('page') > 1) $page = $this->input->get('page');
if (isset($_GET['do'])) $do = $_GET['do'];
if (isset($_GET['test_id'])) $get_id = $get_test_id = $_GET['test_id'];

$list_allow_show = array('header', 'detail', 'result');
if (isset($_GET['show']) && in_array($_GET['show'], $list_allow_show)=== TRUE) $show = $_GET['show']; 

/*
  | SAVE 
*/
if (post('btnInsert')) {
	
	$title = $_POST['title'];
	$detail_description = $_POST['detail_description'];
	$detail_attend = $_POST['detail_attend'];
	$timer = $this->input->post('timer');
	
	$is_publish = 0;
	if (isset($_POST['is_publish']) && $_POST['is_publish'] == 1) $is_publish = 1;
	
	$is_public = 0;
	if (isset($_POST['is_public']) && $_POST['is_public'] == 1) $is_public = 1;
	
	if (isset($_POST['title']) && isset($_POST['detail_description']) && isset($_POST['detail_attend'])) 
	{
		$param = array(
			'title' => $title,
			'detail_description' => $detail_description,
			'detail_attend' => $detail_attend,
			'timer' => $timer,
			'is_publish' => $is_publish,
			'is_public' => $is_public,
		);

		$save = $this->test_model->save_test($param);
		
		($save)?$message['message'] = MESSAGE::SAVE:$message['message'] = MESSAGE::ERROR;
		//$message['message'] = getMessage($message['message']);
		if ($message['message'] == MESSAGE::SAVE)
		{
			$last_insert = $this->test_model->get_test(array('last' => 1));
			redirect(base_url().'admin/test?do=edit&test_id='.$last_insert['test_id']);
		}
	} else {
		$message['message'] = getMessage(MESSAGE::NOT_FOUND);
	}

}

/*
  | UPDATE 
*/
if (post('btnUpdate')) {

	$title = $this->input->post('title');
	$detail_description = $this->input->post('detail_description');
	$detail_attend = $this->input->post('detail_attend');
	$timer = $this->input->post('timer');
	
	$is_publish = 0;
	if (isset($_POST['is_publish']) && $_POST['is_publish'] == 1) $is_publish = 1;
	
	$is_public = 0;
	if (isset($_POST['is_public']) && $_POST['is_public'] == 1) $is_public = 1;
	
	if (isset($_POST['title']) && isset($_POST['detail_description']) && isset($_POST['detail_attend'])) 
	{
		$param = array(
			'title' => $title,
			'detail_description' => $detail_description,
			'detail_attend' => $detail_attend,
			'timer' => $timer,
			'is_publish' => $is_publish,
			'is_public' => $is_public,
		);

		$update = $this->test_model->update_test($get_id, $param);
		($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
		$message['message'] = getMessage($message['message']);
	}
}

/*
  | DELETE 
*/
if ($do == "delete") {
	
	if (is_numeric($get_test_id)) {
		$delete = $this->member_model->delete($get_test_id);
		if ($delete) {
			($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
			$message['message'] = getMessage($message['message']);
		} else {
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}

/*
  | GROUP ACTION 
  | CHECKED BOX
*/
if (isset($_POST['btn_group_action'])) {
	if ($_POST['lst_group_action'] == "delete") {
		if (!empty($_POST['chkbox'])) { 
			$delete = false;
			foreach (post('chkbox') as $key => $val) {
				$delete = $this->test_model->delete_test($val);
			}
			
			if ($delete) {
				($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
				$message['message'] = getMessage($message['message']);
			} else {
				$message['message'] = getMessage(MESSAGE::NOT_FOUND);
			}
			
		}
	} elseif ($_POST['lst_group_action'] == "publish" || $_POST['lst_group_action'] == 'unpublish') {
		if (!empty($_POST['chkbox'])) { 
			$update = false;
			
			if ($_POST['lst_group_action'] == "publish") $param['is_publish'] = 1;
			if ($_POST['lst_group_action'] == "unpublish") $param['is_publish'] = 0;
			
			foreach (post('chkbox') as $key => $val) {
				$update = $this->test_model->update_test($val, $param);
			}
			
			if ($update) {
				($update)?$message['message'] = MESSAGE::UPDATE:$message['message'] = MESSAGE::ERROR;
				$message['message'] = getMessage($message['message']);
			} else {
				$message['message'] = getMessage(MESSAGE::NOT_FOUND);
			}
			
		}
	}
}

$param = NULL;
// SEARCH
if (get('keyword')) {
	$param['keyword'] = get('keyword');
}

$param['paging'] = TRUE;
$param['offset'] = $offset;
$data = $this->test_model->get_list_test($param);

$total_rows = $data['total_rows'];
$list_data = $data['data'];
?>
<div class="col-sm-12">
<?php echo $SIDEMENUBAR ?>
</div>

<div class="col-sm-12">
	<div class="b fntHdr"><?php echo $MODULE?></div><hr/>
	<div class="col-sm-6">
		<?php if (!is_filled($do)) { ?>
		<div class="talLft"><a href="<?php echo base_url()?>admin/<?php echo $MODULE?>?do=insert" class="btn btn-success br"><i class="fa fa-plus"></i>&nbsp; New <? echo $MODULE?></a></div><br/>
		<?php } ?>
	</div>
	<div class="col-sm-6">
		<?php if (!is_filled($do)) { ?>
		<div class="br talRgt">
			<form method="get">
			<input class="input input-sm" type="text" name="keyword" value="<?php if (get('keyword')) echo get('keyword')?>" placeholder="Keyword" />
			<button class="btn btn-info btn-sm" > <i class="fa fa-search-minus"></i> Filter</button>
			</form>
		</div>
		<?php } ?>

		
	</div>
	<div class="clearfix"></div>
	
	<?php if (isset($message['message'])) echo message($message['message'])?>
		
	<?php
	if ((!is_filled($do) && !is_filled($get_test_id)) || $do == "delete")
	{
		if (!empty($data))
		{
			$str = "";
			$total_rows = $data['total_rows'];
			$list_data = $data['data'];
			?>
			
			<form method="post">
			<table class="table table-hover table-striped table-responsive">
			<tr class="b">
				<td width=1><input type="checkbox" class="chkbox togglebox" onclick="togglebox()" /></td>
				<td width=1>#</td>
				<td>Title</td>
				<td>Detail Description</td>
				<td>Publish</td>
				<td>CreatorID</td>
				<td>CreatorDate</td>
				<td class="talRgt" width="90px">Option</td>
			</tr>
			<?php
			$i = 0;
			if (is_numeric($page) && $page > 0) 
			{
				$i = ($page - 1) * $offset;
			}
			foreach($list_data as $key => $rs)
			{
				$rs = (array) $rs;
				$id = $rs['test_id'];
				$i += 1;
				$url = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
				?>
				<tr>
				<td class="parentcheckbox"><input type="checkbox" name="chkbox[]" id="chkbox[]" class="chkbox" value="<?php echo $id?>" /></td>
				<td><?php echo $i ; ?></td>
				<td><?php echo $rs['title']; ?></td>
				<td><?php echo $rs['detail_description']; ?></td>
				<td class="talCnt">
				<?php 
				if ($rs['is_publish'] == 1) 
					echo '<i class="fa fa-check-square clrGrn"></i>'; 
				else
					echo '<i class="fa fa-close clrRed"></i>'; 
				?>
				</td>
				<td><?php echo $rs['creator_id']; ?></td>
				<td><?php echo $rs['creator_date']; ?></td>
				<td class="talRgt"><a href="<?php echo $url.'?do=edit&test_id='.$id; ?>" title="Edit data" alt="Edit data"><i class="clrBlu fa fa-pencil-square-o fa-2x"></i></a> 
				<a href="<?php echo $url.'?do=delete&test_id='.$id; ?>" onclick="return confirm('Yakin menghapus data ini ?')"><i class="clrRed fa fa-times fa-2x" title="Delete data" alt="Delete data"></i></a></td>
				</tr>
				<?php 
			}
			?>
			<tr>
				<td colspan="100%">
					<div id="group_action">With checked do 
					<select class="input" name="lst_group_action">
						<option class="" value="publish">Publish</option>
						<option class="" value="unpublish">Unpublish</option>
						<option class="" value="delete">Delete</option>
					</select>
					<button class="btn btn-default btn-sm" name="btn_group_action" id="btn_group_action"value="1">Action</button>
					</div>
				</td>
			</tr>
			</table>
			</form>
			<br/>
			
		<?php
			if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
		}
		else 
		{
			echo 'No data Exist';
		}
	}
	
	//if (isset($do) || isset($get_id))
	else
	{
		$obj = array();
		if ($do == 'insert')
		{
			$obj = null;
		}
		else
		{
			$obj = $this->test_model->get_test(array('test_id' => $get_id));
		}
		
	?>
		<div class="pull-right">
			<!--<a class="btn btn-info" href="<?php echo base_url().'admin/test_detail?do=edit&test_id='.$get_id ?>">Test Detail</a>
			<a class="btn btn-info" href="<?php echo base_url().'admin/test_result/'.$get_id ?>">Test Result</a>
			-->
			<a class="btn btn-info" href="<?php echo base_url().'admin/test_detail?do=edit&test_id='.$get_id ?>">Test Detail</a>
			<a class="btn btn-info" href="<?php echo base_url().'admin/test_result?do=edit&test_id='.$get_id ?>">Test Result</a>
		</div>
		<div class="clearfix"></div>
		<br/>
		
		<?php if ($do == "edit") echo "<div class='fntLg'>Edit ".$MODULE."</div><br>"; ?>
		<form class='form-horizontal' role='form' method='post'>
			<div class='form-group form-group-sm'>
				<label for='f_title' class='col-sm-2'><?php echo TEST_TITLE ?></label>
				<div class='col-sm-10'><input type='text' class='form-control' name='f_title' id='f_title' placeholder='<?php echo TEST_TITLE ?>' value='<?php if (!empty($obj)) echo $obj['title']?>'></div>
			</div>
			<div class='form-group form-group-sm'>
				<label for='f_detail_description' class='col-sm-2'><?php echo DETAIL_DESCRIPTION ?></label>
				<div class='col-sm-10'><textarea type='text' class='form-control' name='f_detail_description' id='f_detail_description' placeholder='Enter Detail Description'><?php if (!empty($obj)) echo $obj['detail_description']?></textarea></div>
			</div>
			<div class='form-group form-group-sm'>
				<label for='f_detail_atttend' class='col-sm-2'><?php echo DETAIL_ATTEND ?></label>
				<div class='col-sm-10'><textarea class='form-control' name='f_detail_attend' id='f_detail_attend' rows="15" placeholder='<?php echo DETAIL_ATTEND ?>'><?php if (!empty($obj)) echo $obj['detail_attend']?></textarea></div>
			</div>
			<div class='form-group form-group-sm'>
				<label for='f_is_publish' class='col-sm-2'><?php echo IS_PUBLISH ?></label>
				<div class="col-sm-10"><input type="checkbox" name="f_is_publish" id="f_is_publish" value="1" <?php if(isset($obj['is_publish']) && $obj['is_publish'] == 1) echo 'checked'?>/>
				</div>
			</div>
			<div class='form-group form-group-sm'>
				<label for='f_is_publish' class='col-sm-2'><?php echo IS_PUBLIC ?></label>
				<div class="col-sm-10"><input type="checkbox" name="f_is_public" id="f_is_public" value="1" <?php if(isset($obj['is_public']) && $obj['is_public'] == 1) echo 'checked'?>/>
				</div>
			</div>
			<div class='form-group form-group-sm'>
				<label for='f_is_publish' class='col-sm-2'><?php echo TIMER ?></label>
				<div class="col-sm-10"><input type="text" name="f_timer" id="f_timer" value="<?php if(isset($obj['timer'])) echo $obj['timer']?>" size="3" maxlength="3"/>* <?php echo MINUTE?>	</div>
			</div>
			<div class='form-group form-group-sm col-sm-12'>
			<?php if ($do == 'insert') { ?>
			<button class='btn btn-success' name='btnInsert' value='1'><?php echo SAVE?></button>
			<?php } else if ($do == 'edit'){ ?>
			<button class='btn btn-success' name='btnUpdate' value='1'><?php echo UPDATE?></button>
			<?php } ?>
			</div>
			<div class="clearfix"></div>
		</form>
		<?php 
	}
	?>
</div>
