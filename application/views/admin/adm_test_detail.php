<?php 
global $param,$message,$get_test_id,$get_test_detail_id,$show;

$do = $get_id = $get_test_id = $get_test_detail_id = $update_test = NULL;
$offset = OFFSET;
//$offset = 1;
$page = 1;
if ($this->input->get('page') && $this->input->get('page') > 1) $page = $this->input->get('page');
if (isset($_GET['do'])) $do = $_GET['do'];
if (isset($_GET['test_id'])) $get_test_id = $_GET['test_id'];
if (isset($_GET['test_detail_id'])) $get_test_detail_id = $_GET['test_detail_id'];

$list_allow_show = array('header', 'detail', 'result');
if (isset($_GET['show']) && in_array($_GET['show'], $list_allow_show)=== TRUE) $show = $_GET['show']; 

//$get_test_id = 0;
if(is_numeric($get_test_id) && $get_test_id > 0) $obj = $this->test_model->get_test(array('test_id' => $get_test_id));

/*
  | SAVE 
*/
if (post('btnInsert')) {
	
	$list_test_detail_id = $_POST['test_detail_id'];
	$list_test_detail_description = $_POST['test_detail_description'];
	
	// INSERT test_detail
	for ($i = 0; $i < count($list_test_detail_description); $i++)
	{
		if (isset($get_test_id) && isset($list_test_detail_description[$i]))
		{
			$param = array(
			'test_id' => $get_test_id,
			'detail_description' => $list_test_detail_description[$i]
			);
			$save_test_detail = $this->test_model->save_test_detail($param);
		}
	}
	
	$list_option_test_detail_option_id = $_POST['option_test_detail_option_id'];
	$list_option_answer_detail = $_POST['option_answer_detail'];
	$list_option_point_count = $_POST['option_point_count'];
	
	// GET LAST TEST DETAIL
	$obj_test_detail = $this->test_model->get_test_detail(array('last' => 1));
	$test_detail_id = $obj_test_detail['test_detail_id'];
	
	// INSERT test_detail_option
	for ($i = 0; $i < count($list_option_answer_detail); $i++)
	{
		if (isset($test_detail_id) && isset($list_option_answer_detail[$i]) && isset($list_option_point_count[$i]))
		{
			$param = array(
				'test_detail_id' => $test_detail_id,
				'answer_detail' => $list_option_answer_detail[$i],
				'point_count' => $list_option_point_count[$i]
			);
			$save_test_detail_option = $this->test_model->save_test_detail_option($param);
		}
	}
	($save_test_detail_option)?$message['message'] = MESSAGE::SAVE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);

}

/*
  | UPDATE 
*/
if (post('btnUpdate'))
{
	$list_test_detail_id = $_POST['test_detail_id'];
	$list_test_detail_description = $_POST['test_detail_description'];
	
	// UPDATE test_detail
	for ($i = 0; $i < count($list_test_detail_description); $i++)
	{
		$param = array(
			'detail_description' => $list_test_detail_description[$i]
		);
		$update_test_detail = $this->test_model->update_test_detail($list_test_detail_id[$i], $param);
	}
	
	$list_option_test_detail_option_id = $_POST['option_test_detail_option_id'];
	$list_option_test_detail_id = $_POST['option_test_detail_id'];
	$list_option_answer_detail = $_POST['option_answer_detail'];
	$list_option_point_count = $_POST['option_point_count'];
	// UPDATE test_detail_option
	for ($i = 0; $i < count($list_option_answer_detail); $i++)
	{
		$param = array(
			'test_detail_id' => $list_option_test_detail_id[$i],
			'answer_detail' => $list_option_answer_detail[$i],
			'point_count' => $list_option_point_count[$i]
		);
		$update_test_detail_option = $this->test_model->update_test_detail_option($list_option_test_detail_option_id[$i], $param);
	}
	($update_test_detail_option)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);

}

/*
  | DELETE 
*/
if ($do == "delete") {
	
	$get_test_detail_id = $_GET['test_detail_id'];
	$get_test_id = $_GET['test_id'];
	
	if (is_numeric($get_test_detail_id)) {
		$delete = $this->test_model->delete_test_detail($get_test_detail_id);
		if ($delete) {
			($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
			// DELETE SUCCESS
			if ($message['message'] = MESSAGE::DELETE) redirect(base_url().'admin/test_detail?do=edit&test_id='.$get_test_id.'&delete=success');
			//$message['message'] = getMessage($message['message']);
		} else {
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}

// DELETE DETAIL TEST DETAIL OPTION
if ($do == "delete_detail_option" && isset($_GET['test_detail_option_id'])) {
	
	$get_test_detail_option_id = $_GET['test_detail_option_id'];

	if (is_numeric($get_test_detail_option_id)) {
		
		$delete = $this->test_model->delete_test_detail_option($get_test_detail_option_id);
		
		if ($delete) {
			$referer = $_SERVER['HTTP_REFERER'];
			
			($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
			// DELETE SUCCESS
			if ($message['message'] == MESSAGE::DELETE)
				redirect($referer);
				//redirect(base_url().'admin/test_detail?do=edit&test_id='.$get_test_detail_id.'&delete=success');
			
		} else {
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}

/*
  | GROUP ACTION 
  | CHECKED BOX
*/
if (isset($_POST['btn_group_action'])) {
	if ($_POST['lst_group_action'] == "delete") {
		// if (!empty($_POST['chkbox'])) { 
			// $delete = false;
			// foreach (post('chkbox') as $key => $val) {
				// $delete = $this->member_model->delete($val);
			// }
			
			// if ($delete) {
				// ($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
				// $message['message'] = getMessage($message['message']);
			// } else {
				// $message['message'] = getMessage(MESSAGE::NOT_FOUND);
			// }
			
		// }
	}
}

// SEARCH
if (get('keyword')) {
	$param['keyword'] = get('keyword');
}

$param['paging'] = TRUE;
$param['offset'] = $offset;
$data = $this->test_model->get_list_test_detail($param);

$total_rows = $data['total_rows'];
$list_data = $data['data'];
?>

<?php 

?>
<div class="col-sm-12">
<?php echo $SIDEMENUBAR ?>
</div>

<div class="col-sm-12">
	<div class="b fntHdr"><?php echo str_replace('_',' ',$MODULE) ?></div><hr/>
	<?php if (!empty($obj)) { ?>
	<div class="col-sm-12">	
		<a class="btn btn-sm btn-primary" href="<?php echo base_url().'admin/test?test_id='.$get_test_id?>">&lt; Back to Test</a>
		<a class="btn btn-sm btn-success" href="<?php echo base_url().'admin/test_result?test_id='.$get_test_id?>">&gt; Test Result</a><hr/>
		<form class='form-horizontal' role='form' method='post'>
			<div class='form-group form-group-sm'>
				<label for='f_title' class='col-sm-2'><?php echo TEST_TITLE ?></label>
				<div class='col-sm-10'><input type='text' class='form-control' name='f_title' id='f_title' placeholder='<?php echo TEST_TITLE ?>' value='<?php if (!empty($obj)) echo $obj['title']?>' disabled></div>
			</div>
			<div class='form-group form-group-sm'>
				<label for='f_detail_description' class='col-sm-2'><?php echo DETAIL_DESCRIPTION ?></label>
				<div class='col-sm-10'><textarea type='text' class='form-control' name='f_detail_description' id='f_detail_description' placeholder='Enter Detail Description' disabled><?php if (!empty($obj)) echo $obj['detail_description']?></textarea></div>
			</div>
			<div class='form-group form-group-sm'>
				<label for='f_detail_atttend' class='col-sm-2'><?php echo DETAIL_ATTEND ?></label>
				<div class='col-sm-10'><textarea class='form-control' name='f_detail_attend' id='f_detail_attend' rows="15" placeholder='<?php echo DETAIL_ATTEND ?>' disabled><?php if (!empty($obj)) echo $obj['detail_attend']?></textarea></div>
			</div>
			<div class='form-group form-group-sm'>
				<label for='f_is_publish' class='col-sm-2'><?php echo IS_PUBLISH ?></label>
				<div class="col-sm-10"><input type="checkbox" id="f_is_publish" name="f_is_publish" value="1" <?php if(isset($obj['is_publish']) && $obj['is_publish'] == 1) echo 'checked'?> disabled/>
				</div>
			</div>
			<div class='form-group form-group-sm col-sm-12'>
			</div>
			<div class="clearfix"></div>
		</form>
	</div>
	<?php } ?>
	<div class="col-sm-6">
		<?php if (!is_filled($do)) { ?>
		<div class="talLft"><a href="<?php echo base_url()?>admin/<?php echo $MODULE?>?do=insert&test_id=<?php echo $get_test_id?>" class="btn btn-success br"><i class="fa fa-plus"></i>&nbsp; New <? echo $MODULE?></a></div><br/>
		<?php } ?>
	</div>
	<div class="col-sm-6">
		<?php if (!is_filled($do)) { ?>
		<div class="br talRgt">
			<form method="get">
			<input class="input input-sm" type="text" name="keyword" value="<?php if (get('keyword')) echo get('keyword')?>" placeholder="Keyword" />
			<button class="btn btn-info btn-sm" > <i class="fa fa-search-minus"></i> Filter</button>
			</form>
		</div>
		<?php } ?>
	</div>
	<div class="clearfix"></div>
	
	<?php if (isset($message['message'])) echo message($message['message'])?>
		
	<?php
	if ((!is_filled($do) && !is_filled($get_test_id)) || $do == "delete")
	{
		if (!empty($data))
		{
			$str = "";
			$total_rows = $data['total_rows'];
			$list_data = $data['data'];
			?>
			
			<form method="post">
			<table class="table table-hover table-striped table-responsive">
			<tr class="b">
				<td width=1><input type="checkbox" class="chkbox togglebox" onclick="togglebox()" /></td>
				<td width=1>#</td>
				<td>Title</td>
				<td>Detail Description</td>
				<td>CreatorID</td>
				<td>CreatorDate</td>
				<td class="talRgt" width="90px">Option</td>
			</tr>
			<?php
			$i = 0;
			if (is_numeric($page) && $page > 0) 
			{
				$i = ($page - 1) * $offset;
			}
			foreach($list_data as $key => $rs)
			{
				$rs = (array) $rs;
				$id = $rs['test_id'];
				$i += 1;
				$url = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
				?>
				<tr>
				<td><input type="checkbox" name="chkbox[]" id="chkbox[]" class="chkbox" value="<?php echo $id?>" /></td>
				<td><?php echo $i ; ?></td>
				<td><?php echo $rs['title']; ?></td>
				<td><?php echo $rs['detail_description']; ?></td>
				<td><?php echo $rs['creator_id']; ?></td>
				<td><?php echo $rs['creator_date']; ?></td>
				<td class="talRgt"><a href="<?php echo $url.'/edit/'.$id; ?>" title="Edit data" alt="Edit data"><i class="clrBlu fa fa-pencil-square-o fa-2x"></i></a> 
				<a href="<?php echo $url.'/delete/'.$id; ?>" onclick="return confirm('Yakin menghapus data ini ?')"><i class="clrRed fa fa-times fa-2x" title="Delete data" alt="Delete data"></i></a></td>
				</tr>
				<?php 
			}
			?>
			<tr>
				<td colspan="100%">
					<div id="group_action">With checked do <select class="input" name="lst_group_action"><option class="" value="delete">Delete</option></select>
					<button class="btn btn-default btn-sm" name="btn_group_action" id="btn_group_action"value="1">Action</button>
					</div>
				</td>
			</tr>
			</table>
			</form>
			<br/>
			
		<?php
			if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
		}
		else 
		{
			echo 'No data Exist';
		}
	}
	else
	{
		if (isset($get_test_id) && $get_test_id >= 1) 
		{
			$obj_list_test_detail = $this->test_model->get_list_test_detail( array('test_id' => $get_test_id) );
			$obj_list_test_detail = $obj_list_test_detail['data'];
		}
		
		if (!empty($obj_list_test_detail))
		{
		?>
		<div><a class="btn btn-sm btn-success" href="<?php echo current_url().'?do=insert&test_id='.$get_test_id ?>#new_detail">Add New Question</a></div>
		<form class='form-horizontal' role='form' method='post'>
			<?php
			
			
			/*
			DETAIL DAN RESULT
			
			DETAIL untuk input question dan option
			
			RESULT untuk tampung hasil
			*/ 
			

			$i = 1;
			foreach($obj_list_test_detail as $obj_detail)
			{ 
				
			?>
			<div class="padLrg <?php if ($i%2 == 1) echo 'bgSftGry'; ?>">	
				<div class="col-sm-10"></div>
				<div class="lnkWht col-sm-2 padMed">
					<a class="btn btn-danger" href="<?php echo base_url().'admin/test_detail?test_id='.$get_test_id.'&do=delete&test_detail_id='.$obj_detail['test_detail_id']?>"><i class="fa fa-remove"></i> Delete detail</a>
				</div>
				<div class="col-sm-2">Pertanyaan <?=$i?></div>
				<div class="col-sm-10">
				<textarea type="text" class="input wdtFul" name="f_test_detail_description[]" rows="5"><?php echo $obj_detail['detail_description'];?></textarea>
				<input type="hidden" name="f_test_detail_id[]" value="<?php echo $obj_detail['test_detail_id']?>" /></div><br/>
				<?php 
				$obj_list_test_detail_option = array();
				$obj_list_test_detail_option = $this->test_model->get_list_test_detail_option(array('test_detail_id' => $obj_detail['test_detail_id']));
				$obj_list_test_detail_option = $obj_list_test_detail_option['data'];
				
				if (!empty($obj_list_test_detail_option))
				{
					foreach ($obj_list_test_detail_option as $obj_test_detail_option)
					{
						?>
						<div class="col-sm-2"><?php echo ANSWER?></div>
						<div class="col-sm-5"><input type="text" class="input wdtFul" name="f_option_answer_detail[]" value="<?php echo $obj_test_detail_option['answer_detail'] ?>"/></div>
						<div class="col-sm-4">Bobot : <input type="text" class="input" name="f_option_point_count[]" size="4" value="<?php echo $obj_test_detail_option['point_count']?>"/>
						<input type="hidden" name="f_option_test_detail_id[]" value="<?php echo $obj_test_detail_option['test_detail_id']?>"/>
						<input type="hidden" name="f_option_test_detail_option_id[]" value="<?php echo $obj_test_detail_option['test_detail_option_id']?>"/>
						
						</div>
						<div><a class="btn btn-sm btn-danger" href="<?php echo current_url().'?do=delete_detail_option&test_detail_option_id='.$obj_test_detail_option['test_detail_option_id'] ?>" title="delete"><i class="fa fa-trash"></i></a></div>
						<?php
					}
				}
				?>
				<div class="clearfix"></div>
				<hr/>
			</div>
			<?php
				$i++;
			}
			
			
			?>
			<button class="btn btn-success" name="btnUpdate" value="1"><?php echo UPDATE?></button><br/><br/>
		</form>
		
		<!-- INSERT NEW QUESTION BELOW EXISTING QUESTION-->
		<?php
		//if(isset($get_test_id) && $get_test_id > 0) $obj = $this->test_model->get_test(array('test_id' => $get_test_id));
		?>
		<hr>
		<form method="post">
		<div class="padLrg bgSftGry">	
			<div class="col-sm-2 b fntBig">Pertanyaan baru</div>
			<div class="col-sm-10">
				<textarea type="text" class="input wdtFul" name="f_test_detail_description[]" rows="5"></textarea>
				<input type="hidden" name="f_test_detail_id[]" value="" />
			</div><br/>
			
			<div class="col-sm-12"><a class="btn btn-small btn-info" href="javascript:void()" onclick="" id="addnew">Add more option</a></div>
			<?php 
			for ($i = 0;$i<=1;$i++) 
			{ 
			?>
			<div class="option">	
				<div class="col-sm-2"><?php echo ANSWER?></div>
				<div class="col-sm-5"><input type="text" class="input wdtFul" name="f_option_answer_detail[]" value=""/></div>
				<div class="col-sm-5">Bobot : <input type="text" class="input" name="f_option_point_count[]" size="4" value=""/>
					<input type="hidden" name="f_option_test_detail_id[]" value=""/>
					<input type="hidden" name="f_option_test_detail_option_id[]" value=""/>
				</div>
			</div>
				<?php
			}
				?>
			<div id="ajaxnew"></div>
			
			<div id="new_detail" class="clearfix"></div>
			<button class="btn btn-success" name="btnInsert" value="1"><?php echo SAVE?></button><br/><br/>
			<hr/>
		</div>
		</form>
		<?php
		}
		else 
		{
			echo NO_DATA;
		}
	}
	?>
</div>
<script>
str = '<div class="option">';
str+= '<div class="col-sm-2"><?php echo ANSWER?></div>';
str+= '<div class="col-sm-5"><input type="text" class="input wdtFul" name="f_option_answer_detail[]"value=""/></div>';
str+= '<div class="col-sm-5">Bobot : <input type="text" class="input" name="f_option_point_count[]" size="4" value=""/>';
str+= '<input type="hidden" name="f_option_test_detail_id[]" value=""/>';
str+= '<input type="hidden" name="f_option_test_detail_option_id[]" value=""/>';
str+= '<a class="removeme">close</a>';
str+= '</div> ';
str+= '</div>';
$('#addnew').click(function(){
	$('#ajaxnew').append(str);
});

$(document).ready(function(){
	$(".removeme").click(function(){
		alert('hore');
		//alert($(this).parent());
	});
});

</script>