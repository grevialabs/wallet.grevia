	<?php
	global $current_segment;
	if ($this->uri->segment(2)) $current_segment = $this->uri->segment(2);
	
	function active($page) 
	{
		global $current_segment;
		$str = '';
		if ($current_segment === $page)
		{
			$str = 'active';
		}
		return $str;
	}
	?>
	<!--
	<div class="talRgt padMed ">
	  <a class="bgInfo <?php echo active('dashboard').active('index')?>" href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-home fa-fw"></i>&nbsp; Admin Dashboard</a>
	  <a class=" <?php echo active('member')?>" href="<?php echo base_url()?>admin/member"><i class="fa fa-cog fa-fw"></i>&nbsp;Member</a>
	  <a class=" <?php echo active('subscribe')?>" href="<?php echo base_url()?>admin/subscribe"><i class="fa fa-envelope fa-fw"></i>&nbsp;Subscribe</a>
	</div><br/>
	--><br/>
	<div class="bg-warning talRgt lnkRed navbar-collapse collapse">
		<div id="navbar" class="navbar-collapse collapse">
		  <ul class="nav navbar-nav lnkBlk">
			<li><a class=" <?php echo active('dashboard').active('index')?>" href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-home fa-fw"></i>&nbsp; Admin Dashboard</a></li>
			<li><a class=" <?php echo active('order')?>" href="<?php echo base_url()?>admin/order"><i class="fa fa-money fa-fw"></i>&nbsp; Admin Order</a></li>
			<li><a class=" <?php echo active('member')?>" href="<?php echo base_url()?>admin/member"><i class="fa fa-users fa-fw"></i>&nbsp; Member</a></li>
			<li><a class=" <?php echo active('subscribe')?>" href="<?php echo base_url()?>admin/subscribe"><i class="fa fa-user fa-fw"></i>&nbsp; Subscribe</a></li>
			<li class="dropdown ">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <i class="fa fa-bar-chart fa-fw"></i>&nbsp; Create Test<span class="caret"></span></a>
			  <ul class="dropdown-menu " role="menu">
				<li><a class=" <?php echo active('report_search')?>" href="<?php echo base_url()?>admin/test">Test</a></li>
				<li><a class=" <?php echo active('report_search')?>" href="<?php echo base_url()?>admin/test_detail">Test Detail</a></li>
			  </ul>
			</li>
			<li>
				
			</li>
			
		  </ul>
		</div>
	</div><br/>