<?php 
global $PAGE, $PAGE_TITLE, $PAGE_HEADER;
global $param,$message,$getMemberID;

$PAGE = 'Admin Member';
$PAGE_HEADER = 'Admin Member<hr>';
$PAGE_TITLE = $PAGE;

$do = $get_id = '';
$offset = OFFSET;
//$offset = 1;
$page = 1;
if ($this->input->get('page') && $this->input->get('page') > 1) $page = $this->input->get('page');
if ($this->uri->segment(3)) $do = $this->uri->segment(3);
if ($this->uri->segment(4)) $get_id = $getMemberID = $this->uri->segment(4);

/*
  | SAVE 
*/
if (post('btnInsert')) {
	
	var_dump($_POST);die;
	$name = filter( post("f_Name") );
	$email = filter( post("f_Email") );
	$DOB = filter( post("f_DOB") );
	$isActive = 0;
	if (post('f_IsActive') == 1 ) $isActive = 1 ;
	
	if (is_filled($name)) {
		
		$param = array(
			'Name' => $name,
			'Email' => $email,
			'DOB' => $DOB,
			'IsActive' => $isActive,
			//'IsPublish' => $isPublish
		);
		
		$save = $this->member_model->save($param);
		
		($save)?$message['message'] = MESSAGE::SAVE:$message['message'] = MESSAGE::ERROR;
		$message['message'] = getMessage($message['message']);
	} else {
		$message['message'] = getMessage(MESSAGE::NOT_FOUND);
	}
}

/*
  | UPDATE 
*/
if (post('btnUpdate')) {
	
	$is_subscribe = 0;
	$dob = NULL;
	if (isset($_POST['IsSubscribe'])) {
		$is_subscribe = 1;
	}
	if (isset($_POST['DOB'])) {
		$tempdob = explode('-',$_POST['DOB']);
		$dob = $tempdob[2].'-'.$tempdob[1].'-'.$tempdob[0];
	}
	$member_id = member_cookies('MemberID');
	$param = array(
		'FullName' => $_POST['FullName'],
		'Gender' => $_POST['Gender'],
		'DOB' => $dob,
		'Facebook' => $_POST['Facebook'],
		'Twitter' => $_POST['Twitter'],
		'AboutMe' => $_POST['AboutMe'],
		'IsSubscribe' => $is_subscribe,
	);
	
	$update = $this->member_model->update($getMemberID, $param);
	($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
	
	// if (post('f_update') && is_numeric($getMemberID)) {
		// $param['ArticleCategoryID'] = $getMemberID;
		// $objArticleCategory = $this->member_model->get($param);
		// if (!empty($objArticleCategory)) {
			// $param = array(
				// 'CategoryName' => $categoryName,
				// 'Description' => $description
			// );

			// $update = $this->member_model->update($getMemberID, $param);
			
			// ($update)?$message['message'] = MESSAGE::UPDATE:$message['message'] = MESSAGE::ERROR;
			// $message['message'] = getMessage($message['message']);
		// } else {
			// $message['message'] = getMessage(MESSAGE::NOT_FOUND);
		// }
	// }
}

/*
  | DELETE 
*/
if ($do == "delete") {
	
	if (is_numeric($getMemberID)) {
		$delete = $this->member_model->delete($getMemberID);
		if ($delete) {
			($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
			$message['message'] = getMessage($message['message']);
		} else {
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}

/*
  | GROUP ACTION 
  | CHECKED BOX
*/
if (isset($_POST['btn_group_action'])) {
	if ($_POST['lst_group_action'] == "delete") {
		if (!empty($_POST['chkbox'])) { 
			$delete = false;
			foreach (post('chkbox') as $key => $val) {
				$delete = $this->member_model->delete($val);
			}
			
			if ($delete) {
				($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
				$message['message'] = getMessage($message['message']);
			} else {
				$message['message'] = getMessage(MESSAGE::NOT_FOUND);
			}
			
		}
	}
}

// SEARCH
if (get('keyword')) {
	$param['keyword'] = get('keyword');
}

$param['paging'] = TRUE;
$param['offset'] = $offset;
$data = $this->member_model->get_list($param);

$total_rows = $data['total_rows'];
$list_data = $data['data'];
?>

<?php 

?>
<div class="col-xs-12">
<?php echo $SIDEMENUBAR ?>
</div>

<div class="col-xs-12">
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="talLft"><a href="<?php echo base_url()?>admin/<?php echo $MODULE?>/insert" class="btn btn-success br"><i class="fa fa-plus"></i>&nbsp; New <? echo $MODULE?></a></div><br/>
		<?php } ?>
	</div>
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="br talRgt">
			<form method="get">
			<input class="input input-sm" type="text" name="keyword" value="<?php if (get('keyword')) echo get('keyword')?>" placeholder="Keyword" />
			<button class="btn btn-info btn-sm" > <i class="fa fa-search-minus"></i> Filter</button>
			</form>
		</div>
		<?php } ?>
	</div>
	<div class="clearfix"></div>
	
	<?php if (isset($message['message'])) echo message($message['message'])?>
		
	<?php
	if ((!is_filled($do) && !is_filled($getMemberID)) || $do == "delete")
	{
		if (!empty($data))
		{
			$str = "";
			$total_rows = $data['total_rows'];
			$list_data = $data['data'];
			?>
			
			<form method="post">
			<div class="table-responsive">
			<table class="table table-hover table-striped">
			<tr class="b">
				<th width=1><input type="checkbox" class="chkbox togglebox" onclick="togglebox()" /></th>
				<th width=1>#</th>
				<th>Name</th>
				<th>Email</th>
				<th>IsVerify</th>
				<th>Birthday</th>
				<th>JoinDate</th>
				<th class="talRgt">Option</th>
			</tr>
			<?php
			$i = 0;
			if (is_numeric($page) && $page > 0) 
			{
				$i = ($page - 1) * $offset;
			}
			foreach($list_data as $key => $rs)
			{
				$rs = (array) $rs;
				$id = $rs['member_id'];
				$i += 1;
				$url = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2);
				?>
				<tr>
				<td><input type="checkbox" name="chkbox[]" id="chkbox[]" class="chkbox" value="<?php echo $id?>" /></td>
				<td><?php echo $i ; ?></td>
				<td><?php echo $rs['fullname']; ?></td>
				<td><?php echo $rs['email']; ?></td>
				<td><?php echo $rs['is_active']; ?></td>
				<td><?php echo $rs['dob']; ?></td>
				<td><?php echo $rs['creator_date']; ?></td>
				<td class="talRgt"><a href="<?php echo $url.'/edit/'.$id; ?>" title="Edit data" alt="Edit data"><i class="clrBlu fa fa-pencil-square-o fa-2x"></i></a> 
				<a href="<?php echo $url.'/delete/'.$id; ?>" onclick="return confirm('Yakin menghapus data ini ?')"><i class="clrRed fa fa-times fa-2x" title="Delete data" alt="Delete data"></i></a></td>
				</tr>
				<?php 
			}
			?>
			<tr>
				<td colspan="100%">
					<div id="group_action">With checked do <select class="input" name="lst_group_action"><option class="" value="delete">Delete</option></select>
					<button class="btn btn-default btn-sm" name="btn_group_action" id="btn_group_action"value="1">Action</button>
					</div>
				</td>
			</tr>
			</table>
			</div>
			</form>
			<br/>
			
		<?php
			if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
		}
		else 
		{
			echo 'No data Exist';
		}
	}
	
	//if (isset($do) || isset($get_id))
	else
	{
		$obj = array();
		if ($do != 'insert')
		{
			$tmp['ArticleCategoryID'] = $get_id;
			$obj = $this->member_model->get($tmp);
			$dob = explode('-',$obj['DOB']);
			$dob = $dob[2].'-'.$dob[1].'-'.$dob[0];
		}
		
		$tempdob = filter(post('f_DOB'));
		if (is_filled($tempdob)) {
			$tempdob = explode('-',$tempdob);
			$dob = $tempdob[2].'-'.$tempdob[1].'-'.$tempdob[0];
		}
	?>
	<?php if ($do == "edit") echo "<div class='fntLg'>Edit ".$MODULE."</div><br>"; ?>
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<label for='f_Name' class='col-sm-2'>FullName</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_FullName' id='f_FullName' placeholder='Enter FullName' value='<?php if (!empty($obj)) echo $obj['FullName']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_Email' class='col-sm-2'>Email</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_Email' id='f_Email' placeholder='Enter Email' value='<?php if (!empty($obj)) echo $obj['Email']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_IsActive' class='col-sm-2'>IsActive</label>
			<div class='col-sm-10 checkbox'><label><input type='checkbox' class='' name='f_IsActive' id='f_IsActive' value='1' <?php if (!empty($obj) && $obj['IsActive']) echo 'checked=true'?>></label></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_Gender' class='col-sm-2'>Gender</label>
			<div class='col-sm-10'>
			<select class='form-control' name='f_Gender'>
			<option value='1' <?php if (!empty($obj) && $obj['Gender'] == 1) echo 'selected'?>><?php echo MALE?></option>
			<option value='2' <?php if (!empty($obj) && $obj['Gender'] == 2) echo 'selected'?>><?php echo FEMALE?></option>
			</select>
			</div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_DOB' class='col-sm-2'>DOB</label>
			<div class='col-sm-10'><input type='text' class='form-control datepicker' name='f_DOB' id='f_DOB' placeholder='Enter Date Of birth' value='<?php if (!empty($obj)) echo $dob?>'/></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_Facebook' class='col-sm-2'>Facebook</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_Facebook' id='f_Facebook' placeholder='Enter Facebook' value='<?php if (!empty($obj)) echo $obj['Facebook']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_Twitter' class='col-sm-2'>Twitter</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_Twitter' id='f_Twitter' placeholder='Enter Twitter' value='<?php if (!empty($obj)) echo $obj['Twitter']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_AboutMe' class='col-sm-2'>AboutMe</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_AboutMe' id='f_AboutMe' placeholder='Enter AboutMe' value='<?php if (!empty($obj)) echo $obj['AboutMe']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_IsSubscribe' class='col-sm-2'>Berlangganan Newsletter</label>
			<div class='col-sm-10 checkbox'>
				<label><input type='checkbox' name='f_IsSubscribe' id='f_IsSubscribe' value='1' <?php if (!empty($obj) && $obj['IsSubscribe'] == 1) echo 'checked'?>></label>
			</div>
		</div>
		
		<div class='form-group form-group-sm col-sm-12'>
		<?php if ($do == 'insert') { ?>
		<button class='btn btn-success' name='btnInsert' value='1'><?php echo SAVE?></button>
		<?php } else if ($do == 'edit'){ ?>
		<button class='btn btn-success' name='btnUpdate' value='1'><?php echo UPDATE?></button>
		<?php } ?>
		</div>
		<div class="clearfix"></div>
	</form>
	<?php
	}
	?>
</div>
<script>
$(document).ready( function() {
	
	function togglebox(){
		if ($('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',true);
		}
		if (!$('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',false);
		}
	}
	
	$('#group_action').hide();
	$('.chkbox').click(function(){
        var count = $("[type='checkbox']:checked").length;
		if (count >= 1) {
             $('#group_action').show();
        } else if(count <= 0){
            $('#group_action').hide();
        }
    });

});
</script>