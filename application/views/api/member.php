<?php
$json = $method = NULL;
$json['code'] = 200;
$json['action'] = 'nothing';
$json['message'] = "berhasil";

$method = $this->uri->segment(3);

if ($method == "get_list") 
{
    if ($this->input->get())
    {
        $params = $this->input->get();
    }
    $params['paging'] = TRUE;
    if (isset($_GET['page'])) $params['page'] = $_GET['page'];
        
    if ($this->input->get('member_id')) 
    {
        $params['member_id'] = $this->input->get('member_id');
        $json = ($this->member_model->get($params));
    }
    else 
    {
        $json = ($this->member_model->get_list($params));
    }
}

if (isset($_POST))
{
    $post = file_get_contents('php://input');
    if (! empty($post)) $post = json_decode($post, true);

    $json['action'] = 'save';
    $json['message'] = 'nothing';

    $param = NULL;
    if (isset($post['email']))
    {
        $param['email'] = $post['email'];
        $save = $this->member_model->save($param);

        if ($save) 
        {
            $json['code'] = 200;
            $json['action'] = 'save';
            $json['message'] = 'Data ' . $param['email'] .' has been saved'. $save['member_id'];
        } 
        else 
        {
            $json['code'] = 200;
            $json['action'] = 'error';
            $json['message'] = 'error occured';
        }
    }
}

echo json_encode($json);
die;

?>