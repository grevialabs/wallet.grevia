<?php 
function is_internal(){
	$root = $_SERVER['SERVER_NAME'];
	if($root=="localhost")
		return true;
	else
		return false;
}

function is_filled($str){
	// php 7.4 up
	// if (isset($str)) return TRUE; else return FALSE;
	
	// php 7.4 below
	if (isset($str) && $str != '') return TRUE; else return FALSE;
}

function is_alphanumeric($input)
{
	if(preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $input))
		return true;
	else
		return false;
}

function is_valid_email($email)
{
	if(filter_var($email, FILTER_VALIDATE_EMAIL))
		return true;
	else
		return false;
}

function message($string, $type = 'info'){
	$return = '';
	if ($type == "info") {
		$return = '<div class="alert alert-info b"><i class="fa fa-exclamation-circle"></i> &nbsp;&nbsp;'.$string.'</div>';
	}
	if ($type == "success") {
		$return = '<div class="alert alert-success b"><i class="fa fa-check-circle"></i> &nbsp;&nbsp;'.$string.'</div>';
	}
	if ($type == "error") {
		$return = '<div class="alert alert-danger b"><i class="fa fa-remove"></i> &nbsp;&nbsp;'.$string.'</div>';
	}
	return $return;
}

function print_message($string, $type = 'info'){
	$return = '';
	if ($type == "info") {
		$return = '<div class="alert alert-info b"><i class="fa fa-exclamation-circle"></i> &nbsp;&nbsp;'.$string.'</div>';
	}
	if ($type == "success") {
		$return = '<div class="alert alert-success b"><i class="fa fa-check-circle"></i> &nbsp;&nbsp;'.$string.'</div>';
	}
	if ($type == "error") {
		$return = '<div class="alert alert-danger b"><i class="fa fa-remove"></i> &nbsp;&nbsp;'.$string.'</div>';
	}
	return $return;
}

function print_balloon_message($string, $type = 'info'){
	$ret = '';
	// if ($type == "info") {
		// $return = '<div class="alert alert-info b"><i class="fa fa-exclamation-circle"></i> &nbsp;&nbsp;'.$string.'</div>';
	// }
	// if ($type == "success") {
		// $return = '<div class="alert alert-success b"><i class="fa fa-check-circle"></i> &nbsp;&nbsp;'.$string.'</div>';
	// }
	// if ($type == "error") {
		// $return = '<div class="alert alert-danger b"><i class="fa fa-remove"></i> &nbsp;&nbsp;'.$string.'</div>';
	// }
	$ret = '
	<script>
	$(document).ready(function(){
		show_balloon("'.$string.'","'.$type.'");
	});
	</script>
	';
	return $ret;
}
function currentPageUrl() {
    $curpageURL = 'http';
    //if ($_SERVER["HTTPS"] == "on") {$curpageURL.= "s";}
		$curpageURL.= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
		$curpageURL.= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
		$curpageURL.= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $curpageURL;
	// return http://localhost/kisi-kisi.com/showcategoryarticle.php?aid=6&title=iniabsurd&wewew=hehe&jablay=gila 
}
	
//covert to non HTML for security
function filter($input) {
	$input = strip_tags($input);
	// $input = mysql_real_escape_string($input);
	$input = str_replace("<","<",$input);
	$input = str_replace(">",">",$input);
	$input = str_replace("#","%23",$input);
	$input = str_replace("'","`",$input);
	$input = str_replace(";","%3B",$input);
	$input = str_replace("script","",$input);
	$input = str_replace("%3c","",$input);
	$input = str_replace("%3e","",$input);
	$input = trim($input);
	return $input;
}

function post($val){
	//post is more secure, limit file 8mb
	$ret = '';
	if (isset($_POST[$val])) $ret = $_POST[$val];
	return $ret;
}

function get($val){
	//get is visible in url address and not secure
	$ret = '';
	if (isset($_GET[$val])) $ret = $_GET[$val];
	return $ret;
}

// function print_message($val = '', $type = INFO){
	// /*
	// INFO for highlight message
	// ERRO for red warning message 
	// */
	// if($type == INFO){
		// $str = '<div class="padMed wdtFul">
					// <div class="bg-info">
						// <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>'.$val.'.
					// </div>
				// </div>';
	// }
	// if($type == ERROR){
		// $str = '<div class="padMed wdtFul">
					// <div class="bg-warning" style="margin-top: 5px; padding: .7em;">
						// <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>'.$val.'.
					// </div>
				// </div>';
	// }
	// return $str;
// }

function replace_quote($string,$val = "str"){
	if ($val == "str") {
		if ($string == NULL || !is_filled($string)) {
			$retval = "NULL";
		} else {
			$retval = "'".$string."'";
		}
		if (is_numeric($string)) $retval = $string;
	}
	if ($val=="strsearch") {
		$retval = "'%".$string."%'";
	}
	if ($val=="num") {
		if (!isset($string)) {
			$retval = 0;
		} else {
			$retval = $string;
		}
	}
	return $retval;
}
DEFINE('ENCRYPTION_KEY', 'PrsQNb6H9a15578a0a');
function encrypt($str){
	return simple_crypt(ENCRYPTION_KEY, $str, 'encrypt');
}
function decrypt($str){
	return simple_crypt(ENCRYPTION_KEY, $str, 'decrypt');
}

/**
 * http://www.westhost.com/contest/php/function/simple-encrypt-decrypt-using-a-key/227
 * Simple Encrypt / Decrypt using a key, specify the action
 * which defaults to encrypt or we assume it is to decrypt
 * the encrypted text can be saved into cookies, with out 
 * being afraid of getting it cracked, provided the key is 
 * long enough
 *
 * @param {$key} string - key to be used while string is encoded or decoded
 * @param {$string} string - string to be encoded or decoded
 * @param {$action} [encrypt/decrypt] - the action to be performed
 */
function simple_crypt($key, $string, $action = 'encrypt'){
		$res = '';
		if($action !== 'encrypt'){
			$string = base64_decode($string);
		} 
		for( $i = 0; $i < strlen($string); $i++){
				$c = ord(substr($string, $i));
				if($action == 'encrypt'){
					$c += ord(substr($key, (($i + 1) % strlen($key))));
					$res .= chr($c & 0xFF);
				}else{
					$c -= ord(substr($key, (($i + 1) % strlen($key))));
					$res .= chr(abs($c) & 0xFF);
				}
		}
		if($action == 'encrypt'){
			$res = base64_encode($res);
		} 
		return $res;
}

function is_member($user = "member")
{
	if (isset($_COOKIE['hash']))
	{
		explode(',',$_COOKIE['hash']);
		return TRUE;
	}
	else
	return FALSE;
}

function is_company($user = "member")
{
	if (isset($_COOKIE['hash_company']))
	{
		explode(',',$_COOKIE['hash_company']);
		return TRUE;
	}
	else
	return FALSE;
}

function is_admin() {
	if (member_cookies("is_admin"))
		return true; 
	else 
		return false;
}

function member_cookies($field)
{
	if (isset($_COOKIE['hash'])) {
		$str = '';
		$hash = explode('#',decrypt($_COOKIE['hash']));
		switch($field)
		{
			case 'member_id':
				$str = $hash[0];
				break;
			case 'fullname':
				$str = $hash[1];
				break;
			case 'email':
				$str = $hash[2];
				break;
			case 'is_admin':
				$str = $hash[3];
				break;
		}
		return $str;
	} else 
	return NULL;	
	
}

function company_cookies($field)
{
	if (isset($_COOKIE['hash_company'])) {
		$str = '';
		$hash = explode('#',decrypt($_COOKIE['hash_company']));
		switch($field)
		{
			case 'company_id':
				$str = $hash[0];
				break;
			case 'name':
				$str = $hash[1];
				break;
			case 'email':
				$str = $hash[2];
				break;
			case 'is_admin':
				$str = $hash[3];
				break;
		}
		return $str;
	} else 
	return NULL;	
	
}

function pre($str, $is_array = FALSE)
{
	if ($is_array == FALSE) {
		echo '<pre>'.$str.'</pre>';
		
	} else {
		var_dump($str);
	}
	die;
}

function getReferer(){
	return $_SERVER['HTTP_REFERER'];
}
function getIP(){
	return $_SERVER['REMOTE_ADDR'];
}
function getUserAgent(){
	return $_SERVER['HTTP_USER_AGENT'];
}
function getDateTime($format = 'Y-m-d H:i:s'){
	// date_default_timezone_set("Asia/Bangkok");
	// return date("Y-m-d H:i:s", time());
	
	$now = new DateTime();
    $now->setTimezone(new DateTimeZone('Asia/Bangkok'));
    return $now->format($format);
}

// ---------------------
function get_referer(){
	return $_SERVER['HTTP_REFERER'];
}
function get_ip(){
	return $_SERVER['REMOTE_ADDR'];
}
function get_useragent(){
	return $_SERVER['HTTP_USER_AGENT'];
}
function get_datetime($format = 'Y-m-d H:i:s'){
	// date_default_timezone_set("Asia/Bangkok");
	// return date("Y-m-d H:i:s", time());
	
	$now = new DateTime();
    $now->setTimezone(new DateTimeZone('Asia/Bangkok'));
    return $now->format($format);
}
function getTimeStamp(){
	return time();
}
function formatDateTime(){
	return "Y-m-d H:i:s";
}
function getDay($timestamp){
	//input timestamp 110001548787
	//$timestamp = convertToTimeStamp($timestamp);
	$date = date("l", $timestamp);
	switch($date){
		case "Sunday":
			$day = "Minggu";
			break;
		case "Monday":
			$day = "Senin";
			break;
		case "Tuesday":
			$day = "Selasa";
			break;
		case "Wednesday":
			$day = "Rabu";
			break;
		case "Thursday":
			$day = "Kamis";
			break;
		case "Friday":
			$day = "Jumat";
			break;
		case "Saturday":
			$day = "Sabtu";
			break;
	}
	return $day;
}
// Default usage getDateFormat(DATE_FORMAT)
// Default usage getDateFormat(DATE_FORMAT,$strtime)
function getDateFormat($format="",$time = NULL){
	if (!isset($time)) $time = time(); 
	return date($format, strtotime($time));
}

function genRandomString($length = 10) {
    $string = "";    
	$str = array("1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
	for($i=0;$i<$length;$i++){
		$string .= $str[rand(0,count($str)-1)];
	}
    return strtolower($string);
}

function header_status($statusCode) {
    static $status_codes = null;

    if ($status_codes === null) {
        $status_codes = array (
            100 => 'Continue',
            101 => 'Switching Protocols',
            102 => 'Processing',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            207 => 'Multi-Status',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            422 => 'Unprocessable Entity',
            423 => 'Locked',
            424 => 'Failed Dependency',
            426 => 'Upgrade Required',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            506 => 'Variant Also Negotiates',
            507 => 'Insufficient Storage',
            509 => 'Bandwidth Limit Exceeded',
            510 => 'Not Extended'
        );
    }

    if ($status_codes[$statusCode] !== null) {
        $status_string = $statusCode . ' ' . $status_codes[$statusCode];
        header($_SERVER['SERVER_PROTOCOL'] . ' ' . $status_string, true, $statusCode);
    }
}

function debug_array($arr){
	echo "<pre>".print_r($arr,1)."</pre>";
}

function current_date_time()
{
	return date("Y-m-d H:i:s",time());
}

function format_money($money, $currency = "IDR", $separator = '.')
{
	return number_format($money,0,'',$separator).' '.$currency;
}

function order_payment_status($code)
{
	$arr = array(
		1 => ORDER_NOT_PAID,
		2 => ORDER_WAITING_CONFIRMATION,
		3 => ORDER_PENDING,
		5 => ORDER_ISSUED,
		7 => ORDER_CANCELED,
		//'1 = not paid ; 2 = waiting for confirm ; 3 = pending 5 = issued',
	);
	
	$arrcolor = array(
		1 => 'clrBlk',
		2 => 'clrYlw',
		3 => 'clrYlw',
		5 => 'clrGrn',
		7 => 'clrRed',
	);
	
	if ($code == 'get') 
		return $arr;
	else
		return '<div class="b '.$arrcolor[$code].'">'.$arr[$code].'</div>'; 
}

function absent_status($code,$show_div = TRUE)
{
	
	$arr = array(
		'0' => 'Masuk normal',
		'-1' => 'Tidak Masuk',
		'1' => 'Izin',
		'2' => 'Cuti sehari',
		'3' => 'Cuti setengah hari',
	);
	
	$arrcolor = array(
		'0' => 'clrBlk',
		'-1' => 'clrYlw',
		'1' => 'clrYlw',
		'2' => 'clrGrn',
		'3' => 'clrRed',
	);
	
	if ($code == 'get') 
		return $arr;
	else
		if ($show_div) return '<div class="b '.$arrcolor[$code].'">'.$arr[$code].'</div>'; 
		else return $arr[$code]; 
}

function right($v,$len) {
	$ret = substr($v,-$len);
	return $ret;
}
function left($v,$len) {
	$ret = substr($v,0,$len);
	return $ret;
}

function debug($arr,$die = FALSE){
	echo "<pre>".print_r($arr,1)."</pre>";
	if ($die) die; 
}

function get_treasure_type($x)
{
	if ($x == 1) $ret = 'Rekening';
		else if ($x == 2) $ret = 'Tunai';
		return $ret;
}

function get_priority() 
{
    return $arr_priority = array(
		'1' => 'Low',
		'2' => 'Medium',
		'3' => 'High',
	);
}

function get_priority_clr()
{
	return $arr_priority_clr = array(
		'1' => 'btn-primary',
		'2' => 'btn-warning',
		'3' => 'btn-danger',
	);
}


// format 'd D M m y Y H:i:s
// date : 22 aug 2021
function convert_date($format, $date) {
	$date = strtotime($date);
	return date($format,$date);
}

// show fund type
function get_fund_type($fund_type)
{
	$icon = NULL;
	switch($fund_type)
	{
		case 'cash':
			$icon = '<i class="fa fa-money clrGrn" title="Cash"></i>';
			break;
		case 'credit card':
			$icon = '<i class="fa fa-credit-card clrOrg" title="Credit card"></i>';
			break;
		case 'bank account':
			$icon = '<i class="fa fa-bank clrBlu" title="Bank Account"></i>';
			break;
	}
	return $icon;
}

function member_active($page) 
{
	global $current_segment;
	$str = '';
	if ($current_segment === $page)
	{
		$str = 'active';
	}
	return $str;
}

function admin_active($page) 
{
	global $current_segment;
	$str = '';
	if ($current_segment === $page)
	{
		$str = 'active';
	}
	return $str;
}

?>