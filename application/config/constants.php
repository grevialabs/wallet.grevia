<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('OFFSET', '20');
define('PAGING_OFFSET', '20');
define('DEFAULT_PAGE_TITLE', 'Wallet Grevia');
define('DEFAULT_PAGE_DESCRIPTION', 'Situs pelacak keuangan kamu.');
define('DEFAULT_WEBSITE_NAME', 'Wallet Grevia');
define('DEFAULT_WEBSITE_DOMAIN', 'WALLET.GREVIA.COM');
define('IMAGE_ARTICLE_DIR', 'asset/images/');

define('FACEBOOK_APP_ID','1160117714002142' );
define('FACEBOOK_SECRET_ID','84ae220219d3c87258bfbca265754962' );

// define('GOOGLE_CLIENT_ID', '115356414498-nl4fnt9gu76dcmp5m0ao14mlj22cvs40.apps.googleusercontent.com');
// define('GOOGLE_SECRET_ID', 'Nhu1S2uI_aD_hvnizU_UIVPR');
// define('GOOGLE_REDIRECT_URI', 'http://127.0.0.1/grevia.com/login');
//---------------------------------------------------------------------------------------------
define('INFO', 'info');
define('ERROR', 'error');
define('BR', '<br/>');
define('HR', '<hr/>');
define('PERPAGE', '20');
define('PAGING_LIMIT', '20');
DEFINE('STORAGE', 'Storage');
//----------------------------------------------------------------------------------------------
// BUTTON 
// define('SAVE', 'Save');
// define('EDIT', 'Edit');
// define('ADD', 'Add');
// define('UPDATE' ,'Update');
// define('DELETE', 'Delete');

//-----------------------------------------------------------------------------------------------
define('DATE_FORMAT', 'd M Y, H:i');

function get_gender($gender)
{
	if ($gender == 1 ) return MALE;
	if ($gender == 2 ) return FEMALE;
}

interface MESSAGE{
	const ERROR = 0;
	const SAVE = 1;
	const UPDATE = 2;
	const DELETE = 3;
	const CONNECTION_ERROR = 4;
	const NOT_FOUND = 5;
}
function getMessage($val){
	switch($val){
		case MESSAGE::ERROR:
			$str="<span class='fntBld'>Data Process Error</span>";break;
		case MESSAGE::SAVE:
			$str="<span class='fntBld'>Save Success</span>";break;
		case MESSAGE::UPDATE:
			$str="<span class='fntBld'>Update Success</span>";break;
		case MESSAGE::DELETE:
			$str="<span class='fntBld'>Delete Success</span>";break;
		case MESSAGE::NOT_FOUND:
			$str="<span class='fntBld'>Data not found</span>";break;
	}
	return $str;
}

global $list_provinces;
$list_provinces = array (
'Aceh',
'Bali',
'Bangka Belitung',
'Banten',
'Bengkulu',
'DI Jogyakarta',
'DKI Jakarta',
'Gorontalo',
'Jambi',
'Jawa Barat',
'Jawa Tengah',
'Jawa Timur',
'Kalimantan Barat',
'Kalimantan Selatan',
'Kalimantan Tengah',
'Kalimantan Timur',
'Lampung',
'Maluku',
'Maluku Utara',
'Nusa Tenggara Barat',
'Nusa Tenggara Timur',
'Papua',
'Papua Barat',
'Riau',
'Riau Kepulauan',
'Sulawesi Barat',
'Sulawesi Selatan',
'Sulawesi Tengah',
'Sulawesi Tenggara',
'Sulawesi Utara',
'Sumatera Barat',
'Sumatera Selatan',
'Sumatra Utara'
);

/* End of file constants.php */
/* Location: ./application/config/constants.php */