<?php

Class MY_Model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();		
	}
	
	public function pre($str)
	{
		echo '<pre>'.$str.'</pre>';
		die;
	}
	
	/*
	 * Callback: 
	 array
	 response: string,
	 error_msg: string,
	*/
	public function curl_sendblue($method = 'post', $data)
	{
		$result = $smtp = NULL;
		$smtp['url'] = 'https://api.sendinblue.com/v3/smtp/email';
		$smtp['key'] = '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';
		// $smtp['from'] = 'grevianet@gmail.com';
		
		$ch = curl_init($smtp['url']);
		
		# Setup request to send json via POST.
		if (isset($method) && $method == "post")
		{
			
			$data_post = '';
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($data) );
		}
	
		# Return response instead of printing.
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		
		# bypass restriction ssl from localhost
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
		$headers = array(
			'Token: grevia',
			'api-key: ' . $smtp['key'],
			'content-type: application/json',
			'accept: application/json',
		);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);			

		# Send request.
		$result['response'] = curl_exec($ch);
		if (isset($result['response'])) $result['response'] = json_decode($result['response'],1);
		
		if (curl_errno($ch)) {
			$result['error_msg'] = curl_error($ch);
		}
		
		curl_close($ch);
		
		# Print response.
		return $result;
	}
}