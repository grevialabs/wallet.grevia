
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
	<title></title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
</head>

<body>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="font-family: Arial, sans-serif;">
	<tbody>
		<tr>
			<td colspan="100%"><img alt="Muahunter" height="90px" src="http://wallet.grevia.com/asset/images/logo_new.png?v=1" width="90px" /></td>
		</tr>
		<tr>
			<td colspan="100%">
			<p>Dear {name},</p>
			<p>Anda mendapat email reminder dari Apps Expense Tracker Grevia tentang tagihan:</p>
			</td>
		</tr>
		<tr>
			<td style="border-top:1px solid #000;border-left:1px solid #000;text-align:left;padding:10px;font-weight:bold">Tanggal</td>
			<td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;text-align:right;padding:10px;">{tanggal}</td>
		</tr>
		<tr>
			<td style="border-top:1px solid #000;border-left:1px solid #000;text-align:left;padding:10px;font-weight:bold">Tagihan</td>
			<td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;text-align:right;padding:10px;">{expense_name}</td>
		</tr>
		<tr>
			<td style="border-top:1px solid #000;border-left:1px solid #000;text-align:left;padding:10px;font-weight:bold">Nominal</td>
			<td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;text-align:right;padding:10px;">{amount}</td>
		</tr>
		<tr>
			<td style="border-top:1px solid #000;border-left:1px solid #000;text-align:left;padding:10px;font-weight:bold">Jatuh tempo</td>
			<td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;text-align:right;padding:10px;">{reminder_date}</td>
		</tr>
		<tr>
			<td style="border-top:1px solid #000;border-left:1px solid #000;text-align:left;padding:10px;font-weight:bold">Notes</td>
			<td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;text-align:right;padding:10px;">{notes}</td>
		</tr>
		<tr>
			<td colspan="2" style="border:1px solid #000;padding:10px;"> </td>
		</tr>
		<tr>
			<td colspan="2"> 
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse;">
					<tbody>
						<tr>
							<td style="padding-top:0;padding-right:18px;padding-bottom:18px;padding-left:18px;" valign="top" align="center">
								<table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;border-radius:6px;background-color:#000000;">
									<tbody>
										<tr>
											<td align="center" valign="middle" style="font-family:Arial;font-size:16px;padding:10px;">
												<a rel="nofollow" title="Lihat detail" target="_blank" href="{confirmation_link}" style="font-weight:bold;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;color:#FFFFFF;display:block;">Lihat detail</a>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>
