GREVIA WALLET RELEASE 

// ================================================================================================
// Saturday 29 Oct 2016 10:17
FRONT 
- add toggle show / hide report, detail, incoming reminder

// ================================================================================================
// Tuesday 11 Oct 2016 12:07
FRONT 
- repair reminder from showing future date only become => show all month and year active

API
- repair model expense

// ================================================================================================
// Monday 10 Oct 2016 15:07
FRONT
- repair bugfixing dashboard expense
- repair report expense set default on reminder date & time based on release_date
- set paging on absence
- add momentjs

MODEL
- change expense_model query

// ================================================================================================
// Friday 2 Sept 2016
FRONT
- add calendar with custom color, start end time and save ajax

// ================================================================================================
// Monday 22 Aug 2016
FEATURE
- add calendar

// ================================================================================================
// Monday 22 Aug 2016
08:51
FRONT
- bug fixing menu absence

// ================================================================================================
// Monday 22 Aug 2016
FRONT
- bugfixing menu EXPENSE DASHBOARD
- add menu dashboard in EXPENSE SIDEBAR
- add member redirect in MODULAR INDEX

// ================================================================================================
// Sunday 21 Aug 2016
FRONT
- add new feature "DASHBOARD"
- add new function "get_report_all_month" model
- add information message in report reminder date
