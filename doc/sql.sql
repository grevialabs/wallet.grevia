--------------------------------------------------------------------------------------------
--IMPLEMENT LATER
CREATE TABLE job_promo(
promo_id int(12) NOT NULL AUTO_INCREMENT,
promo_code varchar(10) DEFAULT NULL,
product_id int(11) DEFAULT NULL,
discount_amount decimal(12) DEFAULT 0,
PRIMARY KEY(promo_id)
)COLLATE='utf8_general_ci'
ENGINE=InnoDB;


------------------------------------------------------------------------------

ALTER TABLE job_participant_type_test ADD is_active tinyint NULL DEFAULT 0 AFTER test_id

ALTER TABLE job_history_test ADD participant_id int DEFAULT NULL AFTER end_time;

ALTER TABLE job_history_test_detail ADD participant_id int DEFAULT NULL AFTER test_detail_option_id;

CREATE TABLE job_quota(
quota_id int AUTO_INCREMENT NOT NULL,
company_id int NOT NULL,
current_quota tinyint NULL,
increase_quota tinyint DEFAULT 1 NULL,
decrease_quota tinyint DEFAULT 1 NULL,
after_quota tinyint NULL,
remarks varchar(255) NULL,
`creator_id` INT(11) NULL DEFAULT NULL,
	`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
	`creator_date` DATETIME NULL DEFAULT NULL,
	`editor_id` INT(11) NULL DEFAULT NULL,
	`editor_ip` VARCHAR(15) NULL DEFAULT NULL,
	`editor_date` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY(quota_id)
);

ALTER TABLE job_invoice_detail ADD `creator_id` INT(11) NULL DEFAULT NULL;
ALTER TABLE job_invoice_detail ADD `creator_ip` VARCHAR(15) NULL DEFAULT NULL;
ALTER TABLE job_invoice_detail ADD `creator_date` DATETIME NULL DEFAULT NULL;
ALTER TABLE job_invoice_detail ADD 	`editor_id` INT(11) NULL DEFAULT NULL;
ALTER TABLE job_invoice_detail ADD 	`editor_ip` VARCHAR(15) NULL DEFAULT NULL;
ALTER TABLE job_invoice_detail ADD 	`editor_date` DATETIME NULL DEFAULT NULL;

ALTER TABLE job_invoice ADD `order_code` VARCHAR(20) NULL DEFAULT NULL AFTER company_id;

ALTER TABLE `job_invoice_detail`
	ALTER `invoice_id` DROP DEFAULT;
ALTER TABLE `job_invoice_detail`
	CHANGE COLUMN `invoice_detail_id` `order_detail_id` INT(11) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `invoice_id` `order_id` VARCHAR(20) NOT NULL AFTER `order_detail_id`;
RENAME TABLE `job_invoice_detail` TO `job_order_detail`;

ALTER TABLE `job_invoice`
	ALTER `invoice_id` DROP DEFAULT;
ALTER TABLE `job_invoice`
	CHANGE COLUMN `invoice_id` `order_id` VARCHAR(20) NOT NULL FIRST;
RENAME TABLE `job_invoice` TO `job_order`;

UPDATE job_order SET order_id = order_code;
ALTER TABLE job_order DROP COLUMN order_code;
--------------------------------------------------------------------------------
ALTER TABLE job_participant ADD token varchar(50) DEFAULT NULL AFTER notes;

CREATE TABLE job_invoice(
invoice_id int(11) NOT NULL AUTO_INCREMENT,
company_id int(11) NOT NULL,
invoice_code varchar(20) DEFAULT NULL,
promo_code varchar(10) DEFAULT NULL,
detail text DEFAULT NULL,
subtotal DECIMAL(14,2) DEFAULT 0,
tax DECIMAL(14,2) DEFAULT 0,
grandtotal DECIMAL(14,2) DEFAULT 0,
payment_status tinyint DEFAULT 1 COMMENT '1 = not paid ; 2 = waiting for confirm ; 3 = pending 5 = issued',
notes text DEFAULT NULL,
creator_id int(11) DEFAULT NULL,
creator_ip varchar(15) DEFAULT NULL,
creator_date datetime DEFAULT NULL,
editor_id int(11) DEFAULT NULL,
editor_ip varchar(15) DEFAULT NULL,
editor_date datetime DEFAULT NULL,
PRIMARY KEY(invoice_id)

)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE job_invoice_detail(
invoice_detail_id int(11) NOT NULL AUTO_INCREMENT,
invoice_id int(11) NOT NULL,
product_id int(11) DEFAULT NULL,
price decimal(12,2) DEFAULT 0,
quantity mediumint DEFAULT 1,
PRIMARY KEY(invoice_detail_id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

INSERT INTO `job`.`job_invoice_detail` (`invoice_id`, `product_id`, `price`) VALUES (1, 1, 700000);

CREATE TABLE job_participant(
participant_id int(11) NOT NULL AUTO_INCREMENT,
company_id int(11) NOT NULL,
code_name varchar(50) DEFAULT NULL,
name varchar(150) DEFAULT NULL,
email varchar(100) DEFAULT NULL,
thepassword varchar(100) DEFAULT NULL,
gender TINYINT(4) NULL DEFAULT NULL, 
notes varchar(255) DEFAULT NULL,
creator_id int(11) DEFAULT NULL,
creator_ip varchar(15) DEFAULT NULL,
creator_date datetime DEFAULT NULL,
editor_id int(11) DEFAULT NULL,
editor_ip varchar(15) DEFAULT NULL,
editor_date datetime DEFAULT NULL,
PRIMARY KEY(participant_id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE job_product(
product_id int(11) NOT NULL AUTO_INCREMENT,
product_code varchar(10) DEFAULT NULL,
name varchar(100) DEFAULT NULL,
description varchar(100) DEFAULT NULL,
normal_price decimal(12) DEFAULT 0,
special_price decimal(12) DEFAULT 0,
participant_credit smallint DEFAULT 1,
creator_id int(11) DEFAULT NULL,
creator_ip varchar(15) DEFAULT NULL,
creator_date datetime DEFAULT NULL,
editor_id int(11) DEFAULT NULL,
editor_ip varchar(15) DEFAULT NULL,
editor_date datetime DEFAULT NULL,
PRIMARY KEY(product_id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

INSERT INTO `job_product` (`product_id`, `product_code`, `name`, `description`, `normal_price`, `special_price`, `participant_credit`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 'B01', 'Bronze Packet', 'This is a packet with quota 10 Participants ', 1000000, 700000, 10, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'G01', 'Gold Packet', 'This is a packet with quota 50 Participants ', 50000000, 43000000, 50, NULL, NULL, NULL, NULL, NULL, NULL);
--------------------------------------------------------------------------------------------
-- ALTER TABLE 5 Agustus 2015
ALTER TABLE job_test ADD timer smallint NULL DEFAULT 10 COMMENT 'in minutes' AFTER detail_attend ;
ALTER TABLE job_test ADD is_public tinyint NULL DEFAULT 1 AFTER is_publish ;
ALTER TABLE job_history_test ADD start_time datetime NULL DEFAULT NULL AFTER test_id ;
ALTER TABLE job_history_test ADD end_time datetime NULL DEFAULT NULL AFTER start_time ;

CREATE TABLE job_company (
  company_id int(11) NOT NULL AUTO_INCREMENT,
  company_code varchar(15) DEFAULT NULL,
  name varchar(100) DEFAULT NULL,
  address varchar(150) DEFAULT NULL,
  province varchar(150) NULL DEFAULT NULL,
  city varchar(150) NULL DEFAULT NULL,
  phone varchar(150) DEFAULT NULL,
  email varchar(150) DEFAULT NULL,
  thepassword varchar(50) DEFAULT NULL,
  pic_prefix varchar(3) NULL DEFAULT 'Mr.' COMMENT '1 => Miss , 2 => Mister' ,
  pic_name varchar(100) DEFAULT NULL,
  pic_phone varchar(100) DEFAULT NULL,
  pic_mobile varchar(100) DEFAULT NULL,
  notes text NULL DEFAULT NULL,
  active_code varchar(30) DEFAULT NULL,
  validation_code varchar(30) DEFAULT NULL,
  forgot_pass_expired datetime DEFAULT NULL,
  is_subscribe tinyint(4) DEFAULT 1,
  is_deleted tinyint(4) DEFAULT 0,
  is_active tinyint(4) DEFAULT 0,
  creator_id int(11) DEFAULT NULL,
  creator_ip varchar(15) DEFAULT NULL,
  creator_date datetime DEFAULT NULL,
  editor_id int(11) DEFAULT NULL,
  editor_ip varchar(15) DEFAULT NULL,
  editor_date datetime DEFAULT NULL,
  PRIMARY KEY (company_id)

) ENGINE=InnoDB AUTO_INCREMENT=1 ;
INSERT INTO `job`.`job_company` (`company_code`, `name`, `address`, `phone`, `email`, `thepassword`, `pic_name`, `pic_phone`, `pic_mobile`) VALUES ('JOB_TALENTO', 'PT Jobtalento Indonesia', 'Jl. Percaya no 9', '021-88289199', 'rusdi@jobtalento.com', '5OjEsstoeGqT', 'Momotaro', '5521414', '083891998825');

----------------------------------------------------------------------------------------------------------
--- ALTER TABLE HISTORY 24 Juni 2015
ALTER TABLE job_test ADD is_publish tinyint DEFAULT 0 NOT NULL AFTER detail_attend;

INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');
INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES (4, 'Silakan pilih', 1, '::1', '2015-09-12 23:27:59');




