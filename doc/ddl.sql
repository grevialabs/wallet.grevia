-- query get total amount per range calendar
SELECT release_date, FORMAT(CONCAT(SUM(amount),''),3) as expense
FROM wal_expense
GROUP BY release_date;

---------------------------------------------------------------------------------------------
-- total expense per month
SELECT 
(
	SELECT SUM(amount) as total
	FROM wal_expense
	WHERE release_date BETWEEN '2015-08-01' AND '2015-08-31' AND is_increment = 1
) as total_income,
(
	SELECT SUM(amount) as total
	FROM wal_expense
	WHERE release_date BETWEEN '2015-08-01' AND '2015-08-31' AND is_increment = 0
) as total_expense
,
(
	SELECT SUM(amount) as total
	FROM wal_expense
	WHERE release_date BETWEEN '2015-08-01' AND '2015-08-31' AND is_increment = 1
	)-(
	SELECT SUM(amount) as total
	FROM wal_expense
	WHERE release_date BETWEEN '2015-08-01' AND '2015-08-31' AND is_increment = 0
) as free;
---------------------------------------------------------------------------------------------
-- query expense report per category
SELECT c.name, FORMAT(CONCAT(SUM(amount),''),3) as expense 
FROM wal_expense
LEFT JOIN wal_category c USING(category_id)
WHERE release_date BETWEEN '2015-08-01' AND '2015-08-31' AND is_increment = 0
GROUP BY c.name



-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table job.job_company
CREATE TABLE IF NOT EXISTS `job_company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_code` varchar(15) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `province` varchar(150) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `thepassword` varchar(50) DEFAULT NULL,
  `pic_prefix` varchar(3) DEFAULT 'Mr.' COMMENT '1 => Miss , 2 => Mister',
  `pic_name` varchar(100) DEFAULT NULL,
  `pic_phone` varchar(100) DEFAULT NULL,
  `pic_mobile` varchar(100) DEFAULT NULL,
  `notes` text,
  `active_code` varchar(30) DEFAULT NULL,
  `validation_code` varchar(30) DEFAULT NULL,
  `forgot_pass_expired` datetime DEFAULT NULL,
  `is_subscribe` tinyint(4) DEFAULT '1',
  `is_deleted` tinyint(4) DEFAULT '0',
  `is_active` tinyint(4) DEFAULT '0',
  `creator_id` int(11) DEFAULT NULL,
  `creator_ip` varchar(15) DEFAULT NULL,
  `creator_date` datetime DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `editor_ip` varchar(15) DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table job.job_company: ~1 rows (approximately)
/*!40000 ALTER TABLE `job_company` DISABLE KEYS */;
INSERT INTO `job_company` (`company_id`, `company_code`, `name`, `address`, `province`, `city`, `phone`, `email`, `thepassword`, `pic_prefix`, `pic_name`, `pic_phone`, `pic_mobile`, `notes`, `active_code`, `validation_code`, `forgot_pass_expired`, `is_subscribe`, `is_deleted`, `is_active`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 'JOB_TALENTO', 'PT Jobtalento Indonesia', 'Jl. Percaya no 9', NULL, NULL, '021-88289199', 'rusdi@jobtalento.com', '5OjEsstoeGqT', 'Mr.', 'Momotaro', '5521414', '083891998825', NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `job_company` ENABLE KEYS */;


-- Dumping structure for table job.job_history_test
CREATE TABLE IF NOT EXISTS `job_history_test` (
  `history_test_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `creator_ip` varchar(15) DEFAULT NULL,
  `creator_date` datetime DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `editor_ip` varchar(15) DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY (`history_test_id`),
  KEY `test_id` (`test_id`),
  CONSTRAINT `job_history_test_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `job_test` (`test_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table job.job_history_test: ~3 rows (approximately)
/*!40000 ALTER TABLE `job_history_test` DISABLE KEYS */;
INSERT INTO `job_history_test` (`history_test_id`, `test_id`, `start_time`, `end_time`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 1, NULL, NULL, 1, '::1', '2015-05-20 19:09:57', NULL, NULL, NULL),
	(2, 3, NULL, NULL, 1, NULL, '2015-06-21 23:32:33', NULL, NULL, NULL),
	(3, 3, NULL, NULL, NULL, NULL, '2015-06-23 20:54:14', NULL, NULL, NULL);
/*!40000 ALTER TABLE `job_history_test` ENABLE KEYS */;


-- Dumping structure for table job.job_history_test_detail
CREATE TABLE IF NOT EXISTS `job_history_test_detail` (
  `history_test_detail_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `history_test_id` mediumint(9) NOT NULL,
  `test_detail_id` int(11) NOT NULL,
  `test_detail_option_id` int(11) NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `creator_ip` varchar(15) DEFAULT NULL,
  `creator_date` datetime DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `editor_ip` varchar(15) DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY (`history_test_detail_id`),
  KEY `test_detail_id` (`test_detail_id`),
  KEY `test_detail_option_id` (`test_detail_option_id`),
  CONSTRAINT `job_history_test_detail_ibfk_1` FOREIGN KEY (`test_detail_id`) REFERENCES `job_test_detail` (`test_detail_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `job_history_test_detail_ibfk_2` FOREIGN KEY (`test_detail_option_id`) REFERENCES `job_test_detail_option` (`test_detail_option_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table job.job_history_test_detail: ~13 rows (approximately)
/*!40000 ALTER TABLE `job_history_test_detail` DISABLE KEYS */;
INSERT INTO `job_history_test_detail` (`history_test_detail_id`, `history_test_id`, `test_detail_id`, `test_detail_option_id`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(2, 1, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
	(3, 1, 2, 5, 1, NULL, NULL, NULL, NULL, NULL),
	(4, 1, 3, 8, 1, NULL, NULL, NULL, NULL, NULL),
	(5, 2, 4, 10, 1, NULL, '2015-06-21 23:32:33', NULL, NULL, NULL),
	(6, 2, 8, 21, 1, NULL, '2015-06-21 23:32:33', NULL, NULL, NULL),
	(7, 2, 9, 25, 1, NULL, '2015-06-21 23:32:34', NULL, NULL, NULL),
	(8, 2, 10, 28, 1, NULL, '2015-06-21 23:32:34', NULL, NULL, NULL),
	(9, 2, 11, 31, 1, NULL, '2015-06-21 23:32:34', NULL, NULL, NULL),
	(10, 3, 4, 10, NULL, NULL, '2015-06-23 20:54:14', NULL, NULL, NULL),
	(11, 3, 8, 21, NULL, NULL, '2015-06-23 20:54:14', NULL, NULL, NULL),
	(12, 3, 9, 23, NULL, NULL, '2015-06-23 20:54:14', NULL, NULL, NULL),
	(13, 3, 10, 27, NULL, NULL, '2015-06-23 20:54:14', NULL, NULL, NULL),
	(14, 3, 11, 30, NULL, NULL, '2015-06-23 20:54:14', NULL, NULL, NULL);
/*!40000 ALTER TABLE `job_history_test_detail` ENABLE KEYS */;


-- Dumping structure for table job.job_inbox
CREATE TABLE IF NOT EXISTS `job_inbox` (
  `inbox_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(120) DEFAULT NULL,
  `email` varchar(120) DEFAULT NULL,
  `subject` varchar(120) DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`inbox_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table job.job_inbox: 0 rows
/*!40000 ALTER TABLE `job_inbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_inbox` ENABLE KEYS */;


-- Dumping structure for table job.job_invoice
CREATE TABLE IF NOT EXISTS `job_invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `invoice_code` varchar(20) DEFAULT NULL,
  `promo_code` varchar(10) DEFAULT NULL,
  `detail` text,
  `subtotal` decimal(14,2) DEFAULT '0.00',
  `tax` decimal(14,2) DEFAULT '0.00',
  `grandtotal` decimal(14,2) DEFAULT '0.00',
  `payment_status` tinyint(4) DEFAULT '1' COMMENT '1 = not paid ; 2 = waiting for confirm ; 3 = pending 5 = issued',
  `notes` text,
  `creator_id` int(11) DEFAULT NULL,
  `creator_ip` varchar(15) DEFAULT NULL,
  `creator_date` datetime DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `editor_ip` varchar(15) DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table job.job_invoice: ~1 rows (approximately)
/*!40000 ALTER TABLE `job_invoice` DISABLE KEYS */;
INSERT INTO `job_invoice` (`invoice_id`, `company_id`, `invoice_code`, `promo_code`, `detail`, `subtotal`, `tax`, `grandtotal`, `payment_status`, `notes`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 1, 'INV2015080700001', NULL, 'beli baran', 51000000.00, 5100000.00, 56100000.00, 1, NULL, 1, NULL, '2015-08-07 23:54:43', NULL, NULL, NULL);
/*!40000 ALTER TABLE `job_invoice` ENABLE KEYS */;


-- Dumping structure for table job.job_invoice_detail
CREATE TABLE IF NOT EXISTS `job_invoice_detail` (
  `invoice_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `price` decimal(12,2) DEFAULT '0.00',
  `quantity` mediumint(9) DEFAULT '1',
  PRIMARY KEY (`invoice_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table job.job_invoice_detail: ~2 rows (approximately)
/*!40000 ALTER TABLE `job_invoice_detail` DISABLE KEYS */;
INSERT INTO `job_invoice_detail` (`invoice_detail_id`, `invoice_id`, `product_id`, `price`, `quantity`) VALUES
	(1, 1, 1, 1000000.00, 1),
	(2, 1, 2, 50000000.00, 1);
/*!40000 ALTER TABLE `job_invoice_detail` ENABLE KEYS */;


-- Dumping structure for table job.job_member
CREATE TABLE IF NOT EXISTS `job_member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `thepassword` varchar(50) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `active_code` varchar(30) DEFAULT NULL,
  `validation_code` varchar(30) DEFAULT NULL,
  `forgot_pass_expired` datetime DEFAULT NULL,
  `is_subscribe` tinyint(4) DEFAULT '1',
  `is_admin` tinyint(4) DEFAULT '0',
  `is_deleted` tinyint(4) DEFAULT '0',
  `is_active` tinyint(4) DEFAULT '0',
  `creator_id` int(11) DEFAULT NULL,
  `creator_ip` varchar(15) DEFAULT NULL,
  `creator_date` datetime DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `editor_ip` varchar(15) DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table job.job_member: ~4 rows (approximately)
/*!40000 ALTER TABLE `job_member` DISABLE KEYS */;
INSERT INTO `job_member` (`member_id`, `first_name`, `last_name`, `email`, `thepassword`, `gender`, `dob`, `active_code`, `validation_code`, `forgot_pass_expired`, `is_subscribe`, `is_admin`, `is_deleted`, `is_active`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 'admin', 'udin', 'rusdi@jobtalento.com', '5OjEsstoeGqT', 2, '2015-05-04', NULL, NULL, NULL, 1, 1, 0, 0, NULL, NULL, '2015-05-07 09:50:25', NULL, NULL, NULL),
	(2, 'momo', 'taro', 'momo@momoa.com', '4tTEwdCfsA==', 1, '1980-08-30', 'pprdkmf3qms2dxpa2bj8uy3vdkqnpe', NULL, NULL, 1, 0, 0, 1, NULL, '::1', '2015-05-21 23:22:09', NULL, '::1', '2015-05-21 23:26:27'),
	(3, 'doni', 'anto', 'rusdi@kisimedia.com', '5OjEsss=', 1, '1980-08-30', 'ovrnzc3s2g3w7qx9967hl6yu2qt4yb', NULL, NULL, 1, 0, 0, 1, NULL, '::1', '2015-05-22 00:33:51', NULL, NULL, NULL),
	(4, 'r', 'a', 'rusdi@lakupon.com', '5OjEsss=', 1, '0000-00-00', 'fv2lduvmp8iz59ftwvlopdhosm2euy', NULL, NULL, 1, 0, 0, 0, NULL, '::1', '2015-07-23 23:46:49', NULL, NULL, NULL);
/*!40000 ALTER TABLE `job_member` ENABLE KEYS */;


-- Dumping structure for table job.job_participant
CREATE TABLE IF NOT EXISTS `job_participant` (
  `participant_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `code_name` varchar(50) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `thepassword` varchar(100) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `token` varchar(50) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `creator_ip` varchar(15) DEFAULT NULL,
  `creator_date` datetime DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `editor_ip` varchar(15) DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY (`participant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table job.job_participant: ~1 rows (approximately)
/*!40000 ALTER TABLE `job_participant` DISABLE KEYS */;
INSERT INTO `job_participant` (`participant_id`, `company_id`, `code_name`, `name`, `email`, `thepassword`, `gender`, `notes`, `token`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 1, 'momotaro', 'momogi', 'momo@momo.com', '5OjEsstoeGqT', 1, NULL, '8ad79qwe465a1cqwe8ad46q5we7', NULL, NULL, '2015-08-06 23:59:33', NULL, NULL, NULL);
/*!40000 ALTER TABLE `job_participant` ENABLE KEYS */;


-- Dumping structure for table job.job_product
CREATE TABLE IF NOT EXISTS `job_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `normal_price` decimal(12,0) DEFAULT '0',
  `special_price` decimal(12,0) DEFAULT '0',
  `participant_credit` smallint(6) DEFAULT '1',
  `creator_id` int(11) DEFAULT NULL,
  `creator_ip` varchar(15) DEFAULT NULL,
  `creator_date` datetime DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `editor_ip` varchar(15) DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table job.job_product: ~2 rows (approximately)
/*!40000 ALTER TABLE `job_product` DISABLE KEYS */;
INSERT INTO `job_product` (`product_id`, `product_code`, `name`, `description`, `normal_price`, `special_price`, `participant_credit`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 'B01', 'Bronze Packet', 'This is a packet with quota 10 Participants ', 1000000, 700000, 10, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'G01', 'Gold Packet', 'This is a packet with quota 50 Participants ', 50000000, 43000000, 50, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `job_product` ENABLE KEYS */;


-- Dumping structure for table job.job_subscribe
CREATE TABLE IF NOT EXISTS `job_subscribe` (
  `subscribe_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `is_subscribe` smallint(6) DEFAULT '1',
  `creator_date` datetime DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY (`subscribe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table job.job_subscribe: ~4 rows (approximately)
/*!40000 ALTER TABLE `job_subscribe` DISABLE KEYS */;
INSERT INTO `job_subscribe` (`subscribe_id`, `name`, `email`, `is_subscribe`, `creator_date`, `editor_date`) VALUES
	(1, 'momo', 'mahonara@ada.com', 1, '2015-08-03 19:39:52', NULL),
	(2, 'somplak', 'som@plak.com', 1, '2015-08-03 19:45:23', NULL),
	(3, 'tes', 'tes@t.com', 1, '2015-08-03 19:45:48', NULL),
	(4, 'ada', 'ada@ada.ada', 1, '2015-08-03 19:46:35', NULL);
/*!40000 ALTER TABLE `job_subscribe` ENABLE KEYS */;


-- Dumping structure for table job.job_test
CREATE TABLE IF NOT EXISTS `job_test` (
  `test_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `detail_description` varchar(1500) DEFAULT NULL,
  `detail_attend` varchar(350) DEFAULT NULL,
  `timer` smallint(6) DEFAULT '10' COMMENT 'in minutes',
  `is_publish` tinyint(4) NOT NULL DEFAULT '0',
  `is_public` tinyint(4) DEFAULT '1',
  `creator_id` int(11) DEFAULT NULL,
  `creator_ip` varchar(15) DEFAULT NULL,
  `creator_date` datetime DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `editor_ip` varchar(15) DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY (`test_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table job.job_test: ~3 rows (approximately)
/*!40000 ALTER TABLE `job_test` DISABLE KEYS */;
INSERT INTO `job_test` (`test_id`, `title`, `detail_description`, `detail_attend`, `timer`, `is_publish`, `is_public`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 'Test Psikologi Emotional Quotient', 'Test untuk mencari tahu kepribadian anda', 'Test ini adalah test dasar wajib untuk anda lakukan karena menentukan sekali\r\n', NULL, 0, 1, NULL, NULL, '2015-05-07 09:52:17', 1, '::1', '2015-06-24 01:06:54'),
	(2, 'Interaction & Adaptation', 'Tes untuk mengetahui kemampuan beradaptasi dan berinteraksi dengan sesama relasi di tempat kerja.', 'Tes ini adalah tes dasar untuk menentukan tingkat adaptasi seseorang.', NULL, 0, 1, NULL, NULL, '2015-06-06 18:33:01', 1, '::1', '2015-06-24 01:06:54'),
	(3, 'Tes Kepribadian 1 -  Percaya diri', 'Test untuk mengetahui tingkat percaya diri anda', NULL, 4, 1, 0, NULL, NULL, NULL, 1, '::1', '2015-08-05 22:25:03');
/*!40000 ALTER TABLE `job_test` ENABLE KEYS */;


-- Dumping structure for table job.job_test_detail
CREATE TABLE IF NOT EXISTS `job_test_detail` (
  `test_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) NOT NULL,
  `detail_description` varchar(1500) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `creator_ip` varchar(15) DEFAULT NULL,
  `creator_date` datetime DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `editor_ip` varchar(15) DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY (`test_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Dumping data for table job.job_test_detail: ~30 rows (approximately)
/*!40000 ALTER TABLE `job_test_detail` DISABLE KEYS */;
INSERT INTO `job_test_detail` (`test_detail_id`, `test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 1, 'Bagaimana anda menghadapi rekan kantor ? ', NULL, NULL, NULL, 1, '::1', '2015-06-17 23:13:23'),
	(2, 1, 'Apakah anda menikmati pekerjaan anda sekarang ini ?', NULL, NULL, NULL, 1, '::1', '2015-06-17 23:13:23'),
	(3, 1, 'Bagaimana cara anda menyikapi relasi kerja di kantor ?', NULL, NULL, NULL, 1, '::1', '2015-06-17 23:13:23'),
	(4, 3, 'Seberapa besar ketergantungan anda terhadap orang lain, karena hal tersebut membuat saya percaya anda dan merasa nyaman terhadap diri anda ? ', NULL, NULL, NULL, 1, '::1', '2015-06-17 23:13:23'),
	(6, 6, 'pertanyaan x', 1, '::1', '2015-06-18 01:08:51', NULL, NULL, NULL),
	(7, 7, 'Bisa ga nih ?', 1, '::1', '2015-06-18 22:11:12', 1, '::1', '2015-06-18 23:15:30'),
	(8, 3, 'Seberapa nyaman anda jika diundang menghadiri acara seminar ?', 1, '::1', '2015-06-19 00:09:25', NULL, NULL, NULL),
	(9, 3, 'Apakah anda sering khawatir dengan penampilan anda ?', 1, '::1', '2015-06-19 00:20:03', NULL, NULL, NULL),
	(10, 3, 'Nyamankah anda dengan diri sendiri ? ', 1, '::1', '2015-06-19 00:21:34', NULL, NULL, NULL),
	(11, 3, 'Sejauh mana anda aktif bertanya saat menghadiri diskusi ?', 1, '::1', '2015-06-19 00:22:40', NULL, NULL, NULL),
	(12, 3, 'Bagaimana anda menjelaskan harapan anda tentang kehidupan ?', 1, '::1', '2015-06-24 01:53:56', NULL, NULL, NULL),
	(13, 3, 'Seberapa baguskah anda dapat mempromosikan diri anda sendiri ?', 1, '::1', '2015-06-24 01:59:16', NULL, NULL, NULL),
	(16, 3, 'Apakah anda merasa perlu menyesuaikan diri agar diterima oleh orang lain ?', 1, '::1', '2015-06-24 02:24:19', NULL, NULL, NULL),
	(17, 3, 'Seberapa pentingkah untuk tetap bertahan dalam aturan sekolah / kampus, seperti misalnya orang tua ?', 1, '::1', '2015-06-24 02:26:37', NULL, NULL, NULL),
	(18, 3, 'Apakah anda berusaha keras mendapat pengakuan dari teman teman sekelas dalam kehidupan anda ?', 1, '::1', '2015-06-24 02:27:59', NULL, NULL, NULL),
	(19, 3, 'Apakah anda percaya dan yakin dengan penelitian anda sendiri ?', 1, '::1', '2015-06-24 02:30:35', NULL, NULL, NULL),
	(20, 3, 'Apakah anda memiliki standar yang tinggi dalam setiap hal ?', 1, '::1', '2015-06-24 02:32:37', NULL, NULL, NULL),
	(21, 3, 'Sejauh mana keoptimisan pandangan anda dalam menempuh pendidikan ?', 1, '::1', '2015-06-24 02:33:37', NULL, NULL, NULL),
	(22, 3, 'Bagaimana perasaan anda bila telah berusaha, namun mendapat nilai buruk dalam ulangan ? ', 1, '::1', '2015-06-24 02:35:04', NULL, NULL, NULL),
	(23, 3, 'Mudahkah anda bangkit ketika tidak naik kelas / tidak lulus mata kuliah tertentu ?', 1, '::1', '2015-06-24 02:37:34', NULL, NULL, NULL),
	(24, 3, 'Seberapa besar anda bersandar pada kemampuan anda sendiri ?', 1, '::1', '2015-06-24 02:38:37', NULL, NULL, NULL),
	(25, 3, 'seberapa ingin anda mengembangkan standar pribadi dalam kehidupan ?', 1, '::1', '2015-06-24 02:39:49', NULL, NULL, NULL),
	(26, 3, 'Percayakah anda bisa mengendalikan kehidupan anda sendiri  ? ', 1, '::1', '2015-06-24 02:42:12', NULL, NULL, NULL),
	(27, 3, 'Seberapa yakin anda dengan kemampuan anda sendiri ?', 1, '::1', '2015-06-24 02:42:49', NULL, NULL, NULL),
	(28, 3, 'Apakah anda menerima diri sendiri apa adanya ? ', 1, '::1', '2015-06-24 02:43:54', NULL, NULL, NULL),
	(29, 3, 'apakah anda takut mengambil resiko ?', 1, '::1', '2015-06-24 02:45:18', NULL, NULL, NULL),
	(30, 3, 'apakah anda merasa bebas dari jasa baik orang lain ?', 1, '::1', '2015-06-24 02:46:10', NULL, NULL, NULL),
	(31, 3, 'Bagaimana anda memandang pengalaman baru ?', 1, '::1', '2015-06-24 02:47:35', NULL, NULL, NULL),
	(32, 3, 'apakah anda melakukan evaluasi diri ?', 1, '::1', '2015-06-24 02:48:20', NULL, NULL, NULL),
	(33, 3, 'seringkah anda merasa kecewa ?', 1, '::1', '2015-06-24 02:49:24', NULL, NULL, NULL);
/*!40000 ALTER TABLE `job_test_detail` ENABLE KEYS */;


-- Dumping structure for table job.job_test_detail_option
CREATE TABLE IF NOT EXISTS `job_test_detail_option` (
  `test_detail_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_detail_id` int(11) NOT NULL,
  `answer_detail` varchar(250) DEFAULT NULL,
  `point_count` smallint(6) DEFAULT NULL COMMENT 'bobot angka',
  `creator_id` int(11) DEFAULT NULL,
  `creator_ip` varchar(15) DEFAULT NULL,
  `creator_date` datetime DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `editor_ip` varchar(15) DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY (`test_detail_option_id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;

-- Dumping data for table job.job_test_detail_option: ~92 rows (approximately)
/*!40000 ALTER TABLE `job_test_detail_option` DISABLE KEYS */;
INSERT INTO `job_test_detail_option` (`test_detail_option_id`, `test_detail_id`, `answer_detail`, `point_count`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 1, 'Sangat setuju', 50, 1, NULL, '2015-05-07 10:17:22', 1, '::1', '2015-06-17 23:13:24'),
	(2, 1, 'Netral aja', 3, 1, NULL, '2015-05-07 10:18:54', 1, '::1', '2015-06-17 23:13:24'),
	(3, 1, 'Tidak setuju', 1, 1, NULL, '2015-05-07 10:18:54', 1, '::1', '2015-06-17 23:13:24'),
	(4, 2, 'Sangat setuju', 5, NULL, NULL, NULL, 1, '::1', '2015-06-17 23:13:24'),
	(5, 2, 'Netral aja', 30, NULL, NULL, NULL, 1, '::1', '2015-06-17 23:13:24'),
	(6, 2, 'Tidak setuju', 1, NULL, NULL, NULL, 1, '::1', '2015-06-17 23:13:24'),
	(7, 3, 'Sangat setuju', 5, NULL, NULL, NULL, 1, '::1', '2015-06-17 23:13:24'),
	(8, 4, 'Saya tidak percaya pada bantuan orang lain.', 0, 1, '::1', '2015-06-18 01:08:51', 1, '::1', '2015-06-17 23:13:24'),
	(9, 4, 'Ya, saya membutuhkan bantuan orang lain, namun saya juga berusaha keras.', 2, 1, '::1', '2015-06-18 01:08:51', 1, '::1', '2015-06-17 23:13:24'),
	(10, 4, 'Sangat bergantung, karena hal tersebut membuat saya percaya bahwa saya melakukan hal yang benar, untuk sendiri dan orang lain.', 1, 1, '::1', '2015-06-18 01:08:51', 1, '::1', '2015-06-17 23:13:24'),
	(11, 6, 'jawab 2', 2, 1, '::1', '2015-06-18 01:08:51', NULL, NULL, NULL),
	(12, 6, 'jawab 3', 3, 1, '::1', '2015-06-18 01:08:51', NULL, NULL, NULL),
	(13, 7, 'ya aa', 4, 1, '::1', '2015-06-18 22:11:12', 1, '::1', '2015-06-18 23:15:30'),
	(14, 7, 'tidak', 0, 1, '::1', '2015-06-18 22:11:12', 1, '::1', '2015-06-18 23:15:30'),
	(20, 8, 'Sangat tidak nyaman', 0, 1, '::1', '2015-06-19 00:09:25', NULL, NULL, NULL),
	(21, 8, 'amat nyaman dan menunggu para pakar', 2, 1, '::1', '2015-06-19 00:09:25', NULL, NULL, NULL),
	(22, 8, 'cukup senang dapat hadir, tapi juga gugup', 1, 1, '::1', '2015-06-19 00:09:25', NULL, NULL, NULL),
	(23, 9, 'Sering', 0, 1, '::1', '2015-06-19 00:20:03', NULL, NULL, NULL),
	(24, 9, 'Jarang', 2, 1, '::1', '2015-06-19 00:20:03', NULL, NULL, NULL),
	(25, 9, 'Kadang-kadang', 1, 1, '::1', '2015-06-19 00:20:03', NULL, NULL, NULL),
	(26, 10, 'Saya sering frustasi dan merasa bisa lebih baik lagi', 0, 1, '::1', '2015-06-19 00:21:34', NULL, NULL, NULL),
	(27, 10, 'Ya', 2, 1, '::1', '2015-06-19 00:21:34', NULL, NULL, NULL),
	(28, 10, 'Saya tidak berpikir tentang diri saya sendiri', 1, 1, '::1', '2015-06-19 00:21:34', NULL, NULL, NULL),
	(29, 11, 'Saya akan gugup dan pilih tidak ambil bagian', 0, 1, '::1', '2015-06-19 00:22:40', NULL, NULL, NULL),
	(30, 11, 'Amat ingin', 2, 1, '::1', '2015-06-19 00:22:40', NULL, NULL, NULL),
	(31, 11, 'Saya tidak keberatan tapi juga tidak bersemangat', 1, 1, '::1', '2015-06-19 00:22:40', NULL, NULL, NULL),
	(37, 12, 'Saya hidup lebih dengan harapan daripada antisipasi', 0, 1, '::1', '2015-06-24 01:53:56', NULL, NULL, NULL),
	(38, 12, 'Realistis', 2, 1, '::1', '2015-06-24 01:53:56', NULL, NULL, NULL),
	(39, 12, 'Cukup tinggi', 1, 1, '::1', '2015-06-24 01:53:56', NULL, NULL, NULL),
	(40, 13, 'Tidak begitu baik', 0, 1, '::1', '2015-06-24 01:59:16', NULL, NULL, NULL),
	(41, 13, 'Amat baik', 2, 1, '::1', '2015-06-24 01:59:16', NULL, NULL, NULL),
	(42, 13, 'Saya memiliki beberapa kelebihan yang dapat saya tekankan', 1, 1, '::1', '2015-06-24 01:59:16', NULL, NULL, NULL),
	(43, 14, 'ini 1', 1, 1, '::1', '2015-06-24 02:06:49', NULL, NULL, NULL),
	(44, 14, 'ini 2', 2, 1, '::1', '2015-06-24 02:06:49', NULL, NULL, NULL),
	(45, 15, 'jawab 1', 1, 1, '::1', '2015-06-24 02:07:25', NULL, NULL, NULL),
	(46, 15, 'jawab 2', 2, 1, '::1', '2015-06-24 02:07:25', NULL, NULL, NULL),
	(47, 15, 'jwab 3 ', 3, 1, '::1', '2015-06-24 02:07:25', NULL, NULL, NULL),
	(48, 15, 'jawab 4', 4, 1, '::1', '2015-06-24 02:07:25', NULL, NULL, NULL),
	(49, 16, 'Ya, amat sangat', 0, 1, '::1', '2015-06-24 02:24:19', NULL, NULL, NULL),
	(50, 16, 'Saya tidak begitu tertarik dengan penyesuaian diri semata mata agar bisa diterima', 2, 1, '::1', '2015-06-24 02:24:19', NULL, NULL, NULL),
	(51, 16, 'untuk batasan tertentu saja', 1, 1, '::1', '2015-06-24 02:24:19', NULL, NULL, NULL),
	(52, 17, 'Saya yakin bahwa memiliki teladan dalam hidup adalah hal penting', 0, 1, '::1', '2015-06-24 02:26:37', NULL, NULL, NULL),
	(53, 17, 'tidak begitu penting, karena menjadi diri sendiri lebih penting', 2, 1, '::1', '2015-06-24 02:26:37', NULL, NULL, NULL),
	(54, 17, 'akan lebih penting bertahan dalam aturan yang diharapkan masyarakat secara umum daripada aturan individu', 1, 1, '::1', '2015-06-24 02:26:37', NULL, NULL, NULL),
	(55, 18, 'ya biasanya', 0, 1, '::1', '2015-06-24 02:28:00', NULL, NULL, NULL),
	(56, 18, 'tidak, karena hal ini bukan tujuan yang bisa diraih', 2, 1, '::1', '2015-06-24 02:28:00', NULL, NULL, NULL),
	(57, 18, 'kadang-kadang', 1, 1, '::1', '2015-06-24 02:28:00', NULL, NULL, NULL),
	(58, 19, 'tidak secara khusus', 0, 1, '::1', '2015-06-24 02:30:35', NULL, NULL, NULL),
	(59, 19, 'ya', 2, 1, '::1', '2015-06-24 02:30:35', NULL, NULL, NULL),
	(60, 19, 'barangkali tidak setinggi yang saya harapkan', 1, 1, '::1', '2015-06-24 02:30:35', NULL, NULL, NULL),
	(61, 20, 'ya, menurut saya setiap orang harus punya standar yang tinggi', 0, 1, '::1', '2015-06-24 02:32:37', NULL, NULL, NULL),
	(62, 20, 'saya percaya lebih penting memiliki standar yang realistis', 2, 1, '::1', '2015-06-24 02:32:37', NULL, NULL, NULL),
	(63, 20, 'barangkali dalam beberapa hal saya set standar yang tinggi', 1, 1, '::1', '2015-06-24 02:32:37', NULL, NULL, NULL),
	(64, 21, 'saya tidak cenderung pesimis daripada optimis', 0, 1, '::1', '2015-06-24 02:33:37', NULL, NULL, NULL),
	(65, 21, 'amat optimis', 2, 1, '::1', '2015-06-24 02:33:37', NULL, NULL, NULL),
	(66, 21, 'cukup optimis', 1, 1, '::1', '2015-06-24 02:33:37', NULL, NULL, NULL),
	(67, 22, 'kecewa', 0, 1, '::1', '2015-06-24 02:35:04', NULL, NULL, NULL),
	(68, 22, 'minimal saya sudah mencoba, pasti ada hal yang positif', 2, 1, '::1', '2015-06-24 02:35:05', NULL, NULL, NULL),
	(69, 22, 'coba, coba dan coba lagi', 1, 1, '::1', '2015-06-24 02:35:05', NULL, NULL, NULL),
	(70, 23, 'amat sulit dan itu membuat saya sulit untuk pulih', 0, 1, '::1', '2015-06-24 02:37:34', NULL, NULL, NULL),
	(71, 23, 'Ya, mudah bagi saya untuk pulih kembali', 2, 1, '::1', '2015-06-24 02:37:34', NULL, NULL, NULL),
	(72, 23, 'tidak pernah mudah, butuh waktu untuk kembali bersemangat', 1, 1, '::1', '2015-06-24 02:37:34', NULL, NULL, NULL),
	(73, 24, 'kita semua butuh oranglain untuk hal tertentu', 0, 1, '::1', '2015-06-24 02:38:37', NULL, NULL, NULL),
	(74, 24, 'amat sangat', 2, 1, '::1', '2015-06-24 02:38:37', NULL, NULL, NULL),
	(75, 24, 'biasa biasa saja', 1, 1, '::1', '2015-06-24 02:38:37', NULL, NULL, NULL),
	(76, 25, 'sangat penting bagi saya bahwa nilai dan standar saya diakui orang lain', 0, 1, '::1', '2015-06-24 02:39:49', NULL, NULL, NULL),
	(77, 25, 'sangat ingin', 2, 1, '::1', '2015-06-24 02:39:49', NULL, NULL, NULL),
	(78, 25, 'biasa biasa saja', 1, 1, '::1', '2015-06-24 02:39:49', NULL, NULL, NULL),
	(79, 26, 'tidak juga, kita tidak bisa mengendalikan hidup kita', 0, 1, '::1', '2015-06-24 02:42:12', NULL, NULL, NULL),
	(80, 26, 'ya saya percaya nasib ada di tangan saya sendiri', 2, 1, '::1', '2015-06-24 02:42:12', NULL, NULL, NULL),
	(81, 26, 'untuk beberapa hal iya, tapi tidak sebesar apa yang saya inginkan', 1, 1, '::1', '2015-06-24 02:42:12', NULL, NULL, NULL),
	(82, 27, 'Tidak begitu yakin', 0, 1, '::1', '2015-06-24 02:42:49', NULL, NULL, NULL),
	(83, 27, 'Sangat yakin', 2, 1, '::1', '2015-06-24 02:42:49', NULL, NULL, NULL),
	(84, 27, 'biasa biasa saja', 1, 1, '::1', '2015-06-24 02:42:49', NULL, NULL, NULL),
	(85, 28, 'tidak selalu menginginkan peningkatan', 0, 1, '::1', '2015-06-24 02:43:54', NULL, NULL, NULL),
	(86, 28, 'ya', 2, 1, '::1', '2015-06-24 02:43:54', NULL, NULL, NULL),
	(87, 28, 'ya, tapi ada hal yang ingin saya ubah agar lebih baik', 1, 1, '::1', '2015-06-24 02:43:54', NULL, NULL, NULL),
	(88, 29, 'Ya sangat khawatir dan takut gagal', 0, 1, '::1', '2015-06-24 02:45:18', NULL, NULL, NULL),
	(89, 29, 'tidak, karena resiko diperlukan untuk bisa sukses', 2, 1, '::1', '2015-06-24 02:45:18', NULL, NULL, NULL),
	(90, 29, 'tergantung besar resikonya', 1, 1, '::1', '2015-06-24 02:45:18', NULL, NULL, NULL),
	(91, 30, 'tidak', 0, 1, '::1', '2015-06-24 02:46:10', NULL, NULL, NULL),
	(92, 30, 'ya', 2, 1, '::1', '2015-06-24 02:46:10', NULL, NULL, NULL),
	(93, 30, 'kadang-kadang', 1, 1, '::1', '2015-06-24 02:46:10', NULL, NULL, NULL),
	(94, 31, 'Ragu ragu, takut karena tidak semua pengalaman baru adalah baik', 0, 1, '::1', '2015-06-24 02:47:35', NULL, NULL, NULL),
	(95, 31, 'Sebagai kesempatan belajar dan membuka kemungkinan baru', 2, 1, '::1', '2015-06-24 02:47:35', NULL, NULL, NULL),
	(96, 31, 'saatnya kemungkinan menang atau kalah', 1, 1, '::1', '2015-06-24 02:47:36', NULL, NULL, NULL),
	(97, 32, 'jarang atau tidak pernah ', 0, 1, '::1', '2015-06-24 02:48:20', NULL, NULL, NULL),
	(98, 32, 'ya saya sering evaluasi diri', 2, 1, '::1', '2015-06-24 02:48:20', NULL, NULL, NULL),
	(99, 32, 'kadang-kadang', 1, 1, '::1', '2015-06-24 02:48:20', NULL, NULL, NULL),
	(100, 33, 'Saya sering sangat kritis terhadap diri sendiri', 0, 1, '::1', '2015-06-24 02:49:24', NULL, NULL, NULL),
	(101, 33, 'amat jarang atau tdak pernah', 2, 1, '::1', '2015-06-24 02:49:24', NULL, NULL, NULL),
	(102, 33, 'kadang-kadang, namun juga peduli dengan kritik orang lain', 1, 1, '::1', '2015-06-24 02:49:24', NULL, NULL, NULL);
/*!40000 ALTER TABLE `job_test_detail_option` ENABLE KEYS */;


-- Dumping structure for table job.job_test_result
CREATE TABLE IF NOT EXISTS `job_test_result` (
  `test_result_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) NOT NULL,
  `min_score` mediumint(9) NOT NULL,
  `max_score` mediumint(9) NOT NULL,
  `score_description` varchar(1000) NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `creator_ip` varchar(15) DEFAULT NULL,
  `creator_date` datetime DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `editor_ip` varchar(15) DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY (`test_result_id`),
  KEY `test_id` (`test_id`),
  CONSTRAINT `job_test_result_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `job_test` (`test_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table job.job_test_result: ~6 rows (approximately)
/*!40000 ALTER TABLE `job_test_result` DISABLE KEYS */;
INSERT INTO `job_test_result` (`test_result_id`, `test_id`, `min_score`, `max_score`, `score_description`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 1, 3, 4, 'anda sangatlah maho', NULL, NULL, NULL, 1, '::1', '2015-06-22 01:00:46'),
	(2, 1, 5, 8, 'anda berpotensi maho', NULL, NULL, NULL, 1, '::1', '2015-06-22 01:00:46'),
	(3, 1, 9, 32, 'selamat anda normal hoho', NULL, NULL, NULL, 1, '::1', '2015-06-22 01:00:46'),
	(4, 3, 0, 24, 'anda sangat minim percaya diri\r\n\r\nMenandakan rasa kurang percaya diri. Anda cenderung gugup, grogi, terlalu rendah diri dengan prestasi yang dicapai selama ini. Berikut beberapa cara untuk mengatasi rendah diri: \r\n\r\n- Evaluasi dan tekankan kekuatan anda, pujilah diri andauntuk setiap usaha yang anda telah lakukan. Fokus pada prestaso dam bakat yang anda miliki.\r\n\r\n- Jangan takut mengambil resiko, bahkan jika anda gagal, anda telah belajar bahwa pengalaman telah membuat anda menjadi pribadi yang lebih baik.\r\n\r\n- Belajar evaluasi diri anda sendiri, jangan menjadi perfeksionis.', NULL, NULL, NULL, 1, '::1', '2015-06-24 02:50:38'),
	(6, 3, 25, 39, 'anda cukup percaya diri.\r\n\r\nBagi anda yang memiliki nilai rata-rata, rasa percaya diri anda tidak berlebihan, menunjukkan kesediaan berinteaksi dengan orang=orang yang mempunyai basis sama.\r\n\r\nOrang-orang seperti ini selalu menaruh harapan positif dalam segala situasi dan memiliki kemampuan keputusan hati-hati, terukur dan terstruktur setiap mempertimbangkan opsi.', NULL, NULL, NULL, 1, '::1', '2015-06-24 02:50:38'),
	(7, 3, 40, 50, 'anda sangat percaya diri.\r\n\r\nAnda sangat percaya diri dan yakin akan kemampuan anda. Anda senang dilibatkan  atau mengendalikan segala situasi yang berurusan dengan anda, serta aingin menjadi bagian sentral dari perubahan.\r\n\r\nAnda melihat segala hal sebagai peluang karier, sedangkan bagi merreka yang kurang percaya diri menganggap perubahan itu sebagai situasi yang membahayakan dan mengkhawatirkan. Hal negatif dari orang yang sangat percaya diri ini adalh adanya kecenderungan berperilaku sombong dan kurang ajar. \r\n\r\nJika terjadi pada diri anda, anda harus bisa menerima kenyataan dan tanamkan dalam pikiran bahwa sukses adalah sesuatu yang membutuhkan kerja keras, tidak terjadi begitu saja.', NULL, NULL, NULL, 1, '::1', '2015-06-24 02:50:38');
/*!40000 ALTER TABLE `job_test_result` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
