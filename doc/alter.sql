-- 
-- 4 feb 2025
	
CREATE TABLE wal_locstorage(
	locstorage_id int NOT NULL AUTO_INCREMENT ,
	name varchar(50) NOT NULL,
	code varchar(20) NULL,
	shortdesc varchar(200) NULL,
	`status` tinyint(4) DEFAULT '0',
  `creator_id` int(11) DEFAULT NULL,
  `creator_ip` varchar(15) DEFAULT NULL,
  `creator_date` datetime DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `editor_ip` varchar(15) DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY(locstorage_id)
	);
	
	INSERT INTO `wal_locstorage` (`name`, `code`, `shortdesc`, `status`, `creator_id`, `creator_date`) VALUES ('Kulkas', 'KULK', 'ya kulkas aja', 1, 1, '2025-02-12 20:41:58');
	
	
	CREATE TABLE wal_storage(
	storage_id int NOT NULL AUTO_INCREMENT ,
	locstorage_id int NOT NULL DEFAULT 1,
	name varchar(200) NOT NULL,
	shortdesc varchar(200) NULL,
	input_date DATETIME NOT NULL,
	`status` tinyint(4) DEFAULT '0',
  `creator_id` int(11) DEFAULT NULL,
  `creator_ip` varchar(15) DEFAULT NULL,
  `creator_date` datetime DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `editor_ip` varchar(15) DEFAULT NULL,
  `editor_date` datetime DEFAULT NULL,
  PRIMARY KEY(storage_id)
	);

-- Module : Shortcode for input expense
-- Date : 15 okt 2023 11:40
-- Deploy PROD: ok

ALTER TABLE `wal_fund`
	ADD COLUMN `shortcode` VARCHAR(25) NULL DEFAULT NULL COMMENT 'template input kode' AFTER `fund_type` ;
	
UPDATE `wallet`.`wal_fund` SET `shortcode`='ccbca' WHERE  `fund_id`=8;

UPDATE `wallet`.`wal_fund` SET `shortcode`='flazz' WHERE  `fund_id`=9;

UPDATE `wallet`.`wal_fund` SET `shortcode`='emoney' WHERE  `fund_id`=10;

UPDATE `wallet`.`wal_fund` SET `shortcode`='mtix' WHERE  `fund_id`=11;

UPDATE `wallet`.`wal_fund` SET `shortcode`='gopay' WHERE  `fund_id`=12;

UPDATE `wallet`.`wal_fund` SET `shortcode`='ovo' WHERE  `fund_id`=17;

UPDATE `wallet`.`wal_fund` SET `shortcode`='ovopoint' WHERE  `fund_id`=19;

UPDATE `wallet`.`wal_fund` SET `shortcode`='dana' WHERE  `fund_id`=20;

UPDATE `wallet`.`wal_fund` SET `shortcode`='klikbca' WHERE  `fund_id`=4;

UPDATE `wallet`.`wal_fund` SET `shortcode`='seabank' WHERE  `fund_id`=25;

UPDATE `wallet`.`wal_fund` SET `shortcode`='shopay' WHERE  `fund_id`=23;

UPDATE `wallet`.`wal_fund` SET `shortcode`='gopaycoin' WHERE  `fund_id`=24;
	
-- Module : Parent Category ID 
-- Date : 27 Jan 2023 17:40
-- Deploy PROD: ok

ALTER TABLE `wal_category`
	ADD COLUMN `is_show_report` TINYINT(4) NULL DEFAULT '0' AFTER `is_parent`;
	
UPDATE wal_category SET is_show_report = 1 WHERE is_parent =1;

------------------------

-- Module : Parent Category ID 
-- Date : 27 aug 2021 17:40
-- Deploy PROD: 

ALTER TABLE `wal_category`
	ADD COLUMN `is_parent` TINYINT(4) NULL DEFAULT '0' AFTER `is_monthly_billing`,
	ADD COLUMN `parent_category_id` SMALLINT NULL DEFAULT NULL AFTER `is_parent`;

-- Module : Task Project
-- Date : 21 aug 2018 10:00
-- Deploy PROD: 21 aug 2018 10:22
CREATE TABLE wal_task_project (
task_project_id BIGINT NOT NULL AUTO_INCREMENT,
name varchar(180) NULL DEFAULT NULL,
status tinyint NULL DEFAULT 0,
`notes` VARCHAR(100) NULL DEFAULT NULL,
`creator_id` INT(11) NULL DEFAULT NULL,
`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` INT(11) NULL DEFAULT NULL,
`editor_ip` VARCHAR(15) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(task_project_id)
);

ALTER TABLE `wal_task`
	ADD COLUMN `task_project_id` BIGINT(20) NULL DEFAULT NULL AFTER `task_id`;

-- Module : Task
-- Deploy : 14 aug 2018 23:33
ALTER TABLE wal_task ADD est_workday varchar(6) NULL AFTER est_duration;
ALTER TABLE wal_task ADD est_workhour varchar(6) NULL AFTER est_workday;
ALTER TABLE wal_task ADD est_workmin varchar(6) NULL AFTER est_workhour;

Add new module task scheduler
-- 31 juli 2018
CREATE TABLE wal_task(
-- task_id bigint NOT NULL AUTO_INCREMENT, 
title varchar(150),
description varchar(255),
priority tinyint COMMENT '1 low, 2 medium, 3 high',
input_date datetime DEFAULT NULL,
due_date datetime DEFAULT NULL,
`est_duration` VARCHAR(30) NULL DEFAULT NULL COMMENT 'format : day;hour;minute',
is_done tinyint DEFAULT 0,
notes varchar(100),
`creator_id` INT(11) NULL DEFAULT NULL,
`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` INT(11) NULL DEFAULT NULL,
`editor_ip` VARCHAR(15) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(task_id);

-- 15 februari 2018
-- tambah log untuk sms api di mesabot.com
CREATE TABLE wal_log (
log_id BIGINT NOT NULL AUTO_INCREMENT,
codename varchar(150) NULL DEFAULT NULL,
codevalue varchar(150) NULL DEFAULT NULL,
content longtext NULL DEFAULT NULL,
notes text NULL DEFAULT NULL,
`creator_id` INT(11) NULL DEFAULT NULL,
`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(log_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- start jumat 4 januari 2018
-- tambah notes untuk absen
ALTER TABLE wal_absence 
ADD notes varchar(255) NULL DEFAULT NULL AFTER out_date, 
ADD absent_status tinyint NULL DEFAULT 0 COMMENT '-1 = tidak masuk , 0 = masuk, 1 = izin, 2 = cuti sehari, 3 = cuti setengah hari' AFTER input_date

----------------------------------------------------------------------------------------------

-- start jumat 9 december 2017
ALTER TABLE `wal_expense`
	ADD COLUMN `is_routine` TINYINT(4) NULL DEFAULT '0' AFTER `is_paid`;

-- start rabu 22 november 2017
-- Update nominal jadi ribuan lagi
UPDATE wal_expense e SET e.amount = CONCAT(e.amount,'000') WHERE e.creator_id = 1

-- start rabu 18 october 2017
CREATE TABLE wal_fund_mutation(
fund_mutation_id int NOT NULL AUTO_INCREMENT ,
fund_id int NOT NULL,
release_date date NULL DEFAULT NULL,
type ENUM('DB','CR') NULL DEFAULT NULL,
name varchar(155) NULL DEFAULT NULL,
notes varchar(255) NULL DEFAULT NULL,
amount FLOAT(11,2) NULL DEFAULT 0,
last_credit FLOAT(11,2) NULL DEFAULT 0,
`creator_id` SMALLINT(4) NULL DEFAULT NULL,
`creator_ip` VARCHAR(20) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` SMALLINT(4) NULL DEFAULT NULL,
`editor_ip` VARCHAR(20) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
UNIQUE INDEX `release_date_type_name_amount_last_credit` (`release_date`, `type`, `name`, `amount`, `last_credit`),
PRIMARY KEY(fund_mutation_id)
);
ALTER TABLE wal_fund_mutation ADD CONSTRAINT `FK_wal_fund_mutation_1` FOREIGN KEY (`fund_id`) REFERENCES `wal_fund` (`fund_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

-- end 18 october 2017

SELECT MONTH(e.release_date) as permonth, YEAR(e.release_date) as peryear,
SUM(e.amount) as total_amount,
(
	SELECT SUM(amount) 
	FROM wal_expense we 
	LEFT JOIN wal_category wc ON we.category_id = wc.category_id
	WHERE wc.is_increment = 1 AND MONTH(we.release_date) = permonth AND YEAR(we.release_date) = peryear
) as total_expense_month,
(
	SELECT SUM(amount) 
	FROM wal_expense we 
	LEFT JOIN wal_category wc ON we.category_id = wc.category_id
	WHERE wc.is_increment = 0 AND MONTH(we.release_date) = permonth AND YEAR(we.release_date) = peryear
) as total_income_month
FROM wal_expense e 
WHERE e.creator_id = 1 
GROUP BY permonth, peryear
HAVING (permonth != 0 OR peryear != 0)
ORDER BY peryear DESC,permonth DESC

-- 23 mei 2017
-- add config for flexible hour time in puasa, set default 9 hour or 540 minute
ALTER TABLE `wal_member` ADD `set_working_minute` SMALLINT NULL DEFAULT '540' COMMENT 'set jam kerja standar (dalam menit, default 9jam = 540menit)' AFTER `is_active`;

-- 22 april 2017
ALTER TABLE `wal_category`
	ADD COLUMN `monthly_budget_amount` INT NULL DEFAULT '0' AFTER `is_monthly_billing`;

-- 15 april 2017
ALTER TABLE `wal_category` ADD `notes` TEXT NULL AFTER `is_monthly_billing`;
ALTER TABLE `wal_category`
	CHANGE COLUMN `due_date` `due_date` VARCHAR(5) NULL DEFAULT NULL AFTER `is_monthly_billing`;

-- 19 march 2017
-- add attribute extra for fund
ALTER TABLE wal_fund ADD balance int NULL DEFAULT NULL AFTER due_date;
ALTER TABLE wal_fund ADD is_delete tinyint NULL DEFAULT 0 AFTER balance;
ALTER TABLE wal_fund ADD notes text DEFAULT NULL AFTER is_delete;

-- 19 march 2017
-- add source money
CREATE TABLE `wal_fund` (
	`fund_id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	`fund_type` ENUM('cash','rekening bank','credit card') NOT NULL DEFAULT 'credit card',
	`due_date` VARCHAR(10) NULL DEFAULT NULL COMMENT 'tgl jatuh tempo kartu kredit',
	`creator_id` INT(11) NULL DEFAULT NULL,
	`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
	`creator_date` DATETIME NULL DEFAULT NULL,
	`editor_id` INT(11) NULL DEFAULT NULL,
	`editor_ip` VARCHAR(15) NULL DEFAULT NULL,
	`editor_date` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`fund_id`)
);

ALTER TABLE `wal_expense`
	ADD COLUMN `fund_id` INT NULL DEFAULT NULL AFTER `location_id`;
	
ALTER TABLE `wal_expense`
	CHANGE COLUMN `fund_id` `fund_id` INT(11) NULL DEFAULT '1' AFTER `location_id`;

-- alter for angpau
-- 31 desember 2016
CREATE TABLE wal_angpau
(
angpau_id int AUTO_INCREMENT NOT NULL,
side enum('A','B') NULL DEFAULT 'A',
no tinyint NULL DEFAULT 1,
name varchar(150) NULL DEFAULT NULL,
address varchar(150) NULL DEFAULT NULL,
from_relation varchar(100) NULL DEFAULT NULL,
amount smallint NULL DEFAULT 0,
PRIMARY KEY(angpau_id)
)

-- 1 september 2016
ALTER TABLE `wal_calendar`
	ADD COLUMN `start_time` TIME NULL AFTER `input_date`,
	ADD COLUMN `end_time` TIME NULL AFTER `start_time`,
	ADD COLUMN `bg_color` TEXT NULL AFTER `notes`,
	ADD COLUMN `is_global_calendar` TINYINT NULL DEFAULT '0' COMMENT 'untuk penanggalan calendar global user, untuk tgl merah' AFTER `bg_color`;

-- 12 july 2016
CREATE TABLE wal_calendar(
calendar_id BIGINT NOT NULL AUTO_INCREMENT,
input_date date NOT NULL,
notes text NULL DEFAULT NULL,
`creator_id` SMALLINT(4) NULL DEFAULT NULL,
`creator_ip` VARCHAR(20) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` SMALLINT(4) NULL DEFAULT NULL,
`editor_ip` VARCHAR(20) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(calendar_id)
)

-- 12 july 2016
CREATE TABLE `wal_journey` (
	`journey_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	lat_start varchar(150) NULL DEFAULT NULL,
	lng_start varchar(150) NULL DEFAULT NULL,
	gmap_start varchar(355) NULL DEFAULT NULL,
	lat_end varchar(150) NULL DEFAULT NULL,
	lng_end varchar(150) NULL DEFAULT NULL,
	gmap_end varchar(355) NULL DEFAULT NULL,
	is_finish tinyint NULL DEFAULT 0,
	`creator_id` INT(11) NULL DEFAULT NULL,
	`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
	`creator_date` DATETIME NULL DEFAULT NULL,
	`editor_id` INT(11) NULL DEFAULT NULL,
	`editor_ip` VARCHAR(15) NULL DEFAULT NULL,
	`editor_date` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`journey_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;


-- 11 july 2016
CREATE TABLE `wal_absence` (
	`absence_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`input_date` DATE NULL DEFAULT NULL,
	`in_date` DATETIME NULL DEFAULT NULL,
	`out_date` DATETIME NULL DEFAULT NULL,
	`creator_id` INT(11) NULL DEFAULT NULL,
	`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
	`creator_date` DATETIME NULL DEFAULT NULL,
	`editor_id` INT(11) NULL DEFAULT NULL,
	`editor_ip` VARCHAR(15) NULL DEFAULT NULL,
	`editor_date` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`absence_id`),
	UNIQUE INDEX `input_date_creator_id` (`input_date`, `creator_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

-- 27 mar 2016
ALTER TABLE `wal_expense`
	ADD COLUMN `gmap_address` VARCHAR(300) NULL DEFAULT NULL AFTER `release_date`,
	ADD COLUMN `gmap_lat` VARCHAR(50) NULL DEFAULT NULL AFTER `gmap_address`,
	ADD COLUMN `gmap_long` VARCHAR(50) NULL DEFAULT NULL AFTER `gmap_lat`;
	
-- 20 mar 2016
CREATE TABLE wal_location
(
location_id BIGINT AUTO_INCREMENT NOT NULL,
location_name varchar(255) NULL,
location_detail varchar(255) NULL,
	`creator_id` INT(11) NULL DEFAULT NULL,
	`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
	`creator_date` DATETIME NULL DEFAULT NULL,
	`editor_id` INT(11) NULL DEFAULT NULL,
	`editor_ip` VARCHAR(15) NULL DEFAULT NULL,
	`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(location_id)

);

ALTER TABLE `wal_expense`
	ADD COLUMN `location_id` BIGINT NULL DEFAULT NULL AFTER `category_id`,
	ADD COLUMN `Column 4` VARCHAR(255) NULL DEFAULT NULL AFTER `title`,
	ADD CONSTRAINT `wal_expense_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `wal_location` (`location_id`) ON UPDATE CASCADE ON DELETE CASCADE;
	
INSERT INTO `wal_location` (`location_id`, `location_name`, `location_detail`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 'Citraland', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'GI', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'PI', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'Taman Anggrek', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'Gading', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 'Arthagading', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 'MOI', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 'Gandaria City', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 'Kasablanka', NULL, NULL, NULL, NULL, NULL, NULL, NULL);


-- 24 jan 2016
ALTER TABLE wal_expense ADD is_get_notif tinyint DEFAULT 0 AFTER reminder_minus_day;

-- 03 jan 2016
ALTER TABLE wal_category ADD is_monthly_billing tinyint DEFAULT 0 AFTER is_budget;


-- 20 nov
ALTER TABLE wal_expense ADD reminder_minus_day tinyint AFTER reminder_date;
ALTER TABLE `wal_expense` ADD `reminder_date` DATE NULL DEFAULT NULL AFTER `amount`;

-- 26 oct
ALTER TABLE `wal_treasure`
	CHANGE COLUMN `deposito_date` `reminder_date` DATE NULL DEFAULT NULL AFTER `amount`;

-- 14 sept
CREATE TABLE job_notification(
notification_id bigint NOT NULL AUTO_INCREMENT,
company_id int NULL DEFAULT NULL,
participant_id int NULL DEFAULT NULL,
subject varchar(250) NULL,
message text DEFAULT NULL,
is_read tinyint DEFAULT 0,
is_delete tinyint DEFAULT 0,
`creator_id` INT(11) NULL DEFAULT NULL,
	`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
	`creator_date` DATETIME NULL DEFAULT NULL,
	`editor_id` INT(11) NULL DEFAULT NULL,
	`editor_ip` VARCHAR(15) NULL DEFAULT NULL,
	`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(notification_id)
)engine=Innodb

-- 13 sept
ALTER TABLE job_participant ADD is_delete tinyint DEFAULT 0 AFTER token;
ALTER TABLE job_test ADD test_code varchar(15) DEFAULT NULL AFTER title;

ALTER TABLE `job_test_detail_option`
	CHANGE COLUMN `point_count` `point_count` VARCHAR(50) NULL DEFAULT NULL COMMENT 'bobot angka' AFTER `answer_detail`;
	

-- 10 sept
CREATE TABLE job_coupon(
coupon_id int NOT NULL AUTO_INCREMENT,
coupon_code varchar(50) NULL,
quota smallint DEFAULT 1,
start_date datetime DEFAULT NULL,
end_date datetime DEFAULT NULL,
is_active tinyint DEFAULT 0,
`creator_id` INT(11) NULL DEFAULT NULL,
`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` INT(11) NULL DEFAULT NULL,
`editor_ip` VARCHAR(15) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(coupon_id)
)ENGINE=InnoDb;

CREATE TABLE job_coupon_detail(
coupon_detail_id int NOT NULL AUTO_INCREMENT,
coupon_id int NOT NULL,
company_id int NOT NULL,
is_used tinyint DEFAULT 0,
`creator_id` INT(11) NULL DEFAULT NULL,
`creator_ip` VARCHAR(15) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` INT(11) NULL DEFAULT NULL,
`editor_ip` VARCHAR(15) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(coupon_detail_id)
)ENGINE=InnoDb;

ALTER TABLE job_participant ADD job_position varchar(120) DEFAULT NULL AFTER thepassword;

-- 8 sept
ALTER TABLE job_participant ADD expired_test_date datetime DEFAULT NULL AFTER gender;
UPDATE job_participant SET token = md5(email);
UPDATE job_participant SET expired_test_date = now() + INTERVAL 1 MONTH;

-- 06 sept

ALTER TABLE job_order
ADD confirm_payment_type tinyint DEFAULT NULL AFTER notes,
ADD confirm_payment_date datetime DEFAULT NULL AFTER notes,
ADD confirm_payment_amount int DEFAULT NULL AFTER notes,
ADD confirm_payment_notes varchar(255) DEFAULT NULL AFTER notes;

-- 04 sept 2015
ALTER TABLE job_test ADD is_company tinyint DEFAULT 1 AFTER is_public;

ALTER TABLE `job_participant_type_test`
	ADD UNIQUE INDEX `job_unique1` (`participant_id`, `test_id`);

-- ALTER 25 AUGUST 2015 

ALTER TABLE job_participant_type_test ADD is_active tinyint NULL DEFAULT 0 AFTER test_id

ALTER TABLE job_test ADD is_company tinyint NULL DEFAULT 0 AFTER is_public 

----------------------------
----------------------------
----------------------------

INSERT INTO `job_test_detail_option` (`test_detail_id`, `answer_detail`, `point_count`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
(41, 'Praktis', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 'Spontan', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 'Ketat pada waktu', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 'Pemalu', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(42, 'Rapi / Teratur', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'Sopan / hormat', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'Suka bicara terus terang', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'Optimis', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(43, 'Ramah tamah', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 'Jujur', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 'Suka senda gurau', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 'Optimis', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(44, 'Berani / tidak penakut', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'Menyukai kenikmatan', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'Diplomatis / berhati hati', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'Terperinci', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(45, 'Penggembira', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 'Konsisten / tidak mudah berubah', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 'Berbudaya / terpelajar', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 'Percaya diri', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(46, 'Idealis', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'Mandiri', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'Tidak suka menentang', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'Suka memberi ilham / inspirasi', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(47, 'Lincah / suka membuka diri', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'Mampu memutuskan', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'Sedikit humor', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'Tekun / ulet', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(48, 'Perantara / penengah', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'Gemar musik lembut', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'Cepat bertindak', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'Mudah berbaur / bergaul', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(49, 'Senang berpikir', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'Suka ngotot / Kuat bertahan', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'Senang bicara', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'Bersikap toleran', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(50, 'Pendengar yang baik', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'Setia / tidak gampang berubah', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'Senang membimbing', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'Lincah / bersemangat', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(51, 'Mudah menerima saran', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'Suka memimpin', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'Berpikir matematis', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'Lucu / humoris', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(52, 'Perfeksionis / ingin sempurna', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'Suka mengijinkan / memperbolehkan', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'Produktif / menghasilkan', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'Terkenal luas / populer', 4, NULL, NULL, NULL, NULL, NULL, NULL),

(53, 'Bersemangat / gembira', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'Berani / tidak gampang takut', 2, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'Berkelakuan tenang / kalem', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'Berpendirian tetap', 4, NULL, NULL, NULL, NULL, NULL, NULL),

-- weakness here
(54, 'Bicara meriah / ramai', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'Bersikap Seperti Boss', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'Suka malu-malu / segan', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'Tanpa ekspresi / datar', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(55, 'Kurang disiplin', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'Tidak simpatik', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'Kurang antusias / tidak bergairah', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'Tidak mudah memaafkan', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(56, 'Pendiam', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'Gampang tersinggung', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'Suka melawan / membantah', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'Sering mengulang-ulang', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(57, 'Rewel / Ngomel yang tidak perlu', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'Suka takut / kuatir', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'Pelupa', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'Terus terang / blak-blakan', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(58, 'Tidak sabaran', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'Tidak merasa aman / mantap', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'Sering bimbang memutuskan', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'Suka menyela / memotong bicara orang lain', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(59, 'Kurang terkenal / tidak populer', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'Tidak suka melibatkan diri', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'Sulit diramalkan / sukar diduga', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'Tidak gampang terpengaruh', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(60, 'Keras kepala', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'Serampangan / sembrono', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'Sulit mengikhlaskan / merelakan', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'Bimbang / ragu-ragu', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(61, 'Sederhana', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'Pesimis', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'Tinggi hati / gengsi', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'Suka membiarkan', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(62, 'Gampang marah', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'Tanpa arah / tujuan', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'Suka berdebat / berbantah', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'Memisahkan diri / lebih  senang sendiri', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(63, 'Polos / ceplas ceplos', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'Selalu berpikir jelek / sukar dipercaya', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'Kasar / suka menyerang', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'Masa bodo / tidak perduli', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(64, 'Kuatir / cemas', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'Menarik diri / pendiam', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'Bekerja keras / giat', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'Ingin pujian / penghargaan', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(65, 'Terlalu peka / sensitif', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'Tidak bijaksana / tidak taktis', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'Malu / segan', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'Banyak bicara / monopoli percakapan', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(66, 'Banyak ragu-ragu / sangsi', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'Tidak teratur / berantakan', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'Menguasai secara ketat', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'Murung / patah semangat', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(67, 'Gampang berubah / tidak berpendirian tetap', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'Tertutup / sulit membuka diri', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'Sukar bertoleransi', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'lalai / acuh tak acuh', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(68, 'Tidak rapi / berantakan', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'Murung', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'Menggerutu / mengomel', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'Manipulatif / memanfaatkan', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(69, 'Lamban / amat berhati-hati', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'Bandel', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'Berlagak / pamer', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'Ragu-ragu / selalu curiga', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(70, 'Penyendiri', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'Suka memerintah / menggurui', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'Malas / berat langkah', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'Suka membual', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(71, 'Enggan / sering ogah', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'Sering berprasangka', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'Cepat marah / emosi', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'Bingung / sulit berkonsentrasi', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(72, 'Suka membalas dendam', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'Sering gelisah / Resah', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'Perlu dorongan / bimbingan', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'Gegabah / terburu-buru / kurang berpikir panjang', 8, NULL, NULL, NULL, NULL, NULL, NULL),

(73, 'Mudah berkompromi / mengalah', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 'Sering mencela / mengkritik', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 'Senang berbohong / mengakali', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 'Suka berubah pikiran', 8, NULL, NULL, NULL, NULL, NULL, NULL)
;
----------------------------------------------------------------------------------------
(74, '', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(74, '', 6, NULL, NULL, NULL, NULL, NULL, NULL),
(74, '', 7, NULL, NULL, NULL, NULL, NULL, NULL),
(74, '', 8, NULL, NULL, NULL, NULL, NULL, NULL),

-----------------------------------------------------------------

INSERT INTO `job`.`job_test_detail` (`test_id`, `detail_description`, `creator_id`, `creator_ip`, `creator_date`) VALUES 
 -- (5, 'Pendiam, tidak banyak bicara;Berjuang mencapai hasil;Mudah bergaul dengan orang baru;Berusaha menyenangkan orang', 1, '::1', '2015-09-16 15:08:21');
-- (5, 'Mengajak, Pemberi semangat;\r\nMengutamakan kesempurnaan;\r\nMengikuti pemimpin;\r\nKeberhasilan adalah tujuan', 1, '::1', '2015-09-16 15:08:21');
-- dua diatas udah diisi
(5, 'Mudah kecewa, patah semangat;Senang membantu sesama;Suka bercerita tentang diri sendiri;Memihak kepada oposisi', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Mengatur waktu secara efisien;Buru-buru ingin cepat selesai;Pandai bergaul, banyak teman;Mengerjakan sampai selesai', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Menghindari konflik;Suka membuat janji;Bekerja Runtut, sesuai aturan;Berani menghadapi sesuatu', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Mau mengalah dengan sesama;Peraturan itu membosankan;Suka memaksa;Memiliki standar tinggi', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Bersemangat aktif;Bekerja cepat, ingin menang;Menghidari pertengkaran;Menyendiri jika stress', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Takut mengambil keputusan;Penghargaan akan kemenangan;Tenang, tidak terburu-buru;Bersahabat, dikenal banyak orang', 1, '::1', '2015-09-16 15:08:21'),

-- 2 nih
(5, 'Bertanggung jawab akan tugas;Mudah mengekspresikan sesuatu;Mudah ditebak, konsisten;Selalu hati-hati, waspada', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Tidak mudah menyerah;Menjadi anggota dari kelompok;Periang dan selalu ceria;Semua teratur rapih', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Senang Mengarahkan, memimpin;Mengendalikan diri;Dapat membujuk orang lain;Cara berpikir sistematis, logis', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Tulus, tidak berprasangka buruk;Ketawa lepas, tidak ditahan;Berani tegas;Tenang, hati-hati, serius', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Kemauan keras;suka dengan hal baru;menolak perubahan mendadak;mempersiapkan masa depan', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Penyemangat yang baik;sabar mendengarkan pembicaraan;peraturan itu untuk keadilan;Mandiri,tidak tergantung orang lain', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Suka menyenangkan orang lain;Suka mengambil keputusan;Bersemangat dan Optimis;Mengutamakan fakta, data', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Ramah, lembut;Berpikir positif, bersemangat;suka mengambil resiko;Bekerja sesuai perintah', 1, '::1', '2015-09-16 15:08:21'),

--3
(5, 'Hati-hati, kontrol diri ketat;Berkata sesuai pikiran;Tidak mudah cemas akan sesuatu;jika belanja sesuai keinginan', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Ramah, mudah berteman;Cepat bosan dengan hal-hal rutin;Suka berubah-ubah;Menginginkan kepastian', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Mengalah, menghindari konflik;Hal-hal kecil jadi perhatian;Berubah pada saat-saat terakhir;Suka tantangan baru;', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Pendiam, tidak suka bicara;Riang, suka bicara;Cepat merasa puas;Cepat memutuskan, tegas', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Mampu sabar dalam menunggu;Menginginkan petunjuk yang jelas;Suka bercanda;Jika ada keinginan harus dipenuhi', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Peraturan itu menghambat;Suka menganalisa sampai detail;Unik, beda dengan yang lain;Bisa diharapkan bantuannya;', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Berani mengambil Resiko;Menyenangkan, suka membantu;Mudah mengungkapkan perasaan;Rendah hati, sederhana;', 1, '::1', '2015-09-16 15:08:21'),
(5, 'Mengutamakan hasil;Menginginkan akurasi, ketepatan;Betah berbicara lama;Suka berkelompok, bersama-sama', 1, '::1', '2015-09-16 15:08:21');

-----------------

Mengatur waktu secara efisien;
Buru-buru ingin cepat selesai;
Pandai bergaul, banyak teman;
Mengerjakan sampai selesai

Menghindari konflik;
Suka membuat janji;
Bekerja Runtut, sesuai aturan;
Berani menghadapi sesuatu

Mau mengalah dengan sesama;
Peraturan itu membosankan;
Suka memaksa;
Memiliki standar tinggi

Bersemangat aktif;
Bekerja cepat, ingin menang;
Menghidari pertengkaran;
Menyendiri jika stress

Takut mengambil keputusan;
Penghargaan akan kemenangan;
Tenang, tidak terburu-buru;
Bersahabat, dikenal banyak orang

-----------------------------

Bertanggung jawab akan tugas;
Mudah mengekspresikan sesuatu;
Mudah ditebak, konsisten;
Selalu hati-hati, waspada

Tidak mudah menyerah;
Menjadi anggota dari kelompok;
Periang dan selalu ceria;
Semua teratur rapih

Senang Mengarahkan, memimpin;
Mengendalikan diri;
Dapat membujuk orang lain;
Cara berpikir sistematis, logis

Tulus, tidak berprasangka buruk;
Ketawa lepas, tidak ditahan;
Berani tegas;
Tenang, hati-hati, serius

Kemauan keras;
suka dengan hal baru;
menolak perubahan mendadak;
mempersiapkan masa depan

Penyemangat yang baik;
sabar mendengarkan pembicaraan;
peraturan itu untuk keadilan;
Mandiri,tidak tergantung orang lain

Suka menyenangkan orang lain;
Suka mengambil keputusan;
Bersemangat dan Optimis;
Mengutamakan fakta, data

Ramah, lembut;
Berpikir positif, bersemangat;
suka mengambil resiko;
Bekerja sesuai perintah

----------------------------

Hati-hati, kontrol diri ketat;
Berkata sesuai pikiran;
Tidak mudah cemas akan sesuat;
jika belanja sesuai keinginan

Ramah, mudah berteman;
Cepat bosan dengan hal-hal rutin;
Suka berubah-ubah;
Menginginkan kepastian

Mengalah, menghindari konflik;
Hal-hal kecil jadi perhatian;
Berubah pada saat-saat terakhir;
Suka tantangan baru;

Pendiam, tidak suka bicara;
Riang, suka bicara;
Cepat merasa puas;
Cepat memutuskan, tegas

Mampu sabar dalam menunggu;
Menginginkan petunjuk yang jelas;
Suka bercanda;
Jika ada keinginan harus dipenuhi

Peraturan itu menghambat;
Suka menganalisa sampai detail;
Unik, beda dengan yang lain;
Bisa diharapkan bantuannya;

Berani mengambil Resiko;
Menyenangkan, suka membantu;
Mudah mengungkapkan perasaan;
Rendah hati, sederhana;

Mengutamakan hasil;
Menginginkan akurasi, ketepatan;
Betah berbicara lama;
Suka berkelompok, bersama-sama

ni yang uda rata yang
-----------------------------------------------------------------------------------------------

-----------------

Mengatur waktu secara efisien;
Buru-buru ingin cepat selesai;
Pandai bergaul, banyak teman;
Mengerjakan sampai selesai

Menghindari konflik;
Suka membuat janji;
Bekerja Runtut, sesuai aturan;
Berani menghadapi sesuatu

Mau mengalah dengan sesama;
Peraturan itu membosankan;
Suka memaksa;
Memiliki standar tinggi

Bersemangat aktif;
Bekerja cepat, ingin menang;
Menghidari pertengkaran;
Menyendiri jika stress

Takut mengambil keputusan;
Penghargaan akan kemenangan;
Tenang, tidak terburu-buru;
Bersahabat, dikenal banyak orang

-----------------------------
2
Bertanggung jawab akan tugas;Mudah mengekspresikan sesuatu;Mudah ditebak, konsisten;Selalu hati-hati, waspada

Tidak mudah menyerah;Menjadi anggota dari kelompok;Periang dan selalu ceria;Semua teratur rapih

Senang Mengarahkan, memimpin;Mengendalikan diri;Dapat membujuk orang lain;Cara berpikir sistematis, logis

Tulus, tidak berprasangka buruk;Ketawa lepas, tidak ditahan;Berani tegas;Tenang, hati-hati, serius

Kemauan keras;suka dengan hal baru;menolak perubahan mendadak;mempersiapkan masa depan

Penyemangat yang baik;sabar mendengarkan pembicaraan;peraturan itu untuk keadilan;
Mandiri,tidak tergantung orang lain

Suka menyenangkan orang lain;Suka mengambil keputusan;Bersemangat dan Optimis;Mengutamakan fakta, data

Ramah, lembut;Berpikir positif, bersemangat;suka mengambil resiko;Bekerja sesuai perintah

----------------------------
3
Hati-hati, kontrol diri ketat;Berkata sesuai pikiran;Tidak mudah cemas akan sesuatu;jika belanja sesuai keinginan

Ramah, mudah berteman;Cepat bosan dengan hal-hal rutin;Suka berubah-ubah;Menginginkan kepastian

Mengalah, menghindari konflik;Hal-hal kecil jadi perhatian;Berubah pada saat-saat terakhir;Suka tantangan baru;

Pendiam, tidak suka bicara;Riang, suka bicara;Cepat merasa puas;Cepat memutuskan, tegas

Mampu sabar dalam menunggu;Menginginkan petunjuk yang jelas;Suka bercanda;Jika ada keinginan harus dipenuhi

Peraturan itu menghambat;Suka menganalisa sampai detail;Unik, beda dengan yang lain;Bisa diharapkan bantuannya;

Berani mengambil Resiko;Menyenangkan, suka membantu;Mudah mengungkapkan perasaan;Rendah hati, sederhana;

Mengutamakan hasil;Menginginkan akurasi, ketepatan;Betah berbicara lama;Suka berkelompok, bersama-sama

INSERT INTO `job`.`job_test` (`title`, `test_code`,is_publish,is_public,is_company) VALUES ('Test DISC 2', 'disc-2',1,1,1);

-- senin 11 maret 2019
ALTER TABLE `wal_expense`
	ADD COLUMN `cashback` DECIMAL(14,0) NULL DEFAULT '0' AFTER `amount`;
ALTER TABLE `wal_fund`
	ADD COLUMN `color` VARCHAR(30) NULL DEFAULT NULL AFTER `balance`;

	ALTER TABLE wal_category ADD is_publish smallint NULL DEFAULT 1 AFTER is_delete;