// GET ALL DATA REPORT BY MONTH
SELECT CONCAT(YEAR(e.release_date),'-', MONTH(e.release_date))as tgl,
(
	SELECT (
		SELECT IFNULL(SUM(amount),0) tto
		FROM wal_expense we
		LEFT JOIN wal_category wc USING(category_id)
		WHERE 1 AND we.release_date BETWEEN e.release_date AND LAST_DAY(e.release_date) AND wc.is_increment = 1 AND we.creator_id = c.creator_id
	)

) income
,
(
	SELECT IFNULL(SUM(amount),0) tto
	FROM wal_expense we
	LEFT JOIN wal_category wc USING(category_id)
	WHERE 1 AND we.release_date BETWEEN e.release_date AND LAST_DAY(e.release_date) AND wc.is_increment = 0 AND we.creator_id = c.creator_id
) outcome
FROM wal_expense e
LEFT JOIN wal_category c USING(category_id)
WHERE 1 AND e.creator_id = 1
GROUP BY YEAR(e.release_date), MONTH(e.release_date)
HAVING tgl != '0-0'
ORDER BY tgl DESC
-- LIMIT 1000

-- minggu 25 Feb 2017, 21.00 @jco gandaria
-- masi buggy
-- Query get reminder monthly billing already due date each member
SELECT m.member_id, m.email, 
(
	SELECT COUNT(c.category_id)
	FROM wal_expense e, wal_category c
	WHERE e.category_id = c.category_id 
	AND c.creator_id = e.creator_id
	AND c.creator_id = m.member_id
	AND c.is_monthly_billing = 1
	AND c.due_date > STR_TO_DATE(now(),'%dd')
	AND MONTH(e.release_date) = MONTH(now()) 
	AND YEAR(e.release_date) = YEAR(now())
	HAVING COUNT(e.expense_id) >= 1
) as tot_trx
FROM wal_member m
WHERE 1
AND m.member_id = 1 
LIMIT 100