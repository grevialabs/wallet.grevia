<?php

function debug($par)
{
	echo "<pre>";
	print_r($par);
	echo "</pre>";
}
/**

whatsapp api testing

curl -X POST https://api.gupshup.io/wa/api/v1/msg \
        -H "Cache-Control: no-cache" \
        -H "Content-Type: application/x-www-form-urlencoded" \
        -H "apikey: bwk2bixnyhg7fu0es9wiaxhkn2x44wzw" \
        -H "cache-control: no-cache" \
        -d "channel=whatsapp&source=917834811114&destination=6283891998825&message=%7B%22type%22%3A%22text%22%2C%22text%22%3A%22hi%20there%20test%20api%22%7D&src.name=GlabsAPI"


APIKEY = sk_51327a8144274dafb1073c99a1c0db82

*/

// Initialize cURL session
$curl = curl_init();

// Set the URL to send the request to
$url = "https://api.example.com/endpoint";

// Prepare headers
// $headers = [
    // "Authorization: Bearer YOUR_API_TOKEN", // Replace with your actual token
    // "Content-Type: application/json",
    // "Custom-Header: YourHeaderValue"
// ];

$headers = [
    "Authorization: Bearer YOUR_API_TOKEN", // Replace with your actual token
    "Content-Type: application/json",
    "Custom-Header: YourHeaderValue"
];

// Prepare data to send (optional for POST or PUT requests)
// $data = json_encode([
    // "key1" => "value1",
    // "key2" => "value2"
// ]);
$data = NULL;
$data['channel'] = 'whatsapp';
$data['source'] = '917834811114';
$data['destination'] = '6283891998825';
$data['message'] = '%7B%22type%22%3A%22text%22%2C%22text%22%3A%22hi%20there%20test%20api%22%7D';
$data['src.name'] = 'GlabsAPI';


$data = json_encode($data);

// Configure cURL options
curl_setopt_array($curl, [
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,  // Return response as a string
    CURLOPT_CUSTOMREQUEST => "POST", // Change to "GET", "PUT", or "DELETE" as needed
    CURLOPT_HTTPHEADER => $headers,
    CURLOPT_POSTFIELDS => $data     // Remove this line for GET requests
]);

// Execute the request and capture the response
$response = curl_exec($curl);

// Check for errors
if (curl_errno($curl)) {
    echo "cURL Error: " . curl_error($curl);
} else {
    // Display the response
    echo "Response: " . $response;
}

// Close the cURL session
curl_close($curl);

debug($response);
?>
