#!/usr/bin/perl
# makeThumb.cgi
# make a thumbnail

use File::Basename;
use CGI;
use CGI::Carp qw( fatalsToBrowser );
use Image::Magick;

#########################################################################
# ImageMagick is Copyright (C) ImageMagick Studio  www.imagemagick.org  #
#########################################################################

$basePath = "/hsphere/local/home/agonafer/dmeshop.com/";
$baseURL = "http://www.dmeshop.com/";

# Path to the HTML template
$template  = $basePath . "cgi-bin/template.txt";

# Path to the directory containing all the images
$imageDir  = $basePath . "images/";

$Directory   = $basePath . "images/";
$Url_Path    = $baseURL . "cgi-bin/data/contest/photos/";

$debugCmd = "whoops";

$File_Name   = "";
$email       = "";
$title       = "";
$iFile       = "";
$iDir        = "";
$description = "";
$File        = "";

$htmlhead = "Content-type: text/html\n\n";
$bContent = "Content-type: image/bmp\n\n";   # for BMP files
$jContent = "Content-type: image/jpeg\n\n";  # for JPEG files
$gContent = "Content-type: image/gif\n\n";   # for GIF files
$pContent = "Content-type: image/png\n\n";   # for PNG files
$tContent = "Content-type: image/tiff\n\n";  # for TIFF files
$log_file = "log.txt";
$lock_file = "log.lock";
$eCount = 0;  # initialize entry count
$exists = 0;  # initialize entry exists flag
$dCount = 0;  # initialize description line count
$e      = 0;  # initialize counter
$numfiles = 0;
$newWidth = 125; # default width

########################################################################

&getNewQuery;   # get user information

if ($debug eq $debugCmd) { $doDebug = 1; }

if ($doDebug == 1) {
   &doDebug;
 } else {

my ($image, $x);
$image = Image::Magick->new;    # make a new image template
open (IMAGE, "<$imageDir$iFile") or die "$! Cant open $imageDir$iFile";
$x = $image->Read(file=>'IMAGE'); # read in original image
$oldWidth = $image->Get('base-columns');
$oldHeight = $image->Get('base-rows');
close(IMAGE);
if ($newWidth == 0) {$newWidth = 125; }
  if ($oldWidth > 0) {
$ratio = $newWidth/$oldWidth;
  } else {
 $ratio = 1;
 }
$ratio = (int($ratio*1000))/1000;
$newHeight = int($oldHeight * $ratio);
$newSize = $newWidth . "x" . $newHeight;
# $x = $image->Scale(width=>$newWidth,height=>$newHeight);
$x = $image->Scale(geometry=>$newSize);
   $theContent = $jContent;   # default to jpeg
   $l = length($iFile);
   $dot = rindex($iFile, "\.");
   $extension = lc(substr($iFile, $dot, $l-$dot));
   if ($extension eq "\.bmp") { $theContent = $bContent; }
   if ($extension eq "\.gif") { $theContent = $gContent; }
   if ($extension eq "\.jpg") { $theContent = $jContent; }
   if ($extension eq "\.png") { $theContent = $pContent; }
   if ($extension eq "\.tif") { $theContent = $tContent; }

print $theContent;   # Print Content Type Header For Browser
binmode(STDOUT);
$image->Write('jpg:-');

 } # endif $debugCmd

exit;

sub pictDebug {
&getImage;
$theContent = $jContent;
&printImage;
 } # end pictDebug

sub doDebug {

print <<HTMLF;
$htmlhead
<html>
<head>
<title>makeThumb Debugging</title>
</head>
<body>
<h2 align="center">makeThumb Information</h2>
HTMLF

# &doVars;

# $newDir = $ENV{"DOCUMENT_ROOT"} . $ENV{"DOCUMENT_URI"};
$newDir = $imageDir . $iDir;

$x1 = length($newDir);
$x2 = rindex($newDir,"/");
# $newDir = substr($newDir,0,$x2);

  opendir (THISDIR,"$newDir") || die "$htmlhead $! Can't Open Directory $x2 - $newDir";
   @temp = readdir(THISDIR);
  closedir (THISDIR);
  $entries = $#dirlist - $[ +1;

foreach my $file (@temp) {
	push (@files, $file) if ($file =~ /\.jpe?g$/i);
	push (@files, $file) if ($file =~ /\.png$/i);
	push (@files, $file) if ($file =~ /\.gif$/i);
	}
undef @temp;  # kill @temp

@files = sort(@files);
$numFiles = $#files -$[ +1;

print "<B>$numFiles found in $newDir</B><BR>\n";

$maxCols = 4;  # set maximum number of columns
$rowCount = 0; # init rows
$colCount = 0; # init cols
$header = "Directory of $iDir";

# start of table
print <<HTMLF;
<table width="95\%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="blue" colspan="$maxCols" align="center">
\&nbsp;\&nbsp;<b style="font-family:arial,helvetica;font-size:18px;font-weight:bold;color:white;">$header</b><br>
</td>
</tr>
HTMLF

 for ($i=0; $i<$#files+1; $i++) {
# do we need a <tr> ?
  if ($colCount == 0) {
        print "<tr>";
        $rowCount++; # show we did it
        } # endif rowcount

# print a cell
  print "<td style=\"border: solid 1px black; text-align: center;\" valign=\"top\"";
   if ($i == ($#files+1)) { print " colspan=\"" . ($maxCols - $colCount) . " "; }
  print ">";  # end of <td> tag
  print "<strong>$iDir/$files[$i]</strong><br>";
  print "<img src=\"http://www.rainbo.net/cgi-bin/makeThumb.cgi?file=$iDir/$files[$i]\&width=125\"><br>\n";

my ($image, $x);
$image = Image::Magick->new;    # make a new image template
open (IMAGE, "<$imageDir$iDir/$files[$i]") or die "Cant open $imageDir$iDir/$files[$i]";
$x = $image->Read(file=>'IMAGE'); # read in original image
$oldWidth = $image->Get('base-columns');
$oldHeight = $image->Get('base-rows');
close(IMAGE);

  print "<strong>$oldWidth x $oldHeight</strong><br>\n";
  print "</td>";
  $colCount++;                     # bump colCount

# done a whole row?
 if ($colCount == $maxCols) {
   print"</tr>\n";         # end row
   $colCount = 0;        # reset colCount
     } # endif $colcount
  } # end for $i - images

 print "</table><br>\n";

print <<HTMLF;
<BR></B>\n
<FONT SIZE="2">makeThumb&copy; is Copyright (C) 2002-2010 by <A HREF="mailto:rick\@rainbo.net">Richard L. Trethewey</A>.  All rights reserved.</FONT><BR>\n
</BODY>\n
</HTML>\n
HTMLF

 } # end doDebug


sub doVars {
foreach $key (keys %ENV){
 print qq|The value of $key is $ENV{"$key"}<BR>\n|;
 }
} # end doVars

sub getNewQuery {
  $query = new CGI;
#  $header = $query->header;
      $command = $query->param('command');
      $iFile = $query->param('file');
      $newWidth = $query->param('width');
      $newHeight = $query->param('height');
      $maxWidth = $query->param('maxwidth');
      $maxHeight = $query->param('maxheight');
      $iDir = $query->param('dir');
      $debug = $query->param('debugCmd');
 }  # end getNewQuery


sub getImage {
if (-e "$imageDir$iFile") {
open (IMAGE, "<$imageDir$iFile") or die "Cant open $imageDir$iFile";
binmode(IMAGE);
@imgData = <IMAGE>;
close (IMAGE);
$iCount = $#imgData - $[ +1;
  }  # endif -e
}

sub printImage {
print $theContent;   # Print Content Type Header For Browser
binmode(STDOUT);
  if ($iCount >= 0) {
  print @imgData;
    } # endIf $iCount
 }    # end printImage

sub getImages {
opendir(DIR, $imageDir) or die("Cannot open $imageDir");
my @temp = readdir(DIR);
closedir(DIR);

foreach my $file (@temp) {
	push (@files, $file) if ($file =~ /\.jpe?g$/i);
	push (@files, $file) if ($file =~ /\.png$/i);
	push (@files, $file) if ($file =~ /\.gif$/i);
	}
undef @temp;

@files = sort(@files);

$numfiles = $#files  - $[ +1;

 } # end getImages


sub showDir {
#   my $tmp = cwd();
#   $tmp .= "/data/cart";
#   $tmp = $data_dir;
  $tmp = $imageDir;
  opendir (THISDIR,$tmp) || die "Can't Open Directory - " . $tmp;
   @dirlist = readdir(THISDIR);
  closedir (THISDIR);
  $entries = $#dirlist - $[ +1;

 print $htmlhead;
 print <<HTMLF;
<HTML>
<HEAD><TITLE>Directory Reader</TITLE></HEAD>
<BODY>
<H2 ALIGN="CENTER">Directory of $tmp</H2>
There are $entries entries in this directory<BR><BR>
<TABLE WIDTH="100%"><TR><TD ALIGN="CENTER"><B>File Name</B></TD>
<TD ALIGN="CENTER"><B>Status</B></TD></TR>
HTMLF
    for ($i=0; $i<$entries; $i++) {
      print "<TR><TD ALIGN=\"CENTER\">";
     print "$dirlist[$i]";
      print "</TD></TR>\r";
       }  # endfor i
print <<HTMLF;
</TABLE>
</BODY>
</HTML>
HTMLF

 }  # end showDir


