<?
// Rainbo Design PHP ImageMagick Thumbnail Maker
// Copyright (C) 2005-2010 by Richard L. Trethewey  rick@rainbo.net
// All Rights Reserved
// If you use this script, I'd appreciate a link!
// http://www.rainbodesign.com/pub/

// Defaults
$thumbsize = 150;				// Default thumbnail width.
$imagesource = 'images/default_img.jpg';	// Default image file name.
						// Set to empty string for no image output on failure.
$addBorder = 0;					// Set to 1 to add a border to the thumbnail image
$borderWidth = 2;				// Default border width
$borderColor = 'gold';				// Default border color

// Image Magick Version Defaults and Constants
$bContent = "Content-type: image/bmp";   // for BMP files
$jContent = "Content-type: image/jpeg";  // for JPEG files
$gContent = "Content-type: image/gif";   // for GIF files
$pContent = "Content-type: image/png";   // for PNG files
$tContent = "Content-type: image/tiff";  // for TIFF files

// Globals
$error = '';
$imageinfo = array();

if (isset($_GET['width'])) { $thumbsize = $_GET['width']; }
if (isset($_GET['src'])) { $imagesource =  $_SERVER['DOCUMENT_ROOT'] . "/" . $_GET['src']; }
if (isset($_GET['border'])) { $addBorder =  $_GET['border']; }
if (isset($_GET['borderW'])) { $borderWidth =  $_GET['borderW']; }
if (isset($_GET['borderC'])) { $borderColor =  $_GET['borderC']; }

if (file_exists($imagesource)) {


$dot = strrpos($imagesource, '.');
$extension = substr($imagesource, $dot);

  switch($extension) {
   case('.jpg'):
   $theContent = $jContent;
   $theWMType = 'jpg:-';
   break;
   case('.gif'):
   $theContent = $gContent;
   $theWMType = 'gif:-';
   break;
   case('.png'):
   $theContent = $pContent;
   $theWMType = 'png:-';
   break;
   case('.bmp'):
   $theContent = $bContent;
   $theWMType = 'bmp:-';
   break;
   case('.tif'):
   $theContent = $tContent;
   $theWMType = 'tif:-';
   break;
 } // end switch($extension)

$imageinfo = getimagesize($imagesource);
$imagewidth = $imageinfo[0];
$imageheight = $imageinfo[1];

if ($imagewidth >= $thumbsize) {
  $thumbwidth = $thumbsize;
  $factor = $thumbsize / $imagewidth;
  $thumbheight = floor($imageheight * $factor);
} else {
  $thumbwidth = $imagewidth;
  $thumbheight = $imageheight;
  $factor = 1;
 }

$geometry = $thumbwidth . 'x' . $thumbheight;
$wmCmd = "convert $imagesource -resize $geometry";
 if ($addBorder == 1) {
  $wmCmd .= " -bordercolor '$borderColor' -border $borderWidth";
 } // endif addBorder
$wmCmd .= " $theWMType";

header($theContent);	// Pass MIME-type header
passthru($wmCmd);	// Output thumbnail image data
exit;

 } else {
 $error = "File " . $_GET['src'] . " Not Found";
 } // endif file_exists

 if ($error != '') {
      header("HTTP/1.0 404 Not Found");
 } // endif $error

  exit;

?>
