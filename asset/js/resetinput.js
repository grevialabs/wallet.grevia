function resetinput() {
	// each show reset
	$('input.resetinput').each(function(){
		var str = '<span class="reset_input" style="float:right;cursor:pointer;visibility:hidden;text-align:right" onclick="var input = $(this).prev(\'input\');input.val(\'\');$(this).css(\'visibility\',\'hidden\');input.focus();" alt="Reset input"><i class="fa fa-close"></i></span><div style="clear:both"></div>';
		$(this).after(str);
	});

	$('input.resetinput').bind('keyup change propertychange paste',function(){
		if ($(this).val().length <= 0) {
			$(this).next('span').css('visibility','hidden');
		} else {
			$(this).next('span').css('visibility','visible');
		}
	});
}