(function ( $ ) {
 
    $.fn.gcalendar = function( options ) {
 
        // Default options
        var settings = $.extend({
            // setId: 'gcalendar', // div id of calendar
            setYear: '2018', // year 
            setFirstDay: 'monday', // monday / sunday
        }, options );
 
        // Apply options
        // return this.append('Hello ' + settings.name + '!');
        // alert(settings.setFirstDay);

        var str = '';
        //var year = 2018;
        var year = settings.setYear;

        var date = new Date();
        var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Des"];
        var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

        str += 'Tahun ' + year + '<br/>';
        //str += '<br/><table class="table table-bordered">';

        for (month = 1; month<=12; month++) {

            //var lastmonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var dayperweek = 7;

            // set firstday calendar start from 0 = Sunday, 1 Monday
            var firstDay = 0;
            //var tgl;
            if (settings.setFirstDay == 'monday') {
                firstDay = 1;
                //tgl = []
            }
            if (settings.setFirstDay == 'sunday') firstDay = 0; 

            // str += "<div class='col-sm-12'>";
            str += "Bulan " + months[month-1] + "<br/>";
            str += '<table class="table table-bordered">';

            str += '<tr>';

            for (tgl=1;tgl<=7;tgl++) {
                tglkey = tgl;

                // firstDay not SUNDAY (which is monday)
                if (firstDay != 0) {
                    if (tgl == 7) {
                        tglkey = 0;
                    }

                } else {
                    tglkey = tgl-1;
                    //console.log('tglkey: ' + tglkey);
                }

                str += '<td class=' + bgcolor + ' style="text-align:center">' + days[tglkey] + '</td>';
                
            }

            str += '<tr>';

            //var lastday = new Date(year, month, 0);
            var lastday = new Date(year, month, 0).getDate();
            // lastday
            var isNewrow = true;
            var cols = 1;
            var isStart = false;

            for (day=1;day<=lastday;day++) {
                
                // Return 0 = Sunday, 1 = Monday, etc
                dayName = new Date(month + "/" + day + "/" + year).getDay();
                if (day == 1 && isNewrow == true) {
                    str += '<tr>';
                }

                var bgcolor = ' ';
                if (dayName == 0) bgcolor = 'bg-dangeraa';
                else if (dayName == 6) bgcolor = 'bg-warninga';
                
                // check if 
                if (day == 1) {
                    //console.log('dayname ' + dayName + ' firstDay ' + firstDay);
                    // check dayname start not MONDAY
                    if (dayName != firstDay) {
                        
                        // add empty cols to table
                        for (tgl=1;tgl<=7;tgl++) {
                            
                            // firstDay is MONDAY
                            //if (firstDay == 1) {
                            //    tglkey = tgl-1;
                                //else tglkey = tgl-1;

                            //} else {
                                // firstDay is Sunday
                            //    if (tgl == 7) tglkey = 0;
                            //}

                            tglkey = tgl;
                            // firstDay is not SUNDAY
                            if (firstDay != 0) {
                                if (tgl == 7) tglkey = 0;

                            } else {
                                // firstDay is sunday
                                tglkey = tgl-1;
                                //if (tgl == 7) tglkey = 0;
                                
                            }

                            //console.log("loop compare tgl:" + tgl + " dayName:" + dayName);
                            if (tglkey == dayName) {
                                // cols++;
                                //console.log("loop valid nih");
                                // str += "<td>" + days[dayName] + " " + day + " cont:" + cols + "</td>";
                                str += "<td class='" + bgcolor + "' style='text-align:center'><span id='" + day + "-" + month + "-" + year + "'>" + day + "</span></td>";
                                
                                if (cols == dayperweek) {
                                    cols = 0;
                                    str += '</tr>';
                                }
                                break;
                            } else {			
                                
                                //bgcolor = ' ';
                                //if (firstDay == 0 && day == 1 && cols == 1) bgcolor = 'bg-dangeraa';
                                //else if (firstDay == 1 && cols == 6) bgcolor = 'bg-warninga';

                                str += "<td class='" + bgcolor + "'>&nbsp;</td>";
                                // console.log('tutup ayam ' + cols);
                                if (cols == dayperweek) {
                                    //console.log('tutup tr');
                                    cols = 0;
                                    str += '</tr>';
                                }
                            }
                            cols++;
                            
                        }
                        
                        isStart = false;
                        isNewrow = false;
                        day = 1;
                    } else {
                        isStart = true;
                    }

                    
                } else {
                    isStart = true;
                }
                
                if (isStart) {
                    //str += "<td>" + days[dayName] + " " + day + " cont:" + cols + "</td>";
                    
                    //var bgcolor = ' ';
                    //if (dayName == 0) bgcolor = 'bg-danger';
                    //if (dayName == 6) bgcolor = 'bg-warning';
                    
                    str += "<td class='" + bgcolor + "' style='text-align:center'><span id='" + day + "-" + month + "-" + year + "'>" + day + "<span></td>";
                    
                    if (cols == dayperweek) {
                        cols = 0;
                        str += '</tr>';
                    }
                }
                
                cols++;
            }
            str += "</table>";
        }

        //str += "$('table tr').find('td:first').addClass('bg-danger');";
        //str += "$(document).ready(function(){";
        //str += "    $('table tr').find('td:first').addClass('bg-danger');";
        //str += "    $('table tr').find('td:nth-child(7)').addClass('bg-warning');";
        //str += "});";
        //$('#calendar').html(str);
        return this.append(str);

 
    };
    //var ayam = $(this).find('table tr');
    //ayam.find('td:first').addClass('bg-danger');
    //$('table tr').find('td:first').addClass('bg-danger');
    //$('table tr').find('td:nth-child(7)').addClass('bg-warning');

 
}( jQuery ));